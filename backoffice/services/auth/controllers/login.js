const login = async (req, res) => {
  try {
    const result = await fetch("http://website:3000/api/auth/login", {
      method: "POST",
      body: JSON.stringify(req.body),
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    });
    if (result.status !== 200) throw new Error("authentication failed");
    const resultJson = await result.json();
    res.json(resultJson);
  } catch (error) {
    res.status(401).json({ error: { message: error.message || "no" } });
  }
};

module.exports = { login };
