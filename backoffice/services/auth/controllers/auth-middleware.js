const jwt = require("jsonwebtoken");

const jwtSecret = process.env.JWT_SECRET;

const authMiddleware = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    if (token === process.env.INTERNAL_API_KEY) return next();
    verifyToken(token, (error, user) => {
      if (error) return res.status(403).send("sorry");
      if (
        user.email === "farid.khaheshi@gmail.com" ||
        user.role === "superadmin"
      ) {
        req.user = user;
        next();
      } else {
        return res.status(401).send("sorry");
      }
    });
  } catch (error) {
    return res.status(401).json({ error: { message: "Authorization Failed" } });
  }
};

function verifyToken(token, done) {
  jwt.verify(token, jwtSecret, (error, payload) => {
    if (error) done(error);
    else done(null, payload);
  });
}

module.exports = {
  authMiddleware,
};
