const db = require("../../../adapters/db");

const findPoolSubscribedChannels = async (poolId) => {
  const subscriptions = await db.Channel.findAll({
    where: { active: true },
    include: {
      model: db.Subscription,
      through: {
        model: db.Pipe,
        where: {
          active: true,
        },
      },
      where: {
        active: true,
        pool_id: poolId,
      },
    },
  });
  return subscriptions;
};

module.exports = { findPoolSubscribedChannels };
