const { sendErrorResponse } = require("../../../utils/send-error-response");
const { findPoolSubscribedChannels } = require("../processors");

const getPoolSubscribedChannels = async (req, res) => {
  try {
    const {
      params: { poolId },
    } = req;
    const channels = await findPoolSubscribedChannels(poolId);
    return res.json(channels);
  } catch (err) {
    return sendErrorResponse(res, err, "error in finding pool subscribers");
  }
};

module.exports = {
  getPoolSubscribedChannels,
};
