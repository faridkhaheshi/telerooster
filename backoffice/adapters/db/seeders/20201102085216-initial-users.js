"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      { tableName: "Users", schema: "rooster" },
      [
        {
          id: "113e71f0-15a9-48fe-b1a7-13850c423487",
          display_name: "Daily Say",
          email: "info@dailysay.net",
          role: "normal",
          active: true,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete(
      { tableName: "Users", schema: "rooster" },
      null,
      {}
    );
  },
};
