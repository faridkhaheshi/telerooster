"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      { tableName: "Pools", schema: "rooster" },
      [
        {
          id: "14a1cd28-e10c-415f-931a-7ca222d54ea4",
          creator_id: "113e71f0-15a9-48fe-b1a7-13850c423487",
          poster:
            "https://farid.work/wp-content/uploads/2021/12/office-exer.svg",
          title: "Office exercise tips",
          description:
            "One visual tip every working day. Helps your team to avoid work-related physical injuries.",
          slug: "office-exercise-tips",
          is_free: true,
          featured: true,
          active: true,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: "4bb120c1-4248-48e3-a189-b8f765cf22be",
          creator_id: "113e71f0-15a9-48fe-b1a7-13850c423487",
          poster:
            "https://farid.work/wp-content/uploads/2021/12/marketing-case.svg",
          title: "Short marketing case studies",
          description:
            "Real-world examples about how different companies tackled marketing and branding problems.",
          slug: "short-marketing-case-studies",
          is_free: true,
          featured: true,
          active: true,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: "140dc771-27ca-4460-881e-0696cb9a12d5",
          creator_id: "113e71f0-15a9-48fe-b1a7-13850c423487",
          poster: "https://farid.work/wp-content/uploads/2021/12/book-sm.png",
          title: "Educational notes from credible books",
          description:
            "Sends interesting notes from outstanding books. The notes are usually a few paragraphs from important sections of a book.",
          slug: "educational-notes-from-books",
          is_free: true,
          featured: true,
          active: true,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: "0f54bc39-9b81-4e3d-aa01-609441d5df49",
          creator_id: "113e71f0-15a9-48fe-b1a7-13850c423487",
          poster:
            "https://farid.work/wp-content/uploads/2021/12/motivation-sm.png",
          title: "Motivational quotes",
          description:
            "Everyone gets exhausted and needs some positive energy. This channel consists of short daily motivational quotes.",
          slug: "motivational-quotes",
          is_free: true,
          featured: true,
          active: true,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete(
      { tableName: "Pools", schema: "rooster" },
      null,
      {}
    );
  },
};
