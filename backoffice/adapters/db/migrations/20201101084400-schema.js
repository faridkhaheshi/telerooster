"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createSchema("rooster", { ifNotExists: true });
    await queryInterface.sequelize.query(
      `CREATE EXTENSION IF NOT EXISTS "uuid-ossp";`
    );
    await queryInterface.sequelize.query(
      `CREATE EXTENSION IF NOT EXISTS citext;`
    );
  },

  down: async (queryInterface, Sequelize) => {
    // await queryInterface.sequelize.query(`DROP EXTENSION IF EXISTS citext;`);
    // await queryInterface.sequelize.query(
    //   `DROP EXTENSION IF EXISTS "uuid-ossp";`
    // );
    await queryInterface.dropSchema("rooster");
  },
};
