"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      "Channels",
      {
        id: {
          allowNull: false,
          primaryKey: true,
          type: Sequelize.DataTypes.UUID,
          defaultValue: Sequelize.literal("uuid_generate_v4()"),
        },
        platform: {
          type: Sequelize.DataTypes.ENUM(["microsoft", "slack"]),
          allowNull: false,
        },
        id_ext: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        name: {
          type: Sequelize.STRING(500),
          allowNull: false,
        },
        team_id: {
          type: Sequelize.DataTypes.UUID,
          allowNull: false,
          onDelete: "CASCADE",
          onUpdate: "CASCADE",
          references: {
            model: "Teams",
            key: "id",
          },
        },
        type: {
          type: Sequelize.DataTypes.ENUM(["personal", "group"]),
          allowNull: false,
          defaultValue: "group",
        },
        info: {
          type: Sequelize.DataTypes.JSONB,
        },
        private: {
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: false,
        },
        web_url: { type: Sequelize.TEXT },
        active: {
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: true,
        },
        conversation_ref: {
          type: Sequelize.DataTypes.JSONB,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
      },
      {
        schema: "rooster",
      }
    );
    await queryInterface.addIndex(
      {
        tableName: "Channels",
        schema: "rooster",
      },
      {
        unique: true,
        fields: ["platform", "id_ext"],
        name: "one_id_ext_per_platform",
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeIndex(
      {
        tableName: "Channels",
        schema: "rooster",
      },
      "one_id_ext_per_platform"
    );
    await queryInterface.removeConstraint(
      {
        tableName: "Channels",
        schema: "rooster",
      },
      "Channels_team_id_fkey"
    );
    await queryInterface.dropTable({
      tableName: "Channels",
      schema: "rooster",
    });
  },
};
