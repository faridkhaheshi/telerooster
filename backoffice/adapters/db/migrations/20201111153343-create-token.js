"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      "Tokens",
      {
        id: {
          allowNull: false,
          primaryKey: true,
          type: Sequelize.DataTypes.UUID,
          defaultValue: Sequelize.literal("uuid_generate_v4()"),
        },
        provider: {
          type: Sequelize.DataTypes.ENUM(["microsoft", "slack", "google"]),
          allowNull: false,
        },
        access_token: {
          type: Sequelize.TEXT,
        },
        refresh_token: {
          type: Sequelize.TEXT,
        },
        user_id: {
          type: Sequelize.DataTypes.UUID,
          onDelete: "CASCADE",
          onUpdate: "CASCADE",
          references: {
            model: "Users",
            key: "id",
          },
        },
        profile_id: {
          type: Sequelize.DataTypes.UUID,
          onDelete: "CASCADE",
          onUpdate: "CASCADE",
          references: {
            model: "Profiles",
            key: "id",
          },
          unique: true,
        },
        expires_in: {
          type: Sequelize.DataTypes.INTEGER,
        },
        info: {
          type: Sequelize.DataTypes.JSONB,
        },
        active: {
          type: Sequelize.DataTypes.BOOLEAN,
          allowNull: false,
          defaultValue: true,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
      },
      {
        schema: "rooster",
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint(
      {
        tableName: "Tokens",
        schema: "rooster",
      },
      "Tokens_user_id_fkey"
    );
    await queryInterface.removeConstraint(
      {
        tableName: "Tokens",
        schema: "rooster",
      },
      "Tokens_profile_id_fkey"
    );
    await queryInterface.dropTable("Tokens");
  },
};
