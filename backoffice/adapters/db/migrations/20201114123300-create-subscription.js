"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      "Subscriptions",
      {
        id: {
          allowNull: false,
          primaryKey: true,
          type: Sequelize.DataTypes.UUID,
          defaultValue: Sequelize.literal("uuid_generate_v4()"),
        },
        active: {
          type: Sequelize.DataTypes.BOOLEAN,
          allowNull: false,
          defaultValue: true,
        },
        status: {
          type: Sequelize.DataTypes.ENUM(["waiting", "ready"]),
          allowNull: false,
          defaultValue: "waiting",
        },
        user_id: {
          type: Sequelize.DataTypes.UUID,
          onDelete: "CASCADE",
          onUpdate: "CASCADE",
          references: {
            model: "Users",
            key: "id",
          },
        },
        pool_id: {
          type: Sequelize.DataTypes.UUID,
          onDelete: "CASCADE",
          onUpdate: "CASCADE",
          references: {
            model: "Pools",
            key: "id",
          },
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
      },
      {
        schema: "rooster",
      }
    );
    await queryInterface.addIndex(
      {
        tableName: "Subscriptions",
        schema: "rooster",
      },
      {
        unique: true,
        fields: ["user_id", "pool_id"],
        name: "one_pool_subscription_per_user",
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeIndex(
      {
        tableName: "Subscriptions",
        schema: "rooster",
      },
      "one_pool_subscription_per_user"
    );
    await queryInterface.removeConstraint(
      {
        tableName: "Subscriptions",
        schema: "rooster",
      },
      "Subscriptions_user_id_fkey"
    );
    await queryInterface.removeConstraint(
      {
        tableName: "Subscriptions",
        schema: "rooster",
      },
      "Subscriptions_pool_id_fkey"
    );
    await queryInterface.dropTable({
      tableName: "Subscriptions",
      schema: "rooster",
    });
  },
};
