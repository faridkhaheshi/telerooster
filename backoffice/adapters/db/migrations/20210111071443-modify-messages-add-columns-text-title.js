"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(
      {
        tableName: "Messages",
        schema: "rooster",
      },
      "text",
      {
        type: Sequelize.DataTypes.TEXT("long"),
      }
    );
    await queryInterface.addColumn(
      {
        tableName: "Messages",
        schema: "rooster",
      },
      "title",
      {
        type: Sequelize.DataTypes.TEXT,
      }
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn(
      {
        tableName: "Messages",
        schema: "rooster",
      },
      "text"
    );
    await queryInterface.removeColumn(
      {
        tableName: "Messages",
        schema: "rooster",
      },
      "title"
    );
  },
};
