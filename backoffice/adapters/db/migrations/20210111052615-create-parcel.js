"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      "Parcels",
      {
        id: {
          allowNull: false,
          primaryKey: true,
          type: Sequelize.DataTypes.UUID,
          defaultValue: Sequelize.literal("uuid_generate_v4()"),
        },
        status: {
          type: Sequelize.DataTypes.ENUM([
            "waiting",
            "sent",
            "canceled",
            "failed",
          ]),
          allowNull: false,
          defaultValue: "waiting",
        },
        message_id: {
          type: Sequelize.DataTypes.UUID,
          onDelete: "CASCADE",
          onUpdate: "CASCADE",
          references: {
            model: "Messages",
            key: "id",
          },
        },
        channel_id: {
          type: Sequelize.DataTypes.UUID,
          onDelete: "CASCADE",
          onUpdate: "CASCADE",
          references: {
            model: "Channels",
            key: "id",
          },
        },
        info: {
          type: Sequelize.DataTypes.JSONB,
        },
        sent_at: { type: Sequelize.DataTypes.DATE },
        active: {
          type: Sequelize.DataTypes.BOOLEAN,
          allowNull: false,
          defaultValue: true,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
      },
      {
        schema: "rooster",
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint(
      {
        tableName: "Parcels",
        schema: "rooster",
      },
      "Parcels_message_id_fkey"
    );
    await queryInterface.removeConstraint(
      {
        tableName: "Parcels",
        schema: "rooster",
      },
      "Parcels_channel_id_fkey"
    );
    await queryInterface.dropTable({
      tableName: "Parcels",
      schema: "rooster",
    });
  },
};
