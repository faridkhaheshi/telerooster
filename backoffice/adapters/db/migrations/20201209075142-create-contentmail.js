"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      "Contentmails",
      {
        id: {
          allowNull: false,
          primaryKey: true,
          type: Sequelize.DataTypes.UUID,
          defaultValue: Sequelize.literal("uuid_generate_v4()"),
        },
        address: {
          type: Sequelize.STRING,
          unique: true,
          allowNull: false,
        },
        type: {
          type: Sequelize.DataTypes.ENUM(["pool", "channel"]),
          allowNull: false,
          defaultValue: "pool",
        },
        pool_id: {
          type: Sequelize.DataTypes.UUID,
          onDelete: "CASCADE",
          onUpdate: "CASCADE",
          references: {
            model: "Pools",
            key: "id",
          },
        },
        channel_id: {
          type: Sequelize.DataTypes.UUID,
          onDelete: "CASCADE",
          onUpdate: "CASCADE",
          references: {
            model: "Channels",
            key: "id",
          },
        },
        active: {
          type: Sequelize.DataTypes.BOOLEAN,
          allowNull: false,
          defaultValue: true,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
      },
      {
        schema: "rooster",
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint(
      {
        tableName: "Contentmails",
        schema: "rooster",
      },
      "Contentmails_channel_id_fkey"
    );
    await queryInterface.removeConstraint(
      {
        tableName: "Contentmails",
        schema: "rooster",
      },
      "Contentmails_pool_id_fkey"
    );
    await queryInterface.dropTable({
      tableName: "Contentmails",
      schema: "rooster",
    });
  },
};
