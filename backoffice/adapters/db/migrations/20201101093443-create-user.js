"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      "Users",
      {
        id: {
          allowNull: false,
          primaryKey: true,
          type: Sequelize.DataTypes.UUID,
          defaultValue: Sequelize.literal("uuid_generate_v4()"),
        },
        first_name: {
          type: Sequelize.STRING(50),
        },
        last_name: {
          type: Sequelize.STRING(50),
        },
        display_name: {
          type: Sequelize.STRING(100),
        },
        email: {
          unique: true,
          allowNull: false,
          type: Sequelize.CITEXT,
        },
        password_hash: {
          type: Sequelize.TEXT,
        },
        role: {
          type: Sequelize.ENUM(["normal", "admin", "superadmin"]),
          allowNull: false,
          defaultValue: "normal",
        },
        active: {
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: true,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
      },
      {
        schema: "rooster",
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable({
      tableName: "Users",
      schema: "rooster",
    });
  },
};
