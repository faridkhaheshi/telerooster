"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      "Pools",
      {
        id: {
          allowNull: false,
          primaryKey: true,
          type: Sequelize.DataTypes.UUID,
          defaultValue: Sequelize.literal("uuid_generate_v4()"),
        },
        creator_id: {
          type: Sequelize.UUID,
          allowNull: false,
          onDelete: "CASCADE",
          onUpdate: "CASCADE",
          references: {
            model: "Users",
            key: "id",
          },
        },
        poster: { type: Sequelize.TEXT },
        title: {
          type: Sequelize.STRING(100),
          allowNull: false,
        },
        description: {
          type: Sequelize.STRING(1500),
          allowNull: false,
        },
        slug: {
          type: Sequelize.TEXT,
          unique: true,
        },
        is_free: {
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: false,
        },
        price_sm: { type: Sequelize.INTEGER },
        price_md: { type: Sequelize.INTEGER },
        price_lg: { type: Sequelize.INTEGER },
        has_free_trial: {
          type: Sequelize.BOOLEAN,
          defaultValue: true,
        },
        featured: {
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: false,
        },
        active: {
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: true,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
      },
      {
        schema: "rooster",
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint(
      {
        tableName: "Pools",
        schema: "rooster",
      },
      "Pools_creator_id_fkey"
    );
    await queryInterface.dropTable({
      tableName: "Pools",
      schema: "rooster",
    });
  },
};
