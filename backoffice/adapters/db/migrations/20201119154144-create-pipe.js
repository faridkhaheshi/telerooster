"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      "Pipes",
      {
        id: {
          allowNull: false,
          primaryKey: true,
          type: Sequelize.DataTypes.UUID,
          defaultValue: Sequelize.literal("uuid_generate_v4()"),
        },
        active: {
          type: Sequelize.DataTypes.BOOLEAN,
          allowNull: false,
          defaultValue: true,
        },
        subscription_id: {
          type: Sequelize.DataTypes.UUID,
          allowNull: false,
          onDelete: "CASCADE",
          onUpdate: "CASCADE",
          references: {
            model: "Subscriptions",
            key: "id",
          },
        },
        channel_id: {
          type: Sequelize.DataTypes.UUID,
          allowNull: false,
          onDelete: "CASCADE",
          onUpdate: "CASCADE",
          references: {
            model: "Channels",
            key: "id",
          },
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
      },
      {
        schema: "rooster",
      }
    );
    await queryInterface.addIndex(
      {
        tableName: "Pipes",
        schema: "rooster",
      },
      {
        unique: true,
        fields: ["subscription_id", "channel_id"],
        name: "max_one_link_between_subscription_and_channel",
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeIndex(
      {
        tableName: "Pipes",
        schema: "rooster",
      },
      "max_one_link_between_subscription_and_channel"
    );
    await queryInterface.removeConstraint(
      {
        tableName: "Pipes",
        schema: "rooster",
      },
      "Pipes_subscription_id_fkey"
    );
    await queryInterface.removeConstraint(
      {
        tableName: "Pipes",
        schema: "rooster",
      },
      "Pipes_channel_id_fkey"
    );
    await queryInterface.dropTable({
      tableName: "Pipes",
      schema: "rooster",
    });
  },
};
