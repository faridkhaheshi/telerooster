"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      "TeamReports",
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        team_id: {
          type: Sequelize.DataTypes.UUID,
          allowNull: false,
          onDelete: "CASCADE",
          onUpdate: "CASCADE",
          references: {
            model: "Teams",
            key: "id",
          },
        },
        info: {
          type: Sequelize.DataTypes.JSONB,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
      },
      {
        schema: "rooster",
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint(
      {
        tableName: "TeamReports",
        schema: "rooster",
      },
      "TeamReports_team_id_fkey"
    );
    await queryInterface.dropTable({
      tableName: "TeamReports",
      schema: "rooster",
    });
  },
};
