"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      "Profiles",
      {
        id: {
          allowNull: false,
          primaryKey: true,
          type: Sequelize.DataTypes.UUID,
          defaultValue: Sequelize.literal("uuid_generate_v4()"),
        },
        provider: {
          type: Sequelize.DataTypes.ENUM(["microsoft", "slack", "google"]),
          allowNull: false,
        },
        id_ext: {
          type: Sequelize.DataTypes.STRING,
          allowNull: false,
        },
        user_id: {
          type: Sequelize.DataTypes.UUID,
          onDelete: "CASCADE",
          onUpdate: "CASCADE",
          references: {
            model: "Users",
            key: "id",
          },
        },
        info: {
          type: Sequelize.DataTypes.JSONB,
        },
        conversation_ref: {
          type: Sequelize.DataTypes.JSONB,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
      },
      {
        schema: "rooster",
      }
    );
    await queryInterface.addIndex(
      {
        tableName: "Profiles",
        schema: "rooster",
      },
      {
        unique: true,
        fields: ["provider", "id_ext"],
        name: "one_profile_per_provider",
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeIndex(
      {
        tableName: "Profiles",
        schema: "rooster",
      },
      "one_profile_per_provider"
    );
    await queryInterface.removeConstraint(
      {
        tableName: "Profiles",
        schema: "rooster",
      },
      "Profiles_user_id_fkey"
    );
    await queryInterface.dropTable({
      tableName: "Profiles",
      schema: "rooster",
    });
  },
};
