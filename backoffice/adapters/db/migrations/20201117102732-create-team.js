"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      "Teams",
      {
        id: {
          allowNull: false,
          primaryKey: true,
          type: Sequelize.DataTypes.UUID,
          defaultValue: Sequelize.literal("uuid_generate_v4()"),
        },
        id_ext: {
          type: Sequelize.DataTypes.STRING,
          allowNull: false,
        },
        platform: {
          type: Sequelize.DataTypes.ENUM(["microsoft", "slack", "google"]),
          allowNull: false,
        },
        name: {
          type: Sequelize.DataTypes.STRING,
        },
        added_by: {
          type: Sequelize.DataTypes.UUID,
          allowNull: false,
          onDelete: "CASCADE",
          onUpdate: "CASCADE",
          references: {
            model: "Profiles",
            key: "id",
          },
        },
        info: {
          type: Sequelize.DataTypes.JSONB,
        },
        conversation_ref: {
          type: Sequelize.DataTypes.JSONB,
        },
        active: {
          type: Sequelize.DataTypes.BOOLEAN,
          allowNull: false,
          defaultValue: true,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
      },
      {
        schema: "rooster",
      }
    );
    await queryInterface.addIndex(
      {
        tableName: "Teams",
        schema: "rooster",
      },
      {
        unique: true,
        fields: ["platform", "id_ext"],
        name: "one_team_per_platform",
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeIndex(
      {
        tableName: "Teams",
        schema: "rooster",
      },
      "one_team_per_platform"
    );
    await queryInterface.removeConstraint(
      {
        tableName: "Teams",
        schema: "rooster",
      },
      "Teams_added_by_fkey"
    );
    await queryInterface.dropTable({
      tableName: "Teams",
      schema: "rooster",
    });
  },
};
