"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      "TeamAdmins",
      {
        id: {
          allowNull: false,
          primaryKey: true,
          type: Sequelize.DataTypes.UUID,
          defaultValue: Sequelize.literal("uuid_generate_v4()"),
        },
        active: {
          type: Sequelize.DataTypes.BOOLEAN,
          allowNull: false,
          defaultValue: true,
        },
        team_id: {
          type: Sequelize.DataTypes.UUID,
          onDelete: "CASCADE",
          onUpdate: "CASCADE",
          allowNull: false,
          references: {
            model: "Teams",
            key: "id",
          },
        },
        profile_id: {
          type: Sequelize.DataTypes.UUID,
          onDelete: "CASCADE",
          onUpdate: "CASCADE",
          allowNull: false,
          references: {
            model: "Profiles",
            key: "id",
          },
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
      },
      {
        schema: "rooster",
      }
    );
    await queryInterface.addIndex(
      {
        tableName: "TeamAdmins",
        schema: "rooster",
      },
      {
        unique: true,
        fields: ["team_id", "profile_id"],
        name: "once_per_profile_per_team",
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeIndex(
      {
        tableName: "TeamAdmins",
        schema: "rooster",
      },
      "once_per_profile_per_team"
    );
    await queryInterface.removeConstraint(
      {
        tableName: "TeamAdmins",
        schema: "rooster",
      },
      "TeamAdmins_profile_id_fkey"
    );
    await queryInterface.removeConstraint(
      {
        tableName: "TeamAdmins",
        schema: "rooster",
      },
      "TeamAdmins_team_id_fkey"
    );
    await queryInterface.dropTable({
      tableName: "TeamAdmins",
      schema: "rooster",
    });
  },
};
