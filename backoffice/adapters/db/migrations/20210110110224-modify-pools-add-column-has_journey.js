"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(
      {
        tableName: "Pools",
        schema: "rooster",
      },
      "has_journey",
      {
        type: Sequelize.DataTypes.BOOLEAN,
        defaultValue: false,
      }
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn(
      {
        tableName: "Pools",
        schema: "rooster",
      },
      "has_journey"
    );
  },
};
