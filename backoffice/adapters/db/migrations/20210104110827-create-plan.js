"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      "Plans",
      {
        id: {
          allowNull: false,
          primaryKey: true,
          type: Sequelize.DataTypes.UUID,
          defaultValue: Sequelize.literal("uuid_generate_v4()"),
        },
        status: {
          type: Sequelize.DataTypes.ENUM(["active", "expired", "canceled"]),
          allowNull: false,
          defaultValue: "active",
        },
        type: {
          type: Sequelize.DataTypes.ENUM(["creator", "content"]),
          allowNull: false,
          defaultValue: "creator",
        },
        user_id: {
          type: Sequelize.DataTypes.UUID,
          allowNull: false,
          onDelete: "CASCADE",
          onUpdate: "CASCADE",
          references: {
            model: "Users",
            key: "id",
          },
        },
        expires_at: {
          type: Sequelize.DataTypes.INTEGER,
        },
        product_id_ext: {
          type: Sequelize.DataTypes.STRING,
        },
        update_ref: {
          type: Sequelize.DataTypes.JSONB,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
      },
      {
        schema: "rooster",
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint(
      {
        tableName: "Plans",
        schema: "rooster",
      },
      "Plans_user_id_fkey"
    );
    await queryInterface.dropTable({
      tableName: "Plans",
      schema: "rooster",
    });
  },
};
