"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      "ChannelReports",
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        channel_id: {
          type: Sequelize.DataTypes.UUID,
          allowNull: false,
          onDelete: "CASCADE",
          onUpdate: "CASCADE",
          references: {
            model: "Channels",
            key: "id",
          },
        },
        info: {
          type: Sequelize.DataTypes.JSONB,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
      },
      {
        schema: "rooster",
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint(
      {
        tableName: "ChannelReports",
        schema: "rooster",
      },
      "ChannelReports_channel_id_fkey"
    );
    await queryInterface.dropTable({
      tableName: "ChannelReports",
      schema: "rooster",
    });
  },
};
