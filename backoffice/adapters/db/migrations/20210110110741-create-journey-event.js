"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      "JourneyEvents",
      {
        id: {
          allowNull: false,
          primaryKey: true,
          type: Sequelize.DataTypes.UUID,
          defaultValue: Sequelize.literal("uuid_generate_v4()"),
        },
        pool_id: {
          type: Sequelize.DataTypes.UUID,
          allowNull: false,
          onDelete: "CASCADE",
          onUpdate: "CASCADE",
          references: {
            model: "Pools",
            key: "id",
          },
        },
        event: {
          type: Sequelize.DataTypes.STRING,
          allowNull: false,
        },
        message_id: {
          type: Sequelize.DataTypes.UUID,
          onDelete: "CASCADE",
          onUpdate: "CASCADE",
          references: {
            model: "Messages",
            key: "id",
          },
        },
        active: {
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: true,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
      },
      {
        schema: "rooster",
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint(
      {
        tableName: "JourneyEvents",
        schema: "rooster",
      },
      "JourneyEvents_pool_id_fkey"
    );
    await queryInterface.removeConstraint(
      {
        tableName: "JourneyEvents",
        schema: "rooster",
      },
      "JourneyEvents_message_id_fkey"
    );
    await queryInterface.dropTable({
      tableName: "JourneyEvents",
      schema: "rooster",
    });
  },
};
