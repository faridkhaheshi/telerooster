"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(
      {
        tableName: "Tokens",
        schema: "rooster",
      },
      "team_id",
      {
        type: Sequelize.DataTypes.UUID,
        references: {
          model: "Teams",
          key: "id",
        },
        unique: true,
      }
    );
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeConstraint(
      {
        tableName: "Tokens",
        schema: "rooster",
      },
      "Tokens_team_id_fkey"
    );
    await queryInterface.removeColumn(
      {
        tableName: "Tokens",
        schema: "rooster",
      },
      "team_id"
    );
  },
};
