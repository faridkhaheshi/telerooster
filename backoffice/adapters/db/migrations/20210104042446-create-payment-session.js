"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable(
      "PaymentSessions",
      {
        id: {
          allowNull: false,
          primaryKey: true,
          type: Sequelize.DataTypes.UUID,
          defaultValue: Sequelize.literal("uuid_generate_v4()"),
        },
        provider: {
          type: Sequelize.DataTypes.ENUM(["stripe"]),
          allowNull: false,
        },
        id_ext: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        user_id: {
          type: Sequelize.DataTypes.UUID,
          allowNull: false,
          onDelete: "CASCADE",
          onUpdate: "CASCADE",
          references: {
            model: "Users",
            key: "id",
          },
        },
        info: {
          type: Sequelize.DataTypes.JSONB,
        },
        intent: {
          type: Sequelize.DataTypes.JSONB,
          allowNull: false,
        },
        status: {
          type: Sequelize.DataTypes.ENUM([
            "paid",
            "failed",
            "canceled",
            "unpaid",
          ]),
          allowNull: false,
          defaultValue: "unpaid",
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.NOW,
        },
      },
      {
        schema: "rooster",
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeIndex(
      {
        tableName: "PaymentSessions",
        schema: "rooster",
      },
      "one_session_id_ext_per_provider"
    );
    await queryInterface.removeConstraint(
      {
        tableName: "PaymentSessions",
        schema: "rooster",
      },
      "PaymentSessions_user_id_fkey"
    );
    await queryInterface.dropTable({
      tableName: "PaymentSessions",
      schema: "rooster",
    });
  },
};
