"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Channel extends Model {
    static associate(models) {
      Channel.belongsTo(models.Team, {
        foreignKey: "team_id",
        as: "team",
      });
      Channel.belongsToMany(models.Subscription, {
        through: models.Pipe,
        foreignKey: "channel_id",
      });
      Channel.hasMany(models.Contentmail, { foreignKey: "channel_id" });
      Channel.hasMany(models.ChannelReport, { foreignKey: "channel_id" });
      Channel.hasMany(models.Parcel, { foreignKey: "channel_id" });
    }
  }
  Channel.init(
    {
      platform: {
        type: DataTypes.ENUM(["microsoft", "slack"]),
        allowNull: false,
      },
      id_ext: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      name: {
        type: DataTypes.STRING(500),
        allowNull: false,
      },
      team_id: {
        type: DataTypes.UUID,
        allowNull: false,
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
        references: {
          model: "Teams",
          key: "id",
        },
      },
      type: {
        type: DataTypes.ENUM(["personal", "group"]),
        allowNull: false,
        defaultValue: "group",
      },
      info: {
        type: DataTypes.JSONB,
      },
      private: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      web_url: { type: DataTypes.TEXT },
      active: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: true },
      conversation_ref: {
        type: DataTypes.JSONB,
      },
    },
    {
      sequelize,
      modelName: "Channel",
      schema: "rooster",
      indexes: [
        {
          unique: true,
          fields: ["platform", "id_ext"],
          name: "one_id_ext_per_platform",
        },
      ],
    }
  );
  return Channel;
};
