"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class TeamReport extends Model {
    static associate(models) {
      TeamReport.belongsTo(models.Team, {
        foreignKey: "team_id",
        as: "team",
      });
    }
  }
  TeamReport.init(
    {
      team_id: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: "Teams",
          key: "id",
        },
      },
      info: {
        type: DataTypes.JSONB,
      },
    },
    {
      sequelize,
      modelName: "TeamReport",
      schema: "rooster",
    }
  );
  return TeamReport;
};
