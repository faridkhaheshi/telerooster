"use strict";
const { Model, Sequelize } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Token extends Model {
    static associate(models) {
      Token.belongsTo(models.User, {
        foreignKey: "user_id",
        as: "user",
      });
      Token.belongsTo(models.Profile, {
        foreignKey: "profile_id",
        as: "profile",
      });
      Token.belongsTo(models.Team, {
        foreignKey: "team_id",
        as: "team",
      });
    }
  }
  Token.init(
    {
      provider: {
        type: DataTypes.ENUM(["microsoft", "slack", "google"]),
        allowNull: false,
      },
      access_token: DataTypes.TEXT,
      refresh_token: DataTypes.TEXT,
      user_id: {
        type: DataTypes.UUID,
        references: {
          model: "Users",
          key: "id",
        },
        allowNull: false,
      },
      profile_id: {
        type: DataTypes.UUID,
        references: {
          model: "Profiles",
          key: "id",
        },
        unique: true,
      },
      team_id: {
        type: DataTypes.UUID,
        references: {
          model: "Teams",
          key: "id",
        },
        unique: true,
      },
      expires_in: {
        type: DataTypes.INTEGER,
      },
      info: {
        type: DataTypes.JSONB,
      },
      active: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: true },
    },
    {
      sequelize,
      modelName: "Token",
      schema: "rooster",
    }
  );
  return Token;
};
