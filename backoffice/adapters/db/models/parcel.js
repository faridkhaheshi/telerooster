"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Parcel extends Model {
    static associate(models) {
      Parcel.belongsTo(models.Message, {
        foreignKey: "message_id",
        as: "message",
      });
      Parcel.belongsTo(models.Channel, {
        foreignKey: "channel_id",
        as: "channel",
      });
    }
  }
  Parcel.init(
    {
      status: {
        type: DataTypes.ENUM(["waiting", "sent", "canceled", "failed"]),
        allowNull: false,
        defaultValue: "waiting",
      },
      message_id: {
        type: DataTypes.UUID,
        references: {
          model: "Message",
          key: "id",
        },
      },
      channel_id: {
        type: DataTypes.UUID,
        references: {
          model: "Channel",
          key: "id",
        },
      },
      info: {
        type: DataTypes.JSONB,
      },
      sent_at: { type: DataTypes.DATE },
      active: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: true },
    },
    {
      sequelize,
      modelName: "Parcel",
      schema: "rooster",
    }
  );
  return Parcel;
};
