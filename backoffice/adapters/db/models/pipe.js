"use strict";
const { Model, Sequelize } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Pipe extends Model {
    static associate(models) {
      Pipe.belongsTo(models.Channel, {
        foreignKey: "channel_id",
        as: "channel",
      });
      Pipe.belongsTo(models.Subscription, {
        foreignKey: "subscription_id",
        as: "subscription",
      });
    }
  }
  Pipe.init(
    {
      id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: Sequelize.literal("uuid_generate_v4()"),
      },
      active: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: true },
      subscription_id: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: "Subscriptions",
          key: "id",
        },
      },
      channel_id: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: "Channels",
          key: "id",
        },
      },
    },
    {
      sequelize,
      modelName: "Pipe",
      schema: "rooster",
      indexes: [
        {
          unique: true,
          fields: ["subscription_id", "channel_id"],
          name: "max_one_link_between_subscription_and_channel",
        },
      ],
    }
  );
  return Pipe;
};
