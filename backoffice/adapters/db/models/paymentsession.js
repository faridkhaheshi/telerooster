"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class PaymentSession extends Model {
    static associate(models) {
      PaymentSession.belongsTo(models.User, {
        foreignKey: "user_id",
        as: "user",
      });
    }
  }
  PaymentSession.init(
    {
      provider: {
        type: DataTypes.ENUM(["stripe"]),
        allowNull: false,
      },
      id_ext: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      user_id: {
        type: DataTypes.UUID,
        allowNull: false,
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
        references: {
          model: "Users",
          key: "id",
        },
      },
      info: {
        type: DataTypes.JSONB,
      },
      intent: {
        type: DataTypes.JSONB,
        allowNull: false,
      },
      status: {
        type: DataTypes.ENUM(["paid", "failed", "canceled", "unpaid"]),
        allowNull: false,
        defaultValue: "unpaid",
      },
    },
    {
      sequelize,
      modelName: "PaymentSession",
      schema: "rooster",
      indexes: [
        {
          unique: true,
          fields: ["provider", "id_ext"],
          name: "one_session_id_ext_per_provider",
        },
      ],
    }
  );
  return PaymentSession;
};
