"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Contentmail extends Model {
    static associate(models) {
      Contentmail.belongsTo(models.Pool, {
        foreignKey: "pool_id",
        as: "pool",
      });
      Contentmail.belongsTo(models.Channel, {
        foreignKey: "channel_id",
        as: "channel",
      });
    }
  }
  Contentmail.init(
    {
      address: {
        type: DataTypes.STRING,
        validate: { isEmail: { msg: "Must be a valid email address" } },
        unique: true,
        allowNull: false,
      },
      type: {
        type: DataTypes.ENUM(["pool", "channel"]),
        allowNull: false,
        defaultValue: "pool",
      },
      pool_id: {
        type: DataTypes.UUID,
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
        references: {
          model: "Pools",
          key: "id",
        },
      },
      channel_id: {
        type: DataTypes.UUID,
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
        references: {
          model: "Channels",
          key: "id",
        },
      },
      active: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: true },
    },
    {
      sequelize,
      modelName: "Contentmail",
      schema: "rooster",
    }
  );
  return Contentmail;
};
