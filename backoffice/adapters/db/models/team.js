"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Team extends Model {
    static associate(models) {
      Team.belongsTo(models.Profile, {
        foreignKey: "added_by",
        as: "added_by_profile",
      });
      Team.belongsToMany(models.Profile, {
        through: models.TeamAdmin,
        foreignKey: "team_id",
      });
      Team.hasMany(models.Channel, {
        foreignKey: "team_id",
        as: "team",
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
      });
      Team.hasOne(models.Token, {
        as: "token",
        foreignKey: "team_id",
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
      });
      Team.hasMany(models.TeamReport, {
        foreignKey: "team_id",
        as: "reportTeam",
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
      });
    }
  }
  Team.init(
    {
      id_ext: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      platform: {
        type: DataTypes.ENUM(["microsoft", "slack", "google"]),
        allowNull: false,
      },
      name: {
        type: DataTypes.STRING,
      },
      added_by: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: "Profiles",
          key: "id",
        },
      },
      info: {
        type: DataTypes.JSONB,
      },
      conversation_ref: {
        type: DataTypes.JSONB,
      },
      active: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: true },
    },
    {
      sequelize,
      modelName: "Team",
      schema: "rooster",
      indexes: [
        {
          unique: true,
          fields: ["platform", "id_ext"],
          name: "one_team_per_platform",
        },
      ],
    }
  );
  return Team;
};
