const sendErrorResponse = (res, error, defaultText = "unknown error") =>
  res.status(error.statusCode || error.status || 500).json({
    error: { message: error.message || defaultText },
  });

module.exports = { sendErrorResponse };
