const express = require("express");
const next = require("next");

const routes = require("./routes");

const port = process.env.PORT || 3000;
const dev = process.env.NODE_ENV !== "production";

const app = next({ dev });
const handle = app.getRequestHandler();

app.prepare().then(() => {
  const server = express();

  routes(server);

  server.use(express.json({ limit: "10MB" }));
  server.use(express.urlencoded({ limit: "10MB" }));

  server.all("*", (req, res) => {
    return handle(req, res);
  });

  server.listen(port, (err) => {
    if (err) throw err;
    console.log(`> Ready on http://localhost:${port}`);
  });
});
