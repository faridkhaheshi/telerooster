const {
  crud,
  sequelizeCrud,
  sequelizeSearchFields,
} = require("express-sequelize-crud");
const db = require("../../adapters/db");

module.exports = (server) => {
  server.use(
    crud("/papi/plans", {
      ...sequelizeCrud(db.Plan),
      search: sequelizeSearchFields(db.Plan, [
        "status",
        "type",
        "product_id_ext",
      ]),
    })
  );
};
