const {
  crud,
  sequelizeCrud,
  sequelizeSearchFields,
} = require("express-sequelize-crud");
const db = require("../../adapters/db");

module.exports = (server) => {
  server.use(
    crud("/papi/teams", {
      ...sequelizeCrud(db.Team),
      search: sequelizeSearchFields(db.Team, ["name", "id_ext"]),
    })
  );
};
