const {
  crud,
  sequelizeCrud,
  sequelizeSearchFields,
} = require("express-sequelize-crud");
const db = require("../../adapters/db");

module.exports = (server) => {
  server.use(
    crud("/papi/teamadmins", {
      ...sequelizeCrud(db.TeamAdmin),
      search: sequelizeSearchFields(db.TeamAdmin, ["active"]),
    })
  );
};
