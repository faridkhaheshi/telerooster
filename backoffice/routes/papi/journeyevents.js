const {
  crud,
  sequelizeCrud,
  sequelizeSearchFields,
} = require("express-sequelize-crud");
const db = require("../../adapters/db");

module.exports = (server) => {
  server.use(
    crud("/papi/journeyevents", {
      ...sequelizeCrud(db.JourneyEvent),
      search: sequelizeSearchFields(db.JourneyEvent, ["event"]),
    })
  );
};
