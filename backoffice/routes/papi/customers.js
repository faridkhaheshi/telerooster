const {
  crud,
  sequelizeCrud,
  sequelizeSearchFields,
} = require("express-sequelize-crud");
const db = require("../../adapters/db");

module.exports = (server) => {
  server.use(
    crud("/papi/customers", {
      ...sequelizeCrud(db.Customer),
      search: sequelizeSearchFields(db.Customer, ["provider", "id_ext"]),
    })
  );
};
