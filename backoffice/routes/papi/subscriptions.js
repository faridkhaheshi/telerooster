const {
  crud,
  sequelizeCrud,
  sequelizeSearchFields,
} = require("express-sequelize-crud");
const db = require("../../adapters/db");

module.exports = (server) => {
  server.use(
    crud("/papi/subscriptions", {
      ...sequelizeCrud(db.Subscription),
      search: sequelizeSearchFields(db.Subscription, ["active"]),
    })
  );
};
