const {
  crud,
  sequelizeCrud,
  sequelizeSearchFields,
} = require("express-sequelize-crud");
const db = require("../../adapters/db");

module.exports = (server) => {
  server.use(
    crud("/papi/users", {
      ...sequelizeCrud(db.User),
      search: sequelizeSearchFields(db.User, [
        "first_name",
        "last_name",
        "display_name",
        "email",
      ]),
    })
  );
};
