const { authMiddleware } = require("../../services/auth/controllers");

module.exports = (server) => {
  server.use("/papi", authMiddleware);
  require("./users")(server);
  require("./pools")(server);
  require("./profiles")(server);
  require("./subscriptions")(server);
  require("./teams")(server);
  require("./teamadmins")(server);
  require("./channels")(server);
  require("./pipes")(server);
  require("./contentmails")(server);
  require("./misc")(server);
  require("./paymentsessions")(server);
  require("./customers")(server);
  require("./plans")(server);
  require("./teamreports")(server);
  require("./channelreports")(server);
  require("./messages")(server);
  require("./journeyevents")(server);
  require("./parcels")(server);
};
