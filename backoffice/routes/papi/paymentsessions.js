const {
  crud,
  sequelizeCrud,
  sequelizeSearchFields,
} = require("express-sequelize-crud");
const db = require("../../adapters/db");

module.exports = (server) => {
  server.use(
    crud("/papi/paymentsessions", {
      ...sequelizeCrud(db.PaymentSession),
      search: sequelizeSearchFields(db.PaymentSession, [
        "provider",
        "status",
        "id_ext",
      ]),
    })
  );
};
