const {
  crud,
  sequelizeCrud,
  sequelizeSearchFields,
} = require("express-sequelize-crud");
const db = require("../../adapters/db");

module.exports = (server) => {
  server.use(
    crud("/papi/pools", {
      ...sequelizeCrud(db.Pool),
      search: sequelizeSearchFields(db.Pool, [
        "title",
        "description",
        "slug",
        "poster",
        "creator_id",
      ]),
    })
  );
};
