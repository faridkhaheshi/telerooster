const {
  getPoolSubscribedChannels,
} = require("../../../services/subscriptions/controllers");

module.exports = (server) => {
  server.get("/papi/misc/pool-subscribers/:poolId", getPoolSubscribedChannels);
};
