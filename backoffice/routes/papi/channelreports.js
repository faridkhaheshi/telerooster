const { crud, sequelizeCrud } = require("express-sequelize-crud");
const db = require("../../adapters/db");

module.exports = (server) => {
  server.use(crud("/papi/channelreports", sequelizeCrud(db.ChannelReport)));
};
