const {
  crud,
  sequelizeCrud,
  sequelizeSearchFields,
} = require("express-sequelize-crud");
const db = require("../../adapters/db");

module.exports = (server) => {
  server.use(
    crud("/papi/pipes", {
      ...sequelizeCrud(db.Pipe),
      search: sequelizeSearchFields(db.Pipe, ["active"]),
    })
  );
};
