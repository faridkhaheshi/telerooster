const {
  crud,
  sequelizeCrud,
  sequelizeSearchFields,
} = require("express-sequelize-crud");
const db = require("../../adapters/db");

module.exports = (server) => {
  server.use(
    crud("/papi/channels", {
      ...sequelizeCrud(db.Channel),
      search: sequelizeSearchFields(db.Channel, ["name", "web_url", "id_ext"]),
    })
  );
};
