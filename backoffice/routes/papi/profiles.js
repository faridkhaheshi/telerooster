const {
  crud,
  sequelizeCrud,
  sequelizeSearchFields,
} = require("express-sequelize-crud");
const db = require("../../adapters/db");

module.exports = (server) => {
  server.use(
    crud("/papi/profiles", {
      ...sequelizeCrud(db.Profile),
      search: sequelizeSearchFields(db.Profile, ["provider"]),
    })
  );
};
