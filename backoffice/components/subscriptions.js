import {
  BooleanField,
  BooleanInput,
  Create,
  Edit,
  List,
  Datagrid,
  DateField,
  TextField,
  Filter,
  ReferenceField,
  ReferenceInput,
  SelectInput,
  SimpleForm,
  TextInput,
} from "react-admin";
import JsonDataViewer from "./json-data-viewer";

const SubscriptionFilter = (props) => (
  <Filter {...props}>
    <ReferenceInput source="user_id" reference="users" alwaysOn>
      <SelectInput optionText="display_name" />
    </ReferenceInput>
    <ReferenceInput source="pool_id" reference="pools" alwaysOn>
      <SelectInput optionText="title" />
    </ReferenceInput>
  </Filter>
);

export const SubscriptionCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <BooleanInput source="active" />
      <TextInput source="user_id" label="user id" />
      <TextInput source="pool_id" label="pool id" />
    </SimpleForm>
  </Create>
);

export const SubscriptionEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <TextInput source="id" />
      <BooleanInput source="active" />
      <ReferenceInput source="user_id" reference="users">
        <SelectInput optionText="display_name" />
      </ReferenceInput>
      <ReferenceInput source="pool_id" reference="pools">
        <SelectInput optionText="title" />
      </ReferenceInput>
    </SimpleForm>
  </Edit>
);

export const SubscriptionList = (props) => (
  <List {...props} filters={<SubscriptionFilter />}>
    <Datagrid rowClick="edit" expand={<JsonDataViewer />}>
      <ReferenceField source="user_id" reference="users">
        <TextField source="display_name" />
      </ReferenceField>
      <ReferenceField source="pool_id" reference="pools">
        <TextField source="title" />
      </ReferenceField>
      <TextField source="status" />
      <BooleanField source="active" />
      <DateField source="createdAt" showTime />
    </Datagrid>
  </List>
);
