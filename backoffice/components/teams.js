import {
  ArrayField,
  SingleFieldList,
  ChipField,
  BooleanField,
  BooleanInput,
  Create,
  Edit,
  List,
  Datagrid,
  DateField,
  DateInput,
  TextField,
  EmailField,
  Filter,
  ReferenceField,
  ReferenceManyField,
  Show,
  SelectInput,
  SimpleForm,
  SimpleShowLayout,
  TextInput,
} from "react-admin";
import JsonDataViewer from "./json-data-viewer";

const platformChoices = [
  { id: "microsoft", name: "microsoft" },
  { id: "slack", name: "slack" },
  { id: "google", name: "google" },
];

const TeamFilter = (props) => (
  <Filter {...props}>
    <TextInput label="Search" source="q" alwaysOn />
    <BooleanInput source="active" alwaysOn />
    <SelectInput source="provider" choices={platformChoices} />
    <TextInput source="added_by" label="creator id" />
  </Filter>
);

export const TeamEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <TextField source="id" />
      <TextInput source="name" />
      <BooleanInput source="active" />
    </SimpleForm>
  </Edit>
);

export const TeamShow = (props) => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="id" />
      <TextField source="id_ext" />
      <TextField source="platform" />
      <TextField source="name" />
      <ReferenceField source="added_by" reference="profiles">
        <TextField source="info.displayName" />
      </ReferenceField>
      <ArrayField source="info.channels" label="channels available @ install">
        <Datagrid>
          <TextField source="id" label="channel id" />
          <TextField source="name" />
        </Datagrid>
      </ArrayField>
      <ReferenceManyField
        label="Telerooster Channels"
        reference="channels"
        target="team_id"
      >
        <Datagrid expand={<JsonDataViewer />}>
          <TextField source="name" />
          <BooleanField source="private" />
          <BooleanField source="active" />
        </Datagrid>
      </ReferenceManyField>
      <TextField source="info.memberCount" label="members" />
      <TextField source="info.channelCount" label="channels" />
      <TextField source="info.tenantId" />
      <TextField source="info.aadGroupId" />
      <JsonDataViewer source="info" />
      <JsonDataViewer source="conversation_ref" />
      <BooleanField source="active" />
      <DateField source="createdAt" showTime />
      <DateField source="updatedAt" showTime />
    </SimpleShowLayout>
  </Show>
);

export const TeamList = (props) => (
  <List {...props} filters={<TeamFilter />}>
    <Datagrid rowClick="show" expand={<JsonDataViewer />}>
      <DateField source="createdAt" showTime />
      <TextField source="name" />
      <TextField source="platform" />
      <BooleanField source="active" />
      <ReferenceField source="added_by" reference="profiles">
        <TextField source="info.displayName" />
      </ReferenceField>
      <TextField source="info.memberCount" label="members" />
      <TextField source="info.channelCount" label="channels" />
      <TextField source="id" />
      <TextField source="id_ext" label="id in platform" />
    </Datagrid>
  </List>
);
