import {
  List,
  Datagrid,
  DateField,
  TextField,
  Filter,
  ReferenceField,
  ReferenceInput,
  ReferenceManyField,
  Show,
  SelectInput,
  SimpleShowLayout,
  TextInput,
  BooleanField,
} from "react-admin";
import JsonDataViewer from "./json-data-viewer";

const providerChoices = [
  { id: "microsoft", name: "microsoft" },
  { id: "slack", name: "slack" },
  { id: "google", name: "google" },
];

const ProfileFilter = (props) => (
  <Filter {...props}>
    <SelectInput source="provider" choices={providerChoices} alwaysOn />
    <TextInput source="id_ext" label="external id" />
    <ReferenceInput source="user_id" reference="users">
      <SelectInput optionText="display_name" />
    </ReferenceInput>
  </Filter>
);

export const ProfileShow = (props) => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="id" />
      <TextField source="provider" />
      <TextField source="id_ext" />
      <ReferenceField source="user_id" reference="users">
        <TextField source="display_name" />
      </ReferenceField>
      <ReferenceManyField
        label="Profile Teams"
        reference="teams"
        target="added_by"
      >
        <Datagrid expand={<JsonDataViewer />}>
          <TextField source="name" />
          <TextField source="platform" />
          <DateField source="createdAt" />
          <BooleanField source="active" />
        </Datagrid>
      </ReferenceManyField>
      <JsonDataViewer source="info" />
      <JsonDataViewer source="conversation_ref" />
      <DateField source="createdAt" showTime />
      <DateField source="updatedAt" showTime />
    </SimpleShowLayout>
  </Show>
);

export const ProfileList = (props) => (
  <List {...props} filters={<ProfileFilter />}>
    <Datagrid rowClick="show" expand={<JsonDataViewer />}>
      <DateField source="createdAt" showTime />
      <TextField source="provider" />
      <ReferenceField source="user_id" reference="users">
        <TextField source="display_name" />
      </ReferenceField>
      <TextField source="id_ext" />
    </Datagrid>
  </List>
);
