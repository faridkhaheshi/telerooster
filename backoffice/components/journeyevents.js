import {
  ArrayField,
  BooleanField,
  BooleanInput,
  Create,
  Edit,
  List,
  Datagrid,
  DateField,
  DateInput,
  TextField,
  EmailField,
  Filter,
  ImageField,
  ReferenceManyField,
  ReferenceField,
  ReferenceInput,
  Show,
  SelectInput,
  SimpleForm,
  SimpleShowLayout,
  TextInput,
} from "react-admin";
import JsonDataViewer from "./json-data-viewer";
import UnixDateField from "./UnixDateField";

const JourneyeventFilter = (props) => (
  <Filter {...props}>
    <TextInput source="pool_id" />
    <TextInput source="event" />
    <TextInput source="messsage_id" />
    <BooleanInput source="active" />
  </Filter>
);

export const JourneyeventEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <TextField source="id" />
      <ReferenceField source="pool_id" reference="pools">
        <TextField source="title" />
      </ReferenceField>
      <TextInput source="event" />
      <TextInput source="message_id" label="message id" />
      <BooleanInput source="active" />
    </SimpleForm>
  </Edit>
);

export const JourneyeventShow = (props) => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="id" />
      <TextField source="pool_id" />
      <ReferenceField source="pool_id" reference="pools">
        <TextField source="title" />
      </ReferenceField>
      <TextField source="event" />
      <TextField source="message_id" label="Message id" />
      <ReferenceField source="message_id" reference="messages" link={false}>
        <ArrayField source="blocks">
          <Datagrid rowClick="expand">
            <TextField source="type" />
            <TextField source="content.markdown" />
            <TextField source="content.alt" />
            <TextField source="content.template" />
            <ImageField source="content.image.src" />
          </Datagrid>
        </ArrayField>
      </ReferenceField>
      <BooleanField source="active" />
      <DateField source="createdAt" showTime />
      <DateField source="updatedAt" showTime />
    </SimpleShowLayout>
  </Show>
);

export const JourneyeventList = (props) => (
  <List {...props} filters={<JourneyeventFilter />}>
    <Datagrid rowClick="show" expand={<JsonDataViewer />}>
      <ReferenceField source="pool_id" reference="pools">
        <TextField source="title" />
      </ReferenceField>
      <TextField source="event" />
      <BooleanField source="active" />
      <ReferenceField source="message_id" reference="messages">
        <TextField source="id" />
      </ReferenceField>
      <DateField source="createdAt" showTime />
    </Datagrid>
  </List>
);
