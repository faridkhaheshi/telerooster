const dateOptions = { year: "numeric", month: "short", day: "numeric" };

const UnixDateField = ({ record, source }) => {
  try {
    return (
      <span>{`${new Date(record[source] * 1000).toLocaleDateString(
        "en-US",
        dateOptions
      )}`}</span>
    );
  } catch (error) {
    return "not available";
  }
};

UnixDateField.defaultProps = { label: "expires at" };

export default UnixDateField;
