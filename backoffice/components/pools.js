import {
  BooleanField,
  BooleanInput,
  Create,
  Edit,
  List,
  NumberInput,
  Datagrid,
  DateField,
  DateInput,
  TextField,
  Filter,
  ReferenceField,
  ReferenceManyField,
  ReferenceInput,
  Show,
  SelectInput,
  SimpleForm,
  SimpleShowLayout,
  TextInput,
} from "react-admin";
import JsonDataViewer from "./json-data-viewer";

import { JourneyeventShow } from "./journeyevents";

const PoolFilter = (props) => (
  <Filter {...props}>
    <TextInput label="Search" source="q" alwaysOn />
    <ReferenceInput source="creator_id" reference="users">
      <SelectInput optionText="display_name" />
    </ReferenceInput>
    <BooleanInput source="active" />
    <BooleanInput source="has_journey" />
    <BooleanInput source="featured" />
    <BooleanInput source="is_free" />
  </Filter>
);

export const PoolEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <TextInput source="id" />
      <ReferenceInput source="creator_id" reference="users">
        <SelectInput optionText="display_name" />
      </ReferenceInput>
      <TextInput source="poster" />
      <TextInput source="title" />
      <TextInput multiline fullWidth source="description" />
      <TextInput fullWidth source="slug" />
      <BooleanInput source="is_free" />
      <NumberInput source="price_sm" />
      <NumberInput source="price_md" />
      <NumberInput source="price_lg" />
      <BooleanInput source="has_free_trial" />
      <BooleanInput source="featured" />
      <BooleanInput source="active" />
      <BooleanInput source="has_journey" />
      <DateInput source="createdAt" />
      <DateInput source="updatedAt" />
    </SimpleForm>
  </Edit>
);

export const PoolCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <ReferenceInput label="Creator" source="creator_id" reference="users">
        <SelectInput optionText="display_name" />
      </ReferenceInput>
      <TextInput source="poster" type="url" />
      <TextInput source="title" />
      <TextInput source="description" />
      <TextInput source="slug" />
      <BooleanInput source="is_free" />
      <NumberInput source="price_sm" />
      <NumberInput source="price_md" />
      <NumberInput source="price_lg" />
      <BooleanInput source="has_free_trial" />
      <BooleanInput source="featured" />
      <BooleanInput source="active" />
      <BooleanInput source="has_journey" />
    </SimpleForm>
  </Create>
);

export const PoolShow = (props) => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="id" />
      <ReferenceField source="creator_id" reference="users">
        <TextField source="display_name" />
      </ReferenceField>
      <TextField source="poster" />
      <TextField source="title" />
      <TextField source="description" />
      <TextField source="slug" />
      <BooleanField source="is_free" />
      <TextField source="price_sm" />
      <TextField source="price_md" />
      <TextField source="price_lg" />
      <BooleanField source="has_free_trial" />
      <BooleanField source="featured" />
      <BooleanField source="active" />
      <BooleanField source="has_journey" />
      <DateField source="createdAt" showTime />
      <DateField source="updatedAt" showTime />
      <ReferenceManyField
        label="Content Emails"
        reference="contentmails"
        target="pool_id"
      >
        <Datagrid expand={<JsonDataViewer />}>
          <TextField source="address" />
          <BooleanField source="active" />
          <DateField source="createdAt" showTime />
        </Datagrid>
      </ReferenceManyField>
      <ReferenceManyField
        label="Journey events"
        reference="journeyevents"
        target="pool_id"
        sort={{ field: "createdAt", order: "ASC" }}
      >
        <Datagrid expand={<JourneyeventShow />}>
          <TextField source="event" />
          <TextField source="message_id" />
          <TextField source="title" />
          <TextField source="text" />
          <BooleanField source="active" />
        </Datagrid>
      </ReferenceManyField>
    </SimpleShowLayout>
  </Show>
);

export const PoolList = (props) => (
  <List {...props} filters={<PoolFilter />}>
    <Datagrid rowClick="show" expand={<JsonDataViewer />}>
      <ReferenceField source="creator_id" reference="users">
        <TextField source="display_name" />
      </ReferenceField>
      <TextField source="title" />
      <TextField source="description" />
      <TextField source="slug" />
      <BooleanField source="is_free" />
      <BooleanField source="has_journey" />
      <TextField source="price_sm" />
      <TextField source="price_md" />
      <TextField source="price_lg" />
      <BooleanField source="has_free_trial" />
      <BooleanField source="featured" />
      <BooleanField source="active" />
      <DateField source="createdAt" showTime />
    </Datagrid>
  </List>
);
