import login from "./login";
import logout from "./logout";
import checkAuth from "./check-auth";
import checkError from "./check-error";
import getPermissions from "./get-permissions";
import getIdentity from "./get-identiity";
export { default as httpClient } from "./http-client";

const authProvider = {
  login,
  logout,
  checkAuth,
  checkError,
  getPermissions,
  getIdentity,
};

export default authProvider;
