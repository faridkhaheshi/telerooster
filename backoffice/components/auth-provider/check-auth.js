export default () =>
  localStorage.getItem("token") ? Promise.resolve() : Promise.reject();
