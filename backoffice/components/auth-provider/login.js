export default async ({ username, password }) => {
  const authResult = await fetch("/api/auth/login", {
    method: "POST",
    body: JSON.stringify({ email: username, password }),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });

  const { token } = await authResult.json();
  if (token) localStorage.setItem("token", token);
};
