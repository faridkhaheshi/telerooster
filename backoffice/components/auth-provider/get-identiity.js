import jwtDecode from "jwt-decode";

export default async () => {
  const token = localStorage.getItem("token");
  if (!token) return {};
  const { id, display_name, email } = jwtDecode(token);
  return { id, fullName: display_name || email };
};
