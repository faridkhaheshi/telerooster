export default async () => {
  localStorage.removeItem("token");
  return Promise.resolve();
};
