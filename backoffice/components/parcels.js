import {
  ArrayField,
  BooleanField,
  BooleanInput,
  Create,
  Edit,
  List,
  Datagrid,
  DateField,
  DateInput,
  TextField,
  EmailField,
  Filter,
  ImageField,
  ReferenceManyField,
  ReferenceField,
  ReferenceInput,
  Show,
  SelectInput,
  SimpleForm,
  SimpleShowLayout,
  TextInput,
} from "react-admin";
import JsonDataViewer from "./json-data-viewer";
import UnixDateField from "./UnixDateField";

export const ParcelEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <TextField source="id" />
      <TextInput source="status" />
      <TextField source="message_id" />
      <TextField source="channel_id" />
      <TextInput source="info.audienceSize" />
      <BooleanInput source="active" />
      <DateField source="sent_at" showTime />
      <DateField source="createdAt" showTime />
      <DateField source="updatedAt" showTime />
    </SimpleForm>
  </Edit>
);

export const ParcelShow = (props) => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="id" />
      <TextField source="status" />

      <TextField source="message_id" />
      <ReferenceField source="message_id" reference="messages">
        <TextField source="title" />
      </ReferenceField>
      <ReferenceField source="message_id" reference="messages">
        <TextField source="text" />
      </ReferenceField>

      <ReferenceField source="message_id" reference="messages" link={false}>
        <ArrayField source="blocks">
          <Datagrid rowClick="expand">
            <TextField source="type" />
            <TextField source="content.markdown" />
            <TextField source="content.alt" />
            <TextField source="content.template" />
            <ImageField source="content.image.src" />
          </Datagrid>
        </ArrayField>
      </ReferenceField>
      <TextField source="channel_id" />
      <ReferenceField source="channel_id" reference="channels">
        <TextField source="name" />
      </ReferenceField>
      <JsonDataViewer source="info" />
      <BooleanField source="active" />
      <DateField source="sent_at" showTime />
      <DateField source="createdAt" showTime />
      <DateField source="updatedAt" showTime />
    </SimpleShowLayout>
  </Show>
);

export const ParcelList = (props) => (
  <List {...props}>
    <Datagrid rowClick="show" expand={<JsonDataViewer />}>
      <TextField source="status" />
      <ReferenceField source="message_id" reference="messages">
        <TextField source="id" />
      </ReferenceField>
      <ReferenceField source="channel_id" reference="channels">
        <TextField source="name" />
      </ReferenceField>
      <DateField source="sent_at" showTime />
      <DateField source="createdAt" showTime />
    </Datagrid>
  </List>
);
