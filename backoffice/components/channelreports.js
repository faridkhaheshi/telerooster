import {
  ArrayField,
  SingleFieldList,
  ChipField,
  BooleanField,
  BooleanInput,
  Create,
  Edit,
  List,
  NumberField,
  Datagrid,
  DateField,
  DateInput,
  TextField,
  EmailField,
  NumberInput,
  Filter,
  ReferenceField,
  ReferenceInput,
  ReferenceManyField,
  Show,
  SelectInput,
  SimpleForm,
  SimpleShowLayout,
  TextInput,
} from "react-admin";
import JsonDataViewer from "./json-data-viewer";

const ChannelreportFilter = (props) => (
  <Filter {...props}>
    <TextInput source="channel_id" label="channel id" alwaysOn />
  </Filter>
);

export const ChannelreportEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <TextField source="id" />
      <TextField source="channel_id" />
      <ReferenceField source="channel_id" reference="channels">
        <TextField optionText="name" />
      </ReferenceField>
      <TextInput source="info.memberCount" />
      <DateField source="createdAt" showTime />
      <DateField source="updatedAt" showTime />
    </SimpleForm>
  </Edit>
);

export const ChannelreportShow = (props) => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="id" />
      <TextField source="channel_id" />
      <ReferenceField source="channel_id" reference="channels">
        <TextField source="name" />
      </ReferenceField>
      <TextField source="info.name" />
      <JsonDataViewer source="info" />
      <DateField source="createdAt" showTime />
      <DateField source="updatedAt" showTime />
    </SimpleShowLayout>
  </Show>
);

export const ChannelreportList = (props) => (
  <List {...props} filters={<ChannelreportFilter />}>
    <Datagrid rowClick="show" expand={<JsonDataViewer />}>
      <TextField source="id" />
      <ReferenceField source="channel_id" reference="channels">
        <TextField source="name" />
      </ReferenceField>
      <TextField source="info.memberCount" />
      <DateField source="createdAt" showTime />
    </Datagrid>
  </List>
);
