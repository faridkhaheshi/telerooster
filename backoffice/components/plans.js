import {
  BooleanField,
  BooleanInput,
  Create,
  Edit,
  List,
  Datagrid,
  DateField,
  DateInput,
  TextField,
  EmailField,
  Filter,
  NumberField,
  ReferenceManyField,
  ReferenceField,
  Show,
  SelectInput,
  SimpleForm,
  SimpleShowLayout,
  TextInput,
} from "react-admin";
import JsonDataViewer from "./json-data-viewer";
import UnixDateField from "./UnixDateField";

const statusChoices = [
  { id: "active", name: "active" },
  { id: "expired", name: "expired" },
  { id: "canceled", name: "canceled" },
];

const PlanFilter = (props) => (
  <Filter {...props}>
    <TextInput label="prod id" source="product_id_ext" />
    <SelectInput source="status" choices={statusChoices} alwaysOn />
  </Filter>
);

export const PlanEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <TextField source="id" />
      <SelectInput source="status" choices={statusChoices} />
      <TextField source="type" />
      <ReferenceField source="user_id" reference="users">
        <TextField source="display_name" />
      </ReferenceField>
      <TextField label="expires at:" />
      <UnixDateField source="expires_at" />
      <TextField source="product_id_ext" />
      <DateField source="createdAt" showTime />
      <DateField source="updatedAt" showTime />
    </SimpleForm>
  </Edit>
);

export const PlanShow = (props) => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="id" />
      <TextField source="status" />
      <TextField source="type" />
      <ReferenceField source="user_id" reference="users">
        <TextField source="display_name" />
      </ReferenceField>
      <TextField label="expires at:" />
      <UnixDateField source="expires_at" />
      <TextField source="product_id_ext" />
      <DateField source="createdAt" showTime />
      <DateField source="updatedAt" showTime />
    </SimpleShowLayout>
  </Show>
);

export const PlanList = (props) => (
  <List {...props} filters={<PlanFilter />}>
    <Datagrid rowClick="show" expand={<JsonDataViewer />}>
      <TextField source="status" />
      <TextField source="type" />
      <ReferenceField source="user_id" reference="users">
        <TextField source="display_name" />
      </ReferenceField>
      <UnixDateField source="expires_at" />
      <TextField source="product_id_ext" />
      <DateField source="createdAt" showTime />
    </Datagrid>
  </List>
);
