import {
  BooleanField,
  BooleanInput,
  Create,
  Edit,
  List,
  Datagrid,
  DateField,
  DateInput,
  TextField,
  EmailField,
  Filter,
  ReferenceManyField,
  ReferenceField,
  Show,
  SelectInput,
  SimpleForm,
  SimpleShowLayout,
  TextInput,
} from "react-admin";
import JsonDataViewer from "./json-data-viewer";

const CustomerFilter = (props) => (
  <Filter {...props}>
    <TextInput label="external id" source="id_ext" alwaysOn />
    <BooleanInput source="active" />
  </Filter>
);

export const CustomerEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <TextField source="id" />
      <TextField source="provider" />
      <TextField source="id_ext" />
      <BooleanInput source="active" />
      <DateField source="createdAt" />
      <DateField source="updatedAt" />
      <JsonDataViewer source="info" />
    </SimpleForm>
  </Edit>
);

export const CustomerShow = (props) => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="id" />
      <TextField source="provider" />
      <TextField source="id_ext" />
      <ReferenceField source="user_id" reference="users">
        <TextField source="display_name" />
      </ReferenceField>
      <BooleanField source="active" />
      <DateField source="createdAt" showTime />
      <DateField source="updatedAt" showTime />
      <JsonDataViewer source="info" />
    </SimpleShowLayout>
  </Show>
);

export const CustomerList = (props) => (
  <List {...props} filters={<CustomerFilter />}>
    <Datagrid rowClick="show" expand={<JsonDataViewer />}>
      <TextField source="provider" />
      <TextField source="id_ext" />
      <BooleanField source="active" />
      <ReferenceField source="user_id" reference="users">
        <TextField source="display_name" />
      </ReferenceField>
      <DateField source="createdAt" showTime />
    </Datagrid>
  </List>
);
