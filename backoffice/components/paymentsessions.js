import {
  BooleanField,
  BooleanInput,
  Create,
  Edit,
  List,
  Datagrid,
  DateField,
  DateInput,
  TextField,
  EmailField,
  Filter,
  ReferenceManyField,
  ReferenceField,
  Show,
  SelectInput,
  SimpleForm,
  SimpleShowLayout,
  TextInput,
} from "react-admin";
import JsonDataViewer from "./json-data-viewer";

const providerChoices = [{ id: "stripe", name: "stripe" }];
const statusChoices = [
  { id: "paid", name: "paid" },
  { id: "failed", name: "failed" },
  { id: "canceled", name: "canceled" },
  { id: "unpaid", name: "unpaid" },
];

const PaymentsessionFilter = (props) => (
  <Filter {...props}>
    <SelectInput source="provider" choices={providerChoices} />
    <SelectInput source="status" choices={statusChoices} alwaysOn />
    <TextInput source="id_ext" label="external id" />
    <TextInput source="user_id" label="user id" />
  </Filter>
);

export const PaymentsessionShow = (props) => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="id" />
      <TextField source="provider" />
      <TextField source="id_ext" />
      <ReferenceField source="user_id" reference="users">
        <TextField source="display_name" />
      </ReferenceField>
      <TextField source="info.id" />
      <TextField source="status" />
      <DateField source="createdAt" showTime />
      <DateField source="updatedAt" showTime />
      <JsonDataViewer source="info" />
    </SimpleShowLayout>
  </Show>
);

export const PaymentsessionList = (props) => (
  <List {...props} filters={<PaymentsessionFilter />}>
    <Datagrid rowClick="show" expand={<JsonDataViewer />}>
      <TextField source="status" />
      <TextField source="provider" />
      <TextField source="id_ext" />
      <ReferenceField source="user_id" reference="users">
        <TextField source="display_name" />
      </ReferenceField>
      <DateField source="createdAt" showTime />
    </Datagrid>
  </List>
);
