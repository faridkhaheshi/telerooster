import {
  ArrayField,
  BooleanField,
  BooleanInput,
  Create,
  Edit,
  List,
  Datagrid,
  DateField,
  TextField,
  Filter,
  ReferenceField,
  ReferenceInput,
  SelectInput,
  SimpleForm,
  TextInput,
} from "react-admin";
import JsonDataViewer from "./json-data-viewer";

const TeamadminFilter = (props) => (
  <Filter {...props}>
    <TextInput source="team_id" alwaysOn label="team id" />
    <TextInput source="profile_id" alwaysOn label="profile id" />
    <BooleanInput source="active" />
    <ReferenceInput source="profile_id" reference="profiles">
      <SelectInput optionText="info.displayName" />
    </ReferenceInput>
    <ReferenceInput source="team_id" reference="teams">
      <SelectInput optionText="name" />
    </ReferenceInput>
  </Filter>
);

export const TeamadminCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <BooleanInput source="active" />
      <TextInput source="team_id" label="team id" />
      <TextInput source="profile_id" label="profile id" />
    </SimpleForm>
  </Create>
);

export const TeamadminEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <TextField source="id" />
      <BooleanInput source="active" />
      <ReferenceInput source="team_id" reference="teams">
        <TextInput optionText="id" />
      </ReferenceInput>
      <ReferenceInput source="profile_id" reference="profiles">
        <TextInput optionText="id" />
      </ReferenceInput>
    </SimpleForm>
  </Edit>
);

export const TeamadminList = (props) => (
  <List {...props} filters={<TeamadminFilter />}>
    <Datagrid rowClick="edit" expand={<JsonDataViewer />}>
      <DateField source="createdAt" showTime />
      <BooleanField source="active" />
      <ReferenceField source="team_id" reference="teams">
        <TextField source="name" />
      </ReferenceField>
      <ReferenceField source="profile_id" reference="profiles">
        <TextField source="info.displayName" />
      </ReferenceField>
    </Datagrid>
  </List>
);
