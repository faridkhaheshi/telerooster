import {
  Admin,
  Resource,
  ListGuesser,
  ShowGuesser,
  EditGuesser,
} from "react-admin";
import authProvider, { httpClient } from "./auth-provider";
import simpleRestProvider from "ra-data-simple-rest";
import { UserList, UserShow, UserEdit, UserCreate } from "./users";
import { PoolCreate, PoolList, PoolShow, PoolEdit } from "./pools";
import { ProfileList, ProfileShow } from "./profiles";
import {
  SubscriptionList,
  SubscriptionEdit,
  SubscriptionCreate,
} from "./subscriptions";
import { TeamList, TeamShow, TeamEdit } from "./teams";
import { TeamadminList, TeamadminEdit, TeamadminCreate } from "./teamadmins";
import { ChannelList, ChannelShow, ChannelEdit } from "./channels";
import { PipeList, PipeShow, PipeEdit, PipeCreate } from "./pipes";
import {
  ContentmailList,
  ContentmailShow,
  ContentmailEdit,
} from "./contentmails";
import { PaymentsessionList, PaymentsessionShow } from "./paymentsessions";
import { CustomerList, CustomerShow, CustomerEdit } from "./customers";
import { PlanList, PlanShow, PlanEdit } from "./plans";
import { TeamreportList, TeamreportShow, TeamreportEdit } from "./teamreports";
import {
  ChannelreportList,
  ChannelreportShow,
  ChannelreportEdit,
} from "./channelreports";
import { MessageList, MessageShow, MessageEdit } from "./messages";
import {
  JourneyeventList,
  JourneyeventShow,
  JourneyeventEdit,
} from "./journeyevents";

import { ParcelList, ParcelShow, ParcelEdit } from "./parcels";

function AdminPage() {
  return (
    <Admin
      dataProvider={simpleRestProvider("/papi", httpClient)}
      authProvider={authProvider}
    >
      <Resource
        name="users"
        list={UserList}
        show={UserShow}
        edit={UserEdit}
        create={UserCreate}
      />
      <Resource
        name="pools"
        list={PoolList}
        show={PoolShow}
        edit={PoolEdit}
        create={PoolCreate}
      />
      <Resource
        name="messages"
        list={MessageList}
        show={MessageShow}
        edit={MessageEdit}
      />

      <Resource name="profiles" list={ProfileList} show={ProfileShow} />
      <Resource
        name="subscriptions"
        list={SubscriptionList}
        edit={SubscriptionEdit}
        create={SubscriptionCreate}
      />
      <Resource name="teams" list={TeamList} show={TeamShow} edit={TeamEdit} />
      <Resource
        name="teamadmins"
        list={TeamadminList}
        edit={TeamadminEdit}
        create={TeamadminCreate}
      />
      <Resource
        name="channels"
        list={ChannelList}
        show={ChannelShow}
        edit={ChannelEdit}
      />
      <Resource
        name="pipes"
        list={PipeList}
        show={PipeShow}
        edit={PipeEdit}
        create={PipeCreate}
      />
      <Resource
        name="contentmails"
        list={ContentmailList}
        show={ContentmailShow}
        edit={ContentmailEdit}
      />
      <Resource
        name="paymentsessions"
        list={PaymentsessionList}
        show={PaymentsessionShow}
      />
      <Resource
        name="customers"
        list={CustomerList}
        show={CustomerShow}
        edit={CustomerEdit}
      />
      <Resource name="plans" list={PlanList} show={PlanShow} edit={PlanEdit} />
      <Resource
        name="teamreports"
        list={TeamreportList}
        show={TeamreportShow}
        edit={TeamreportEdit}
      />
      <Resource
        name="channelreports"
        list={ChannelreportList}
        show={ChannelreportShow}
        edit={ChannelreportEdit}
      />
      <Resource
        name="journeyevents"
        list={JourneyeventList}
        show={JourneyeventShow}
        edit={JourneyeventEdit}
      />
      <Resource
        name="parcels"
        list={ParcelList}
        show={ParcelShow}
        edit={ParcelEdit}
      />
    </Admin>
  );
}

export default AdminPage;
