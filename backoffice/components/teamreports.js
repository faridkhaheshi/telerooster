import {
  ArrayField,
  SingleFieldList,
  ChipField,
  BooleanField,
  BooleanInput,
  Create,
  Edit,
  List,
  NumberField,
  Datagrid,
  DateField,
  DateInput,
  TextField,
  EmailField,
  NumberInput,
  Filter,
  ReferenceField,
  ReferenceInput,
  ReferenceManyField,
  Show,
  SelectInput,
  SimpleForm,
  SimpleShowLayout,
  TextInput,
} from "react-admin";
import JsonDataViewer from "./json-data-viewer";

const TeamReportFilter = (props) => (
  <Filter {...props}>
    <TextInput source="team_id" label="team id" alwaysOn />
  </Filter>
);

export const TeamreportEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <TextField source="id" />
      <TextField source="team_id" />
      <ReferenceField source="team_id" reference="teams">
        <TextField source="name" />
      </ReferenceField>
      <JsonDataViewer source="info" />
      <NumberInput source="info.memberCount" />
      <DateField source="createdAt" showTime />
      <DateField source="updatedAt" showTime />
    </SimpleForm>
  </Edit>
);

export const TeamreportShow = (props) => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="id" />
      <ReferenceField source="team_id" reference="teams">
        <TextField source="name" />
      </ReferenceField>
      <JsonDataViewer source="info" />
      <DateField source="createdAt" showTime />
      <DateField source="updatedAt" showTime />
    </SimpleShowLayout>
  </Show>
);

export const TeamreportList = (props) => (
  <List {...props} filters={<TeamReportFilter />}>
    <Datagrid rowClick="show" expand={<JsonDataViewer />}>
      <TextField source="id" />
      <ReferenceField source="team_id" reference="teams">
        <TextField source="name" />
      </ReferenceField>
      <NumberField source="info.memberCount" />
      <DateField source="createdAt" showTime />
    </Datagrid>
  </List>
);
