import {
  BooleanField,
  BooleanInput,
  Create,
  Edit,
  List,
  NumberInput,
  Datagrid,
  DateField,
  DateInput,
  TextField,
  Filter,
  ReferenceField,
  ReferenceInput,
  Show,
  SelectInput,
  SimpleForm,
  SimpleShowLayout,
  TextInput,
} from "react-admin";
import JsonDataViewer from "./json-data-viewer";

const typeChoices = [
  { id: "pool", name: "pool" },
  { id: "channel", name: "channel" },
];

const ContentmailFilter = (props) => (
  <Filter {...props}>
    <TextInput label="Search" source="q" alwaysOn />
    <BooleanInput source="active" alwaysOn />
    <SelectInput source="type" choices={typeChoices} />
  </Filter>
);

export const ContentmailEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <TextField source="id" />
      <TextInput source="address" />
      <TextField source="type" />
      <ReferenceField source="pool_id" reference="pools">
        <TextField source="title" />
      </ReferenceField>
      <ReferenceField source="channel_id" reference="channels">
        <TextField source="name" />
      </ReferenceField>
      <BooleanInput source="active" />
      <DateField source="createdAt" showTime />
      <DateField source="updatedAt" showTime />
    </SimpleForm>
  </Edit>
);

export const ContentmailShow = (props) => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="id" />
      <TextField source="address" />
      <TextField source="type" />
      <ReferenceField source="pool_id" reference="pools">
        <TextField source="title" />
      </ReferenceField>
      <ReferenceField source="channel_id" reference="channels">
        <TextField source="name" />
      </ReferenceField>
      <BooleanField source="active" />
      <DateField source="createdAt" showTime />
      <DateField source="updatedAt" showTime />
    </SimpleShowLayout>
  </Show>
);

export const ContentmailList = (props) => (
  <List {...props} filters={<ContentmailFilter />}>
    <Datagrid rowClick="show" expand={<JsonDataViewer />}>
      <TextField source="address" />
      <TextField source="type" />
      <ReferenceField source="pool_id" reference="pools">
        <TextField source="title" />
      </ReferenceField>
      <ReferenceField source="channel_id" reference="channels">
        <TextField source="name" />
      </ReferenceField>
      <BooleanField source="active" />
      <DateField source="createdAt" showTime />
    </Datagrid>
  </List>
);
