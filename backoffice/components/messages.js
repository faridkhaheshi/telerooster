import {
  ArrayField,
  BooleanField,
  BooleanInput,
  Create,
  Edit,
  List,
  Datagrid,
  DateField,
  DateInput,
  TextField,
  EmailField,
  Filter,
  ImageField,
  ReferenceManyField,
  ReferenceField,
  ReferenceInput,
  ArrayInput,
  Show,
  SelectInput,
  SimpleForm,
  SimpleFormIterator,
  SimpleShowLayout,
  TextInput,
  ImageInput,
} from "react-admin";
import JsonDataViewer from "./json-data-viewer";

const MessageFilter = (props) => (
  <Filter {...props}>
    <TextInput source="pool_id" />
    <BooleanInput source="active" alwaysOn />
  </Filter>
);

export const MessageEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <TextField source="id" />
      <TextField source="pool_id" label="Pool id" />
      <ReferenceField source="pool_id" reference="pools">
        <TextField source="title" />
      </ReferenceField>
      <ArrayInput source="blocks">
        <SimpleFormIterator>
          <TextInput source="type" />
          <TextInput source="content.markdown" />
          <TextInput source="content.alt" />
          <TextInput source="content.template" />
        </SimpleFormIterator>
      </ArrayInput>
      <BooleanInput source="active" />
      <DateField source="createdAt" showTime />
      <DateField source="updatedAt" showTime />
    </SimpleForm>
  </Edit>
);

export const MessageShow = (props) => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="id" />
      <TextField source="pool_id" label="Pool id" />
      <ReferenceField source="pool_id" reference="pools">
        <TextField source="title" />
      </ReferenceField>
      <TextField source="title" />
      <TextField source="text" />
      <ArrayField source="blocks">
        <Datagrid expand={<JsonDataViewer />}>
          <TextField source="type" />
          <TextField source="content.markdown" />
          <TextField source="content.alt" />
          <TextField source="content.template" />
          <ImageField source="content.image.src" />
        </Datagrid>
      </ArrayField>
      <BooleanField source="active" />
      <DateField source="createdAt" showTime />
      <DateField source="updatedAt" showTime />
    </SimpleShowLayout>
  </Show>
);

export const MessageList = (props) => (
  <List {...props} filters={<MessageFilter />}>
    <Datagrid rowClick="show" expand={<JsonDataViewer />}>
      <TextField source="pool_id" />
      <TextField source="title" />
      <ReferenceField source="pool_id" reference="pools">
        <TextField source="title" />
      </ReferenceField>
      <BooleanField source="active" />
      <DateField source="createdAt" showTime />
    </Datagrid>
  </List>
);
