import {
  BooleanField,
  BooleanInput,
  Create,
  Edit,
  List,
  Datagrid,
  DateField,
  DateInput,
  TextField,
  EmailField,
  Filter,
  ReferenceManyField,
  ReferenceField,
  Show,
  SelectInput,
  SimpleForm,
  SimpleShowLayout,
  TextInput,
} from "react-admin";
import JsonDataViewer from "./json-data-viewer";
import UnixDateField from "./UnixDateField";

const userRoleChoices = [
  { id: "normal", name: "normal" },
  { id: "admin", name: "admin" },
  { id: "superadmin", name: "superadmin" },
];

const UserFilter = (props) => (
  <Filter {...props}>
    <TextInput label="Search" source="q" alwaysOn />
    <BooleanInput source="active" />
    <SelectInput source="role" choices={userRoleChoices} />
  </Filter>
);

export const UserShow = (props) => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="id" />
      <TextField source="role" />
      <TextField source="first_name" />
      <TextField source="last_name" />
      <TextField source="display_name" />
      <EmailField source="email" />
      <ReferenceManyField
        label="Subscriptions"
        reference="subscriptions"
        target="user_id"
      >
        <Datagrid expand={<JsonDataViewer />}>
          <ReferenceField label="pool title" source="pool_id" reference="pools">
            <TextField source="title" />
          </ReferenceField>
          <ReferenceField label="creator" source="pool_id" reference="pools">
            <ReferenceField source="creator_id" reference="users">
              <TextField source="display_name" />
            </ReferenceField>
          </ReferenceField>
          <ReferenceField label="featured" source="pool_id" reference="pools">
            <BooleanField source="featured" />
          </ReferenceField>
          <DateField source="createdAt" />
          <BooleanField source="active" />
        </Datagrid>
      </ReferenceManyField>
      <ReferenceManyField
        label="User profiles"
        reference="profiles"
        target="user_id"
      >
        <Datagrid expand={<JsonDataViewer />}>
          <ReferenceField label="id_ext" source="id" reference="profiles">
            <TextField source="id_ext" />
          </ReferenceField>
          <TextField source="provider" />
        </Datagrid>
      </ReferenceManyField>

      <ReferenceManyField
        label="User customers"
        reference="customers"
        target="user_id"
      >
        <Datagrid expand={<JsonDataViewer />}>
          <BooleanField source="active" />
          <TextField source="id_ext" />
          <TextField source="provider" />
        </Datagrid>
      </ReferenceManyField>

      <ReferenceManyField label="User plans" reference="plans" target="user_id">
        <Datagrid expand={<JsonDataViewer />}>
          <TextField source="status" />
          <TextField source="type" />
          <UnixDateField source="expires_at" />
          <TextField source="product_id_ext" />
          <DateField source="createdAt" showTime />
        </Datagrid>
      </ReferenceManyField>
      <BooleanField source="active" />
      <DateField source="createdAt" />
      <DateField source="updatedAt" />
    </SimpleShowLayout>
  </Show>
);

export const UserEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <TextInput source="id" />
      <SelectInput
        source="role"
        choices={userRoleChoices}
        defaultValue={userRoleChoices[0].id}
      />
      <TextInput source="first_name" />
      <TextInput source="last_name" />
      <TextInput source="display_name" />
      <TextInput source="email" />
      <BooleanInput source="active" />
    </SimpleForm>
  </Edit>
);

export const UserCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <SelectInput
        source="role"
        choices={userRoleChoices}
        defaultValue={userRoleChoices[0].id}
      />
      <TextInput source="first_name" />
      <TextInput source="last_name" />
      <TextInput source="display_name" />
      <TextInput source="email" />
      <BooleanInput source="active" />
    </SimpleForm>
  </Create>
);

export const UserList = (props) => (
  <List {...props} filters={<UserFilter />}>
    <Datagrid rowClick="show" expand={<JsonDataViewer />}>
      <DateField source="createdAt" showTime />
      <TextField source="role" />
      <TextField source="first_name" />
      <TextField source="last_name" />
      <TextField source="display_name" />
      <EmailField source="email" />
      <BooleanField source="active" />
    </Datagrid>
  </List>
);
