import {
  BooleanField,
  BooleanInput,
  Create,
  Edit,
  List,
  NumberInput,
  Datagrid,
  DateField,
  DateInput,
  TextField,
  Filter,
  ReferenceField,
  ReferenceInput,
  Show,
  SelectInput,
  SimpleForm,
  SimpleShowLayout,
  TextInput,
} from "react-admin";
import JsonDataViewer from "./json-data-viewer";

const platformChoices = [
  { id: "microsoft", name: "microsoft" },
  { id: "slack", name: "slack" },
];

const ChannelFilter = (props) => (
  <Filter {...props}>
    <TextInput label="Search" source="q" alwaysOn />
    <TextInput label="team id" source="team_id" alwaysOn />
    <SelectInput source="platform" choices={platformChoices} />
    <ReferenceInput source="team_id" reference="teams">
      <SelectInput optionText="name" />
    </ReferenceInput>
    <BooleanInput source="active" />
  </Filter>
);

export const ChannelEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <TextField source="id" />
      <TextField source="platform" />
      <TextField source="id_ext" />
      <TextField source="name" />
      <ReferenceField source="team_id" reference="teams">
        <TextField source="name" />
      </ReferenceField>
      <TextField source="type" />
      <TextInput source="info.email" />
      <BooleanField source="private" />
      <TextField source="web_url" />
      <BooleanInput source="active" />
      <DateField source="createdAt" showTime />
      <DateField source="updatedAt" showTime />
    </SimpleForm>
  </Edit>
);

export const ChannelShow = (props) => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="id" />
      <TextField source="name" />
      <TextField source="platform" />
      <TextField source="id_ext" />
      <ReferenceField source="team_id" reference="teams">
        <TextField source="name" />
      </ReferenceField>
      <TextField source="type" />
      <BooleanField source="private" />
      <TextField source="web_url" />
      <BooleanField source="active" />
      <DateField source="createdAt" showTime />
      <DateField source="updatedAt" showTime />
    </SimpleShowLayout>
  </Show>
);

export const ChannelList = (props) => (
  <List {...props} filters={<ChannelFilter />}>
    <Datagrid rowClick="show" expand={<JsonDataViewer />}>
      <TextField source="name" />
      <TextField source="platform" />
      <TextField source="id_ext" />
      <ReferenceField source="team_id" reference="teams">
        <TextField source="name" />
      </ReferenceField>
      <TextField source="type" />
      <BooleanField source="private" />
      <BooleanField source="active" />
      <DateField source="createdAt" showTime />
    </Datagrid>
  </List>
);
