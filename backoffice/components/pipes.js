import {
  BooleanField,
  BooleanInput,
  Create,
  Edit,
  List,
  Datagrid,
  DateField,
  DateInput,
  TextField,
  EmailField,
  Filter,
  ReferenceField,
  ReferenceInput,
  Show,
  SelectInput,
  SimpleForm,
  SimpleShowLayout,
  TextInput,
} from "react-admin";
import JsonDataViewer from "./json-data-viewer";

const PipeFilter = (props) => (
  <Filter {...props}>
    <BooleanInput source="active" />
    <TextInput source="subscription_id" label="subscription id" alwaysOn />
    <TextInput source="channel_id" label="channel id" alwaysOn />
  </Filter>
);

export const PipeCreate = (props) => (
  <Create {...props}>
    <SimpleForm>
      <BooleanInput source="active" />
      <TextInput source="subscription_id" label="subscription id" />
      <TextInput source="channel_id" label="channel id" />
    </SimpleForm>
  </Create>
);

export const PipeEdit = (props) => (
  <Edit {...props}>
    <SimpleForm>
      <TextField source="id" />
      <BooleanInput source="active" />
      <TextInput source="subscription_id" />
      <TextInput source="channel_id" />
      <DateField source="createdAt" />
      <DateField source="updatedAt" />
    </SimpleForm>
  </Edit>
);

export const PipeShow = (props) => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="id" />
      <BooleanField source="active" />
      <ReferenceField source="subscription_id" reference="subscriptions">
        <TextField source="id" />
      </ReferenceField>
      <ReferenceField
        label="pool"
        source="subscription_id"
        reference="subscriptions"
      >
        <ReferenceField source="pool_id" reference="pools">
          <TextField source="title" />
        </ReferenceField>
      </ReferenceField>
      <ReferenceField
        label="user"
        source="subscription_id"
        reference="subscriptions"
      >
        <ReferenceField source="user_id" reference="users">
          <TextField source="display_name" />
        </ReferenceField>
      </ReferenceField>
      <ReferenceField source="channel_id" reference="channels">
        <TextField source="id" />
      </ReferenceField>
      <ReferenceField source="channel_id" reference="channels">
        <TextField source="name" />
      </ReferenceField>
      <DateField source="createdAt" showTime />
      <DateField source="updatedAt" showTime />
    </SimpleShowLayout>
  </Show>
);

export const PipeList = (props) => (
  <List {...props} filters={<PipeFilter />}>
    <Datagrid rowClick="show" expand={<JsonDataViewer />}>
      <BooleanField source="active" />
      <ReferenceField source="subscription_id" reference="subscriptions">
        <ReferenceField source="pool_id" reference="pools">
          <TextField source="title" />
        </ReferenceField>
      </ReferenceField>
      <ReferenceField
        label="user"
        source="subscription_id"
        reference="subscriptions"
      >
        <ReferenceField source="user_id" reference="users">
          <TextField source="display_name" />
        </ReferenceField>
      </ReferenceField>
      <ReferenceField source="channel_id" reference="channels">
        <TextField source="name" />
      </ReferenceField>
      <ReferenceField label="team" source="channel_id" reference="channels">
        <ReferenceField source="team_id" reference="teams">
          <TextField source="name" />
        </ReferenceField>
      </ReferenceField>
      <DateField source="createdAt" showTime />
    </Datagrid>
  </List>
);
