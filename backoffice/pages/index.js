import * as React from "react";
import dynamic from "next/dynamic";

const AdminPageClientOnly = dynamic(() => import("../components/AdminPage"), {
  ssr: false,
});

function Home() {
  return <AdminPageClientOnly />;
}

export default Home;
