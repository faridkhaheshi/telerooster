import { login } from "../../../services/auth/controllers";

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case "POST":
      return login(req, res);
    default:
      return res.status(405).send("The method is not supported");
  }
};

export default handleReqs;

export const config = {
  api: {
    externalResolver: true,
    bodyParser: false,
  },
};
