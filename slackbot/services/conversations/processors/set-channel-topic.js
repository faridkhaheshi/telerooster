const webClient = require("../../../bot/web-client");

const setChannelTopic = async ({ token, channelId, topic }) => {
  const { channel } = await webClient.conversations.setTopic({
    token,
    channel: channelId,
    topic,
  });
  return channel;
};

module.exports = setChannelTopic;
