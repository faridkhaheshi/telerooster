const webClient = require("../../../bot/web-client");

const setChannelPurpose = async ({ token, channelId, purpose }) => {
  const { channel } = await webClient.conversations.setPurpose({
    token,
    channel: channelId,
    purpose: purpose.slice(0, 240),
  });
  return channel;
};

module.exports = setChannelPurpose;
