const { NotFoundError, InternalServerError } = require("restify-errors");
const webClient = require("../../../bot/web-client");

const getChannelInfo = async ({ token, channelId }) => {
  try {
    const { channel } = await webClient.conversations.info({
      token,
      channel: channelId,
      include_num_members: true,
    });
    return channel;
  } catch (err) {
    const { code, data: { error } = {} } = err || {};
    if (error === "channel_not_found")
      throw new NotFoundError("channel not found");
    throw new InternalServerError("Unknown error from slack api");
  }
};

module.exports = getChannelInfo;
