const webClient = require("../../../bot/web-client");

const getUserConversationRef = async ({ token, userId }) => {
  const {
    ok,
    already_open,
    channel: { id },
  } = await webClient.conversations.open({ token, users: userId });
  return { channelId: id };
};

module.exports = getUserConversationRef;
