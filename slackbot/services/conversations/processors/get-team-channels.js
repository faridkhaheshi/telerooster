const webClient = require("../../../bot/web-client");

const LIMIT = 100;

const getTeamChannels = async ({ token, teamId, excludeArchived = true }) => {
  const channels = [];
  let cursor = undefined;
  do {
    const {
      ok,
      channels: newChannels,
      response_metadata: { next_cursor },
    } = await webClient.conversations.list({
      token,
      limit: LIMIT,
      exclude_archived: excludeArchived,
      team_id: teamId,
      cursor,
    });
    channels.push(...newChannels);
    cursor = next_cursor;
  } while (cursor.length > 0);

  return channels;
};

module.exports = getTeamChannels;
