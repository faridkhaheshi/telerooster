const getTeamChannels = require("./get-team-channels");

const findTeamGeneralChannel = async ({ token, teamId }) => {
  const channels = await getTeamChannels({ token, teamId });
  const generalChannels = channels.filter((c) => c.is_general);
  if (generalChannels.length === 1) return generalChannels[0];
  return null;
};

module.exports = findTeamGeneralChannel;
