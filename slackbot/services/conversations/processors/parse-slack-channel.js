const generateDeepLinkToChannel = require("./../utils/generate-deep-link-to-channel");

const parseSlackChannel = ({
  id,
  is_private,
  name_normalized,
  ...otherInfo
}) => ({
  platform: "slack",
  id_ext: id,
  info: { ...otherInfo, memberCount: otherInfo.num_members },
  private: is_private,
  name: name_normalized,
  conversation_ref: { channelId: id },
  web_url: generateDeepLinkToChannel(id),
});

module.exports = parseSlackChannel;
