const webClient = require("../../../bot/web-client");

const inviteToChannel = async ({ token, channelId, userId }) => {
  try {
    const result = await webClient.conversations.invite({
      token,
      channel: channelId,
      users: userId,
    });

    return result;
  } catch (err) {
    const {
      data: { error },
    } = err;
    if (error === "already_in_channel") {
      return { ok: true, alreadyInChannel: true };
    }
    throw err;
  }
};

module.exports = inviteToChannel;
