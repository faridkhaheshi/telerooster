const getTeamChannels = require("./get-team-channels");
const webClient = require("../../../bot/web-client");
const normalizeSlackChannelName = require("../utils/normalize-slack-channel-name");

const NUMBER_OF_RETRIES = 5;
const RANDOM_MAX = 10000;

const createChannel = async ({ token, name, teamId, isPrivate }) => {
  const teamChannels = await getTeamChannels({
    token,
    teamId,
    excludeArchived: false,
  });
  const excludeNames = teamChannels.map((c) => c.name_normalized);
  const baseName = normalizeSlackChannelName(name);
  let remainingTries = NUMBER_OF_RETRIES;
  let nameError = false;
  let newChannelName;
  do {
    try {
      newChannelName = suggestChannelName(baseName, excludeNames);
      const { channel } = await webClient.conversations.create({
        token,
        name: newChannelName,
        is_private: isPrivate,
        team_id: teamId,
      });
      return channel;
    } catch (err) {
      const {
        data: { error },
      } = err;
      nameError = error === "name_taken";
      if (nameError) {
        excludeNames.push(newChannelName);
      }
    }
    remainingTries--;
  } while (remainingTries > 0 && nameError);
  return;
};

module.exports = createChannel;

function suggestChannelName(baseName, excludeNames = []) {
  if (excludeNames.indexOf(baseName) === -1) return baseName;
  let newName;
  do {
    newName = `${baseName}-${Math.floor(Math.random() * RANDOM_MAX)}`;
  } while (excludeNames.indexOf(newName) > -1);
  return newName;
}
