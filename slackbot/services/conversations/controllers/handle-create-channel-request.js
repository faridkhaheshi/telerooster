const { BadRequestError } = require("restify-errors");
const {
  inviteToChannel,
  createChannel,
  setChannelPurpose,
  setChannelTopic,
  parseSlackChannel,
  getChannelInfo,
} = require("../processors");

const handleCreateChannelRequest = async (req, res) => {
  try {
    const {
      body: {
        installation: {
          user: { id: userId },
          bot: { token },
        },
        teamId,
        channelData,
      },
    } = req;

    const channel = await createChannelAndUpdateInfo({
      token,
      channelData,
      teamId,
    });
    await inviteToChannel({ token, channelId: channel.id, userId });
    const updatedChannelInfo = await getChannelInfo({
      token,
      channelId: channel.id,
    });

    return res.json(parseSlackChannel(updatedChannelInfo));
  } catch (err) {
    console.error("error when creating slack channel...");
    console.log(err);
    res
      .status(err.statusCode || err.status || 500)
      .send(err.message || "something went wrong...");
  }
};

module.exports = handleCreateChannelRequest;

async function createChannelAndUpdateInfo({ token, channelData, teamId }) {
  const { name, description, topic } = channelData;
  if (!name) throw new BadRequestError("Channel name is required");
  let newChannel;
  newChannel = await createChannel({ token, name, teamId });
  if (description)
    newChannel = await setChannelPurpose({
      token,
      channelId: newChannel.id,
      purpose: description,
    });
  if (topic)
    newChannel = await setChannelTopic({
      token,
      channelId: newChannel.id,
      topic,
    });
  return newChannel;
}
