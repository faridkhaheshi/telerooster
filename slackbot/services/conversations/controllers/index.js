module.exports = {
  handleCreateChannelRequest: require("./handle-create-channel-request"),
  handleGetChannelinfoRequest: require("./handle-get-channel-info-request"),
};
