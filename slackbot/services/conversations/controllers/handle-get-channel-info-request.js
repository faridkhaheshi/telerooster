const { getChannelInfo, parseSlackChannel } = require("../processors");

const handleGetChannelInfoRequest = async (req, res) => {
  try {
    const channel = await getChannelInfo({
      token: req.query.token,
      channelId: req.params.id,
    });
    return res.json(parseSlackChannel(channel));
  } catch (err) {
    res
      .status(err.statusCode || err.status || 500)
      .send(err.message || "something went wrong...");
  }
};

module.exports = handleGetChannelInfoRequest;
