module.exports = {
  normalizeSlackChannelName: require("./normalize-slack-channel-name"),
  generateDeepLinkToChannel: require("./generate-deep-link-to-channel"),
};
