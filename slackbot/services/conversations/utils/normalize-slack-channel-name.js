const normalizeSlackChannelName = (baseName) =>
  baseName
    .replace(/[^\w-_]+/g, " ")
    .replace(/\s\s+/g, " ")
    .trim()
    .replace(/\s/g, "_")
    .toLowerCase();

module.exports = normalizeSlackChannelName;
