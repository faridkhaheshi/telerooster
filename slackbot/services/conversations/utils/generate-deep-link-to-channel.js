const generateDeepLinkToChannel = (channelId) =>
  `https://slack.com/app_redirect?channel=${channelId}`;

module.exports = generateDeepLinkToChannel;
