const MAX_LENGTH = 3000;

const buildTextSlackBlock = (markdown) => ({
  type: "section",
  text: {
    type: "mrkdwn",
    text: markdown.slice(0, MAX_LENGTH),
  },
});

module.exports = buildTextSlackBlock;
