const querystring = require("querystring");

const generatePosterImageUrl = ({ template, payload }) =>
  `${process.env.PAINTER_BASE_URL}/image?${querystring.stringify({
    template,
    ...payload,
  })}`;

module.exports = generatePosterImageUrl;
