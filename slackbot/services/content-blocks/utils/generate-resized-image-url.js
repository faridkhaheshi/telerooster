const querystring = require("querystring");

const generateResizedImageUrl = ({ src, width, height }) => {
  const query = { url: src };
  if (
    width == 0 ||
    height == 0 ||
    parseInt(width || 0) > 100 ||
    parseInt(height || 0) > 100
  )
    return src;
  if (width) query.w = width;
  if (height) query.h = height;
  return `${process.env.PAINTER_BASE_URL}/resize?${querystring.stringify(
    query
  )}`;
};

module.exports = generateResizedImageUrl;
