const convertHtmlToSlackMarkdown = require("../../../utils/convert-html-to-slack-markdown");
const convertMarkdownToSlackMarkdown = require("../../parser/processors/convert-markdown-to-slack-markdown");

const buildSmallBlock = ({
  source: { element } = {},
  content: { markdown } = {},
}) => {
  let blockText = element
    ? convertHtmlToSlackMarkdown(element)
    : convertMarkdownToSlackMarkdown(markdown);
  if (blockText.length === 0) return undefined;

  return {
    type: "context",
    elements: [
      {
        type: "mrkdwn",
        text: blockText.replace(/\n/g, ""),
      },
    ],
  };
};

module.exports = buildSmallBlock;
