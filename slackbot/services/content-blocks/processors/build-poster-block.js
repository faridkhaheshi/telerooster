const generatePosterImageUrl = require("../utils/generate-poster-image-url");

const buildPosterBlock = ({
  content: { template, payload, alt = "poster" },
}) => {
  if (!template) return undefined;
  return {
    type: "image",
    image_url: generatePosterImageUrl({ payload, template }),
    alt_text: alt,
  };
};

module.exports = buildPosterBlock;
