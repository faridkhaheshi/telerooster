const convertHtmlToSlackMarkdown = require("../../../utils/convert-html-to-slack-markdown");
const convertMarkdownToSlackMarkdown = require("../../parser/processors/convert-markdown-to-slack-markdown");

const buildIconBlock = ({
  content: { markdown, image = {}, link = {} },
  source: { element },
}) => {
  if (markdown) {
    const blockMarkdown = element
      ? convertHtmlToSlackMarkdown(element)
      : convertMarkdownToSlackMarkdown(markdown);
    if (blockMarkdown.length === 0) return undefined;
    return {
      type: "section",
      text: {
        type: "mrkdwn",
        text: blockMarkdown,
      },
    };
  }

  if (image) {
    if (!image.src && !link.href) return undefined;
    const blocks = [];
    if (image.src)
      blocks.push({
        type: "image",
        image_url: image.src,
        alt_text: image.alt || "image",
      });
    if (link.href)
      blocks.push({
        type: "context",
        elements: [
          {
            type: "mrkdwn",
            text: `<${link.href}|${image.alt || "image link"}>`,
          },
        ],
      });
    return blocks;
  }
};

module.exports = buildIconBlock;
