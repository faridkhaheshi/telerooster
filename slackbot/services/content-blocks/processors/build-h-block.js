const convertHtmlToSlackMarkdown = require("../../../utils/convert-html-to-slack-markdown");
const convertMarkdownToSlackMarkdown = require("../../parser/processors/convert-markdown-to-slack-markdown");
const buildTextSlackBlock = require("../utils/build-text-slack-block");

const buildHBlock = ({ source: { element } = {}, content: { markdown } }) => {
  let blockText = element
    ? convertHtmlToSlackMarkdown(element)
    : `*${convertMarkdownToSlackMarkdown(markdown)}*`;
  blockText = blockText.replace(/\n/g, "").replace(/(\*+)/g, "*");
  if (blockText.length === 0) return undefined;
  return buildTextSlackBlock(blockText);
};

module.exports = buildHBlock;
