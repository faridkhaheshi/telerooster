const buildCustomBlock = require("./build-custom-block");
const buildDividerBlock = require("./build-divider-block");
const buildEmptyBlock = require("./build-empty-block");
const buildHBlock = require("./build-h-block");
const buildHrBlock = require("./build-hr-block");
const buildIconBlock = require("./build-icon-block");
const buildImageBlock = require("./build-image-block");
const buildOlBlock = require("./build-ol-block");
const buildPBlock = require("./build-p-block");
const buildPosterBlock = require("./build-poster-block");
const buildSmallBlock = require("./build-small-block");
const buildUlBlock = require("./build-ul-block");
const buildButtonrowBlock = require("./built-buttonrow-block");

const findBuilderFunction = ({ type }) => {
  try {
    switch (type) {
      case "buttonrow":
        return buildButtonrowBlock;
      case "p":
        return buildPBlock;
      case "image":
        return buildImageBlock;
      case "divider":
        return buildDividerBlock;
      case "icon":
        return buildIconBlock;
      case "h":
        return buildHBlock;
      case "hr":
        return buildHrBlock;
      case "ul":
        return buildUlBlock;
      case "ol":
        return buildOlBlock;
      case "small":
        return buildSmallBlock;
      case "custom":
        return buildCustomBlock;
      case "poster":
        return buildPosterBlock;
      default:
        return buildEmptyBlock;
    }
  } catch (err) {
    return buildEmptyBlock;
  }
};

module.exports = findBuilderFunction;
