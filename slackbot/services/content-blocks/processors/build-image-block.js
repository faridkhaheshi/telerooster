const generateResizedImageUrl = require("../utils/generate-resized-image-url");

const buildImageBlock = ({
  content: {
    image: { src, alt, width, height },
    caption,
  },
  options = [],
}) => {
  if (!src) return undefined;

  const shouldResize = width || height ? true : false;

  const links = options.filter(({ tag, attrs }) => tag === "a");
  const blocks = [];
  blocks.push({
    type: "image",
    title: caption
      ? {
          type: "plain_text",
          text: caption,
          emoji: true,
        }
      : undefined,
    image_url: shouldResize
      ? generateResizedImageUrl({ src, width, height })
      : src,
    alt_text: alt || "image",
  });
  if (links.length > 0) {
    try {
      blocks.push({
        type: "context",
        elements: [
          {
            type: "mrkdwn",
            text: `<${links.pop().attrs.href}|${alt || "image link"}>`,
          },
        ],
      });
    } catch (err) {}
  }
  return blocks;
};

module.exports = buildImageBlock;
