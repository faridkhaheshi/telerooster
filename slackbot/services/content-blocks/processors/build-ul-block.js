const convertHtmlToSlackMarkdown = require("../../../utils/convert-html-to-slack-markdown");
const convertMarkdownToSlackMarkdown = require("../../parser/processors/convert-markdown-to-slack-markdown");
const buildTextSlackBlock = require("../utils/build-text-slack-block");

const MAX_ITEM_SIZE = 3000;

const buildUlBlock = ({
  source: { element } = {},
  content: { markdown } = {},
}) => {
  let blockText = element
    ? convertHtmlToSlackMarkdown(element)
    : convertMarkdownToSlackMarkdown(markdown);
  if (blockText.length === 0) return undefined;
  if (blockText.length > MAX_ITEM_SIZE)
    return blockText.split("\n").map((b) => buildTextSlackBlock(b));
  return buildTextSlackBlock(blockText);
};

module.exports = buildUlBlock;
