const buildEmptyBlock = require("./build-empty-block");
const findBuilderFunction = require("./find-builder-function");

const convertContentBlockToMessageBlock = (block) => {
  try {
    const builderFunction = findBuilderFunction(block);
    return builderFunction(block);
  } catch (err) {
    return buildEmptyBlock();
  }
};

module.exports = convertContentBlockToMessageBlock;
