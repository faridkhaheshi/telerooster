const buildEmptyBlock = (block) => ({
  type: "section",
  text: {
    type: "mrkdwn",
    text: " ",
  },
});

module.exports = buildEmptyBlock;
