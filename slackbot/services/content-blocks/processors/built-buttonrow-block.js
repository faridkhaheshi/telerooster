const convertMarkdownToSlackMarkdown = require("../../parser/processors/convert-markdown-to-slack-markdown");

const buildButtonrow = ({
  content: { markdown, buttonText, url, actionId, value },
}) => ({
  type: "section",
  text: {
    type: "mrkdwn",
    text: convertMarkdownToSlackMarkdown(markdown),
  },
  accessory: url
    ? {
        type: "button",
        text: {
          type: "plain_text",
          text: buttonText,
          emoji: true,
        },
        url,
        action_id: "open-link",
      }
    : value
    ? {
        type: "button",
        text: {
          type: "plain_text",
          text: buttonText,
          emoji: true,
        },
        action_id: actionId,
        value,
      }
    : {
        type: "button",
        text: {
          type: "plain_text",
          text: buttonText,
          emoji: true,
        },
        action_id: actionId,
      },
});

module.exports = buildButtonrow;
