module.exports = {
  isURL: require("./is-url"),
  wrap: require("./wrap"),
  slackify: require("./slackify"),
};
