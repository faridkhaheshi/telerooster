const { URL } = require("url");

const isURL = (string) => {
  try {
    return Boolean(new URL(string));
  } catch (error) {
    return false;
  }
};

module.exports = isURL;
