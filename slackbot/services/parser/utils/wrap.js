const wrap = (string, ...wrappers) =>
  [...wrappers, string, ...wrappers.reverse()].join("");

module.exports = wrap;
