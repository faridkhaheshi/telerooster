const convertMarkdownToHtml = require("./convert-markdown-to-html");
const convertHtmlToSlackMarkdown = require("../../../utils/convert-html-to-slack-markdown");

const convertMarkdownToSlackMarkdown = (markdown) => {
  const html = convertMarkdownToHtml(markdown);
  return convertHtmlToSlackMarkdown(html);
};

module.exports = convertMarkdownToSlackMarkdown;
