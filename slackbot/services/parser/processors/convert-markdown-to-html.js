const unified = require("unified");
const markdownParser = require("remark-parse");
const remark2rehype = require("remark-rehype");
const stringifyToHtml = require("rehype-stringify");

const convertMarkdownToHtml = (markdown) => {
  const html = unified()
    .use(markdownParser)
    .use(remark2rehype)
    .use(stringifyToHtml)
    .processSync(markdown);
  return String(html);
};

module.exports = convertMarkdownToHtml;
