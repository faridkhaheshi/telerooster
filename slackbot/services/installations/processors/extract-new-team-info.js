const {
  getTeamConversationRef,
  getUserConversationRef,
  getTeamChannels,
} = require("../../conversations/processors");
const { getUserInfo, getTeamMembers } = require("../../users/processors");
const { parseSlackProfile } = require("../../users/processors");
const parseSlackTeam = require("./parse-slack-team");
const parseSlackToken = require("./parse-slack-token");

const extractNewTeamInfo = async ({ installation, installOptions }) => {
  const {
    team,
    team: { id: teamId },
    user: { id: userId },
    bot: { token },
  } = installation;
  const teamConversationRef = await getTeamConversationRef({
    token,
    teamId,
  });
  const admin = await getUserInfo({ token, userId });
  const adminConversationRef = await getUserConversationRef({ token, userId });
  const members = await getTeamMembers({ token, teamId });
  const channels = await getTeamChannels({ token, teamId });

  const profileData = parseSlackProfile(
    { ...admin, conversationRef: adminConversationRef },
    "bot-install"
  );

  const teamData = parseSlackTeam({
    team,
    conversationRef: teamConversationRef,
    channels,
    members,
  });

  const tokenData = parseSlackToken(installation);

  return {
    profileData,
    teamData,
    tokenData,
    installOptions,
  };
};

module.exports = extractNewTeamInfo;
