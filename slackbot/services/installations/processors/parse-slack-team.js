const parseSlackTeam = ({
  team,
  conversationRef,
  channels = [],
  members = [],
}) => {
  const info = {
    channelCount: channels.length,
    memberCount: members.length,
  };
  if (channels) info.channels = channels;
  if (members) info.members = members;
  const teamData = {
    id_ext: team.id,
    platform: "slack",
    name: team.name,
    info,
  };
  if (conversationRef) teamData.conversation_ref = conversationRef;
  return teamData;
};

module.exports = parseSlackTeam;
