module.exports = {
  extractNewTeamInfo: require("./extract-new-team-info"),
  parseInstallOptions: require("./parse-install-options"),
};
