const parseSlackToken = (installation) => ({
  provider: "slack",
  info: installation,
});

module.exports = parseSlackToken;
