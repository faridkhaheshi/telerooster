const parseInstallOptions = (installOptions) => {
  try {
    const { metadata } = installOptions;
    if (metadata) return JSON.parse(metadata);
    return {};
  } catch (error) {
    return {};
  }
};

module.exports = parseInstallOptions;
