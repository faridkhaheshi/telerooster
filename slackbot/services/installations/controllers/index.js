module.exports = {
  handleInstallationFailure: require("./handle-installation-failure"),
  handleInstallationSuccess: require("./handle-installation-success"),
  fetchInstallation: require("./fetch-installation"),
};
