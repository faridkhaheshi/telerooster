const handleInstallationFailure = async (error, installOptions, req, res) => {
  res.writeHead(302, { Location: `${process.env.WEBSITE_BASE_URL}` });
  return res.end();
};

module.exports = handleInstallationFailure;
