const registerNewSlackTeam = require("../../../adapters/website/register-new-slack-team");
const { extractNewTeamInfo, parseInstallOptions } = require("../processors");

const handleInstallationSuccess = async (
  installation,
  installOptions,
  req,
  res
) => {
  try {
    const teamInfo = await extractNewTeamInfo({ installation, installOptions });
    const { token } = await registerNewSlackTeam(teamInfo, req.headers.cookie);
    const { ref } = parseInstallOptions(installOptions);
    const redirectUrl = ref
      ? `${
          process.env.WEBSITE_BASE_URL
        }/auth?token=${token}&ref=${encodeURIComponent(ref)}`
      : `${process.env.WEBSITE_BASE_URL}/auth?token=${token}`;
    res.writeHead(302, { Location: redirectUrl });
    return res.end();
  } catch (err) {
    res
      .status(err.statusCode || err.status || 500)
      .send(err.message || "something went wrong...");
  }
};

module.exports = handleInstallationSuccess;
