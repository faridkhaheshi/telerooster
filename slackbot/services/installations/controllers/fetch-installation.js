const querystring = require("querystring");
const sendInternalApiRequest = require("../../../adapters/api/send-internal-api-request");

const apiBaseURL = process.env.WEBSITE_API_BASE_URL;

const fetchInstallation = async ({ teamId, ...query }) => {
  try {
    const installation = await sendInternalApiRequest(
      `${apiBaseURL}/private/slack/teams/${teamId}?${querystring.stringify(
        query
      )}`
    );
    return installation;
  } catch (err) {
    return undefined;
  }
};

module.exports = fetchInstallation;
