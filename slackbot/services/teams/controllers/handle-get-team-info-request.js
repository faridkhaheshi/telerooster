const { withErrorHandling } = require("../../errors/processors");
const { getTeamMembers } = require("../../users/processors");

const handleGetTeamInfoRequest = async (req, res) => {
  const members = await getTeamMembers({
    token: req.query.token,
    teamId: req.params.id,
  });
  return res.json({
    id_ext: req.params.id,
    platform: "slack",
    info: { memberCount: members.length },
  });
};

module.exports = withErrorHandling(handleGetTeamInfoRequest);
