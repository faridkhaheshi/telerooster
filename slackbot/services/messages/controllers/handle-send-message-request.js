const { withErrorHandling } = require("../../errors/processors");
const { buildSlackMessage } = require("../processors");
const sendMessage = require("../processors/send-message");

const handleSendMessageRequest = async (req, res) => {
  const {
    body: {
      message,
      conversation,
      installation: {
        bot: { token },
      },
    },
  } = req;
  const sentMessage = await sendMessage({
    token,
    message: buildSlackMessage(message),
    conversation,
  });
  return res.json({ done: true, message: sentMessage });
};

module.exports = withErrorHandling(handleSendMessageRequest);
