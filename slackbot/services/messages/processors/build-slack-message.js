const {
  convertContentBlockToMessageBlock,
} = require("../../content-blocks/processors");

const MAX_BLOCKS = 50;

const buildSlackMessage = (message) => {
  const finalMessage =
    typeof message === "string" ? { text: message } : message;
  const { text = "new message...", blocks = [] } = finalMessage;
  return {
    text,
    blocks: blocks
      .reduce((acc, blk) => {
        const messageBlock = convertContentBlockToMessageBlock(blk);
        return Array.isArray(messageBlock)
          ? [...acc, ...messageBlock]
          : [...acc, messageBlock];
      }, [])
      .filter((blk) => blk)
      .slice(0, MAX_BLOCKS),
  };
};

module.exports = buildSlackMessage;
