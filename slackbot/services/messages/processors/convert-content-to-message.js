const {
  convertContentBlockToMessageBlock,
} = require("../../content-blocks/processors");

const MAX_BLOCKS = 50;

const convertContentToMessage = (content) => {
  const { text, blocks } = content;
  return {
    text,
    blocks: blocks
      .reduce((acc, blk) => {
        const messageBlock = convertContentBlockToMessageBlock(blk);
        return Array.isArray(messageBlock)
          ? [...acc, ...messageBlock]
          : [...acc, messageBlock];
      }, [])
      .filter((blk) => blk)
      .slice(0, MAX_BLOCKS),
  };
};

module.exports = convertContentToMessage;
