const webClient = require("../../../bot/web-client");

const sendMessage = async ({ token, message, conversation: { channelId } }) => {
  const { message: theMessage } = await webClient.chat.postMessage({
    token,
    channel: channelId,
    ...message,
  });
  return theMessage;
};

module.exports = sendMessage;
