const withErrorHandlingForBotEvents = (
  handler,
  title = "slack bot event"
) => async (props) => {
  try {
    await handler(props);
  } catch (err) {
    console.log(`-------------------[Error in ${title}]:`);
    console.error(err);
  }
};

module.exports = withErrorHandlingForBotEvents;
