const { scopes, installer } = require("../../../bot");

const getAuthUrl = (metadata) =>
  metadata
    ? installer.generateInstallUrl({ scopes, metadata })
    : installer.generateInstallUrl({ scopes });

module.exports = getAuthUrl;
