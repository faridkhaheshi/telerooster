const fetchInstallation = require("../../installations/controllers/fetch-installation");

const authorizeRequest = async (installQuery) => {
  try {
    const {
      bot: { id: botId, token: botToken, userId: botUserId },
      team: { id: teamId, name: teamName },
      context = {},
    } = await fetchInstallation(installQuery);
    return {
      teamId,
      botToken,
      botId,
      botUserId,
      ...context,
    };
  } catch (err) {}
};

module.exports = authorizeRequest;
