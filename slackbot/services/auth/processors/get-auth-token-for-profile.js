const sendInternalApiRequest = require("../../../adapters/api/send-internal-api-request");

const getAuthTokenForProfile = async (profileId) => {
  try {
    const result = await sendInternalApiRequest(
      `${process.env.WEBSITE_API_BASE_URL}/private/auth/login?profileId=${profileId}`
    );
    if (result) return result.token;
    return null;
  } catch (err) {
    return null;
  }
};

module.exports = getAuthTokenForProfile;
