const { installer } = require("../../../bot");
const { withErrorHandling } = require("../../errors/processors");
const {
  handleInstallationSuccess,
  handleInstallationFailure,
} = require("../../installations/controllers");

const handleAuthResult = (req, res) =>
  installer.handleCallback(req, res, {
    success: handleInstallationSuccess,
    failure: handleInstallationFailure,
  });

module.exports = withErrorHandling(handleAuthResult);
