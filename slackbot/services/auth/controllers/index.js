module.exports = {
  handleAuthRequest: require("./handle-auth-request"),
  handleAuthResult: require("./handle-auth-result"),
  protectRoute: require("./protect-route"),
  serverInstallButton: require("./serve-install-button"),
};
