const { withErrorHandling } = require("../../errors/processors");
const { getAuthUrl } = require("../processors");

const serveInstallButton = async (req, res, next) => {
  const { query } = req;
  const metadata = query ? JSON.stringify(query) : "{}";
  const authUrl = await getAuthUrl(metadata);
  res.send(`<a href=${authUrl}><img alt=""Add to Slack"" height="40" width="139"
        src="https://platform.slack-edge.com/img/add_to_slack.png"
        srcset="https://platform.slack-edge.com/img/add_to_slack.png 1x,
        https://platform.slack-edge.com/img/add_to_slack@2x.png 2x" /></a>`);
};

module.exports = withErrorHandling(serveInstallButton);
