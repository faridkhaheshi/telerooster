const protectRoute = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    if (token === process.env.INTERNAL_API_KEY) return next();
    throw new Error("no");
  } catch (err) {
    return res.status(401).json({ error: { message: "Authorization Failed" } });
  }
};

module.exports = protectRoute;
