const { withErrorHandling } = require("../../errors/processors");
const { getAuthUrl } = require("../processors");

const handleAuthRequest = async (req, res) => {
  const { query } = req;
  const metadata = query ? JSON.stringify(query) : undefined;
  const authUrl = await getAuthUrl(metadata);

  // return res.send(authUrl);

  res.writeHead(302, { Location: authUrl });
  return res.end();
};

module.exports = withErrorHandling(handleAuthRequest);
