const { withErrorHandling } = require("../../errors/processors");
const { getUserInfo, parseSlackProfile } = require("../processors");

const handleGetUserInfoRequest = async (req, res) => {
  const userInfo = await getUserInfo({
    token: req.query.token,
    userId: req.params.id,
  });
  return res.json(parseSlackProfile(userInfo, "bot-update").profileInfo);
};

module.exports = withErrorHandling(handleGetUserInfoRequest);
