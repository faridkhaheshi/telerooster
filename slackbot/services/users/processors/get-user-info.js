const webClient = require("../../../bot/web-client");

const getUserInfo = async ({ token, userId }) => {
  const { ok, user } = await webClient.users.info({
    token,
    user: userId,
    include_locale: true,
  });
  return user;
};

module.exports = getUserInfo;
