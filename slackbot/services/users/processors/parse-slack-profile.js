const parseSlackProfile = (profile, source = "unknown") => {
  const displayName = profile.profile.display_name_normalized;
  const email = profile.profile.email;
  const { conversationRef, ...otherProfileInfo } = profile;
  const profileInfo = {
    provider: "slack",
    id_ext: profile.id,
    info: { source, ...otherProfileInfo, email, displayName },
  };
  if (profile.conversationRef)
    profileInfo.conversation_ref = profile.conversationRef;
  return {
    userInfo: {
      first_name: profile.profile.first_name,
      last_name: profile.profile.last_name,
      display_name: displayName,
      email,
    },
    profileInfo,
  };
};

module.exports = parseSlackProfile;
