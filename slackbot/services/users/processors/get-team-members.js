const webClient = require("../../../bot/web-client");

const LIMIT = 100;

const getTeamMembers = async ({ token, teamId }) => {
  const members = [];
  let cursor = undefined;
  do {
    const {
      ok,
      members: newMembers,
      response_metadata: { next_cursor },
    } = await webClient.users.list({
      token,
      limit: LIMIT,
      include_locale: true,
      team_id: teamId,
    });
    members.push(...newMembers);
    cursor = next_cursor;
  } while (cursor.length > 0);

  return members.filter(
    (m) => !m.deleted && !m.is_bot && m.name !== "slackbot"
  );
};

module.exports = getTeamMembers;
