module.exports = {
  baseUrl: process.env.WEBSITE_BASE_URL,
  panelBaseUrl: process.env.PANEL_BASE_URL,
};
