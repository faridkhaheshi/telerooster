const {
  handleCreateChannelRequest,
  handleGetChannelinfoRequest,
} = require("../../services/conversations/controllers");

module.exports = (server) => {
  server.post("/papi/channels", handleCreateChannelRequest);
  server.get("/papi/channels/:id", handleGetChannelinfoRequest);
};
