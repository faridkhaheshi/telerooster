const protectRoute = require("../../services/auth/controllers/protect-route");

module.exports = (server) => {
  server.use("/papi", protectRoute);
  require("./channels")(server);
  require("./messages")(server);
  require("./teams")(server);
  require("./users")(server);
};
