const handleSendMessageRequest = require("../../services/messages/controllers/handle-send-message-request");

module.exports = (server) => {
  server.post("/papi/messages", handleSendMessageRequest);
};
