const handleGetTeamInfoRequest = require("../../services/teams/controllers/handle-get-team-info-request");

module.exports = (server) => {
  server.get("/papi/teams/:id", handleGetTeamInfoRequest);
};
