const handleGetUserInfoRequest = require("../../services/users/controllers/handle-get-user-info-request");

module.exports = (server) => {
  server.get("/papi/users/:id", handleGetUserInfoRequest);
};
