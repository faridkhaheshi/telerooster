const bodyParser = require("body-parser");

module.exports = (server) => {
  server.use(bodyParser.json({ limit: "200mb" }));
  require("./papi")(server);
  require("./api")(server);
  require("./auth")(server);
  require("./slack")(server);
};
