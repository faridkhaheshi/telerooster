module.exports = (server) => {
  require("./install")(server);
  require("./redirect")(server);
};
