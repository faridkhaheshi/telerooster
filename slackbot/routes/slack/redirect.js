const handleAuthResult = require("../../services/auth/controllers/handle-auth-result");

module.exports = (server) => {
  server.get("/slack/redirect", handleAuthResult);
};
