const { serverInstallButton } = require("../../services/auth/controllers");

module.exports = (server) => {
  server.get("/slack/install", serverInstallButton);
};
