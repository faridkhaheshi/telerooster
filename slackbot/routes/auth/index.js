const handleAuthRequest = require("../../services/auth/controllers/handle-auth-request");

module.exports = (server) => {
  server.get("/auth", handleAuthRequest);
};
