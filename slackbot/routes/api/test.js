const convertMarkdownToSlackMarkdown = require("../../services/parser/processors/convert-markdown-to-slack-markdown");

module.exports = (server) => {
  server.get("/api/test", async (req, res) => {
    try {
      // const {
      //   body: { markdown },
      // } = req;
      // const slackMarkdown = convertMarkdownToSlackMarkdown(markdown, {});
      // return res.json({ hi: true, markdown, slackMarkdown });
      return res.json({ ok: true });
    } catch (err) {
      console.error(err);
      res.json({ error: err });
    }
  });
};
