const { installer, scopes } = require("./../../bot");

module.exports = (server) => {
  server.get("/api/install-link", async (req, res) => {
    try {
      const { query: { metadata } = {} } = req;
      const url = await installer.generateInstallUrl({
        scopes,
        metadata,
      });
      return res.json({ url });
    } catch (err) {
      res
        .status(err.statusCode || err.status || 500)
        .send(err.message || "something went wrong...");
    }
  });
};
