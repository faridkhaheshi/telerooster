module.exports = (server) => {
  require("./install-link")(server);
  require("./test")(server);
};
