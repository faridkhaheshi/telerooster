const sendInternalApiRequest = require("../api/send-internal-api-request");

const apiBaseURL = process.env.WEBSITE_API_BASE_URL;

const registerNewSlackTeam = async (newTeamInfo, cookie) =>
  sendInternalApiRequest(
    `${apiBaseURL}/private/slack/teams/new-install`,
    cookie
      ? {
          method: "POST",
          body: newTeamInfo,
          headers: { cookie },
        }
      : { method: "POST", body: newTeamInfo }
  );

module.exports = registerNewSlackTeam;
