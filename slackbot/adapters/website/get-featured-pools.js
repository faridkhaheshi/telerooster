const sendInternalApiRequest = require("../api/send-internal-api-request");

const getFeaturedPools = () =>
  sendInternalApiRequest(`${process.env.WEBSITE_API_BASE_URL}/pools/featured`);

module.exports = getFeaturedPools;
