const { App } = require("@slack/bolt");
const scopes = require("./scopes");
const installer = require("./installer");
const authorizeRequest = require("../services/auth/processors/authorize-request");
const events = require("./events");

const bot = new App({
  signingSecret: process.env.SLACK_SIGNING_SECRET,
  authorize: authorizeRequest,
});

events(bot);

module.exports = {
  bot,
  receiver: bot.receiver,
  installer,
  scopes,
};
