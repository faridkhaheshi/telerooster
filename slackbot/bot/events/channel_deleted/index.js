const handleChannelDeleted = require("../../../features/keep-channel-status-updated/handle-channel-deleted");

module.exports = (bot) => {
  bot.event("channel_deleted", handleChannelDeleted);
};
