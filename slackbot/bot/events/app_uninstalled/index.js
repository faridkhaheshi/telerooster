const handleAppUninstalled = require("../../../features/keep-team-status-updated/handle-app-uninstalled");

module.exports = (bot) => {
  bot.event("app_uninstalled", handleAppUninstalled);
};
