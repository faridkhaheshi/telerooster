const handleAppHomeOpened = require("../../../features/onboard-user/handle-app-home-opened");

module.exports = (bot) => {
  bot.event("app_home_opened", handleAppHomeOpened);
};
