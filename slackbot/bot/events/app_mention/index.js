const { ignoreSelf } = require("@slack/bolt");
const { cleanMessage } = require("../../../features/app-mention/middlewares");
const messageProcessors = require("../messages/_message_processors");

module.exports = (bot) => {
  bot.event("app_mention", ignoreSelf(), cleanMessage, ...messageProcessors);
};
