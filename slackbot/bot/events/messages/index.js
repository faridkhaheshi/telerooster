const { ignoreSelf } = require("@slack/bolt");
const { cleanMessage } = require("../../../features/app-mention/middlewares");
const messageProcessors = require("./_message_processors");

module.exports = (bot) => {
  bot.message(ignoreSelf(), cleanMessage, ...messageProcessors);
};
