const messageProcessors = [
  require("./hello"),
  require("./help"),
  require("./panel"),
  require("./newsletter"),
  require("./default"),
];

module.exports = messageProcessors;
