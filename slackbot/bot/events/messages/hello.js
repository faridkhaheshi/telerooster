const processMessagePattern = require("../../../features/app-mention/middlewares/process-message-pattern");
const sendGreetingMessage = require("../../../features/greeting/send-greeting-message");

module.exports = processMessagePattern(
  /^(hello|hi|hey|yo)/i,
  sendGreetingMessage
);
