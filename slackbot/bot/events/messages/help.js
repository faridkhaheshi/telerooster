const sendHelpMessage = require("../../../features/help/send-help-message");
const {
  processMessagePattern,
} = require("../../../features/app-mention/middlewares");

module.exports = processMessagePattern(/^(help|guide)/i, sendHelpMessage);
