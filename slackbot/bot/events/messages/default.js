const withErrorHandlingForBotEvents = require("../../../services/errors/processors/with-error-handling-for-bot-events");
const buildSlackMessage = require("../../../services/messages/processors/build-slack-message");

const handleDefaultMessages = async ({ ack, say, context }) => {
  if (ack) await ack();
  //   const response = buildSlackMessage({
  //     blocks: [
  //       {
  //         type: "image",
  //         content: {
  //           image: {
  //             src: `${process.env.WEBSITE_BASE_URL}/images/ms-icon-150x150.png`,
  //             alt: "welcome",
  //           },
  //         },
  //       },
  //       {
  //         type: "p",
  //         content: {
  //           markdown: `Sorry. I didn't understand what you said.
  // I am a rooster. I don't know English very well. But I am very good at delivering high-quality content in Slack.

  // Want to know more? Send \`help\` to learn more.

  //           `,
  //         },
  //       },
  //     ],
  //   });
  //   await say(response);
};

module.exports = withErrorHandlingForBotEvents(handleDefaultMessages);
