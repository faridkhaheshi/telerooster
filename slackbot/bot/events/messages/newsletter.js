const processMessagePattern = require("../../../features/app-mention/middlewares/process-message-pattern");
const handleGetFeaturedPoolsEvent = require("../../../features/featured-pools/handle-get-featured-pools-event");

module.exports = processMessagePattern(
  /^(newsletter|channel|channels|featured)/i,
  handleGetFeaturedPoolsEvent
);
