const sendPanelLink = require("../../../features/panel/send-panel-link");
const processMessagePattern = require("../../../features/app-mention/middlewares/process-message-pattern");

module.exports = processMessagePattern(
  /^(panel|control panel|control|portal|settings|setting)/i,
  sendPanelLink
);
