const openFeaturedPoolsModal = require("../../../features/add-newsletter-shortcut/open-featured-pool-modal");

module.exports = (bot) => {
  bot.shortcut("open-featured-modal", openFeaturedPoolsModal);
};
