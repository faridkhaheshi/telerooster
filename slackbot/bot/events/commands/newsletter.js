const handleNewsletterCommand = require("../../../features/newsletter-command/handle-newsletter-command");

module.exports = (bot) => {
  bot.command("/newsletter", handleNewsletterCommand);
};
