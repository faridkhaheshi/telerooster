const handleSubscribeEvent = require("./../../../features/add-newsletter-shortcut/handle-subscribe-event");

module.exports = (bot) => {
  bot.action("subscribe-to-pool", handleSubscribeEvent);
};
