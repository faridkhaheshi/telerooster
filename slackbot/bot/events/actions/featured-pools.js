const handleGetFeaturedPoolsEvent = require("../../../features/featured-pools/handle-get-featured-pools-event");

module.exports = (bot) => {
  bot.action("featured-pools", handleGetFeaturedPoolsEvent);
};
