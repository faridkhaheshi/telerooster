module.exports = (bot) => {
  require("./open-link")(bot);
  require("./featured-pools")(bot);
  require("./subscribe-to-pool")(bot);
};
