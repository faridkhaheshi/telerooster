module.exports = (bot) => {
  require("./actions")(bot);
  require("./commands")(bot);
  require("./messages")(bot);
  require("./shortcuts")(bot);
  require("./app_mention")(bot);
  require("./app_home_opened")(bot);
  require("./channel_deleted")(bot);
  require("./app_uninstalled")(bot);
};
