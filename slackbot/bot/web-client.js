const { WebClient } = require("@slack/web-api");

const webClient = new WebClient();

module.exports = webClient;
