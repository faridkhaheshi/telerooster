module.exports = [
  "app_mentions:read",
  "channels:read",
  "channels:manage",
  "chat:write.customize",
  "chat:write",
  "im:history",
  "im:write",
  "users:read",
  "users:read.email",
  "commands",
];
