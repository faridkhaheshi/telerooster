const { InstallProvider } = require("@slack/oauth");

const installer = new InstallProvider({
  signingSecret: process.env.SLACK_SIGNING_SECRET,
  clientId: process.env.SLACK_CLIENT_ID,
  clientSecret: process.env.SLACK_CLIENT_SECRET,
  stateSecret: process.env.SLACK_OAUTH_STATE_SECRET,
});

module.exports = installer;
