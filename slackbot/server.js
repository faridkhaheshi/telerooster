const { bot, receiver } = require("./bot");
const routes = require("./routes");

routes(receiver.router);

(async () => {
  try {
    await bot.start(process.env.PORT || 3000);

    console.log("⚡️ Bolt app is running!");
  } catch (error) {
    console.error(error);
  }
})();
