const cookieParser = require("cookie");

const parseRequestCookies = (req) => {
  try {
    return cookieParser.parse(req.headers.cookie) || {};
  } catch (error) {
    return {};
  }
};

module.exports = parseRequestCookies;
