const raiseEvent = require("../../adapters/dispatcher/raise-event");
const withErrorHandlingForBotEvents = require("../../services/errors/processors/with-error-handling-for-bot-events");

const handleChannelDeleted = async ({ event, client }) => {
  const { channel: channelId } = event;
  await raiseEvent({
    event: "channel-deleted",
    payload: { platform: "slack", idExt: channelId },
  });
};

module.exports = withErrorHandlingForBotEvents(handleChannelDeleted);
