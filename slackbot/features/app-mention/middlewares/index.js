module.exports = {
  cleanMessage: require("./clean-message"),
  processMessagePattern: require("./process-message-pattern"),
};
