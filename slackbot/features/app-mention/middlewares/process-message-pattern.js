const processMessagePattern = (pattern, processor) => async (args) => {
  try {
    console.log("processing message.");
    const { event, context, next } = args;
    let tempMatches;
    if (!("text" in event) && !("cleanText" in event)) return;
    if (event.text === undefined && event.cleanText === undefined) return;

    const messageText = event.cleanText || event.text;

    if (typeof pattern === "string" && messageText.includes(pattern)) {
      return processor(args);
    } else if (typeof pattern === "string") {
      await next();
    } else {
      tempMatches = messageText.match(pattern);
      if (tempMatches !== null) {
        context["matches"] = tempMatches;
        return processor(args);
      } else {
        await next();
      }
    }
  } catch (err) {
    console.log("error happened");
  }
};

module.exports = processMessagePattern;
