const withErrorHandlingForBotEvents = require("../../../services/errors/processors/with-error-handling-for-bot-events");

const slackLink = /<(?<type>[@#!])?(?<link>[^>|]+)(?:\|(?<label>[^>]+))?>/;

const cleanMessage = async (args) => {
  const { event, context, next } = args;
  if (context.botUserId === undefined) return;
  if (!("text" in event) || event.text === undefined) return;

  const text = event.text.trim();
  const matches = slackLink.exec(text);
  if (
    matches === null || // stop when no matches are found
    matches.index !== 0 || // stop if match isn't at the beginning
    // stop if match isn't a user mention with the right user ID
    matches.groups === undefined ||
    matches.groups.type !== "@" ||
    matches.groups.link !== context.botUserId
  ) {
    await next();
  } else {
    event.cleanText = text.replace(slackLink, "").trim();
    await next();
  }
};

module.exports = withErrorHandlingForBotEvents(cleanMessage);
