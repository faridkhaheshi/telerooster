const withErrorHandlingForBotEvents = require("../../services/errors/processors/with-error-handling-for-bot-events");
const greetingMessage = require("../greeting/greeting-message");
const isNewUser = require("./is-new-user");

const handleAppHomeOpened = async ({ event, client, say }) => {
  console.log("APP HOME OPENED");
  if (event.tab === "messages" && (await isNewUser({ event, client }))) {
    await say(greetingMessage);
  }
};

module.exports = withErrorHandlingForBotEvents(
  handleAppHomeOpened,
  "app-home-opened"
);
