const isNewUser = async ({ event, client }) => {
  try {
    const { event_ts: eventTs } = event;
    const { messages } = await client.conversations.history({
      channel: event.channel,
      limit: 10,
    });

    let botMessages = 0;

    return messages.every((message, index) => {
      const { bot_id, text = "", ts } = message;
      if (bot_id) botMessages++;
      if (botMessages >= 3) return false;
      const secondsAgo = eventTs - ts;
      if (secondsAgo > 60 * 60) return false;
      if (bot_id && text.match(/^(Hello)/i)) return false;
      return true;
    });
  } catch (error) {
    return false;
  }
};

module.exports = isNewUser;
