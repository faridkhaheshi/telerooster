const buildSlackMessage = require("../../services/messages/processors/build-slack-message");

const greetingMessage = buildSlackMessage({
  text: "Hello",
  blocks: [
    {
      type: "p",
      content: { markdown: "Hello!" },
    },
    {
      type: "p",
      content: {
        markdown: `
I am a rooster. I don't know English very well. But I can deliver high-quality content to your team.

To see more about my job, send \`\`\`help\`\`\`.
      `,
      },
    },
  ],
});

module.exports = greetingMessage;
