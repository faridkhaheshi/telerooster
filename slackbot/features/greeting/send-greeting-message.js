const withErrorHandlingForBotEvents = require("../../services/errors/processors/with-error-handling-for-bot-events");
const greetingMessage = require("./greeting-message");

const sendGreetingMessage = async ({ ack, say, context, next }) => {
  if (ack) await ack();

  await say(greetingMessage);
};

module.exports = withErrorHandlingForBotEvents(sendGreetingMessage);
