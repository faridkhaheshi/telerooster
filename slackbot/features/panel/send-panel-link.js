const { baseUrl, panelBaseUrl } = require("./../../config");
const getAuthTokenForProfile = require("../../services/auth/processors/get-auth-token-for-profile");
const buildSlackMessage = require("../../services/messages/processors/build-slack-message");
const raiseEvent = require("../../adapters/dispatcher/raise-event");
const withErrorHandlingForBotEvents = require("../../services/errors/processors/with-error-handling-for-bot-events");

const sendPanelLink = async ({ ack, say, context }) => {
  context.processed = true;
  if (ack) await ack();

  const { isAdmin, roosterInfo: { adminProfile = {} } = {} } = context;

  let roosterProfileId;

  try {
    roosterProfileId = adminProfile.id;
  } catch (error) {
    roosterProfileId = undefined;
  }

  const panelUrl = !isAdmin
    ? `${panelBaseUrl}/subscriptions`
    : `${baseUrl}/auth?ref=${encodeURIComponent(
        `${panelBaseUrl}/subscriptions`
      )}&token=${await getAuthTokenForProfile(roosterProfileId)}`;

  const panelMessage = {
    text: "Link to your panel:",
    blocks: [
      {
        type: "buttonrow",
        content: {
          markdown: "Link to your panel:",
          buttonText: "Go to panel",
          url: panelUrl,
        },
      },
    ],
  };

  if (!isAdmin) {
    await say(buildSlackMessage(panelMessage));
  } else {
    await raiseEvent({
      event: "send-message-to-profile",
      payload: {
        profileId: roosterProfileId,
        message: panelMessage,
      },
    });
  }
};

module.exports = withErrorHandlingForBotEvents(sendPanelLink);
