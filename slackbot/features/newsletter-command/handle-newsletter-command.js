const raiseEvent = require("../../adapters/dispatcher/raise-event");
const { panelBaseUrl, baseUrl } = require("../../config");
const getAuthTokenForProfile = require("../../services/auth/processors/get-auth-token-for-profile");
const withErrorHandlingForBotEvents = require("../../services/errors/processors/with-error-handling-for-bot-events");
const openFeaturedPoolsModal = require("../add-newsletter-shortcut/open-featured-pool-modal");
const buildHelpMessage = require("../help/build-help-message");

const handleNewsletterCommand = async ({
  ack,
  payload,
  command,
  client,
  context,
}) => {
  if (ack) await ack();
  const { text: commandText = "" } = command || {};
  if (commandText && ["help", "how"].indexOf(commandText.toLowerCase()) > -1) {
    await sendHelpMessageToUser(context);
  } else {
    const { trigger_id } = payload;
    await openFeaturedPoolsModal({
      client,
      shortcut: { trigger_id },
      context,
    });
  }
};

module.exports = withErrorHandlingForBotEvents(
  handleNewsletterCommand,
  "newsletter command"
);

async function sendHelpMessageToUser(context) {
  const { isAdmin, roosterInfo: { adminProfile = {} } = {} } = context;

  let roosterProfileId;

  try {
    roosterProfileId = adminProfile.id;
  } catch (error) {
    roosterProfileId = undefined;
  }

  const panelUrl = !isAdmin
    ? `${panelBaseUrl}/subscriptions`
    : `${baseUrl}/auth?ref=${encodeURIComponent(
        `${panelBaseUrl}/subscriptions`
      )}&token=${await getAuthTokenForProfile(roosterProfileId)}`;
  const helpMessage = buildHelpMessage({ panelUrl });
  await raiseEvent({
    event: "send-message-to-profile",
    payload: {
      profileId: roosterProfileId,
      message: helpMessage,
    },
  });
}
