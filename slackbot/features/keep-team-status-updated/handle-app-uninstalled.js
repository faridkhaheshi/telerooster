const raiseEvent = require("../../adapters/dispatcher/raise-event");
const withErrorHandlingForBotEvents = require("../../services/errors/processors/with-error-handling-for-bot-events");

const handleAppUninstalled = async ({ event, client, context }) => {
  const {
    roosterInfo: { teamId },
  } = context;
  await raiseEvent({
    event: "team-left",
    payload: { platform: "slack", teamId },
  });
};

module.exports = withErrorHandlingForBotEvents(
  handleAppUninstalled,
  "app-uninstalled event"
);
