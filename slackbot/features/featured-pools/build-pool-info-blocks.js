const { baseUrl } = require("../../config");

const buildPoolInfoBlocks = ({ pool, showLink = true }) => [
  {
    type: "buttonrow",
    content: showLink
      ? {
          markdown: `**${pool.title}**`,
          buttonText: "Visit channel's page",
          url: `${baseUrl}/pools/${pool.id}`,
        }
      : {
          markdown: `**[${pool.title}](${`${baseUrl}/pools/${pool.id}`})**`,
          buttonText: "Subscribe",
          actionId: "subscribe-to-pool",
          value: JSON.stringify({ poolId: pool.id, poolTitle: pool.title }),
        },
  },
  {
    type: "p",
    content: {
      markdown: `description: _${pool.description}_

creator: _${pool.creator.display_name}_

price: Free
`,
    },
  },
  {
    type: "hr",
  },
];

module.exports = buildPoolInfoBlocks;
