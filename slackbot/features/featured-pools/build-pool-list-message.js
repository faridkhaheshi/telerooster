const buildPoolInfoBlocks = require("./build-pool-info-blocks");

const buildPoolListMessage = ({ pools, showLink = true }) => {
  return {
    blocks: [
      {
        type: "h",
        content: {
          markdown: "Featured Channels",
        },
      },
      ...pools.reduce(
        (acc, pool) => [
          ...acc,
          ...buildPoolInfoBlocks({ pool, showLink }).filter((b) =>
            b ? true : false
          ),
        ],
        []
      ),
    ],
  };
};

module.exports = buildPoolListMessage;
