const getFeaturedPools = require("../../adapters/website/get-featured-pools");
const withErrorHandlingForBotEvents = require("../../services/errors/processors/with-error-handling-for-bot-events");
const buildSlackMessage = require("../../services/messages/processors/build-slack-message");
const sendHelpMessage = require("../help/send-help-message");
const buildPoolListMessage = require("./build-pool-list-message");

const MAX_CHANNELS = 10;

const handleGetFeaturedPoolsEvent = async ({
  ack,
  say,
  context,
  payload,
  command,
  event,
}) => {
  if (ack) await ack();
  const { isAdmin } = context;
  const { text: commandText = "" } = command || {};
  if (commandText && ["help", "how"].indexOf(commandText.toLowerCase()) > -1) {
    await sendHelpMessage({ say, context });
  } else {
    const pools = await getFeaturedPools();
    let showLink = true;
    try {
      showLink = !(
        isAdmin &&
        (event?.channel_type === "im" ||
          payload?.channel_name === "directmessage" ||
          payload?.type === "button")
      );
    } catch (err) {}
    await say(
      buildSlackMessage(
        buildPoolListMessage({
          pools: pools.slice(0, MAX_CHANNELS),
          showLink,
        })
      )
    );
  }
};

module.exports = withErrorHandlingForBotEvents(
  handleGetFeaturedPoolsEvent,
  "get-featured-pools-event"
);
