const buildHelpMessage = ({ panelUrl }) => ({
  blocks: [
    {
      type: "image",
      content: {
        image: {
          src: `${process.env.WEBSITE_BASE_URL}/images/ms-icon-150x150.png`,
          alt: "welcome",
        },
      },
    },
    { type: "h", content: { markdown: "About TeleRooster" } },
    {
      type: "p",
      content: {
        markdown: `
With TeleRooster you can add newsletter channels to your team.

These channels are like other channels, but they are managed by professional content creators. They enable you to receive high-quality posts.

You can always delete a channel to stop receiving new posts. So don't worry about spam.

Don't know what channels to add? Have a look at our featured channels.

You can also type \`\`\`Add a newsletter\`\`\` in the search box above to get a list of featured channels.
`,
      },
    },
    {
      type: "buttonrow",
      content: {
        markdown: "Get featured channels:",
        buttonText: "Featured channels",
        actionId: "featured-pools",
      },
    },
    {
      type: "buttonrow",
      content: {
        markdown: "Manage your account:",
        buttonText: "Go to panel",
        url: panelUrl,
      },
    },
  ],
});

module.exports = buildHelpMessage;
