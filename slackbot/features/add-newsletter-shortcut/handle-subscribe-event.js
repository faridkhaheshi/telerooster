const withErrorHandlingForBotEvents = require("../../services/errors/processors/with-error-handling-for-bot-events");
const raiseEvent = require("./../../adapters/dispatcher/raise-event");

const handleSubscribeEvent = async ({ ack, payload, context, say }) => {
  await ack();
  const {
    isAdmin,
    roosterInfo: {
      teamId,
      adminProfile: { id: roosterProfileId },
    },
  } = context;
  const { value } = payload;
  const { poolId, poolTitle } = JSON.parse(value);
  if (isAdmin) {
    await raiseEvent({
      event: "subscribe-to-pool",
      payload: {
        poolId,
        teamId,
        platform: "slack",
        profileId: roosterProfileId,
      },
    });
    await raiseEvent({
      event: "send-message-to-profile",
      payload: {
        profileId: roosterProfileId,
        message: `Adding *${poolTitle}* to your team`,
      },
    });
  }
};

module.exports = withErrorHandlingForBotEvents(
  handleSubscribeEvent,
  "subscribe-to-pool-event"
);
