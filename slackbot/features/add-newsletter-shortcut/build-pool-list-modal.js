const buildSlackMessage = require("../../services/messages/processors/build-slack-message");
const buildPoolInfoBlocks = require("../featured-pools/build-pool-info-blocks");

const buildPoolListModal = ({ pools, showLink = true }) => {
  const { blocks } = buildSlackMessage({
    blocks: pools.reduce(
      (acc, pool) => [
        ...acc,
        ...buildPoolInfoBlocks({ pool, showLink }).filter((b) =>
          b ? true : false
        ),
      ],
      []
    ),
  });
  return {
    type: "modal",
    title: {
      type: "plain_text",
      text: "Featured channels",
      emoji: true,
    },
    blocks,
  };
};

module.exports = buildPoolListModal;
