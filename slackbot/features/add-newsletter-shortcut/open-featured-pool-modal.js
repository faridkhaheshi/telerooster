const getFeaturedPools = require("../../adapters/website/get-featured-pools");
const buildPoolListModal = require("./build-pool-list-modal");

const openFeaturedPoolsModal = async ({ ack, client, shortcut, context }) => {
  try {
    if (ack) await ack();
    const { isAdmin } = context;
    const pools = await getFeaturedPools();
    const result = await client.views.open({
      trigger_id: shortcut.trigger_id,
      view: buildPoolListModal({ pools, showLink: !isAdmin }),
    });
  } catch (err) {
    console.error(err);
  }
};

module.exports = openFeaturedPoolsModal;
