const resizeImage = require("../services/images/controllers/resize-image");

module.exports = (server) => {
  server.get("/resize", resizeImage);
};
