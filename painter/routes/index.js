const { plugins } = require("restify");

module.exports = (server) => {
  server.use(plugins.queryParser({ mapParams: false }));
  require("./template")(server);
  require("./image")(server);
  require("./resize")(server);
};
