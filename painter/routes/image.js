const getImage = require("../services/images/controllers/get-image");

module.exports = (server) => {
  server.get("/image", getImage);
};
