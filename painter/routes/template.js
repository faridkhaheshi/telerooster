const { showTemplate } = require("../services/templates/controllers");

module.exports = (server) => {
  server.get("/template/:template", showTemplate);
};
