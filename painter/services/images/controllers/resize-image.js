const got = require("got");
const sharp = require("sharp");
const {
  BadRequestError,
  NotFoundError,
  InternalServerError,
} = require("restify-errors");

const resizeImage = async (req, res, next) => {
  try {
    const { query: { url, w, h } = {} } = req;
    if (!url) throw new BadRequestError("url not provided");
    const resizer = sharp()
      .resize(parseInt(w), parseInt(h), { fit: "fill" })
      .on("error", () => {
        throw new InternalServerError("image conversion failed");
      });

    const downloadStream = got.stream(url).on("error", (error) => {
      throw new NotFoundError("image not found");
    });

    downloadStream.pipe(resizer).pipe(res);
  } catch (err) {
    next(err);
  }
};

module.exports = resizeImage;
