module.exports = {
  getImage: require("./get-image"),
  resizeImage: require("./resize-image"),
};
