const findTemplate = require("../../templates/processors/find-template");
const generateImage = require("../processors/generate-image");

const getImage = async (req, res, next) => {
  try {
    const {
      query: { template },
    } = req;
    await findTemplate(template);
    const imageBuffer = await generateImage({
      template,
      queryString: req._url.query,
    });
    res.header("Content-Type", "image/png");
    res.write(imageBuffer);
    res.end();
    next();
  } catch (err) {
    next(err);
  }
};

module.exports = getImage;
