const puppeteer = require("puppeteer");

const generateImage = async ({ template, queryString }) => {
  const browser = await puppeteer.launch({
    executablePath: "google-chrome-stable",
    headless: true,
    ignoreHTTPSErrors: true,
    defaultViewport: {
      width: 600,
      height: 600,
      isLandscape: true,
    },
  });
  const page = await browser.newPage();
  // await page.emulate(puppeteer.devices["iPhone 6"]);

  await page.goto(`http://localhost:3000/template/${template}?${queryString}`, {
    waitUntil: "networkidle0",
    timeout: 10000,
  });

  await page.waitForSelector("#target");
  const element = await page.$("#target");

  const screenshot = await element.screenshot({
    encoding: "binary",
  });

  await page.close();
  await browser.close();
  return screenshot;
};

module.exports = generateImage;
