module.exports = {
  generateImage: require("./generate-image"),
  downloadImage: require("./download-image"),
};
