const http = require("http");
const https = require("https");
const Stream = require("stream").Transform;

const downloadImage = (url, callback = () => {}) => {
  console.log("downloading image");
  const client = url.toString().indexOf("https") === 0 ? https : http;
  const data = new Stream();
  client
    .request(url, (response) => {
      response.on("data", (chunk) => {
        data.push(chunk);
      });

      response.on("end", callback);
    })
    .end();
  return data;
};

module.exports = downloadImage;
