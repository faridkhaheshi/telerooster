const renderTemplate = require("../processors/render-template");

const showTemplate = async (req, res, next) => {
  try {
    const {
      params: { template },
      query,
    } = req;
    const output = await renderTemplate({ template, payload: query });
    res.write(output);
    res.end();
    next();
  } catch (err) {
    next(err);
  }
};

module.exports = showTemplate;
