module.exports = `
<html lang="en">
<head>
  <meta charset="utf-8">
</head>

<body>
<div id="target" style="color: {{text_color}}; width: 560px; min-height: 200px; background-color: {{background_color}}; padding: 20px; display: flex; flex-direction: column; align-items: center; justify-content: center;">
<h2 style="font-family: Arial;">{{title}}</h2>
</div>
</body>
</html>`;
