const fs = require("fs");
const path = require("path");
const basename = path.basename(__filename);

const templates = {};

fs.readdirSync(__dirname)
  .filter(
    (file) =>
      file.indexOf(".") !== 0 && file !== basename && file.slice(-3) === ".js"
  )
  .forEach((file) => {
    const temp = require(path.join(__dirname, file));
    const tempName = file.split(".")[0];
    templates[tempName] = temp;
  });

module.exports = templates;
