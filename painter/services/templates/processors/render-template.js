const Mustache = require("mustache");
const findTemplate = require("./find-template");

const renderTemplate = async ({ template, payload }) => {
  const temp = await findTemplate(template);
  const output = Mustache.render(temp, payload);

  return output;
};

module.exports = renderTemplate;
