const { NotFoundError } = require("restify-errors");
const templates = require("./../data");

const findTemplate = async (template) => {
  if (Object.keys(templates).indexOf(template) > -1) return templates[template];
  throw new NotFoundError("template not found");
};

module.exports = findTemplate;
