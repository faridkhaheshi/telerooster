FROM node:lts-slim

RUN apt-get update \
  && apt-get install -y wget gnupg \
  && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
  && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
  && apt-get update \
  && apt-get install -y google-chrome-stable fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst fonts-freefont-ttf libxss1 \
  --no-install-recommends \
  && rm -rf /var/lib/apt/lists/*

ADD https://github.com/Yelp/dumb-init/releases/download/v1.2.2/dumb-init_1.2.2_x86_64 /usr/local/bin/dumb-init
RUN chmod +x /usr/local/bin/dumb-init

ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD true
ENV FONT_DIR /usr/share/fonts/noto

RUN mkdir -p $FONT_DIR
RUN cd $FONT_DIR &&\
  wget https://github.com/googlefonts/noto-cjk/blob/main/Sans/Mono/NotoSansMonoCJKsc-Regular.otf?raw=true && \
  fc-cache -f -v

RUN cd $FONT_DIR &&\
  wget https://github.com/googlefonts/noto-emoji/blob/main/fonts/NotoColorEmoji.ttf?raw=true && \
  fc-cache -f -v


WORKDIR /app
COPY package*.json /app/

RUN npm i \
  && groupadd -r pptruser && useradd -r -g pptruser -G audio,video pptruser \
  && mkdir -p /home/pptruser/Downloads \
  && chown -R pptruser:pptruser /home/pptruser \
  && chown -R pptruser:pptruser /app/node_modules

USER pptruser


EXPOSE 3000
ENTRYPOINT ["dumb-init", "--"]
CMD ["google-chrome-stable"]