const sendInternalApiRequest = require("../api/send-internal-api-request");

const apiBaseURL = process.env.PANEL_API_BASE_URL;

const parseDocument = (doc) =>
  sendInternalApiRequest(`${apiBaseURL}/private/documents`, {
    method: "POST",
    body: { doc },
  });

module.exports = parseDocument;
