const sendInternalApiRequest = require("../api/send-internal-api-request");

const apiBaseURL = process.env.WEBSITE_API_BASE_URL;

const parseContentmail = async (emailAddress) =>
  sendInternalApiRequest(
    `${apiBaseURL}/private/contentmails?address=${encodeURIComponent(
      emailAddress
    )}`
  );

module.exports = parseContentmail;
