const sendInternalApiRequest = require("../api/send-internal-api-request");

const apiBaseURL = process.env.WEBSITE_API_BASE_URL;

const registerNewMicrosoftTeam = async (newTeamInfo) =>
  sendInternalApiRequest(`${apiBaseURL}/private/msteams/teams/new-install`, {
    method: "POST",
    body: newTeamInfo,
  });

module.exports = registerNewMicrosoftTeam;
