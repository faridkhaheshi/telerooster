const sendInternalApiRequest = require("../api/send-internal-api-request");

const apiBaseURL = process.env.WEBSITE_API_BASE_URL;

const cancelStripeSubscription = async (subscriptionId) =>
  sendInternalApiRequest(
    `${apiBaseURL}/private/stripe/subscriptions/${subscriptionId}`,
    {
      method: "DELETE",
    }
  );

module.exports = cancelStripeSubscription;
