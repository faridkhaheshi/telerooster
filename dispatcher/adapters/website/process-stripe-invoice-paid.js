const sendInternalApiRequest = require("../api/send-internal-api-request");

const apiBaseURL = process.env.WEBSITE_API_BASE_URL;

const processStripeInvoiePaid = async (update) =>
  sendInternalApiRequest(`${apiBaseURL}/private/stripe/invoices`, {
    method: "POST",
    body: update,
  });

module.exports = processStripeInvoiePaid;
