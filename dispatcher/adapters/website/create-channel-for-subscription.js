const sendInternalApiRequest = require("../api/send-internal-api-request");

const apiBaseURL = process.env.WEBSITE_API_BASE_URL;

const createChannelForSubscription = ({
  subscriptionId,
  teamId,
  profileId,
  platform,
}) =>
  sendInternalApiRequest(`${apiBaseURL}/private/channels`, {
    method: "POST",
    body: { subscriptionId, teamId, profileId, platform },
  });

module.exports = createChannelForSubscription;
