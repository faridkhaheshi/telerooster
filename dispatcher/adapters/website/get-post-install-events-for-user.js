const sendInternalApiRequest = require("../api/send-internal-api-request");

const apiBaseURL = process.env.WEBSITE_API_BASE_URL;

const getPostInstallEventsForUser = async ({ userId, platform }) =>
  sendInternalApiRequest(
    `${apiBaseURL}/private/users/${userId}/post-install-events?platform=${platform}`
  );

module.exports = getPostInstallEventsForUser;
