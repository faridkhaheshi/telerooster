const sendInternalApiRequest = require("../api/send-internal-api-request");

const apiBaseURL = process.env.WEBSITE_API_BASE_URL;

const processStripeCheckoutResult = async ({ sessionId, userId }) =>
  sendInternalApiRequest(`${apiBaseURL}/private/stripe/sessions/${sessionId}`, {
    method: "POST",
    body: { userId },
  });

module.exports = processStripeCheckoutResult;
