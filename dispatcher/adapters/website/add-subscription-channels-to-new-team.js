const sendInternalApiRequest = require("../api/send-internal-api-request");

const apiBaseURL = process.env.WEBSITE_API_BASE_URL;

const addSubscriptionChannelsToNewTeam = async ({ team, profile }) =>
  sendInternalApiRequest(
    `${apiBaseURL}/private/msteams/teams/add-team-subscription-channels`,
    {
      method: "POST",
      body: { team, profile },
    }
  );

module.exports = addSubscriptionChannelsToNewTeam;
