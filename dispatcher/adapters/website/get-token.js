const querystring = require("querystring");
const sendInternalApiRequest = require("../api/send-internal-api-request");

const apiBaseURL = process.env.WEBSITE_API_BASE_URL;

const getToken = async (query) =>
  sendInternalApiRequest(
    `${apiBaseURL}/private/tokens?${querystring.stringify(query)}`
  );

module.exports = getToken;
