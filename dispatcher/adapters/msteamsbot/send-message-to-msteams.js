const sendInternalApiRequest = require("../api/send-internal-api-request");

const apiBaseURL = process.env.MSTEAMSBOT_API_BASE_URL;

const sendMessageToMsTeams = async ({ message, conversation }) =>
  sendInternalApiRequest(`${apiBaseURL}/messages`, {
    method: "POST",
    body: { message, conversation },
  });

module.exports = sendMessageToMsTeams;
