const sendInternalApiRequest = require("../api/send-internal-api-request");

const apiBaseURL = process.env.SLACKBOT_API_BASE_URL;

const sendMessageToSlack = async ({ message, conversation, installation }) =>
  sendInternalApiRequest(`${apiBaseURL}/messages`, {
    method: "POST",
    body: { message, conversation, installation },
  });

module.exports = sendMessageToSlack;
