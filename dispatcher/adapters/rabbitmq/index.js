const amqp = require("amqp-connection-manager");

let channels = {};

const connection = amqp.connect([
  `amqp://${process.env.RABBITMQ_DISPATCHER_USER}:${process.env.RABBITMQ_DISPATCHER_PASS}@rabbitmq:5672/telerooster`,
]);

connection.on("connect", () => console.log("Connected to rabbitMQ!"));
connection.on("disconnect", (err) => console.log("Disconnected.", err));

const connectToQueue = (queue, handler) => {
  if (channels[queue] == null) {
    const newChannel = connection.createChannel({
      json: true,
      setup: function (channel) {
        if (!handler) return channel.assertQueue(queue, { durable: true });
        return Promise.all([
          channel.assertQueue(queue, { durable: true }),
          channel.consume(queue, handler),
        ]);
      },
    });
    channels[queue] = newChannel;
    return newChannel;
  }
  return channels[queue];
};

module.exports = connectToQueue;
