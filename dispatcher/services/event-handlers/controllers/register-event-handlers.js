const connectToQueue = require("../../../adapters/rabbitmq");
const eventHandlers = require("./../processors");
let queues = {};

module.exports = () => {
  Object.keys(eventHandlers).forEach((event) => {
    console.log(`registering handler for event "${event}"`);
    queues[event] = connectToQueue(
      event,
      preparePayload(eventHandlers[event], event)
    );
    queues[event]
      .waitForConnect()
      .then(() => console.log(`Listening for "${event}" events`));
  });
};

function preparePayload(handler, event) {
  return (data) => {
    const payload = JSON.parse(data.content.toString());
    handler(payload, () => {
      queues[event].ack(data);
    });
  };
}
