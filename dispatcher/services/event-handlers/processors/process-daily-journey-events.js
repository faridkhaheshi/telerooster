const sendInternalApiRequest = require("../../../adapters/api/send-internal-api-request");

module.exports = async (payload, ack) => {
  try {
    console.log("processing daily journey events");
    ack();
    const result = await sendInternalApiRequest(
      `${process.env.WEBSITE_API_BASE_URL}/private/journeys/process`,
      { method: "POST" }
    );
    console.log("Daily Journey events processed:");
    console.log(result);
  } catch (err) {
    console.error(err);
  }
};
