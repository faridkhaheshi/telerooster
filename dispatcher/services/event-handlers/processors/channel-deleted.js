const sendInternalApiRequest = require("../../../adapters/api/send-internal-api-request");

module.exports = async (payload, ack) => {
  try {
    ack();
    const { platform, idExt } = payload;
    const result = await sendInternalApiRequest(
      `${process.env.WEBSITE_API_BASE_URL}/private/channels/update`,
      {
        method: "PUT",
        body: {
          query: { platform, id_ext: idExt },
          updateData: { active: false },
        },
      }
    );
  } catch (err) {
    console.error(err);
  }
};
