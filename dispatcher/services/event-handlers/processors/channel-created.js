const raiseEvent = require("./../../events/processors/raise-event");
const sendInternalApiRequest = require("../../../adapters/api/send-internal-api-request");
const renderTemplate = require("../../templates/processors/render-template");

module.exports = async (payload, ack) => {
  try {
    ack();
    const { channelId, teamId, subscriptionId } = payload;
    const subscription = await sendInternalApiRequest(
      `${process.env.WEBSITE_API_BASE_URL}/private/subscriptions/${subscriptionId}`
    );

    const teamAdmins = await sendInternalApiRequest(
      `${
        process.env.BACKOFFICE_API_BASE_URL
      }/teamadmins?filter=${encodeURIComponent(
        JSON.stringify({ team_id: teamId, active: true })
      )}`
    );

    const authedAdminProfiles = await Promise.all(
      teamAdmins.map(async ({ profile_id }) => ({
        profileId: profile_id,
        ...(await sendInternalApiRequest(
          `${process.env.WEBSITE_API_BASE_URL}/private/auth/login?profileId=${profile_id}`
        )),
      }))
    );

    await Promise.all(
      authedAdminProfiles.map(({ profileId, token }) =>
        raiseEvent({
          event: "send-message-to-profile",
          payload: {
            profileId: profileId,
            message: renderTemplate({
              template: "channel-created",
              data: {
                pool: subscription.pool,
                token,
              },
            }),
          },
        })
      )
    );
    await raiseEvent({
      event: "process-journey-event",
      payload: { channelId, poolId: subscription.pool.id, event: "welcome" },
    });
  } catch (err) {
    console.error(err);
  }
};
