const createChannelForSubscription = require("../../../adapters/website/create-channel-for-subscription");

module.exports = async (payload, ack) => {
  try {
    ack();
    const {
      subscriptionId,
      team: { id: teamId, platform },
      profile: { id: profileId },
    } = payload;
    const channel = await createChannelForSubscription({
      subscriptionId,
      teamId,
      platform,
      profileId,
    });
  } catch (err) {
    console.error(err);
  }
};
