const sendInternalApiRequest = require("../../../adapters/api/send-internal-api-request");
const { sendMessageToProfile } = require("../../messages/processors");
const renderTemplate = require("../../templates/processors/render-template");

module.exports = async (payload, ack) => {
  try {
    ack();
    const { profileId, teamId, userId, platform } = payload;
    const result = await sendInternalApiRequest(
      `${process.env.WEBSITE_API_BASE_URL}/private/auth/login?profileId=${profileId}`
    );
    await sendMessageToProfile({
      profileId,
      message: renderTemplate({
        template: "welcome-new-team",
        data: { token: result.token },
      }),
    });
  } catch (err) {
    console.error(err);
  }
};
