const sendInternalApiRequest = require("../../../adapters/api/send-internal-api-request");

module.exports = async (payload, ack) => {
  try {
    ack();
    const { teamId } = payload;
    const result = await sendInternalApiRequest(
      `${process.env.WEBSITE_API_BASE_URL}/private/teams/${teamId}`,
      {
        method: "PUT",
        body: {
          active: false,
        },
      }
    );
  } catch (err) {
    console.error(err);
  }
};
