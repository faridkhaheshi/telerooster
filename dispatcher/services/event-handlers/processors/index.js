const fs = require("fs");
const path = require("path");
const basename = path.basename(__filename);

const processors = {};

fs.readdirSync(__dirname)
  .filter(
    (file) =>
      file.indexOf(".") !== 0 && file !== basename && file.slice(-3) === ".js"
  )
  .forEach((file) => {
    const eventProcessor = require(path.join(__dirname, file));
    const eventName = file.split(".")[0];
    processors[eventName] = eventProcessor;
  });

module.exports = processors;
