const sendInternalApiRequest = require("../../../adapters/api/send-internal-api-request");
const { sendMessage } = require("../../messages/processors");

module.exports = async (payload, ack) => {
  let parcel;
  try {
    ack();

    const { channel, channelId, message } = payload;

    let theChannel = channel
      ? channel
      : await sendInternalApiRequest(
          `${process.env.BACKOFFICE_API_BASE_URL}/channels/${channelId}`
        );

    const {
      conversation_ref: conversation,
      platform,
      team_id: teamId,
    } = theChannel;

    if (message.id) {
      parcel = await sendInternalApiRequest(
        `${process.env.BACKOFFICE_API_BASE_URL}/parcels`,
        {
          method: "POST",
          body: {
            message_id: message.id,
            channel_id: theChannel.id,
          },
        }
      );
    }

    const resultingMessage = await sendMessage({
      message,
      conversation,
      platform,
      teamId,
    });

    if (parcel) {
      const updatedChannel = await sendInternalApiRequest(
        `${process.env.WEBSITE_API_BASE_URL}/private/channels/${theChannel.id}/update`,
        { method: "POST" }
      );
      await sendInternalApiRequest(
        `${process.env.BACKOFFICE_API_BASE_URL}/parcels/${parcel.id}`,
        {
          method: "PUT",
          body: {
            status: "sent",
            info: {
              audienceSize: updatedChannel.info.memberCount,
              platform: updatedChannel.platform,
            },
            sent_at: new Date(),
          },
        }
      );
    }
  } catch (error) {
    console.log(
      `error in sending message to channel ${
        payload.channelId || payload.channel.id
      }`
    );
    console.error(error);
    if (parcel) {
      await sendInternalApiRequest(
        `${process.env.BACKOFFICE_API_BASE_URL}/parcels/${parcel.id}`,
        {
          method: "PUT",
          body: {
            status: "failed",
            info: { error: error.message || "unknown" },
          },
        }
      );
    }
  }
};
