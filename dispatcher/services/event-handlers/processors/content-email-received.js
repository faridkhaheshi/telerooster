const parseContentmail = require("../../../adapters/website/parse-contentmail");
const raiseEvent = require("../../events/processors/raise-event");
const { createMessage } = require("../../messages/processors");

const MESSAGE_TEXT_MAX_LEN = 200;

module.exports = async (payload, ack) => {
  try {
    ack();
    const {
      msg: { from_email, subject, text, html, to, spam_report, email },
    } = payload;
    const { type, pool_id, channel_id } = await parseContentmail(email);

    console.log("message received...");
    const message = await createMessage({
      subject: subject.replace(
        /[0-9a-z]{5,}@contentmail\.telerooster\.com/gi,
        `your team's channel in telerooster`
      ),
      text: (text || "new message")
        .slice(0, MESSAGE_TEXT_MAX_LEN)
        .replace(
          /[0-9a-z]{5,}@contentmail\.telerooster\.com/gi,
          `your team's channel in telerooster`
        ),
      html,
      poolId: pool_id,
    });

    if (type === "pool") {
      await raiseEvent({
        event: "send-pool-message",
        payload: {
          poolId: pool_id,
          message,
        },
      });
    } else if (type === "channel") {
      await raiseEvent({
        event: "send-message-to-channel",
        payload: {
          channelId: channel_id,
          message,
        },
      });
    }
  } catch (error) {
    console.error(error);
    // console.error(error);
  }
};
