const registerNewSlackTeam = require("../../../adapters/website/register-new-slack-team");

module.exports = async (payload, ack) => {
  try {
    ack();
    const result = await registerNewSlackTeam(payload);
  } catch (error) {
    console.error(error);
  }
};
