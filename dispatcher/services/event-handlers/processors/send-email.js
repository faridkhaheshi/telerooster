const sendEmail = require("../../utils/send-email");

module.exports = async (payload, ack) => {
  try {
    ack();
    await sendEmail(payload);
  } catch (err) {
    console.error(err);
  }
};
