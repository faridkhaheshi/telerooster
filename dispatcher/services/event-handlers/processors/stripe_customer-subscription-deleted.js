const cancelStripeSubscription = require("../../../adapters/website/cancel-stripe-subscription");

module.exports = async (payload, ack) => {
  console.log("new stripe event: customer subscription deleted");
  try {
    ack();
    const {
      data: {
        object: { id: subscriptionId },
      },
    } = payload;
    const updatedPlan = await cancelStripeSubscription(subscriptionId);
  } catch (error) {
    console.error(error);
  }
};
