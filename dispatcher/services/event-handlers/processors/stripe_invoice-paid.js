const processStripeInvoiePaid = require("../../../adapters/website/process-stripe-invoice-paid");

module.exports = async (payload, ack) => {
  console.log("new stripe event: invoice paid");
  try {
    ack();
    const {
      data: {
        object: { customer: customerId, subscription: subscriptionId },
      },
    } = payload;

    await processStripeInvoiePaid({
      customerId,
      subscriptionId,
    });
  } catch (error) {
    console.error(error);
  }
};
