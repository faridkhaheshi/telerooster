const sendInternalApiRequest = require("../../../adapters/api/send-internal-api-request");
const raiseEvent = require("../../events/processors/raise-event");

module.exports = async (payload, ack) => {
  try {
    ack();
    console.log(`event 'process-journey-event'`);
    // console.log(payload);
    const { channelId, poolId, event } = payload;
    const [journeyEvent] = await sendInternalApiRequest(
      `${
        process.env.BACKOFFICE_API_BASE_URL
      }/journeyevents?filter=${encodeURIComponent(
        JSON.stringify({ pool_id: poolId, event, active: true })
      )}`
    );
    if (journeyEvent && journeyEvent.message_id) {
      console.log("sending message");
      const message = await sendInternalApiRequest(
        `${process.env.BACKOFFICE_API_BASE_URL}/messages/${journeyEvent.message_id}`
      );
      raiseEvent({
        event: "send-message-to-channel",
        payload: { message, channelId },
      });
    }
  } catch (err) {
    console.error(err);
  }
};
