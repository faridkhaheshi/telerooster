const processStripeCheckoutResult = require("../../../adapters/website/process-stripe-checkout-result");

module.exports = async (payload, ack) => {
  console.log("new stripe event: checkout session complete");
  try {
    ack();
    const {
      data: {
        object: { id: sessionId, client_reference_id: userId },
      },
    } = payload;
    await processStripeCheckoutResult({ sessionId, userId });
  } catch (error) {
    console.error(error);
  }
};
