const sendInternalApiRequest = require("../../../adapters/api/send-internal-api-request");
const createChannelForSubscription = require("../../../adapters/website/create-channel-for-subscription");

module.exports = async (payload, ack) => {
  try {
    ack();
    const { poolId, teamId, platform, profileId } = payload;
    const { user_id: userId } = await sendInternalApiRequest(
      `${process.env.BACKOFFICE_API_BASE_URL}/profiles/${profileId}`
    );
    const {
      id: subscriptionId,
    } = await sendInternalApiRequest(
      `${process.env.WEBSITE_API_BASE_URL}/private/subscriptions`,
      { method: "POST", body: { userId, poolId } }
    );
    const channel = await createChannelForSubscription({
      subscriptionId,
      teamId,
      platform,
      profileId,
    });
  } catch (err) {
    console.log(err);
  }
};
