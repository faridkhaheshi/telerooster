const sendInternalApiRequest = require("../../../adapters/api/send-internal-api-request");
const raiseEvent = require("../../events/processors/raise-event");

module.exports = async (payload, ack) => {
  try {
    ack();
    const { poolId, message } = payload;
    const subscribedChannels = await sendInternalApiRequest(
      `${process.env.BACKOFFICE_API_BASE_URL}/misc/pool-subscribers/${poolId}`
    );
    await Promise.all(
      subscribedChannels.map((channel) =>
        raiseEvent({
          event: "send-message-to-channel",
          payload: { channel, message },
        })
      )
    );
  } catch (error) {
    console.log(`error in sending message to pool ${payload.poolId}`);
    console.error(error);
  }
};
