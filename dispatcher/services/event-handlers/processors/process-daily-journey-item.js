const moment = require("moment");
const raiseEvent = require("../../events/processors/raise-event");

module.exports = async (payload, ack) => {
  try {
    ack();
    const {
      channelId,
      poolId,
      pipe: { createdAt },
    } = payload;
    console.log(
      `processing journey item for channel ${channelId} and pool ${poolId}`
    );
    const creationTime = moment(createdAt);
    console.log(creationTime);
    const now = moment();
    console.log(now);
    const dayIndex = now.diff(creationTime, "days");
    const eventTag = `d${dayIndex}`;
    console.log(`event tag: ${eventTag}`);

    raiseEvent({
      event: "process-journey-event",
      payload: {
        channelId,
        poolId,
        event: eventTag,
      },
    });
  } catch (err) {
    console.error(err);
  }
};
