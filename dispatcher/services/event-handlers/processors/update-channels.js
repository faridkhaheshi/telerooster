const sendInternalApiRequest = require("../../../adapters/api/send-internal-api-request");

module.exports = async (payload, ack) => {
  try {
    console.log("update channels event raised");
    ack();
    const result = await sendInternalApiRequest(
      `${process.env.WEBSITE_API_BASE_URL}/private/channels/update`,
      { method: "POST" }
    );
    console.log("update channels event finished:");
    console.log(result);
  } catch (err) {
    console.log(`error in running update teams event:`);
    console.error(err);
  }
};
