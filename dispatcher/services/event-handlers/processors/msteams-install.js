const addSubscriptionChannelsToNewTeam = require("../../../adapters/website/add-subscription-channels-to-new-team");
const getPostInstallEventsForUser = require("../../../adapters/website/get-post-install-events-for-user");
const registerNewMicrosoftTeam = require("../../../adapters/website/register-new-microsoft-team");
const raiseEvent = require("../../events/processors/raise-event");

module.exports = async (payload, ack) => {
  try {
    ack();
    const { team, profile } = await registerNewMicrosoftTeam(payload);
    const postInstallEvents = await getPostInstallEventsForUser({
      userId: profile.user.id,
      platform: "microsoft",
    });

    postInstallEvents.forEach(async (pe) => {
      await raiseEvent({
        event: pe.event,
        payload: { ...pe.payload, team, profile },
      });
    });
  } catch (error) {
    console.error(error);
  }
};
