const sendInternalApiRequest = require("../../../adapters/api/send-internal-api-request");
const {
  sendMessage,
  sendMessageToProfile,
} = require("../../messages/processors");

module.exports = async (payload, ack) => {
  try {
    ack();
    const { profileId, message } = payload;
    await sendMessageToProfile({ message, profileId });
  } catch (err) {
    console.error(err);
  }
};
