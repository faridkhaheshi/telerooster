const addTitleToMessage = require("./add-title-to-message");
const parseDocument = require("../../../adapters/panel/parse-document");
const sendInternalApiRequest = require("../../../adapters/api/send-internal-api-request");

const createMessage = async ({ subject, text, html, poolId }) => {
  let message;
  if (!html)
    message = {
      blocks: [{ type: "p", content: { markdown: text } }],
      text: text,
      title: subject,
    };
  else {
    message = { blocks: await parseDocument(html) };
    message.text = text;
    message.title = subject;
  }
  await addTitleToMessage({ title: subject, message });
  message.pool_id = poolId;
  const theMessage = await sendInternalApiRequest(
    `${process.env.WEBSITE_API_BASE_URL}/private/messages`,
    {
      method: "POST",
      body: message,
    }
  );
  return theMessage;
};

module.exports = createMessage;
