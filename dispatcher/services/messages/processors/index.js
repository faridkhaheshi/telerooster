module.exports = {
  sendMessage: require("./send-message"),
  addTitleToMessage: require("./add-title-to-message"),
  createMessage: require("./create-message"),
  sendMessageToProfile: require("./send-message-to-profile"),
};
