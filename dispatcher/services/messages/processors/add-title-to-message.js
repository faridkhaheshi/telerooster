const addTitleToMessage = async ({ message, title }) => {
  if (!title || !message.blocks || typeof message === "string") return message;
  message.blocks.unshift({
    type: "poster",
    content: {
      payload: {
        text_color: "black",
        // background_color: "#a83232",
        background_color: "white",
        title: title,
      },
      template: "public-title",
      alt: title,
    },
  });
  return message;
};

module.exports = addTitleToMessage;
