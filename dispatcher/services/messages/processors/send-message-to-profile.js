const sendMessage = require("./send-message");
const sendInternalApiRequest = require("../../../adapters/api/send-internal-api-request");

const sendMessageToProfile = async ({ message, profileId }) => {
  const profile = await sendInternalApiRequest(
    `${process.env.BACKOFFICE_API_BASE_URL}/profiles/${profileId}`
  );
  const options = {
    message,
    conversation: profile.conversation_ref,
    platform: profile.provider,
  };

  if (profile.provider === "slack") {
    const teamIdExt = profile.info.team_id;
    const [team] = await sendInternalApiRequest(
      `${process.env.BACKOFFICE_API_BASE_URL}/teams?filter=${encodeURIComponent(
        JSON.stringify({ id_ext: teamIdExt })
      )}`
    );
    options.teamId = team.id;
  }
  const result = await sendMessage(options);
  return result;
};

module.exports = sendMessageToProfile;
