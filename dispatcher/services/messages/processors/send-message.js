const sendMessageToMsteams = require("../../../adapters/msteamsbot/send-message-to-msteams");
const sendMessageToSlack = require("../../../adapters/slackbot/send-message-to-slack");
const getToken = require("../../../adapters/website/get-token");

const messageSenders = {
  microsoft: sendMessageToMsteams,
  slack: sendMessageToSlack,
};

const sendMessage = async ({ message, conversation, platform, teamId }) => {
  if (Object.keys(messageSenders).indexOf(platform) === -1) return;

  const sender = messageSenders[platform];
  if (platform === "microsoft") return sender({ message, conversation });
  if (platform === "slack") {
    const { info: installation } = await getToken({ team_id: teamId });
    return sender({ message, conversation, installation });
  }
};

module.exports = sendMessage;
