const { BadRequestError } = require("restify-errors");
const isSupportedEvent = require("./is-supported-event");
const connectToQueue = require("../../../adapters/rabbitmq");

const raiseEvent = async ({ event, payload = {} } = {}) => {
  if (!isSupportedEvent(event))
    throw new BadRequestError("Event not supported");
  const queue = connectToQueue(event);
  return queue.sendToQueue(event, payload);
};

module.exports = raiseEvent;
