const raiseEvent = require("../processors/raise-event");

module.exports = async (req, res, next) => {
  try {
    const result = await raiseEvent(req.body);
    res.json({ result });
  } catch (err) {
    next(err);
  }
};
