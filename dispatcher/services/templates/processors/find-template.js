const templates = require("./templates");

const findTemplate = (template) => {
  const templateNames = Object.keys(templates);
  if (templateNames.indexOf(template) > -1) return templates[template];
  return () => "template-not-found";
};

module.exports = findTemplate;
