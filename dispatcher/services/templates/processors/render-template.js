const findTemplate = require("./find-template");

const renderTemplate = ({ template, data }) => {
  const renderFunc = findTemplate(template);
  return renderFunc(data);
};

module.exports = renderTemplate;
