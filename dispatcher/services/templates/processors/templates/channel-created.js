const { baseUrl, panelBaseUrl } = require("./../../../../config");

module.exports = ({ pool, token }) => ({
  blocks: [
    {
      type: "h",
      content: {
        markdown: "New channel added to your team",
      },
      source: {
        nodeName: "h1",
      },
    },
    {
      type: "p",
      content: {
        markdown: `

title: _[${pool.title}](${baseUrl}/pools/${pool.id})_

description: _${pool.description}_

creator: _${pool.creator.display_name}_
`,
      },
    },
    { type: "hr" },
    {
      type: "buttonrow",
      content: {
        markdown: "You can always manage your subscriptions at your panel:",
        buttonText: "Go to panel",
        url: `${baseUrl}/auth?ref=${encodeURIComponent(
          `${panelBaseUrl}/subscriptions`
        )}&token=${token}`,
      },
    },
  ],
});
