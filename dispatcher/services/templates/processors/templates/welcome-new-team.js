const { baseUrl, panelBaseUrl } = require("../../../../config");

module.exports = ({ pool, token } = {}) => {
  return {
    blocks: [
      {
        type: "image",
        content: {
          image: {
            src: `${process.env.WEBSITE_BASE_URL}/images/ms-icon-150x150.png`,
            alt: "welcome",
          },
        },
      },
      { type: "h", content: { markdown: "Welcome to teleRooster" } },
      {
        type: "p",
        content: {
          markdown: `
With TeleRooster you can add newsletter channels to your team. 
These channels are like other channels, but they are managed by professional content creators. They enable you to receive high-quality posts.

You can always delete a channel to stop receiving new posts. So don't worry about spam.

To get a list of featured newsletters send \`\/newsletter\` to teleRooster.
`,
        },
      },
      {
        type: "buttonrow",
        content: {
          markdown: "Get featured channels:",
          buttonText: "Featured channels",
          actionId: "featured-pools",
        },
      },
      {
        type: "buttonrow",
        content: {
          markdown: "Manage your account:",
          buttonText: "Go to panel",
          url: `${baseUrl}/auth?ref=${encodeURIComponent(
            `${panelBaseUrl}/subscriptions`
          )}&token=${token}`,
        },
      },
    ],
  };
};
