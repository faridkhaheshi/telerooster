const fs = require("fs");
const path = require("path");
const basename = path.basename(__filename);

const templates = {};

fs.readdirSync(__dirname)
  .filter(
    (file) =>
      file.indexOf(".") !== 0 && file !== basename && file.slice(-3) === ".js"
  )
  .forEach((file) => {
    const template = require(path.join(__dirname, file));
    const templateName = file.split(".")[0];
    templates[templateName] = template;
  });

module.exports = templates;
