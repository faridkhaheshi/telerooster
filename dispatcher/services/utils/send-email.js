const mailgun = require("mailgun-js");

const mg = mailgun({
  apiKey: process.env.MAILGUN_API_KEY,
  domain: process.env.MAILGUN_DOMAIN,
});

const sendEmail = ({
  to,
  subject = undefined,
  text = undefined,
  html = undefined,
}) =>
  mg.messages().send({
    from: process.env.MAILGUN_SENDER,
    to,
    subject,
    text,
    html,
  });

module.exports = sendEmail;
