const restify = require("restify");
const handleEventRequests = require("./services/events/controllers/handle-event-requests");

const registerEventHandlers = require("./services/event-handlers/controllers/register-event-handlers");

const server = restify.createServer();
registerEventHandlers();

server.use(restify.plugins.bodyParser({ maxBodySize: 10 * 1024 * 1024 }));
server.post("/api/events", handleEventRequests);

server.listen(3000, () => {
  console.log(`${server.name} listening at ${server.url}`);
});
