module.exports = {
  baseUrl: `http://localhost:${process.env.TELEROOSTER_WEBSITE_PORT}`,
  panelBaseUrl: `http://localhost:${process.env.PANEL_PORT}`,
};
