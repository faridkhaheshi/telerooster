const appName = 'telerooster';
const deployToAddress = '/home/rooster/telerooster.com';
const gitRepo = 'https://gitlab.com/faridkhaheshi/telerooster.git';
// const serverSSHaddress = 'agreateadmin@194.5.193.23';
const serverSSHaddress = 'rooster@telerooster';

const workingDir = 'deploy';

module.exports = (shipit) => {
  require('shipit-deploy')(shipit);
  shipit.initConfig({
    default: {
      deployTo: deployToAddress,
      repositoryUrl: gitRepo,
      keepReleases: 5,
    },
    production: {
      servers: serverSSHaddress,
    },
  });

  shipit.blTask('update-server', async () => {
    await shipit.remote(`cd ${deployToAddress} && git pull origin master`);
    const envFileName = `.env.${shipit.environment}`;
    await shipit.local(`rm -rf ${workingDir} && mkdir ${workingDir}`);
    await shipit.local(`cp ${envFileName} ${workingDir}/.env`);
    await shipit.copyToRemote(`${workingDir}/.env`, deployToAddress);
    // await shipit.remote(`cd ${deployToAddress} && docker-compose restart`);
  });

  shipit.blTask('setup-server', async () => {
    await shipit.remote(`cd ${deployToAddress} && git pull origin master`);
    const envFileName = `.env.${shipit.environment}`;
    await shipit.local(`rm -rf ${workingDir} && mkdir ${workingDir}`);
    await shipit.local(`cp ${envFileName} ${workingDir}/.env`);
    await shipit.copyToRemote(`${workingDir}/.env`, deployToAddress);
    // await shipit.remote(`sudo chmod +x ${deployToAddress}/init-letsencrypt.sh`);
    // await shipit.remote(`sudo ${deployToAddress}/init-letsencrypt.sh`);
    // await shipit.remote(`cd ${deployToAddress} && docker-compose up -d`);
  });
};
