DROP TABLE IF EXISTS rooster.pools;

CREATE TABLE IF NOT EXISTS rooster.pools (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4 () NOT NULL,
  creator_id uuid NOT NULL,
  title varchar(50) NOT NULL,
  description varchar(1500) NOT NULL,
  is_free boolean NOT NULL DEFAULT FALSE,
  price_sm smallint CONSTRAINT non_negative_price_sm CHECK (price_sm > 0),
  price_md smallint CONSTRAINT non_negative_price_md CHECK (price_md > 0),
  price_lg smallint CONSTRAINT non_negative_price_lg CHECK (price_lg > 0),
  has_free_trial boolean DEFAULT TRUE,
  featured boolean NOT NULL DEFAULT FALSE,
  active boolean NOT NULL DEFAULT TRUE,
  created_at timestamp NOT NULL DEFAULT NOW(),
  updated_at timestamp NOT NULL DEFAULT NOW(),
  CONSTRAINT fk_creator FOREIGN KEY (creator_id) REFERENCES rooster.users (id)
);

DROP TRIGGER IF EXISTS set_timestamp ON rooster.pools;

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON rooster.pools
  FOR EACH ROW
  EXECUTE PROCEDURE rooster.trigger_set_timestamp ();

