DROP TABLE IF EXISTS rooster.users;

CREATE TABLE IF NOT EXISTS rooster.users (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4 () NOT NULL,
  first_name varchar(50),
  last_name varchar(50),
  display_name varchar(100),
  email rooster.email CONSTRAINT unique_email UNIQUE,
  password_hash text,
  active boolean NOT NULL DEFAULT TRUE,
  created_at timestamp NOT NULL DEFAULT NOW(),
  updated_at timestamp NOT NULL DEFAULT NOW()
);

DROP TRIGGER IF EXISTS set_timestamp ON rooster.users;

CREATE TRIGGER set_timestamp
  BEFORE UPDATE ON rooster.users
  FOR EACH ROW
  EXECUTE PROCEDURE rooster.trigger_set_timestamp ();

