const {
  handleSendMessageRequest,
} = require("../../../services/messages/controllers");

module.exports = (server) => {
  server.post("/papi/messages", handleSendMessageRequest);
};
