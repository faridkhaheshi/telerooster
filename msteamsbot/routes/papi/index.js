const restify = require("restify");

module.exports = (server) => {
  server.use(restify.plugins.bodyParser());
  require("./messages")(server);
};
