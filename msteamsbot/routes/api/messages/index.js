const { botAdapter, bot } = require("../../../bot");

module.exports = (server) => {
  server.post("/api/messages", (req, res) => {
    botAdapter.processActivity(req, res, async (context) => {
      await bot.run(context);
    });
  });
};
