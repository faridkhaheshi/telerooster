const buildCustomBlock = ({ content: { markdown } }) => ({
  type: "TextBlock",
  text: markdown,
  wrap: true,
});

module.exports = buildCustomBlock;
