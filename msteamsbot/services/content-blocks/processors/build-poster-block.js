const generatePosterImageUrl = require("../utils/generate-poster-image-url");

const buildPosterBlock = ({
  content: { template, payload, alt = "poster" },
}) => {
  if (!template) return undefined;
  return {
    type: "Image",
    url: generatePosterImageUrl({ payload, template }),
    altText: alt || "image",
    horizontalAlignment: "center",
  };
};

module.exports = buildPosterBlock;
