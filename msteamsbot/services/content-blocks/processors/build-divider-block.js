const buildDividerBlock = () => ({
  type: "TextBlock",
  text: " ",
  wrap: true,
  separator: true,
  spacing: "ExtraLarge",
  horizontalAlignment: "Center",
  size: "Small",
  weight: "Lighter",
});

module.exports = buildDividerBlock;
