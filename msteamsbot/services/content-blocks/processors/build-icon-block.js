const buildPBlock = require("./build-p-block");

const buildIconBlock = ({ content: { markdown, image = {}, link = {} } }) => {
  if (markdown) return buildPBlock({ content: { markdown } });
  if (image) {
    if (!image.src && !link.href) return undefined;
    return {
      type: "Image",
      url: image.src,
      altText: image.alt || "image",
      spacing: "ExtraLarge",
      selectAction: {
        type: "Action.OpenUrl",
        title: image.alt,
        url: link.href,
      },
      horizontalAlignment: "center",
    };
  }
  return undefined;
};

module.exports = buildIconBlock;
