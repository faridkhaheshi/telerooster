const buildUlBlock = ({ content: { markdown } }) => ({
  type: "TextBlock",
  text: markdown,
  wrap: true,
});

module.exports = buildUlBlock;
