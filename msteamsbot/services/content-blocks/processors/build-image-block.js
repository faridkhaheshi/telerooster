const buildImageBlock = ({
  content: { image: { src, alt }, caption } = {},
}) => {
  if (!src) return undefined;
  if (!caption)
    return {
      type: "Image",
      url: src,
      altText: alt || "image",
      horizontalAlignment: "center",
    };

  return [
    {
      type: "Image",
      url: src,
      altText: alt || caption,
    },
    {
      type: "Container",
      items: [
        {
          type: "TextBlock",
          text: caption,
          wrap: true,
          spacing: "None",
          size: "Small",
          weight: "Lighter",
          isSubtle: true,
        },
      ],
      style: "accent",
      bleed: false,
      spacing: "None",
    },
  ];
};

module.exports = buildImageBlock;
