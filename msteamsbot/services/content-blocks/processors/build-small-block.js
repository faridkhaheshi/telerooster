const buildSmallBlock = ({ content: { markdown } }) => ({
  type: "TextBlock",
  text: markdown,
  wrap: true,
  size: "Small",
});

module.exports = buildSmallBlock;
