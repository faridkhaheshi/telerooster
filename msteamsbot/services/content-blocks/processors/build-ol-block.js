const buildOlBlock = ({ content: { markdown } }) => ({
  type: "TextBlock",
  text: markdown,
  wrap: true,
});

module.exports = buildOlBlock;
