const buildPBlock = ({ content: { markdown } = {} }) => {
  if (markdown.length === 0) return [];
  return {
    type: "TextBlock",
    text: markdown,
    wrap: true,
  };
};

module.exports = buildPBlock;
