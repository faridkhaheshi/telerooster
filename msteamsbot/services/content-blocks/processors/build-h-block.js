const buildHBlock = ({
  source: { nodeName = "h2" } = {},
  content: { markdown } = {},
}) => {
  const blockText = `**${markdown.replace(/\n/g, "")}**`;
  switch (nodeName) {
    case "h1":
      return buildH1Block(blockText);
    case "h2":
      return buildH2Block(blockText);
    case "h3":
      return buildH3Block(blockText);
    case "h4":
      return buildH4Block(blockText);
    case "h5":
      return buildH4Block(blockText);
    case "h6":
      return buildH4Block(blockText);
    default:
      return buildH2Block(blockText);
  }
};

module.exports = buildHBlock;

function buildH1Block(text) {
  return {
    type: "TextBlock",
    text: text,
    wrap: true,
    spacing: "ExtraLarge",
    size: "ExtraLarge",
    weight: "Bolder",
  };
}

function buildH2Block(text) {
  return {
    type: "TextBlock",
    text: text,
    wrap: true,
    spacing: "ExtraLarge",
    size: "Large",
    weight: "Bolder",
  };
}

function buildH3Block(text) {
  return {
    type: "TextBlock",
    text: text,
    wrap: true,
    spacing: "Large",
    size: "Medium",
    weight: "Bolder",
  };
}

function buildH4Block(text) {
  return {
    type: "TextBlock",
    text: text,
    wrap: true,
    spacing: "Large",
    weight: "Bolder",
  };
}
