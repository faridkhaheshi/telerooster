const { sendMessage, buildMsteamsMessage } = require("../processors");

const handleSendMessageRequest = async (req, res, next) => {
  try {
    const {
      body: { conversation, message },
    } = req;
    const result = await sendMessage({
      message: buildMsteamsMessage(message),
      conversation,
    });
    res.json({ done: true, data: result });
    next();
  } catch (err) {
    next(err);
  }
};

module.exports = { handleSendMessageRequest };
