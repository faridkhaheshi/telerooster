module.exports = {
  ...require("./send-message"),
  buildMsteamsMessage: require("./build-msteams-message"),
};
