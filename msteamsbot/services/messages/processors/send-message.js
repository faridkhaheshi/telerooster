const { teamsNotifyUser } = require("botbuilder");
const { botAdapter } = require("../../../bot");

const sendMessage = ({ message, conversation }) =>
  new Promise(async (resolve, reject) => {
    {
      try {
        await botAdapter.continueConversation(conversation, async (context) => {
          if (typeof message !== "string") teamsNotifyUser(message);
          const result = await context.sendActivity(message);
          resolve(result);
        });
      } catch (error) {
        reject(error);
      }
    }
  });

module.exports = { sendMessage };
