const { MessageFactory, CardFactory } = require("botbuilder");
const convertContentBlockToMessageBlock = require("../../content-blocks/processors/convert-content-block-to-message-block");

const MAX_BLOCKS = 50;
// The maximum message size: ~28 KB per post4 -> https://docs.microsoft.com/en-us/microsoftteams/limits-specifications-teams#teams-and-channels

const buildMsteamMessage = (message) => {
  const finalMessage =
    typeof message === "string" ? { text: message } : message;

  const { text = "new message...", blocks = [] } = finalMessage;

  if (blocks.length === 0) return MessageFactory.text(text);

  const card = CardFactory.adaptiveCard({
    $schema: "http://adaptivecards.io/schemas/adaptive-card.json",
    type: "AdaptiveCard",
    version: "1.2",
    body: blocks
      .reduce((acc, blk) => {
        const messageBlock = convertContentBlockToMessageBlock(blk);
        if (!messageBlock) return acc;
        return Array.isArray(messageBlock)
          ? [...acc, ...messageBlock]
          : [...acc, messageBlock];
      }, [])
      .filter((blk) => blk)
      .slice(0, MAX_BLOCKS),
  });

  return MessageFactory.attachment(card);
};

module.exports = buildMsteamMessage;
