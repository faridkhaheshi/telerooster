const fetch = require("node-fetch");
const { InternalServerError } = require("restify-errors");

const raiseEvent = async ({ event, payload }) => {
  const response = await fetch(process.env.DISPATCHER_URL, {
    method: "POST",
    body: JSON.stringify({ event, payload }),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });
  if (response.status !== 200)
    throw new InternalServerError("Raising event failed for unknown reasons");
};

module.exports = raiseEvent;
