module.exports = {
  registerInstallation: require("./register-installation"),
  registerUninstall: require("./register-uninstall"),
};
