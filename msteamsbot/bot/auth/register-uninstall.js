const raiseEvent = require("../../adapters/dispatcher/raise-event");

const registerUninstall = async (context) => {
  try {
    const {
      activity: {
        channelData: {
          team: { id: teamId },
        },
      },
    } = context;
    await raiseEvent({
      event: "msteams-uninstall",
      payload: { team: { id_ext: teamId } },
    });
  } catch (error) {
    console.error(error);
    return;
  }
};

module.exports = registerUninstall;
