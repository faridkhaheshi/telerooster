const raiseEvent = require("../../adapters/dispatcher/raise-event");
const extractNewTeamInfo = require("../team-info/extract-new-team-info");

const registerInstallation = async (context) => {
  try {
    const teamInfo = await extractNewTeamInfo(context);
    await raiseEvent({ event: "msteams-install", payload: teamInfo });
  } catch (error) {
    console.error(error);
    return;
  }
};

module.exports = registerInstallation;
