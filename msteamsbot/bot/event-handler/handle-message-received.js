const handleMessageReceived = async (context, next) => {
  console.log("message");
  const text = context.activity.text.trim().toLocaleLowerCase();
  await context.sendActivity("You said " + text);
};

module.exports = handleMessageReceived;
