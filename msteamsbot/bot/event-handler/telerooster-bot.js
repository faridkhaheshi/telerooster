const { TeamsActivityHandler } = require("botbuilder");
const handleMembersAdded = require("./handle-members-added");
const handleMembersRemoved = require("./handle-members-removed");
const handleMessageReceived = require("./handle-message-received");

class TeleroosterBot extends TeamsActivityHandler {
  constructor() {
    super();

    this.onTurn(async (context, next) => {
      await next();
    });
    this.onMessage(handleMessageReceived);
    this.onMembersAdded(handleMembersAdded);
    this.onMembersRemoved(handleMembersRemoved);
  }
}

const bot = new TeleroosterBot();

module.exports = bot;
