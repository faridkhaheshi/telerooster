const registerInstallation = require("../auth/register-installation");
const updateTeamInfo = require("../team-info/update-team-info");

const handleMembersAdded = async (context, next) => {
  await next();
  const eventType = identifyTypeOfAddingMemberEvent(context);
  console.log(eventType);
  if (isNewInstallation(eventType)) {
    await registerInstallation(context);
  } else {
    await updateTeamInfo(context);
  }
};

module.exports = handleMembersAdded;

function isNewInstallation(eventType) {
  return (
    [
      "bot-added-to-channel",
      "bot-added-to-group-chat",
      "bot-added-to-personal-chat",
      "bot-added-to-team",
    ].indexOf(eventType) > -1
  );
}

function identifyTypeOfAddingMemberEvent(context) {
  try {
    const botId = context.activity.recipient.id;
    const membersAdded = context.activity.membersAdded;
    const membersAddedIds = membersAdded.map((mem) => mem.id);
    const conversationType = context.activity.conversation.conversationType;

    const whoAdded = membersAddedIds.includes(botId) ? "bot" : "someone";

    if (conversationType === "personal")
      return `${whoAdded}-added-to-personal-chat`;
    else if (conversationType === "groupChat")
      return `${whoAdded}-added-to-group-chat`;
    else if (conversationType === "channel") {
      const conversationId = context.activity.conversation.id;
      const teamId = context.activity.channelData.team.id;
      const channelType = teamId === conversationId ? "team" : "channel";
      return `${whoAdded}-added-to-${channelType}`;
    }

    return "unknown";
  } catch (err) {
    return "unknown";
  }
}
