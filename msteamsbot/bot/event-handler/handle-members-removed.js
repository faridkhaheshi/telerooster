const registerUninstall = require("../auth/register-uninstall");
const updateTeamInfo = require("../team-info/update-team-info");

const handleMembersRemoved = async (context, next) => {
  await next();
  const eventType = identifyTypeOfRemovingMemberEvent(context);
  console.log(eventType);
  if (isUninstall(eventType)) {
    await registerUninstall(context);
  } else {
    await updateTeamInfo(context);
  }
};

module.exports = handleMembersRemoved;

function isUninstall(eventType) {
  return (
    [
      "bot-removed-from-channel",
      "bot-removed-from-group-chat",
      "bot-removed-from-team",
    ].indexOf(eventType) > -1
  );
}

function identifyTypeOfRemovingMemberEvent(context) {
  const botId = context.activity.recipient.id;
  const membersRemoved = context.activity.membersRemoved;
  const membersRemovedIds = membersRemoved.map((mem) => mem.id);
  const conversationType = context.activity.conversation.conversationType;

  const whoRemoved = membersRemovedIds.includes(botId) ? "bot" : "someone";

  if (conversationType === "personal")
    return `${whoRemoved}-removed-from-personal-chat`;
  else if (conversationType === "groupChat")
    return `${whoRemoved}-removed-from-group-chat`;
  else if (conversationType === "channel") {
    const conversationId = context.activity.conversation.id;
    const teamId = context.activity.channelData.team.id;
    const channelType = teamId === conversationId ? "team" : "channel";
    return `${whoRemoved}-removed-from-${channelType}`;
  }

  return "unknown";
}
