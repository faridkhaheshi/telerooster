const { BotFrameworkAdapter } = require("botbuilder");

const buildBotAdapter = () => {
  const theAdapter = new BotFrameworkAdapter({
    appId: process.env.MICROSOFT_BOT_ID,
    appPassword: process.env.MICROSOFT_BOT_SECRET,
  });
  theAdapter.onTurnError = async (context, error) => {
    console.error(`\n [onTurnError] unhandled error: ${error}`);
    await context.sendTraceActivity(
      "OnTurnError Trace",
      `${error}`,
      "https://www.botframework.com/schemas/error",
      "TurnError"
    );
  };

  return theAdapter;
};

const botAdapter = buildBotAdapter();

module.exports = {
  botAdapter,
  buildBotAdapter,
};
