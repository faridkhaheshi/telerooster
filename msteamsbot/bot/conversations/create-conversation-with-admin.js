const { TurnContext } = require("botbuilder");

const createConversationWithAdmin = async (adapter, teamConversationRef) =>
  new Promise((resolve, reject) => {
    adapter.createConversation(teamConversationRef, (adminConvContext) => {
      try {
        const adminConversationRef = TurnContext.getConversationReference(
          adminConvContext.activity
        );
        resolve(adminConversationRef);
      } catch (error) {
        reject(error);
      }
    });
  });

module.exports = createConversationWithAdmin;
