const { TurnContext, TeamsInfo } = require("botbuilder");
const createConversationWithAdmin = require("../conversations/create-conversation-with-admin");

const extractNewTeamInfo = async (context) => {
  const {
    adapter,
    activity,
    activity: {
      from: { aadObjectId: userId },
    },
  } = context;
  const teamConversationRef = TurnContext.getConversationReference(activity);
  const channels = await TeamsInfo.getTeamChannels(context);
  const team = await TeamsInfo.getTeamDetails(context);
  const admin = await TeamsInfo.getMember(context, userId);
  const adminConversationRef = await createConversationWithAdmin(
    adapter,
    teamConversationRef
  );

  return {
    team,
    teamConversationRef,
    channels,
    admin,
    adminConversationRef,
  };
};

module.exports = extractNewTeamInfo;
