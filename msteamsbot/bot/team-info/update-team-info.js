const { TeamsInfo } = require("botbuilder");
const raiseEvent = require("../../adapters/dispatcher/raise-event");

const updateTeamInfo = async (context) => {
  try {
    const channels = await TeamsInfo.getTeamChannels(context);
    const team = await TeamsInfo.getTeamDetails(context);
    await raiseEvent({
      event: "msteams-teaminfo-update",
      payload: { channels, team },
    });
  } catch (error) {
    console.error(error);
    return;
  }
};

module.exports = updateTeamInfo;
