# Telerooster

Telerooster enable owners of email newsletters to send their content in Slack and Microsoft Teams with minimum amount of work. It does this by giving them two "Add to" buttons: one for adding the newsletter to Slack (Add to Slack) and the other for adding it to MS Teams (Add to Teams). To connect the newsletter to Telerooster the owners receives an email address and she adds this email address to her newsletter. That's it. Every new message will be sent to channels in Slack and MS Teams.


## Starting the project

To start the project for development, you should do the following:

1. Run reverse proxies to expose some apis. To run these proxies, you can use *ngrok*, update the config file as explained below and just run `npm run tunnel`,
2. Create a `.env` file from the provided `.env.example`,
3. Run the infrastructure by `docker-compose up --build -d`,
4. Create the database models by running `npm run migrate`,
5. Fill the database with seed data by running `npm run seed`,

## Running a reverse proxy to expose APIs to Slack and MS Teams

Here, we use ngrok. You should put a config file with the following structure in `~/.ngrok2/ngrok.yml`:

```
authtoken: [AUTH TOKEN FROM NGROK]

tunnels:
  telerooster:
    proto: http
    hostname: dev.telerooster.ngrok.io
    addr: 127.0.0.1:[TELEROOSTER_WEBSITE_PORT]
  # msteams:
  #   proto: http
  #   hostname: msteams.dev.telerooster.ngrok.io
  #   addr: 127.0.0.1:[MSTEAMSBOT_PORT]
  slack:
    proto: http
    hostname: slack.dev.telerooster.ngrok.io
    addr: 127.0.0.1:[SLACKBOT_PORT]
  painter:
    proto: http
    hostname: painter.dev.telerooster.ngrok.io
    addr: 127.0.0.1:[PAINTER_PORT]
  panel:
    proto: http
    hostname: panel.dev.telerooster.ngrok.io
    addr: 127.0.0.1:[PANEL_PORT]
```

Then use the following addresses for the api uri of the Slack BOT: `slack.dev.telerooster.ngrok.io/slack/events`