import { parseDocument } from "../processors";

export const handleParseDocumentReq = async (req, res) => {
  try {
    const blocks = parseDocument(req.body);
    return res.json(blocks);
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
