import { parseDocument } from "../processors";

export const handleParseDocumentJsonReq = async (req, res) => {
  try {
    const blocks = parseDocument(req.body.doc);
    return res.json(blocks);
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
