import { createDomElementServer } from "./create-dom-element-server";
import { convertHtmlToContentBlocks } from "../../../features/Editor/services/parsing";
import { processLinks } from "../../../features/link-processing";
import { removeContentmail } from "../../../features/remove-contentmail";

export const parseDocument = (html = "") => {
  const processedHtml = removeContentmail(processLinks(html));
  const blocks = convertHtmlToContentBlocks(processedHtml, {
    createElement: createDomElementServer,
  });
  return blocks;
};
