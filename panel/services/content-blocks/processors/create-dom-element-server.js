import { JSDOM } from "jsdom";
import serializer from "xmlserializer";

const dom = new JSDOM(`<!DOCTYPE html><html><body></body></html>`);

export const createDomElementServer = ({ element, options = [], nodeData }) => {
  const resultingElement = options
    .reverse()
    .reduce((acc, { tag, attrs = {} }) => {
      const newElement = dom.window.document.createElement(tag);
      Object.keys(attrs).forEach((attr) => {
        newElement.setAttribute(attr, attrs[attr]);
      });
      newElement.innerHTML = acc;
      return serializer.serializeToString(newElement);
    }, element);
  return resultingElement;
};
