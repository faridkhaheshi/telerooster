module.exports = {
  websiteUrl: process.env.NEXT_PUBLIC_WEBSITE_BASE_URL,
  baseUrl: process.env.NEXT_PUBLIC_PANEL_BASE_URL,
  google: {
    recaptchaSiteKey: process.env.NEXT_PUBLIC_GOOGLE_RECAPTCHA_SITE_KEY,
  },
  stripe: {
    publicKey: process.env.NEXT_PUBLIC_STRIPE_PUBLIC_KEY,
  },
};
