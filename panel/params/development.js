module.exports = {
  websiteUrl: `http://localhost:${process.env.NEXT_PUBLIC_WEBSITE_PORT}`,
  baseUrl: `http://localhost:${process.env.NEXT_PUBLIC_PANEL_PORT}`,
  google: {
    recaptchaSiteKey: process.env.NEXT_PUBLIC_GOOGLE_RECAPTCHA_SITE_KEY,
  },
  stripe: {
    publicKey: process.env.NEXT_PUBLIC_STRIPE_PUBLIC_KEY,
  },
};
