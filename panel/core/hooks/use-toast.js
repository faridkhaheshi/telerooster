import { toast } from "react-toastify";
import { Media } from "./../../components";

const defaultOptions = {
  autoClose: 5000,
  hideProgressBar: true,
  position: toast.POSITION.TOP_RIGHT,
  pauseOnHover: true,
};

const useToast = () => {
  const success = React.useCallback((content, options = defaultOptions) => {
    toast.success(buildSuccessMessage.bind(null, content), options);
  }, []);

  const info = React.useCallback((content, options = defaultOptions) => {
    toast.info(buildInfoMessage.bind(null, content));
  }, []);

  const warn = React.useCallback((content, options = defaultOptions) => {
    toast.warn(buildWarnMessage.bind(null, content), options);
  }, []);

  const danger = React.useCallback((content, options = defaultOptions) => {
    toast.error(buildDangerMessage.bind(null, content), options);
  }, []);

  const message = React.useCallback((content, options = defaultOptions) => {
    toast(buildDefaultMessage.bind(null, content), options);
  }, []);

  const handleError = React.useCallback(
    (errMsg) => danger({ title: "Error", message: errMsg }),
    [danger]
  );

  return { toast, success, info, warn, danger, message, handleError };
};

export default useToast;

function buildSuccessMessage(
  { title = "Success!", message = "Done." } = {},
  { closeToast }
) {
  return (
    <Media>
      <Media middle left className="mr-3">
        <i className="fa fa-fw fa-2x fa-check"></i>
      </Media>
      <Media body>
        <Media heading tag="h6">
          {title}
        </Media>
        <p>{message}</p>
      </Media>
    </Media>
  );
}

function buildInfoMessage(
  { title = "Information!", message = "Done." } = {},
  { closeToast }
) {
  return (
    <Media>
      <Media middle left className="mr-3">
        <i className="fa fa-fw fa-2x fa-info"></i>
      </Media>
      <Media body>
        <Media heading tag="h6">
          {title}
        </Media>
        <p>{message}</p>
      </Media>
    </Media>
  );
}

function buildWarnMessage(
  { title = "Warning!", message = "Done." } = {},
  { closeToast }
) {
  return (
    <Media>
      <Media middle left className="mr-3">
        <i className="fa fa-fw fa-2x fa-exclamation"></i>
      </Media>
      <Media body>
        <Media heading tag="h6">
          {title}
        </Media>
        <p>{message}</p>
      </Media>
    </Media>
  );
}

function buildDangerMessage(
  { title = "Error!", message = "Done." } = {},
  { closeToast }
) {
  return (
    <Media>
      <Media middle left className="mr-3">
        <i className="fa fa-fw fa-2x fa-close"></i>
      </Media>
      <Media body>
        <Media heading tag="h6">
          {title}
        </Media>
        <p>{message}</p>
      </Media>
    </Media>
  );
}

function buildDefaultMessage(
  { title = "Attention!", message = "Done." } = {},
  { closeToast }
) {
  return (
    <Media>
      <Media middle left className="mr-3">
        <i className="fa fa-fw fa-2x fa-question"></i>
      </Media>
      <Media body>
        <Media heading tag="h6">
          {title}
        </Media>
        <p>{message}</p>
      </Media>
    </Media>
  );
}
