import React from "react";

const RESET_SECONDS = 1;

const useCopyToClipboard = (reset = true) => {
  const [copied, setCopied] = React.useState(false);
  const timer = React.useRef();

  const copy = React.useCallback((text) => {
    if (!copied) {
      const result = copyToClipboard(text);
      setCopied(result);
      if (result && reset) {
        timer.current = setTimeout(() => {
          setCopied(false);
        }, 1000 * RESET_SECONDS);
      }
    }
  }, []);

  React.useEffect(() => () => setCopied(false), []);

  React.useEffect(() => {
    return () => {
      if (timer && timer.current) {
        clearTimeout(timer.current);
      }
    };
  }, []);

  return { copy, copied };
};

export default useCopyToClipboard;

function copyToClipboard(str) {
  const el = document.createElement("textarea");
  el.value = str;
  el.setAttribute("readonly", "");
  el.style.position = "absolute";
  el.style.left = "-9999px";
  document.body.appendChild(el);
  const selected =
    document.getSelection().rangeCount > 0
      ? document.getSelection().getRangeAt(0)
      : false;
  el.select();
  const success = document.execCommand("copy");
  document.body.removeChild(el);
  if (selected) {
    document.getSelection().removeAllRanges();
    document.getSelection().addRange(selected);
  }
  return success;
}
