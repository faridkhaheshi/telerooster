import React from "react";
import useSWR, { mutate } from "swr";
import { sendApiRequest } from "../../adapters/api/send-api-request";

const useServerResource = (url, options = {}) => {
  const { data, error } = useSWR(url, sendApiRequest, options);
  const refresh = React.useCallback(() => {
    mutate(url);
  }, [url]);

  return { data, isLoading: !error && !data, error, refresh };
};

export default useServerResource;
