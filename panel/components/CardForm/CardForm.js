import React from "react";
import PropTypes from "prop-types";

import {
  Row,
  Col,
  Card,
  Form,
  CardBody,
  CardTitle,
  CardFooter,
  ThreeStateButton,
} from "./../.";
const CardForm = ({
  title,
  hideRequiredHint = false,
  buttonText = "Submit",
  children,
  isProcessing = false,
  waitingText = "Submitting...",
  disabled = false,
  ...rest
}) => (
  <Row>
    <Col lg={12}>
      <Form {...rest}>
        <Card className="mb-3">
          <CardBody>
            <HeaderRow title={title} hideRequiredHint={hideRequiredHint} />
            {children}
          </CardBody>
          <CardFooter className="text-right">
            <ThreeStateButton
              color="primary"
              isProcessing={isProcessing}
              waitingContent={waitingText}
              disabled={disabled}
            >
              {buttonText}
            </ThreeStateButton>
          </CardFooter>
        </Card>
      </Form>
    </Col>
  </Row>
);

CardForm.propTypes = {
  title: PropTypes.string,
  hideRequiredHint: PropTypes.bool,
  buttonText: PropTypes.string,
  children: PropTypes.node,
  isProcessing: PropTypes.bool,
  waitingText: PropTypes.string,
};

CardForm.defaultProps = {
  hideRequiredHint: false,
  buttonText: "Submit",
  isProcessing: false,
  waitingText: "Submitting...",
};

export { CardForm };

export function HeaderRow({ title, hideRequiredHint }) {
  if (!title && hideRequiredHint) return null;
  return (
    <div className="d-flex mb-4">
      {title && <CardTitle tag="h6">{title}</CardTitle>}
      {!hideRequiredHint && (
        <span className="ml-auto align-self-start small">
          Fields marked as <span className="text-danger">*</span> are required.
        </span>
      )}
    </div>
  );
}
