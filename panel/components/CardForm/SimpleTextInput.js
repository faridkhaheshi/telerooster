import React from "react";
import PropTypes from "prop-types";

import { Col, FormGroup, Label, Input, FormText } from "./../.";

const SimpleTextInput = ({
  id,
  label,
  required = false,
  guideText,
  ...otherProps
}) => (
  <FormGroup row>
    <Label for={id} sm={3}>
      {required ? (
        <>
          {`${label} `}
          <span className="text-danger">*</span>:
        </>
      ) : (
        `${label}:`
      )}
    </Label>
    <Col sm={8}>
      <Input id={id} {...otherProps} required={required} />
      {guideText && <FormText>{guideText}</FormText>}
    </Col>
  </FormGroup>
);

SimpleTextInput.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  required: PropTypes.bool,
  guideText: PropTypes.string,
};

export { SimpleTextInput };
