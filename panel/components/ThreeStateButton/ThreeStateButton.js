import PropTypes from "prop-types";
import { Button } from "reactstrap";

const ThreeStateButton = ({
  children,
  isProcessing = false,
  waitingContent = "processing...",
  disabled = false,
  ...otherProps
}) => {
  return (
    <Button disabled={disabled || isProcessing} {...otherProps}>
      {isProcessing && !disabled ? waitingContent : children}
    </Button>
  );
};

ThreeStateButton.propTypes = {
  waitingContent: PropTypes.string,
  isProcessing: PropTypes.bool,
};

export { ThreeStateButton };
