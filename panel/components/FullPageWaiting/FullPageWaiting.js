import React from "react";
import { Spinner } from "reactstrap";

import "./FullPageWaiting.css";

const FullPageWaiting = ({ show = true }) => {
  if (show)
    return (
      <main className="full-page-waiting-container">
        <Spinner />
      </main>
    );
  return null;
};

export { FullPageWaiting };
