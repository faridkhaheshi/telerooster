import React from "react";
import PropTypes from "prop-types";

import { Spinner, CardBody } from "reactstrap";

import { Card } from "./Card";

const AsyncCard = ({ children, errorMessage, isLoading, ...otherProps }) => {
  if (isLoading)
    return (
      <Card {...otherProps}>
        <CardBody className="d-flex flex-direction-column justify-content-center align-items-center">
          <Spinner color="secondary" />
        </CardBody>
      </Card>
    );
  if (errorMessage)
    return (
      <Card {...otherProps}>
        <CardBody className="text-center text-danger">
          <i className="fa fa-times-circle mr-1 alert-icon"></i>
          {errorMessage || "failed to load content"}
        </CardBody>
      </Card>
    );
  return <Card {...otherProps}>{children}</Card>;
};

AsyncCard.propTypes = {
  ...Card.propTypes,
  errorMessage: PropTypes.string,
  isLoading: PropTypes.bool,
};

AsyncCard.defaultProps = {
  isLoading: false,
  errorMessage: undefined,
};

export { AsyncCard };
