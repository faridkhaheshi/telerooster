import React from "react";
import PropTypes from "prop-types";

import Link from "./../Link";

const ErrorMessage = (props) => (
  <div className="mb-4">
    <div className="mb-4 text-center">
      <Link to="/" className="d-inline-block">
        {props.icon ? (
          <i className={`fa fa-${props.icon} fa-3x ${props.iconClassName}`}></i>
        ) : null}
      </Link>
    </div>
    <h5 className="text-center mb-4">{props.message}</h5>
  </div>
);

ErrorMessage.propTypes = {
  icon: PropTypes.node,
  iconClassName: PropTypes.node,
  message: PropTypes.string,
};

ErrorMessage.defaultProps = {
  message: "Something went wrong...",
  iconClassName: "text-danger",
  icon: "close",
};

export { ErrorMessage };
