import React from "react";
import PropTypes from "prop-types";
import { Helmet } from "react-helmet";

const CustomHead = ({ title = "Admin panel @teleRooster" }) => (
  <Helmet>
    <title>{title}</title>
  </Helmet>
);

CustomHead.propTypes = {
  title: PropTypes.string,
};
CustomHead.defaultProps = {
  title: "Admin panel @teleRooster",
};

export { CustomHead };
