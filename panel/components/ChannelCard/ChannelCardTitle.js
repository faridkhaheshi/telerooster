import React from "react";
import PropTypes from "prop-types";
import { ChannelCardMenu } from "./ChannelCardMenu";

const ChannelCardTitle = ({ featured, title, id, dropdownMenu }) => {
  return (
    <div className="d-flex justify-content-center align-items-center">
      <h5 className="p-0 m-0">
        {title} {featured && <i className="fa fa-check text-success"></i>}
      </h5>
      <ChannelCardMenu id={id} dropdownMenu={dropdownMenu} />
    </div>
  );
};

ChannelCardTitle.propTypes = {
  featured: PropTypes.bool,
  title: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  dropdownMenu: PropTypes.element,
};

ChannelCardTitle.defaultProps = {
  featured: false,
  dropdownMenu: undefined,
};

export { ChannelCardTitle };
