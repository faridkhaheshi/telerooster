import React from "react";
import PropTypes from "prop-types";

import {
  UncontrolledButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Link,
} from "./../.";
import { websiteUrl } from "./../../params";

const ChannelCardMenu = ({ dropdownMenu, id }) => (
  <UncontrolledButtonDropdown className="ml-auto" direction="up">
    <DropdownToggle color="link" size="sm">
      <i className="fa fa-ellipsis-v" />
    </DropdownToggle>
    {dropdownMenu || renderDefaultMenu(id)}
  </UncontrolledButtonDropdown>
);

ChannelCardMenu.propTypes = {
  id: PropTypes.string.isRequired,
  dropdownMenu: PropTypes.element,
};

ChannelCardMenu.defaultProps = {
  dropdownMenu: undefined,
};

export { ChannelCardMenu };

function renderDefaultMenu(id) {
  return (
    <DropdownMenu right>
      <DropdownItem tag={Link} to={`${websiteUrl}/pools/${id}`} target="_blank">
        Open Channel Page
      </DropdownItem>
    </DropdownMenu>
  );
}
