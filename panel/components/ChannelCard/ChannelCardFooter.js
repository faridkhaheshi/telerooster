import React from "react";
import PropTypes from "prop-types";

import { CardFooter, UncontrolledTooltip } from "./../.";
import { websiteUrl } from "./../../params";

const ChannelCardFooter = ({ id, membersCount, teamsCount }) => (
  <CardFooter className="d-flex bt-0">
    {(membersCount || membersCount === 0) && (
      <>
        <span className="mr-3" id={`members-for-${id}`}>
          <i className="fa fa-fw fa-user mr-1"></i>{" "}
          <span className="text-inverse">{membersCount}</span>
        </span>
        <UncontrolledTooltip placement="top" target={`members-for-${id}`}>
          Number of individuals receiving contents from this channel
        </UncontrolledTooltip>
      </>
    )}
    {(teamsCount || teamsCount === 0) && (
      <>
        <span id={`teams-for-${id}`}>
          <i className="fa fa-fw fa-group mr-1"></i>{" "}
          <span className="text-inverse">{teamsCount}</span>
        </span>
        <UncontrolledTooltip placement="top" target={`teams-for-${id}`}>
          Number of Teams following this channel
        </UncontrolledTooltip>
      </>
    )}
    <a href={`${websiteUrl}/pools/${id}`} target="_blank" className="ml-auto">
      <i className="fa fa-external-link"></i>
    </a>
  </CardFooter>
);

ChannelCardFooter.propTypes = {
  id: PropTypes.string.isRequired,
  membersCount: PropTypes.number,
  teamsCount: PropTypes.number,
};

export { ChannelCardFooter };
