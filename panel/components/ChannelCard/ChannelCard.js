import React from "react";
import PropTypes from "prop-types";

import { Card, CardBody, CardImg, HolderProvider } from "./../.";
import { ChannelPrice } from "./ChannelPrice";
import { ChannelCardTitle } from "./ChannelCardTitle";
import { ChannelCardFooter } from "./ChannelCardFooter";

const ChannelCard = ({
  sayBye = false,
  id,
  poster,
  title,
  featured,
  description,
  is_free,
  has_free_trial,
  price_sm,
  price_md,
  price_lg,
  dropdownMenu,
  membersCount,
  teamsCount,
}) => {
  return (
    <Card>
      <HolderProvider.Icon iconChar={sayBye ? "bye" : ""} size={32}>
        <CardImg top src={poster} alt={title} />
      </HolderProvider.Icon>
      <CardBody>
        <ChannelCardTitle
          title={title}
          id={id}
          featured={featured}
          dropdownMenu={dropdownMenu}
        />
        <div className="mt-4">{description}</div>
        <ChannelPrice
          isFree={is_free}
          priceInfo={{
            hasFreeTrial: has_free_trial,
            priceSm: price_sm,
            priceMd: price_md,
            priceLg: price_lg,
          }}
        />
      </CardBody>
      <ChannelCardFooter
        id={id}
        membersCount={membersCount}
        teamsCount={teamsCount}
      />
    </Card>
  );
};

ChannelCard.propTypes = {
  sayBye: PropTypes.bool,
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  poster: PropTypes.string,
  featured: PropTypes.bool,
  is_free: PropTypes.bool.isRequired,
  has_free_trial: PropTypes.bool,
  price_sm: PropTypes.number,
  price_md: PropTypes.number,
  price_lg: PropTypes.number,
  dropdownMenu: PropTypes.element,
  membersCount: PropTypes.number,
  teamsCount: PropTypes.number,
};
ChannelCard.defaultProps = {
  sayBye: false,
  featured: false,
};

export { ChannelCard };
