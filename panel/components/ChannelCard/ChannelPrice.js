import React from "react";
import PropTypes from "prop-types";
import { Badge } from "./../.";

const ChannelPrice = ({ isFree, priceInfo }) => {
  return (
    <>
      <hr />
      <Badge pill className="mr-1" color={isFree ? "primary" : "light"}>
        free
      </Badge>
      <Badge pill color={isFree ? "light" : "primary"}>
        paid
      </Badge>
      {!isFree && (
        <div className="mt-4 border-primary">
          <h6>
            <strong>Price for different teams:</strong>
          </h6>
          <div className="mt-4 mb-4 d-flex justify-content-between">
            <div className="text-center">
              <h6 className="mb-1">{parsePrice(priceInfo.priceSm) || "-"}</h6>
              <small>small</small>
            </div>
            <div className="text-center">
              <h6 className="mb-1">{parsePrice(priceInfo.priceMd) || "-"}</h6>
              <small>medium</small>
            </div>
            <div className="text-center">
              <h6 className="mb-1">{parsePrice(priceInfo.priceLg) || "-"}</h6>
              <small>large</small>
            </div>
          </div>
          <small>* Prices are mentioned per user per months</small>
        </div>
      )}
    </>
  );
};

ChannelPrice.propTypes = {
  isFree: PropTypes.bool,
  priceInfo: PropTypes.shape({
    hasFreeTrial: PropTypes.bool,
    priceSm: PropTypes.number,
    priceMd: PropTypes.number,
    priceLg: PropTypes.number,
  }),
};

ChannelPrice.defaultProps = {
  isFree: true,
  priceInfo: {},
};

export { ChannelPrice };

function parsePrice(price) {
  const dollars = Math.floor(price / 100);
  const cents = price % 100;
  return `$ ${dollars}.${cents}`;
}
