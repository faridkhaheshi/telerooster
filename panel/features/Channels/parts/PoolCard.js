import React from "react";
import PropTypes from "prop-types";
import { mutate } from "swr";

import { ChannelCard } from "./../../../components";
import useToast from "../../../core/hooks/use-toast";
import useAsyncFormHandler from "../../../core/hooks/use-async-form-handler";
import { DeleteChannelModal } from "./DeleteChannelModal";
import { PoolDropdownMenu } from "./PoolDropdownMenu";

const PoolCard = (pool) => {
  const [isModalOpen, setIsModalOpen] = React.useState(false);
  const { success, danger } = useToast();
  const toggleModal = React.useCallback(() => {
    setIsModalOpen((old) => !old);
  }, []);
  const { handleSubmit: deleteChannel, isProcessing } = useAsyncFormHandler({
    url: `/api/v1/pools/${pool.id}`,
    requestOptions: { method: "DELETE" },
    onSuccess: () => {
      success({
        message: (
          <>
            Channel <strong>"{pool.title}"</strong> deleted.
          </>
        ),
      });
      mutate("/api/v1/pools");
    },
    onError: (errMsg) => {
      danger({
        title: (
          <>
            Deleting <strong>"{pool.title}"</strong> failed
          </>
        ),
        message: errMsg,
      });
    },
  });
  return (
    <>
      <ChannelCard
        sayBye={isProcessing}
        {...pool}
        dropdownMenu={<PoolDropdownMenu id={pool.id} onDelete={toggleModal} />}
      />
      <DeleteChannelModal
        isOpen={isModalOpen}
        toggle={toggleModal}
        title={pool.title}
        onConfirm={deleteChannel}
      />
    </>
  );
};

PoolCard.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  poster: PropTypes.string,
  featured: PropTypes.bool,
  is_free: PropTypes.bool.isRequired,
  has_free_trial: PropTypes.bool,
  price_sm: PropTypes.number,
  price_md: PropTypes.number,
  price_lg: PropTypes.number,
  membersCount: PropTypes.number,
  teamsCount: PropTypes.number,
};
PoolCard.defaultProps = {
  featured: false,
};

export { PoolCard };
