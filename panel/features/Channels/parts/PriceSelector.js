import React from "react";
import PropTypes from "prop-types";

import "./PriceSelector.css";

const PriceSelector = ({
  initialValue = 149,
  dollarClassName = "",
  centClassName = "",
  prefix = "price-selector",
}) => (
  <div className="price-selctor-container">
    <NumberInput
      initialValue={Math.floor(initialValue / 100)}
      min={0}
      max={999}
      step={1}
      className={dollarClassName}
      name={`${prefix}-dollars`}
    />
    <div>.</div>
    <NumberInput
      initialValue={initialValue % 100}
      min={0}
      max={99}
      step={10}
      className={centClassName}
      name={`${prefix}-cents`}
    />
    <div className="price-selctor-currency">
      USD <br /> per user <br /> per month
    </div>
  </div>
);

PriceSelector.propTypes = {
  initialValue: PropTypes.number,
  dollarClassName: PropTypes.string,
  centClassName: PropTypes.string,
  prefix: PropTypes.string,
};

PriceSelector.defaultProps = {
  initialValue: 149,
  dollarClassName: "",
  centClassName: "",
  prefix: "price-selector",
};

export { PriceSelector };

function NumberInput({
  onChange = () => ({}),
  min,
  max,
  step = 1,
  initialValue = 0,
  ...rest
}) {
  const [value, setValue] = React.useState(initialValue);
  const increment = React.useCallback(() => {
    setValue((old) => Math.min(max, old + step));
    onChange();
  }, []);
  const decrement = React.useCallback(() => {
    setValue((old) => Math.max(min, old - step));
    onChange();
  }, []);
  const preventDecimalPoint = React.useCallback((e) => {
    if (e.key === ".") e.preventDefault();
  }, []);
  return (
    <div className="price-selector-number-container">
      <button onClick={increment} type="button">
        ▲
      </button>
      <input
        required
        type="number"
        pattern="[0-9]"
        {...rest}
        min={min}
        max={max}
        value={value}
        onKeyDown={preventDecimalPoint}
        onChange={(e) => setValue(Math.max(Math.min(e.target.value, max), min))}
      />
      <button onClick={decrement} type="button">
        ▼
      </button>
    </div>
  );
}

NumberInput.propTypes = {
  onChange: PropTypes.func,
  min: PropTypes.number,
  max: PropTypes.number,
  step: PropTypes.number,
  initialValue: PropTypes.number,
};

NumberInput.defaultProps = {
  onChange: () => ({}),
  min: 0,
  max: 1000000,
  step: 1,
  initialValue: 0,
};
