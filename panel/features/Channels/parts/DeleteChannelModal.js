import React from "react";
import PropTypes from "prop-types";

import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
} from "./../../../components";

const DeleteChannelModal = ({ title, isOpen = false, toggle, onConfirm }) => (
  <Modal isOpen={isOpen} toggle={toggle} className="modal-danger">
    <ModalHeader className="py-3" />
    <ModalBody className="table-danger text-center px-5">
      <i className="fa fa-5x fa-close fa-fw modal-icon mb-3"></i>
      <h6>
        Delete channel <strong>"{title}"</strong>?
      </h6>
      <p className="modal-text">
        This channel will be removed from your panel. You won't be able to post
        new contents to it.
      </p>
      <Button
        color="danger"
        className="mr-2 btn btn-danger"
        onClick={() => {
          toggle();
          onConfirm();
        }}
      >
        Yes - Delete it
      </Button>
      <Button color="link" className="text-secondary" onClick={toggle}>
        No - return
      </Button>
    </ModalBody>
    <ModalFooter className="py-3" />
  </Modal>
);

DeleteChannelModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
};

export { DeleteChannelModal };
