import React from "react";
import PropTypes from "prop-types";

import { Button, UncontrolledTooltip } from "./../../../components";
import { websiteUrl } from "./../../../params";

const MAX_CHANNELS = 5;

const CreateChannelButton = ({ currentChannels }) => {
  if (currentChannels >= MAX_CHANNELS) return null;
  return (
    <div className="mb-3">
      <Button
        href={`${websiteUrl}/pools/new`}
        color="primary"
        id="add-new-channel-button"
      >
        <i className="fa-fw fa fa-plus"></i>
      </Button>
      <UncontrolledTooltip placement="top" target="add-new-channel-button">
        Create a new channel
      </UncontrolledTooltip>
    </div>
  );
};

CreateChannelButton.propTypes = {
  currentChannels: PropTypes.number.isRequired,
};

export { CreateChannelButton };
