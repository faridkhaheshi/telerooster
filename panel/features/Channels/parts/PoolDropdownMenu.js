import React from "react";
import PropTypes from "prop-types";

import { DropdownMenu, DropdownItem, Link } from "./../../../components";
import { websiteUrl } from "./../../../params";

const PoolDropdownMenu = ({ id, onDelete }) => (
  <DropdownMenu right>
    <DropdownItem tag={Link} to={`/post?poolId=${id}`}>
      Post new content
    </DropdownItem>
    <DropdownItem tag={Link} to={`${websiteUrl}/pools/${id}`} target="_blank">
      Open channel page
    </DropdownItem>
    <DropdownItem
      tag={Link}
      to={`${websiteUrl}/pools/${id}/embed`}
      target="_blank"
    >
      Get embed code
    </DropdownItem>
    <DropdownItem tag="a" href={`/mychannels/${id}`}>
      Edit
    </DropdownItem>
    <DropdownItem onClick={onDelete}>Delete</DropdownItem>
  </DropdownMenu>
);

PoolDropdownMenu.propTypes = {
  id: PropTypes.string.isRequired,
  onDelete: PropTypes.func.isRequired,
};

export { PoolDropdownMenu };
