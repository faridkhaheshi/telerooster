import React from "react";
import PropTypes from "prop-types";

import { FormGroup, Input, Label } from "./../../../components";
import { PricingPlanSelector } from "./PricingPlanSelector";

const PricingTypeSelector = ({
  currentValues,
  pricingPlanName = "pool-pricing-plan",
}) => {
  const [pricingType, setPricingType] = React.useState(
    currentValues.is_free ? "free" : "paid"
  );

  const updatePrice = React.useCallback((e) => {
    setPricingType(e.target.value);
  }, []);

  return (
    <FormGroup tag="fieldset">
      <legend>Pricing:</legend>
      <FormGroup check>
        <Label check>
          <Input
            checked={pricingType === "free"}
            type="radio"
            name={pricingPlanName}
            value="free"
            onChange={updatePrice}
          />
          Free
        </Label>
      </FormGroup>
      <FormGroup check>
        <Label check>
          <Input
            checked={pricingType === "paid"}
            type="radio"
            name={pricingPlanName}
            value="paid"
            onChange={updatePrice}
            disabled
          />
          Paid (coming soon...)
        </Label>
      </FormGroup>
      <PricingPlanSelector
        pricingType={pricingType}
        currentValues={currentValues}
        hasTrialName={`${pricingPlanName}-has-trial`}
      />
    </FormGroup>
  );
};

PricingTypeSelector.propTypes = {
  currentValues: PropTypes.shape({
    is_free: PropTypes.bool.isRequired,
  }).isRequired,
  pricingPlanName: PropTypes.string,
};

PricingTypeSelector.defaultProps = {
  pricingPlanName: "pool-pricing-plan",
};

export { PricingTypeSelector };
