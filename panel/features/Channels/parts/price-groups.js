export default [
  {
    label: "For small teams (under 10 users):",
    prefix: "price-small-teams",
    dbKey: "price_sm",
    initialValue: 149,
  },
  {
    label: "For medium-sized teams (10 to 500 users):",
    prefix: "price-medium-teams",
    dbKey: "price_md",
    initialValue: 99,
  },
  {
    label: "For large teams (501 users and more):",
    prefix: "price-large-teams",
    dbKey: "price_lg",
    initialValue: 49,
  },
];
