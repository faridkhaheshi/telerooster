import React from "react";
import PropTypes from "prop-types";
import priceGroups from "./price-groups";
import { PriceSelector } from "./PriceSelector";
import { FormGroup, Input, Label, Col } from "./../../../components";

const INPUT_COL_SIZE = 6;

const PricingPlanSelector = ({
  pricingType,
  currentValues,
  hasTrialName = "has-trial",
}) => {
  if (pricingType !== "paid") return null;
  return (
    <>
      {priceGroups.map((pr) => (
        <PricingRow
          key={pr.prefix}
          {...pr}
          initialValue={currentValues[pr.dbKey] || pr.initialValue}
        />
      ))}
      <FormGroup check>
        <Label check>
          <Input
            type="checkbox"
            name={hasTrialName}
            defaultChecked={currentValues.has_free_trial}
          />{" "}
          Include a 14-day free trial
        </Label>
      </FormGroup>
    </>
  );
};

PricingPlanSelector.propTypes = {
  pricingType: PropTypes.string.isRequired,
  currentValues: PropTypes.shape({
    has_free_trial: PropTypes.bool.isRequired,
  }).isRequired,
  hasTrialName: PropTypes.string,
};

PricingPlanSelector.defaultProps = {
  hasTrialName: "has-trial",
};

export { PricingPlanSelector };

function PricingRow({ size = INPUT_COL_SIZE, label, prefix, initialValue }) {
  return (
    <FormGroup row>
      <Label className="d-flex align-items-center" sm={12 - size}>
        {label}
      </Label>
      <Col sm={size}>
        <PriceSelector prefix={prefix} initialValue={initialValue} />
      </Col>
    </FormGroup>
  );
}
