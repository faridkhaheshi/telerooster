import React from "react";
import PropTypes from "prop-types";

import { CardForm, SimpleTextInput } from "./../../components";
import { PricingTypeSelector } from "./parts/PricingTypeSelector";
import useServerResource from "../../core/hooks/use-server-resource";
import ErrorMessage from "../../components/ErrorMessage";
import { parseChannelEditFormData } from "./parse-channel-edit-form-data";
import useAsyncFormHandler from "../../core/hooks/use-async-form-handler";
import useToast from "../../core/hooks/use-toast";

const ChannelEditFormInternal = ({ id }) => {
  const { data: pool, error, isLoading } = useServerResource(
    `/api/v1/pools/${id}`
  );
  const { success, danger } = useToast();
  const { handleSubmit, isProcessing } = useAsyncFormHandler({
    url: `/api/v1/pools/${id}`,
    requestOptions: { method: "PUT" },
    parseForm: parseChannelEditFormData,
    onSuccess: () => {
      success({ message: "Channel updated." });
    },
    onError: (errorMessage) => {
      danger({ title: "Update Failed", message: errorMessage });
    },
  });

  if (isLoading) return <p>Loading...</p>;
  if (error) return <ErrorMessage message={error.message} />;

  return (
    <CardForm
      title="Edit channel info"
      buttonText="Update Channel"
      isProcessing={isProcessing}
      waitingText="Updating..."
      onSubmit={handleSubmit}
    >
      <legend>Info:</legend>
      <SimpleTextInput
        required
        label="Title"
        type="text"
        name="title"
        id="pool-title"
        placeholder="Write an eye-catching title"
        maxLength={50}
        defaultValue={pool.title}
      />
      <SimpleTextInput
        required
        label="Description"
        type="textarea"
        rows={10}
        name="description"
        id="pool-description"
        placeholder="Describe to your audience what they will find in this channel."
        maxLength={1500}
        defaultValue={pool.description}
      />
      <SimpleTextInput
        label="Poster"
        type="url"
        name="poster"
        id="pool-poster"
        placeholder="paste a url to an image. For example: https://telerooster.com/logo.png"
        defaultValue={pool.poster}
      />
      <PricingTypeSelector
        pricingPlanName="pool-pricing-plan"
        currentValues={pool}
      />
    </CardForm>
  );
};

ChannelEditFormInternal.propTypes = {
  id: PropTypes.string.isRequired,
};

const ChannelEditForm = React.memo(ChannelEditFormInternal);

export { ChannelEditForm };
