import React from "react";
import { CardColumns } from "reactstrap";
import ErrorMessage from "../../components/ErrorMessage";
import useServerResource from "../../core/hooks/use-server-resource";
import { CreateChannelButton } from "./parts/CreateChannelButton";
import { PoolCard } from "./parts/PoolCard";

export const ChannelList = () => {
  const { data: channels, error, isLoading } = useServerResource(
    `/api/v1/pools`
  );
  if (isLoading) return <p>Loading...</p>;
  if (error) return <ErrorMessage message={error.message} />;

  return (
    <>
      <CreateChannelButton currentChannels={channels.length} />
      <CardColumns>
        {channels.map((ch) => (
          <PoolCard key={ch.id} {...ch} />
        ))}
      </CardColumns>
    </>
  );
};
