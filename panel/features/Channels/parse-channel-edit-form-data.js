export const parseChannelEditFormData = ({ target } = {}) => {
  if (!target) return null;
  const formData = {
    title: target["pool-title"].value,
    description: target["pool-description"].value,
    pricingPlan: target["pool-pricing-plan"].value,
  };
  if (target["pool-poster"].value.length > 0)
    formData.poster = target["pool-poster"].value;
  if (target["pool-pricing-plan"].value === "paid") {
    formData.prices = {
      smallTeams:
        parseInt(target["price-small-teams-dollars"].value) * 100 +
        parseInt(target["price-small-teams-cents"].value),
      mediumTeams:
        parseInt(target["price-medium-teams-dollars"].value * 100) +
        parseInt(target["price-medium-teams-cents"].value),
      largeTeams:
        parseInt(target["price-large-teams-dollars"].value * 100) +
        parseInt(target["price-large-teams-cents"].value),
      hasTrial: target["pool-pricing-plan-has-trial"].checked,
    };
  }
  return formData;
};
