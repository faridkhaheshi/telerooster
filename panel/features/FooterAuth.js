import React from "react";
import PropTypes from "prop-types";
import { FooterText } from "./RoosterLayout";

const FooterAuth = ({ className }) => (
  <div className={className}>
    <p className="small">
      <FooterText sidebar={false} />
    </p>
  </div>
);

FooterAuth.propTypes = {
  className: PropTypes.string,
};

export { FooterAuth };
