import React from "react";
import { CardColumns } from "reactstrap";
import { ErrorMessage, Link } from "../../components";
import useServerResource from "../../core/hooks/use-server-resource";
import { SubscriptionChannelCard } from "./parts/SubscriptionChannelCard";
import { websiteUrl } from "./../../params";

const SubscriptionsList = () => {
  const { data: subscriptions, error, isLoading } = useServerResource(
    "/api/v1/subscriptions"
  );

  if (isLoading) return <p>Loading...</p>;
  if (error) return <ErrorMessage message={error.message} />;

  if (subscriptions.length === 0)
    return (
      <p>
        You don't follow any channels. Fine one{" "}
        <strong>
          <Link to={`${websiteUrl}#featured-pools-vitrin`} target="_blank">
            here
          </Link>
        </strong>
        .
      </p>
    );

  return (
    <CardColumns>
      {subscriptions.map(({ id, pool }) => (
        <SubscriptionChannelCard key={id} subscriptionId={id} pool={pool} />
      ))}
    </CardColumns>
  );
};

export { SubscriptionsList };
