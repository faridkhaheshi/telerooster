import React from "react";
import PropTypes from "prop-types";
import { mutate } from "swr";

import { ChannelCard } from "../../../components";
import { UnsubscribeModal } from "./UnsubscribeModal";
import { SubscriptionChannelDropdownMenu } from "./SubscriptionChannelDropdownMenu";
import useAsyncFormHandler from "../../../core/hooks/use-async-form-handler";
import useToast from "../../../core/hooks/use-toast";

const SubscriptionChannelCard = ({ subscriptionId, pool }) => {
  const [isModalOpen, setIsModalOpen] = React.useState(false);
  const { success, danger } = useToast();
  const { handleSubmit: unsubscribe, isProcessing } = useAsyncFormHandler({
    url: `/api/v1/subscriptions/${subscriptionId}`,
    requestOptions: { method: "DELETE" },
    onSuccess: () => {
      success({
        message: (
          <>
            Unsubscribed you from channel <strong>"{pool.title}"</strong>.
          </>
        ),
      });
      mutate("/api/v1/subscriptions");
    },
    onError: (errMsg) => {
      danger({
        title: (
          <>
            Unsubscribing you from channel <strong>"{pool.title}"</strong>{" "}
            failed
          </>
        ),
        message: errMsg,
      });
    },
  });

  const toggleModal = React.useCallback(() => {
    setIsModalOpen((old) => !old);
  }, []);

  return (
    <>
      <ChannelCard
        sayBye={isProcessing}
        {...pool}
        dropdownMenu={
          <SubscriptionChannelDropdownMenu
            poolId={pool.id}
            unsubscribe={toggleModal}
          />
        }
      />
      <UnsubscribeModal
        title={pool.title}
        isOpen={isModalOpen}
        toggle={toggleModal}
        onConfirm={unsubscribe}
      />
    </>
  );
};

SubscriptionChannelCard.propTypes = {
  subscriptionId: PropTypes.string.isRequired,
  pool: PropTypes.shape({
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    poster: PropTypes.string,
    featured: PropTypes.bool,
    is_free: PropTypes.bool.isRequired,
    has_free_trial: PropTypes.bool,
    price_sm: PropTypes.number,
    price_md: PropTypes.number,
    price_lg: PropTypes.number,
  }).isRequired,
};

SubscriptionChannelCard.defaultProps = {};

export { SubscriptionChannelCard };
