import React from "react";
import PropTypes from "prop-types";

import { DropdownMenu, DropdownItem, Link } from "../../../components";
import { websiteUrl } from "../../../params";

const SubscriptionChannelDropdownMenu = ({ poolId, unsubscribe }) => (
  <DropdownMenu right>
    <DropdownItem
      tag={Link}
      to={`${websiteUrl}/pools/${poolId}`}
      target="_blank"
    >
      Open Channel Page
    </DropdownItem>
    <DropdownItem onClick={unsubscribe}>Unsubscribe</DropdownItem>
  </DropdownMenu>
);

SubscriptionChannelDropdownMenu.propTypes = {
  poolId: PropTypes.string.isRequired,
  unsubscribe: PropTypes.func.isRequired,
};

export { SubscriptionChannelDropdownMenu };
