import React from "react";
import PropTypes from "prop-types";

import { Modal, ModalHeader, ModalBody, Button } from "../../../components";

const UnsubscribeModal = ({ title, isOpen = false, toggle, onConfirm }) => (
  <Modal isOpen={isOpen} toggle={toggle} className="modal-danger">
    <ModalHeader className="py-3" />
    <ModalBody className="table-danger text-center px-5">
      <i className="fa fa-5x fa-close fa-fw modal-icon mb-3"></i>
      <h6>
        Unsubscribe from channel <strong>"{title}"</strong>?
      </h6>
      <p className="modal-text">
        Confirming this action will unsubscribe you from this channel. You won't
        receive any messages from this channel. The channel will be removed from
        this panel, but you can subscribe again.
      </p>
      <Button
        color="danger"
        className="mr-2 btn btn-danger"
        onClick={() => {
          toggle();
          onConfirm();
        }}
      >
        Yes - Unsubscribe
      </Button>
      <Button color="link" className="text-secondary" onClick={toggle}>
        No - do nothing
      </Button>
    </ModalBody>
  </Modal>
);

UnsubscribeModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
};

export { UnsubscribeModal };
