import React from "react";
import { Row, Col } from "./../../components";
import AccountDeactivatorCard from "./AccountDeactivatorCard";

const SettingsList = () => (
  <main>
    <Row>
      <Col lg={8}>
        <AccountDeactivatorCard />
      </Col>
    </Row>
  </main>
);

export default SettingsList;
