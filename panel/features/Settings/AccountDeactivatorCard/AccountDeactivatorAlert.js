import React from "react";
import PropTypes from "prop-types";

import { Alert, Button, Collapse, Media } from "../../../components";

const AccountDeactivatorAlert = ({ isOpen, toggle, onConfirm }) => {
  return (
    <Collapse isOpen={isOpen}>
      <Alert color="danger" className="mt-4">
        <Media>
          <Media left middle className="mr-3">
            <span className="fa-stack fa-lg">
              <i className="fa fa-circle fa-fw fa-stack-2x alert-bg-icon"></i>
              <i className="fa fa-close fa-stack-1x fa-inverse alert-icon"></i>
            </span>
          </Media>
          <Media body>
            <h6 className="alert-heading mb-3">Danger!</h6>
            <p className="mb-4">
              This is <strong>the last step</strong>. Deactivation cannot be
              undone. Are you sure?
            </p>

            <div className="mt-2 d-flex justify-content-end">
              <Button
                color="danger"
                className="mx-2"
                outline
                onClick={onConfirm}
              >
                Yes, deactivate
              </Button>
              <Button color="success" onClick={toggle}>
                No, do nothing
              </Button>
            </div>
          </Media>
        </Media>
      </Alert>
    </Collapse>
  );
};

AccountDeactivatorAlert.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  toggle: PropTypes.func,
  onConfirm: PropTypes.func,
};

AccountDeactivatorAlert.defaultProps = {
  toggle: () => ({}),
  onConfirm: () => ({}),
};

export default AccountDeactivatorAlert;
