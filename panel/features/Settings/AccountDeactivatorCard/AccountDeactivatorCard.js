import React from "react";
import { Spinner } from "reactstrap";

import { Button, Card, CardBody, CardTitle } from "../../../components";
import { useAuth } from "../../authentication";
import AccountDeactivatorAlert from "./AccountDeactivatorAlert";

const AccountDeactivatorCard = () => {
  const [isToggleOpen, setIsToggleOpen] = React.useState(false);
  const { deactivate, isDeactivating } = useAuth();

  const toggle = React.useCallback(() => {
    setIsToggleOpen((old) => !old);
  }, []);

  return (
    <Card className="mb-3">
      <CardBody>
        <CardTitle tag="h6" className="mb-4">
          Deactivating your account
        </CardTitle>
        <p>
          If you want to deactivate your account, click on the button below.
          Deactivating your account <strong>will remove all your data</strong>.
          You will be logged out of this panel. You won't be able to log in
          again.
        </p>
        {isDeactivating ? (
          <div className="d-flex justify-content-center mt-5">
            <Spinner />
          </div>
        ) : (
          <>
            <div className="d-flex justify-content-end mt-4">
              <Button color="warning" onClick={toggle}>
                Deactive my account
              </Button>
            </div>
            <AccountDeactivatorAlert
              isOpen={isToggleOpen}
              toggle={toggle}
              onConfirm={deactivate}
            />
          </>
        )}
      </CardBody>
    </Card>
  );
};

export { AccountDeactivatorCard };
