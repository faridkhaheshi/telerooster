import React from "react";
import { websiteUrl } from "../../params";

import {
  Card,
  CardBody,
  CardTitle,
  CardText,
  Button,
} from "./../../components";

const EditPasswordCard = () => (
  <Card tag="section" className="mb-3 mt-3">
    <CardBody>
      <CardTitle tag="h6" className="mb-4">
        Change your password
      </CardTitle>
      <CardText>To change your password click on the button below:</CardText>
      <div className="d-flex justify-content-center">
        <Button
          color="warning"
          href={`${websiteUrl}/auth/reset-password`}
          target="_blank"
        >
          Change password
        </Button>
      </div>
    </CardBody>
  </Card>
);

export default React.memo(EditPasswordCard);
