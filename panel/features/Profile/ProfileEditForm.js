import React from "react";
import useAsyncFormHandler from "../../core/hooks/use-async-form-handler";
import useToast from "../../core/hooks/use-toast";
import { useAuth } from "../authentication";
import {
  Col,
  Label,
  FormGroup,
  SimpleTextInput,
  CardForm,
} from "./../../components";

const ProfileEditFormInternal = () => {
  const {
    updateUserInfo,
    user: { first_name, last_name, display_name, email },
  } = useAuth();
  const { success, danger } = useToast();
  const [firstName, setFirstName] = React.useState(first_name);
  const [lastName, setLastName] = React.useState(last_name);
  const [displayName, setDisplayName] = React.useState(display_name);
  const { handleSubmit, isProcessing } = useAsyncFormHandler({
    url: "/api/me",
    requestOptions: {
      method: "PUT",
      body: { firstName, lastName, displayName },
    },
    onSuccess: () => {
      updateUserInfo();
      success({ message: "Profile updated." });
    },
    onError: (errorMessage) => {
      danger({ title: "Update Failed", message: errorMessage });
    },
  });
  return (
    <CardForm
      title="Edit your profile info"
      buttonText="Update Profile"
      waitingText="Updating..."
      isProcessing={isProcessing}
      onSubmit={handleSubmit}
    >
      <div className="small mt-4 mb-3">Basic info:</div>
      <FormGroup row>
        <Label for="email" sm={3}>
          Email:
        </Label>
        <Col sm={8}>
          <p id="email">{email}</p>
        </Col>
      </FormGroup>

      <SimpleTextInput
        label="First Name"
        type="text"
        name="firstName"
        id="firstName"
        placeholder="First Name..."
        value={firstName}
        onChange={(e) => setFirstName(e.target.value)}
        maxLength={50}
      />
      <SimpleTextInput
        label="Last Name"
        type="text"
        name="lastName"
        id="lastName"
        placeholder="Last Name..."
        value={lastName}
        onChange={(e) => setLastName(e.target.value)}
        maxLength={50}
      />
      <SimpleTextInput
        label="Display Name"
        required
        type="text"
        name="displayName"
        id="displayName"
        placeholder="Display Name..."
        guideText="This is what your audience will see."
        value={displayName}
        onChange={(e) => setDisplayName(e.target.value)}
        maxLength={100}
      />
    </CardForm>
  );
};

const ProfileEditForm = React.memo(ProfileEditFormInternal);

export { ProfileEditForm };
