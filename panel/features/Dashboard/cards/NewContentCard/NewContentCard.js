import React from "react";

import {
  Card,
  CardBody,
  CardTitle,
  Link,
  ListGroup,
  ListGroupItem,
} from "../../../../components";
import { PoolInfoItem } from "./PoolInfoItem";
import { useDashboardData } from "../../DashboardDataContext";

const NewContentCard = () => {
  const { pools } = useDashboardData();

  if (pools.length === 0) return null;

  return (
    <Card className="mb-3">
      <CardBody>
        <CardTitle tag="h6" className="mb-0 font-weight-bold text-windows">
          Post new content to your channels:
        </CardTitle>
      </CardBody>
      <ListGroup flush>
        {(pools || []).map((pool) => (
          <PoolInfoItem key={pool.id} {...pool} />
        ))}
        <ListGroupItem
          action
          tag={Link}
          to="/mychannels"
          className="text-center"
        >
          Your channels
          <i className="fa fa-angle-right ml-2"></i>
        </ListGroupItem>
      </ListGroup>
    </Card>
  );
};

export { NewContentCard };
