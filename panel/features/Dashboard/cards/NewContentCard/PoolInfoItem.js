import React from "react";
import PropTypes from "prop-types";

import { Link, ListGroupItem, Media } from "../../../../components";

const PoolInfoItem = ({ id, title, description }) => (
  <ListGroupItem tag={Link} to={`/post?poolId=${id}`} action className="by-0">
    <Media>
      <Media body>
        <span className="mt-0 d-flex h6 mb-1">{title}</span>
        <p className="mb-0">{description.slice(0, 100)}</p>
      </Media>
      <Media bottom className="ml-2">
        <i className="fa fa-fw fa-pencil"></i>
      </Media>
    </Media>
  </ListGroupItem>
);

PoolInfoItem.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
};

export { PoolInfoItem };
