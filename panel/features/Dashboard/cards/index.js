import NewContentCard from "./NewContentCard";
import EmbedCard from "./EmbedCard";
import ContentMailCard from "./ContentMailCard";
import AudienceCard from "./AudienceCard";
import AddChannelCard from "./AddChannelCard";

export default [
  {
    Component: EmbedCard,
    key: "embed-card",
  },
  {
    Component: ContentMailCard,
    key: "content-mail-card",
  },
  {
    Component: AudienceCard,
    key: "audience-card",
  },
  {
    Component: NewContentCard,
    key: "new-content-card",
  },
  {
    Component: AddChannelCard,
    key: "add-channel-card",
  },
];
