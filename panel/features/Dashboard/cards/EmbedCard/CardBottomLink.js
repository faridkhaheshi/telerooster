import React from "react";
import PropTypes from "prop-types";

import { Link, ListGroup, ListGroupItem } from "../../../../components";

const CardBottomLink = ({ to, text, ...otherProps }) => (
  <ListGroup flush>
    <ListGroupItem
      action
      tag={Link}
      to={to}
      className="text-center"
      {...otherProps}
    >
      {text}
      <i className="fa fa-angle-right ml-2"></i>
    </ListGroupItem>
  </ListGroup>
);

CardBottomLink.propTypes = {
  to: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
};

export { CardBottomLink };
