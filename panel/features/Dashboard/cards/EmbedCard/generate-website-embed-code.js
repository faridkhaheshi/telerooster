import { generateSubscribeLink } from "./generate-subscribe-link";

export const generateWebsiteEmbedCode = (poolId = "{{poolId}}") =>
  `
<div>
        <a href="${generateSubscribeLink({
          poolId,
          platform: "slack",
        })}" target="_blank" style="display: block; margin: 16px;">
          <img
            src="https://cdn.brandfolder.io/5H442O3W/as/pl54cs-bd9mhs-czsxst/btn-add-to-slack.svg"
            alt="Add to Slack"
            style="display: block; margin-left: auto; margin-right: auto; width: 140px; height: 41px;"
            width="140"
            height="41"
          />
        </a>
      </div>
`
    .replace(/\s+/g, " ")
    .trim();

// export const generateWebsiteEmbedCode = (poolId = "{{poolId}}") =>
//   `
// <div>
//         <p>Receive newsletter in business messengers:</p>
//         <a href="${generateSubscribeLink({
//           poolId,
//           platform: "slack",
//         })}" target="_blank" style="display: block; margin: 16px;">
//           <img
//             src="https://cdn.brandfolder.io/5H442O3W/as/pl54cs-bd9mhs-czsxst/btn-add-to-slack.svg"
//             alt="Add to Slack"
//             style="display: block; margin-left: auto; margin-right: auto; width: 140px; height: 41px;"
//             width="140"
//             height="41"
//           />
//         </a>
//         <a href="${generateSubscribeLink({
//           poolId,
//           platform: "microsoft",
//         })}" target="_blank" style="display: block; margin: 16px;">
//         <img
//             src="https://telerooster.com/images/add-to-teams.png"
//             alt="Add to Microsoft Teams"
//             style="display: block; margin-left: auto; margin-right: auto; width: 140px; height: 41px;"
//             width="140"
//             height="41"
//           />
//         </a>
//       </div>
// `
//     .replace(/\s+/g, " ")
//     .trim();
