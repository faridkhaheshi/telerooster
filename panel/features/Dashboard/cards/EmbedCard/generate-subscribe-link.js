import { websiteUrl } from "./../../../../params";

export const generateSubscribeLink = ({ poolId = "{{poolId}}", platform }) =>
  platform
    ? `${websiteUrl}/pools/${poolId}/add?platform=${platform}`
    : `${websiteUrl}/pools/${poolId}`;
