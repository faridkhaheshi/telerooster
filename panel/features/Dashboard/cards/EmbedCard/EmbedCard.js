import React from "react";

import { Card, CardBody, CardTitle } from "../../../../components";
import { PoolSelector } from "./PoolSelector";
import { EmbedButtons } from "./EmbedButtons";
import { TargetMediumSelector } from "./TargetMediumSelector";
import { websiteUrl } from "./../../../../params";
import { CardBottomLink } from "./CardBottomLink";
import { CopyButton } from "./CopyButton";
import { useDashboardData } from "../../DashboardDataContext";

const DEFAULT_EMBED_MODE = "website";

const EmbedCard = () => {
  const { pools } = useDashboardData();
  const [selectedPoolId, setSelectedPoolId] = React.useState(null);
  const [embedMode, setEmbedMode] = React.useState(DEFAULT_EMBED_MODE);

  if (pools.length === 0) return null;

  return (
    <Card className="mb-3">
      <CardBody>
        <CardTitle tag="h6" className="mb-4 font-weight-bold text-windows">
          Get your "Add to" buttons for:
        </CardTitle>
        <PoolSelector
          pools={pools}
          onChange={setSelectedPoolId}
          className="border-0 mb-5"
        />
        <EmbedButtons poolId={selectedPoolId} />
        <TargetMediumSelector
          className="mt-5"
          defaultMode={DEFAULT_EMBED_MODE}
          onChange={setEmbedMode}
        />
        <CopyButton embedMode={embedMode} poolId={selectedPoolId} />
      </CardBody>
      <CardBottomLink
        to={`${websiteUrl}/pools/${selectedPoolId}/embed`}
        text="Open embed page"
        target="_blank"
      />
    </Card>
  );
};

export { EmbedCard };
