import React from "react";
import PropTypes from "prop-types";

import { Input } from "../../../../components";

const PoolSelector = React.forwardRef(
  ({ pools = [], onChange = () => ({}), ...otherProps }, ref) => {
    const inputRef = React.useRef();

    React.useEffect(() => {
      onChange(inputRef.current.value);
    }, [onChange, inputRef.current]);
    return (
      <Input
        style={{
          backgroundColor: "transparent",
        }}
        type="select"
        name="pool"
        id="dashboard-embed-card-pool-selector"
        innerRef={inputRef}
        onChange={(e) => onChange(e.target.value)}
        {...otherProps}
      >
        {pools.map(({ id, title }) => (
          <option key={id} value={id}>
            {title}
          </option>
        ))}
      </Input>
    );
  }
);

PoolSelector.propTypes = {
  pools: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
    })
  ),
  onChange: PropTypes.func,
};
PoolSelector.defaultProps = {
  onChange: () => ({}),
};

PoolSelector.displayName = "PoolSelector";

export { PoolSelector };
