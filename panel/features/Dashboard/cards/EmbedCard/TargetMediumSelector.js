import React from "react";
import PropTypes from "prop-types";

import { FormGroup, Label, Input } from "../../../../components";

const TargetMediumSelector = ({
  onChange = () => ({}),
  defaultMode,
  ...otherProps
}) => {
  const handleChange = React.useCallback(
    (e) => {
      onChange(e.target.value);
    },
    [onChange]
  );
  return (
    <FormGroup {...otherProps}>
      <h6>Copy embed code to be used in:</h6>
      <FormGroup check>
        <Label check>
          <Input
            type="radio"
            name="radioStacked"
            value="website"
            defaultChecked={defaultMode === "website"}
            onChange={handleChange}
          />
          your website
        </Label>
      </FormGroup>
      <FormGroup check>
        <Label check>
          <Input
            type="radio"
            name="radioStacked"
            value="email"
            defaultChecked={defaultMode === "email"}
            onChange={handleChange}
          />
          your email newsletters
        </Label>
      </FormGroup>
      <FormGroup check>
        <Label check>
          <Input
            type="radio"
            name="radioStacked"
            value="link"
            defaultChecked={defaultMode === "link"}
            onChange={handleChange}
          />
          everywhere else (get a link)
        </Label>
      </FormGroup>
    </FormGroup>
  );
};

TargetMediumSelector.propTypes = {
  onChange: PropTypes.func,
  defaultMode: PropTypes.string.isRequired,
};

TargetMediumSelector.defaultProps = {
  onChange: () => ({}),
};

export { TargetMediumSelector };
