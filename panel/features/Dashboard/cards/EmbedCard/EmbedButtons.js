import React from "react";
import PropTypes from "prop-types";

import { websiteUrl } from "./../../../../params";

const EmbedButtons = ({ poolId }) => {
  return (
    <div>
      <a
        href={`${websiteUrl}/pools/${poolId}/add?platform=slack`}
        target="_blank"
        style={{ display: "block", margin: 16 }}
      >
        <img
          src="https://cdn.brandfolder.io/5H442O3W/as/pl54cs-bd9mhs-czsxst/btn-add-to-slack.svg"
          alt="Add to Slack"
          width="140"
          height="41"
          style={{ display: "block", marginLeft: "auto", marginRight: "auto" }}
        />
      </a>
      {/* <a
        href={`${websiteUrl}/pools/${poolId}/add?platform=microsoft`}
        target="_blank"
        style={{ display: "block", margin: 16 }}
      >
        <img
          src={`${websiteUrl}/images/add-to-teams.png`}
          alt="Add to Microsoft Teams"
          width="140"
          height="41"
          style={{ display: "block", marginLeft: "auto", marginRight: "auto" }}
        />
      </a> */}
    </div>
  );
};

EmbedButtons.propTypes = {
  poolId: PropTypes.string,
};

export { EmbedButtons };
