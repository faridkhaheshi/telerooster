import React from "react";
import PropTypes from "prop-types";

import { Button, UncontrolledTooltip } from "../../../../components";

import { generateEmailEmbedCode } from "./generate-email-embed-code";
import { generateWebsiteEmbedCode } from "./generate-website-embed-code";
import { generateSubscribeLink } from "./generate-subscribe-link";
import useCopyToClipboard from "./../../../../core/hooks/use-copy-to-clipboard";

const CopyButton = ({ embedMode, poolId }) => {
  const { copied, copy } = useCopyToClipboard();

  return (
    <div className="d-flex justify-content-center">
      <Button
        color="link"
        id="copy-embed-code-button"
        size="lg"
        onClick={copy.bind(null, generateEmbedCode({ poolId, embedMode }))}
      >
        {copied ? (
          <i className="fa fa-fw fa-check" />
        ) : (
          <i className="fa fa-fw fa-copy" />
        )}
      </Button>
      <UncontrolledTooltip
        placement="top-start"
        target="copy-embed-code-button"
      >
        Copy embed code to clipboard
      </UncontrolledTooltip>
    </div>
  );
};

CopyButton.propTypes = {
  embedMode: PropTypes.oneOf(["website", "email", "link"]).isRequired,
  poolId: PropTypes.string,
};

export { CopyButton };

function generateEmbedCode({ poolId, embedMode }) {
  if (embedMode === "website") return generateWebsiteEmbedCode(poolId);
  if (embedMode === "email") return generateEmailEmbedCode(poolId);
  return generateSubscribeLink({ poolId });
}
