import {
  Button,
  Card,
  CardBody,
  CardTitle,
  UncontrolledTooltip,
} from "../../../../components";
import { useDashboardData } from "../../DashboardDataContext";
import { websiteUrl } from "./../../../../params";

const MAX_CHANNELS = 5;

const AddChannelCard = () => {
  const { pools } = useDashboardData();
  if (pools.length >= MAX_CHANNELS) return null;
  return (
    <Card className="mb-3">
      <CardBody>
        <CardTitle tag="h6" className="mb-0 font-weight-bold text-windows">
          Create a new channel
        </CardTitle>
        <div className="d-flex justify-content-center pt-5 pb-4">
          <Button
            color="primary"
            id="dashboard-add-new-channel-button"
            href={`${websiteUrl}/pools/new?ref=${encodeURIComponent(
              window.location.href
            )}`}
          >
            <i className="fa fa-fw fa-plus" aria-hidden="true"></i>
          </Button>
          <UncontrolledTooltip
            placement="top"
            target="dashboard-add-new-channel-button"
          >
            Create a new channel
          </UncontrolledTooltip>
        </div>
      </CardBody>
    </Card>
  );
};

export { AddChannelCard };
