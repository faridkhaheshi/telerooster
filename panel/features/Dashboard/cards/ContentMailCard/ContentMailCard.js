import React from "react";

import {
  Card,
  CardBody,
  CardTitle,
  CardText,
  CardFooter,
} from "../../../../components";
import { useDashboardData } from "../../DashboardDataContext";
import { PoolSelector } from "../EmbedCard/PoolSelector";
import { ContentMailList } from "./ContentMailList";

const ContentMailCard = () => {
  const { pools } = useDashboardData();
  const [selectedPoolId, setSelectedPoolId] = React.useState(null);
  const [contentmails, setContentmails] = React.useState([]);

  React.useEffect(() => {
    if (pools && selectedPoolId) {
      const selectedPool = pools.find((pool) => pool.id === selectedPoolId);
      setContentmails(selectedPool.Contentmails);
    }
  }, [pools, selectedPoolId]);

  if (pools.length === 0) return null;

  return (
    <Card className="mb-3">
      <CardBody>
        <CardTitle tag="h6" className="mb-4 font-weight-bold text-windows">
          Easily send your newsletter contents to your channels:
        </CardTitle>
        <PoolSelector
          pools={pools}
          onChange={setSelectedPoolId}
          className="border-0"
        />
      </CardBody>
      <ContentMailList contentmails={contentmails} />
      <CardFooter>
        <small>
          To send your email newsletters to this channel, just add this email
          address to the newsletter.
        </small>
      </CardFooter>
    </Card>
  );
};

export { ContentMailCard };
