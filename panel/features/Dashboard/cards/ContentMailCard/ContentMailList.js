import React from "react";
import PropTypes from "prop-types";

import {
  ListGroup,
  ListGroupItem,
  Button,
  Media,
  UncontrolledTooltip,
} from "../../../../components";
import useCopyToClipboard from "../../../../core/hooks/use-copy-to-clipboard";

const ContentMailList = ({ contentmails }) => {
  return (
    <ListGroup flush>
      {contentmails.map(({ id, address }) => (
        <ListGroupItem key={id} className="by-0">
          <Media>
            <Media body className="w-75">
              {hideEmailAddress(address)}
            </Media>
            <CopyAddressButton address={address} id={id} />
          </Media>
        </ListGroupItem>
      ))}
    </ListGroup>
  );
};

ContentMailList.propTypes = {
  contentmails: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      address: PropTypes.string.isRequired,
    })
  ),
};
ContentMailList.defaultProps = {
  contentmails: [],
};

export { ContentMailList };

function CopyAddressButton({ address, id }) {
  const { copy, copied } = useCopyToClipboard();
  return (
    <>
      <Button
        className="ml-2"
        color="link"
        id={`copy-contentmail-buddon-${id}`}
        onClick={copy.bind(null, address)}
      >
        {copied ? (
          <i className="fa fa-fw fa-check" />
        ) : (
          <i className="fa fa-fw fa-copy" />
        )}
      </Button>
      <UncontrolledTooltip
        placement="left-start"
        target={`copy-contentmail-buddon-${id}`}
      >
        Copy to clipboard
      </UncontrolledTooltip>
    </>
  );
}

function hideEmailAddress(email) {
  const parts = email.split("@");
  const masked = parts[0].slice(0, 5);
  return `${masked}******@${parts[1]}`;
}
