import React from "react";
import { Spinner } from "reactstrap";

import { Card, CardBody, CardTitle, Link } from "../../../../components";
import { useDashboardData } from "../../DashboardDataContext";

const AudienceCard = () => {
  const { audience } = useDashboardData();
  return (
    <Card className="mb-3">
      <CardBody>
        <CardTitle tag="h6" className="mb-0 font-weight-bold text-windows">
          Audience size
        </CardTitle>
        <div className="text-center my-4">
          <h2>{audience ? audience.total : <Spinner color="secondary" />}</h2>
        </div>
        <div className="d-flex">
          <Link className="ml-auto" to="/audience">
            more details
            <i className="ml-1 fa fa-angle-right" />
          </Link>
        </div>
      </CardBody>
    </Card>
  );
};

export { AudienceCard };
