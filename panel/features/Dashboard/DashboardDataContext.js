import React from "react";
import { Spinner } from "reactstrap";
import { ErrorMessage } from "../../components";
import useServerResource from "../../core/hooks/use-server-resource";

const DashboardDataContext = React.createContext();
export const DashboardDataConsumer = DashboardDataContext.Consumer;

export const useDashboardData = () => React.useContext(DashboardDataContext);

export const DashboardDataProvider = ({ children }) => {
  const {
    data: pools,
    error: poolsError,
    isLoading: isPoolsLoading,
  } = useServerResource(`/api/v1/pools`);
  const {
    data: audience,
    error: audienceError,
    isLoading: isAudienceLoading,
  } = useServerResource("/api/v1/audience");
  const dashboardData = {
    pools,
    audience,
  };
  return (
    <DashboardDataContext.Provider value={dashboardData}>
      <Dialog
        isLoading={isPoolsLoading || isAudienceLoading}
        error={poolsError || audienceError}
      >
        {children}
      </Dialog>
    </DashboardDataContext.Provider>
  );
};

export default DashboardDataContext;

function Dialog({ children, isLoading, error }) {
  if (isLoading)
    return (
      <div className="d-flex justify-content-center align-items-center min-vh-100">
        <Spinner color="secondary" />
      </div>
    );
  if (error)
    return (
      <div className="d-flex justify-content-center align-items-center min-vh-100">
        <ErrorMessage
          message={
            error
              ? error.message || "failed to load data"
              : "failed to load data"
          }
        />
      </div>
    );
  return children;
}
