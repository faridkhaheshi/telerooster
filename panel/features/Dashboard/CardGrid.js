import React from "react";
import cards from "./cards";

import { CardColumns } from "./../../components";
import { DashboardDataProvider } from "./DashboardDataContext";

const CardGrid = () => {
  return (
    <DashboardDataProvider>
      <CardColumns className="dashboard-card-columns-container">
        {cards.map(({ Component, key }) => (
          <Component key={key} />
        ))}
      </CardColumns>
    </DashboardDataProvider>
  );
};

export { CardGrid };
