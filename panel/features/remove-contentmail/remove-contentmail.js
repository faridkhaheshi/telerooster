const pattern = /[0-9a-z]{5,}@contentmail\.telerooster\.com/gi;

export const removeContentmail = (text) =>
  text.replace(pattern, `your team's channel in telerooster`);
