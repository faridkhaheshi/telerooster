import React from "react";
import PropTypes from "prop-types";

import { Card, CardBody, CardTitle, Table } from "../../../components";
import { TeamsActions } from "./TeamsActions";

const TeamsTable = ({ teams }) => {
  return (
    <Card>
      <CardBody>
        <CardTitle>
          <h6>These are the teams you have added:</h6>
        </CardTitle>
      </CardBody>
      <Table responsive striped className="mb-0">
        <thead>
          <tr>
            <th className="bt-0">Team Name</th>
            <th className="bt-0 text-center">Platform</th>
            <th className="bt-0 text-right pr-4">Actions</th>
          </tr>
        </thead>
        <tbody>
          {teams.map(({ id, name, platform }) => (
            <tr key={id}>
              <td className="align-middle">{name}</td>
              <td className="align-middle text-center">{platform}</td>
              <td className="align-middle text-right">
                <TeamsActions team={{ id, name, platform }} />
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </Card>
  );
};

TeamsTable.propTypes = {
  tables: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      platform: PropTypes.string.isRequired,
    })
  ),
};

TeamsTable.defaultProps = {
  tables: [],
};

export default TeamsTable;
