import React from "react";
import PropTypes from "prop-types";

import { Modal, ModalHeader, ModalBody, Button } from "../../../components";

const ConfirmDeleteModal = ({
  teamName,
  platform,
  isOpen,
  toggle,
  onConfirm,
}) => (
  <Modal isOpen={isOpen} toggle={toggle} className="modal-danger">
    <ModalHeader className="py-3" />
    <ModalBody className="table-danger text-center px-5">
      <i className="fa fa-5x fa-close fa-fw modal-icon mb-3"></i>
      <h6>
        Remove <span>{platform}</span> team <strong>"{teamName}"</strong>?
      </h6>
      <p className="modal-text">
        Are you sure you want to remove this team? Removing a team will also
        remove all of its subscriptions. The team will not receive any content
        after this action.
      </p>
      <Button
        color="danger"
        className="mr-2 btn btn-danger"
        onClick={() => {
          toggle();
          onConfirm();
        }}
      >
        Yes - Unsubscribe
      </Button>
      <Button color="link" className="text-secondary" onClick={toggle}>
        No - do nothing
      </Button>
    </ModalBody>
  </Modal>
);

ConfirmDeleteModal.propTypes = {
  teamName: PropTypes.string,
  platform: PropTypes.string,
  isOpen: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  onConfirm: PropTypes.func,
};

ConfirmDeleteModal.defaultProps = {
  teamName: "UNKNOWN",
  platform: "",
  onConfirm: () => ({}),
};

export { ConfirmDeleteModal };
