import React from "react";
import PropTypes from "prop-types";
import { mutate } from "swr";

import {
  UncontrolledButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "../../../components";
import { ConfirmDeleteModal } from "./ConfirmDeleteModal";
import useAsyncFormHandler from "../../../core/hooks/use-async-form-handler";
import useToast from "../../../core/hooks/use-toast";

const TeamsActions = ({ team }) => {
  const { success, danger } = useToast();
  const { handleSubmit: removeTeam } = useAsyncFormHandler({
    url: `/api/v1/teams/${team.id}`,
    requestOptions: { method: "DELETE" },
    onSuccess: () => {
      success({
        message: (
          <>
            Successfully removed team <strong>"{team.name}"</strong>.
          </>
        ),
      });
      mutate("/api/v1/teams");
    },
    onError: (errMsg) => {
      danger({
        title: (
          <>
            Failed to remove team <strong>"{team.name}"</strong>.
          </>
        ),
        message: errMsg,
      });
    },
  });

  const [isModalOpen, setIsModalOpen] = React.useState(false);
  const toggleModal = React.useCallback(() => {
    setIsModalOpen((old) => !old);
  }, []);

  return (
    <>
      <UncontrolledButtonDropdown>
        <DropdownToggle color="link" className="text-decoration-none">
          <i className="fa fa-gear"></i>
          <i className="fa fa-angle-down ml-2"></i>
        </DropdownToggle>
        <DropdownMenu right>
          <DropdownItem onClick={toggleModal}>
            <i className="fa fa-fw fa-remove mr-2"></i>
            Remove
          </DropdownItem>
        </DropdownMenu>
      </UncontrolledButtonDropdown>
      <ConfirmDeleteModal
        teamName={team.name}
        platform={team.platform}
        isOpen={isModalOpen}
        toggle={toggleModal}
        onConfirm={removeTeam}
      />
    </>
  );
};

TeamsActions.propTypes = {
  team: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    platform: PropTypes.string.isRequired,
  }).isRequired,
};

TeamsActions.defaultProps = {};

export { TeamsActions };
