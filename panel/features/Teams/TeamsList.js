import React from "react";

import useServerResource from "../../core/hooks/use-server-resource";
import { ErrorMessage } from "../../components";
import TeamsTable from "./parts/TeamsTable";

const TeamsList = () => {
  const { data: teams, error, isLoading } = useServerResource("/api/v1/teams");

  if (isLoading) return <p>Loading...</p>;
  if (error) return <ErrorMessage message={error.message} />;

  return <TeamsTable teams={teams} />;
};

export { TeamsList };
