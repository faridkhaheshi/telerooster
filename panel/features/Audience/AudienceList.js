import React from "react";

import ErrorMessage from "../../components/ErrorMessage";
import useServerResource from "../../core/hooks/use-server-resource";
import { Row, Col, Card, CardBody, CardTitle, Table } from "./../../components";

export const AudienceList = () => {
  const { data: { total, teams } = {}, error, isLoading } = useServerResource(
    "/api/v1/audience"
  );
  if (isLoading) return <p>Loading...</p>;
  if (error) return <ErrorMessage message={error.message} />;
  // return <pre>{JSON.stringify(teams, null, 2)}</pre>;

  return (
    <>
      <Row>
        <Col md={3}>
          <NumberCard title="channels in teams" number={teams.length || 0} />
        </Col>
        <Col md={3}>
          <NumberCard title="total audience" number={total || 0} />
        </Col>
      </Row>
      <TeamsTable teams={teams || []} />
    </>
  );
};

function NumberCard({ title, number }) {
  return (
    <Card className="mb-3">
      <CardBody>
        <CardTitle tag="h6" className="mb-4">
          {title}
        </CardTitle>
        <h1 className="text-center mb-4">{number}</h1>
      </CardBody>
    </Card>
  );
}

function TeamsTable({ teams }) {
  return (
    <Card className="mb-3">
      <CardBody>
        <CardTitle className="mb-1 d-flex">
          <h6>Teams</h6>
        </CardTitle>
      </CardBody>
      <Table responsive striped className="mb-0">
        <thead>
          <tr>
            <th className="bt-0">Team Name</th>
            <th className="bt-0 text-center">Platform</th>
            <th className="bt-0 text-center">Subscribed to</th>
            <th className="bt-0 text-center">Team Members</th>
            <th className="bt-0 text-center">Your audience</th>
          </tr>
        </thead>
        <tbody>
          {teams.map(
            ({
              id,
              platform,
              name,
              teamMemberCount,
              channelMemberCount,
              pool: { title: poolTitle },
            }) => (
              <tr key={id}>
                <td className="align-middle">{name}</td>
                <td className="align-middle text-center">{platform}</td>
                <td className="align-middle text-center">{poolTitle}</td>
                <td className="align-middle text-center">{teamMemberCount}</td>
                <td className="align-middle text-center">
                  {channelMemberCount}
                </td>
              </tr>
            )
          )}
        </tbody>
      </Table>
    </Card>
  );
}
