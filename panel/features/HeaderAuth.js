import React from "react";
import PropTypes from "prop-types";

import { Link } from "./../components";
import { TeleroosterLogo } from "./LogoThemed/TeleroosterLogo";

const HeaderAuth = (props) => (
  <div className="mb-4">
    <div className="mb-4 text-center">
      <Link to="/" className="d-inline-block">
        {props.icon && (
          <i className={`fa fa-${props.icon} fa-3x ${props.iconClassName}`}></i>
        )}
      </Link>
    </div>
    <h5 className="text-center mb-4">{props.title}</h5>
    <p className="text-center">{props.text}</p>
  </div>
);
HeaderAuth.propTypes = {
  icon: PropTypes.node,
  iconClassName: PropTypes.node,
  title: PropTypes.node,
  text: PropTypes.node,
};
HeaderAuth.defaultProps = {
  title: "Waiting for Data...",
  text: "",
  iconClassName: "text-theme",
};

export { HeaderAuth };
