const padding = "_";

const getBase64Signatures = (text) => {
  const textWith1Padding = padding + text;
  const textWith2Padding = padding + padding + text;
  return [
    getSignature(text),
    getSignature(textWith1Padding, true),
    getSignature(textWith2Padding, true),
  ];
};

export default getBase64Signatures;

function toBase64(text) {
  return Buffer.from(text).toString("base64");
}

function getSignature(text, skipFirst = false) {
  const chunks = text.match(/.{1,3}/g);

  if (skipFirst) chunks.shift();
  const encodedChunks = chunks
    .filter((chunk) => chunk.length === 3)
    .map((chunk) => toBase64(chunk));
  return encodedChunks.join("");
}
