import { JSDOM } from "jsdom";
import { shouldOmitLink } from "./should-omit-link";

export const processLinks = (html) => {
  let modified = false;
  const jsdom = new JSDOM(html);
  const links = jsdom.window.document.querySelectorAll("a");
  // console.log("=======================================");
  // console.log("=======================================");
  // console.log("=======================================");
  links.forEach((a) => {
    try {
      if (
        shouldOmitLink({
          textContent: a.textContent,
          href: a.href,
          node: a,
        })
      ) {
        modified = true;
        const newElemenet = jsdom.window.document.createElement("span");
        newElemenet.innerHTML = a.innerHTML;
        a.replaceWith(newElemenet);
      }
    } catch (err) {
      modified = true;
      const newElemenet = jsdom.window.document.createElement("span");
      newElemenet.innerHTML = a.innerHTML;
      a.replaceWith(newElemenet);
    }
  });

  return modified ? jsdom.serialize() : html;
};
