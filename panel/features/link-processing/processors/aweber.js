const whitelistUrls = ["clicks.aweber.com"];

export default ({ hostname }) => {
  // console.log("processor for aweber");
  if (!hostname.toLowerCase().includes("aweber")) return { stop: false };

  return {
    stop: true,
    shouldOmit: !whitelistUrls.some((url) => hostname.includes(url)),
  };
};
