const domainUrls = ["getdrip.com"];
const whitelistUrls = [];

export default ({ hostname }) => {
  // console.log("processor for drip");
  const lowercaseHostname = hostname.toLowerCase();
  if (!domainUrls.some((url) => lowercaseHostname.includes(url)))
    return { stop: false };
  return {
    stop: true,
  };
};
