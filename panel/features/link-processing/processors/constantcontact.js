const domainUrls = [
  "constantcontact.com",
  "visitor.constantcontact.com",
  "rs6.net",
];
const whitelistUrls = ["rs6.net"];

export default ({ hostname, href }) => {
  // console.log("processor for constantcontact");
  const lowercaseHostname = hostname.toLowerCase();
  if (!domainUrls.some((url) => lowercaseHostname.includes(url)))
    return { stop: false };
  return {
    stop: true,
    shouldOmit: !whitelistUrls.some((url) => hostname.includes(url)),
  };
};
