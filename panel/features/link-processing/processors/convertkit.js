const whitelistUrls = ["click.convertkit"];

export default ({ hostname }) => {
  // console.log("processor for convertkit");
  if (!hostname.toLowerCase().includes("convertkit")) return { stop: false };

  return {
    stop: true,
    shouldOmit: !whitelistUrls.some((url) => hostname.includes(url)),
  };
};
