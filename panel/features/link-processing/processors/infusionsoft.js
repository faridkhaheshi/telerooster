const domainUrls = [
  "pages.keap.com",
  "mailserver.keap.com",
  "keap.com",
  "infusionsoft.com",
];
const whitelistUrls = ["mailserver.keap.com"];

export default ({ hostname, href }) => {
  // console.log("processor for infusionsoft");
  const lowercaseHostname = hostname.toLowerCase();
  if (!domainUrls.some((url) => lowercaseHostname.includes(url)))
    return { stop: false };
  return {
    stop: true,
    shouldOmit: !whitelistUrls.some((url) => href.includes(url)),
  };
};
