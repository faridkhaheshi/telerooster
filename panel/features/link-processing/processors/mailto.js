const pattern = /^mailto:/gi;

export default ({ href }) => {
  const matches = href.match(pattern);
  if (matches !== null) {
    return { stop: true, shouldOmit: false };
  }
  return { stop: false };
};
