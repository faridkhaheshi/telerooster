const domainUrls = ["tinyletterapp.com", "tinyletter.com"];
const whitelistUrls = ["tinyletterapp.com", "www.tinyletter.com"];

export default ({ hostname, href }) => {
  // console.log("processor for tinyletter");
  const lowercaseHostname = hostname.toLowerCase();
  if (!domainUrls.some((url) => lowercaseHostname.includes(url)))
    return { stop: false };
  return {
    stop: true,
    shouldOmit: !whitelistUrls.some((url) => href.includes(url)),
  };
};
