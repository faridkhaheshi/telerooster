const pattern = /(unsubscribe|un-subscribe|optout|opt out|opt-out)/gi;

export default ({ textContent }) => {
  // console.log("processor for unsubscribe");
  const matches = textContent.trim().match(pattern);
  if (matches !== null) {
    return { stop: true };
  }
  return { stop: false };
};
