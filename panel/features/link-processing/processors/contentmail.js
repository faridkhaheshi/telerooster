import getBase64Signatures from "../utils/get-base64-signatures";

const contentMailDomain = "contentmail.telerooster.com";

const containMainDomainEncodings = [
  contentMailDomain,
  ...getBase64Signatures(contentMailDomain),
  Buffer.from(contentMailDomain).toString("ucs2"),
  Buffer.from(contentMailDomain).toString("hex"),
];

export default ({ path }) => {
  // console.log("processor for contentmail");
  if (
    containMainDomainEncodings.some((s) => {
      return path.includes(s);
    })
  ) {
    return { stop: true };
  }
  return { stop: false };
};
