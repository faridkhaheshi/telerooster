const domainUrls = ["mailjet.com", "mjt.lu"];
const whitelistUrls = ["mjt.lu/lnk"];

export default ({ hostname, href }) => {
  // console.log("processor for mailjet");
  const lowercaseHostname = hostname.toLowerCase();
  if (!domainUrls.some((url) => lowercaseHostname.includes(url)))
    return { stop: false };
  return {
    stop: true,
    shouldOmit: !whitelistUrls.some((url) => href.includes(url)),
  };
};
