const domainUrls = ["thehustle.co"];
const whitelistUrls = ["link.thehustle.co/click"];

export default ({ hostname, href }) => {
  // console.log("processor for mailchimp");
  const lowercaseHostname = hostname.toLowerCase();
  if (!domainUrls.some((url) => lowercaseHostname.includes(url)))
    return { stop: false };
  return {
    stop: true,
    shouldOmit: !whitelistUrls.some((url) => href.includes(url)),
  };
};
