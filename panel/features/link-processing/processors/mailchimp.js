const domainUrls = ["list-manage.com", "mailchi.mp", "mailchimp.com"];
const whitelistUrls = ["list-manage.com/track/click", "mailchi.mp"];

export default ({ hostname, href }) => {
  // console.log("processor for mailchimp");
  const lowercaseHostname = hostname.toLowerCase();
  if (!domainUrls.some((url) => lowercaseHostname.includes(url)))
    return { stop: false };
  return {
    stop: true,
    shouldOmit: !whitelistUrls.some((url) => href.includes(url)),
  };
};
