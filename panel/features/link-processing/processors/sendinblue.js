const domainUrls = ["sendibm", "sendinblue.com"];
const whitelistUrls = [/sendibm*.*\/mk\/cl/gi];

export default ({ hostname, href }) => {
  // console.log("processor for sendinblue");
  const lowercaseHostname = hostname.toLowerCase();
  if (!domainUrls.some((url) => lowercaseHostname.includes(url)))
    return { stop: false };
  return {
    stop: true,
    shouldOmit: !whitelistUrls.some((url) => href.match(url) !== null),
  };
};
