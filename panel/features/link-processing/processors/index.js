import { default as unsubscribe } from "./unsubscribe";
import { default as mailto } from "./mailto";
import { default as contentmail } from "./contentmail";
import { default as convertkit } from "./convertkit";
import { default as substack } from "./substack";
import { default as mailchimp } from "./mailchimp";
import { default as tinyletter } from "./tinyletter";
import { default as infusionsoft } from "./infusionsoft";
import { default as aweber } from "./aweber";
import { default as drip } from "./drip";
import { default as mailjet } from "./mailjet";
import { default as activecampaign } from "./activecampaign";
import { default as constantcontact } from "./constantcontact";
import { default as sendinblue } from "./sendinblue";
import { default as thehustle } from "./thehustle";

export default [
  unsubscribe,
  mailto,
  contentmail,
  convertkit,
  substack,
  mailchimp,
  tinyletter,
  infusionsoft,
  aweber,
  drip,
  mailjet,
  activecampaign,
  constantcontact,
  sendinblue,
  thehustle,
];
