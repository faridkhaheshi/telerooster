const domainUrls = ["activehosted.com", "emlnk.com"];
const whitelistUrls = ["lt.emlnk.com/Prod/link-tracker"];

export default ({ hostname, href }) => {
  // console.log("processor for activecampaign");
  const lowercaseHostname = hostname.toLowerCase();
  if (!domainUrls.some((url) => lowercaseHostname.includes(url)))
    return { stop: false };
  return {
    stop: true,
    shouldOmit: !whitelistUrls.some((url) => href.includes(url)),
  };
};
