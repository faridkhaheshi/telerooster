const whitelistUrls = ["substack.com"];

export default ({ hostname }) => {
  // console.log("processor for substack");
  if (!hostname.toLowerCase().includes("substack")) return { stop: false };

  return {
    stop: true,
    shouldOmit: !whitelistUrls.some((url) => hostname.includes(url)),
  };
};
