import processors from "./processors";

/*
How processors work:
They return two values:
const { shouldOmit, stop } = processor(props);
If shouldOmit is true, the a tag will be replaced with <span> (i.e. no link)
If stop is true the chain of processors will be stopped.
*/

export const shouldOmitLink = ({ textContent, href, node }) => {
  let omit = true;
  const { hostname, pathname, search } = new URL(href);

  processors.some((processor) => {
    try {
      const { shouldOmit, stop } = processor({
        textContent,
        href,
        node,
        hostname,
        pathname,
        search,
        path: pathname + search,
      });

      if (shouldOmit === false) {
        omit = false;
      }
      return stop ? true : false;
    } catch (err) {
      return false;
    }
  });

  return omit;
};
