import { websiteUrl } from "./../../params";

export default `${websiteUrl}/unsubscribe`;
