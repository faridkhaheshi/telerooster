import React from "react";
import PropTypes from "prop-types";
import {
  CardElement,
  CardNumberElement,
  CardExpiryElement,
  CardCvcElement,
} from "@stripe/react-stripe-js";
import ReCaptcha from "react-google-recaptcha";

import useAsyncFormHandler from "../../../core/hooks/use-async-form-handler";
import useToast from "../../../core/hooks/use-toast";
import {
  Button,
  CardForm,
  CardText,
  Row,
  Col,
  Card,
  CardBody,
  CardFooter,
  CardTitle,
  Input,
  Label,
} from "./../../../components";

const PaymentCard = ({ plan = "creator" }) => {
  return (
    <CardForm title="Payment form" buttonText="Pay" waitingText="Processing...">
      <CardNumberElement className="form-control" />
      <CardExpiryElement className="form-control" />
      <CardCvcElement className="form-control" />
      <Label for="payment-form-name-on-card">Name on card</Label>
      <Input
        required
        type="text"
        name="nameOnCard"
        id="payment-form-name-on-card"
      />
    </CardForm>
  );
};

PaymentCard.propTypes = {
  plan: PropTypes.string,
};
PaymentCard.defaultProps = {
  plan: "creator",
};

export { PaymentCard };
