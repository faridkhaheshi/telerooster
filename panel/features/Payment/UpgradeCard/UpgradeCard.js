import React from "react";
import ReCaptcha from "react-google-recaptcha";
import { useStripe } from "@stripe/react-stripe-js";

import useAsyncFormHandler from "../../../core/hooks/use-async-form-handler";
import useToast from "../../../core/hooks/use-toast";
import { CardForm, CardText } from "./../../../components";
import { google } from "./../../../params";
import { useAuth } from "../../authentication";

const UpgradeCard = () => {
  const { isCustomer, planType, customerId } = useAuth();
  const [loading, setLoading] = React.useState(false);
  const stripe = useStripe();
  const { danger } = useToast();
  const [captchaKey, setCaptchaKey] = React.useState(null);
  const captchaRef = React.useRef();
  const proceedToCheckout = React.useCallback(
    async ({ id }) => {
      try {
        setLoading(true);
        await stripe.redirectToCheckout({ sessionId: id });
      } catch (err) {
        danger({ title: "Error", message: err.message });
      }
    },
    [stripe, danger]
  );
  const handleError = React.useCallback(
    (errMsg) => {
      captchaRef.current.reset();
      danger({ title: "Error", message: errMsg });
    },
    [captchaRef, danger]
  );
  const { handleSubmit, isProcessing } = useAsyncFormHandler({
    url: customerId
      ? `/api/v1/payments?captcha=${captchaKey}&customerId=${customerId}`
      : `/api/v1/payments?captcha=${captchaKey}`,
    onSuccess: proceedToCheckout,
    onError: handleError,
  });

  if (isCustomer && planType === "creator") return null;

  return (
    <CardForm
      disabled={!stripe || !captchaKey}
      hideRequiredHint
      title="Upgrade to Creator plan"
      buttonText="Proceed to payment"
      waitingText="Loading..."
      isProcessing={isProcessing || loading}
      onSubmit={handleSubmit}
    >
      <CardText>
        If you want to manage a channel of your own and regularly post your
        contents there, upgrade to the Creator Plan. With the Creator Plan you
        can post 100 messages each month.
      </CardText>
      <CardText>The price for the Creator Plan is:</CardText>
      <ul>
        <li>Base subscription: $29.99 /month.</li>
        <li>
          Usage fee: free for up to 2,000 subscribers, $5 /month for every 1,000
          subscribers above 2,000
        </li>
      </ul>
      <div className="d-flex justify-content-center mt-4">
        <ReCaptcha
          ref={captchaRef}
          sitekey={google.recaptchaSiteKey}
          onChange={setCaptchaKey}
        />
      </div>
    </CardForm>
  );
};
export { UpgradeCard };
