import React from "react";
import useAsyncFormHandler from "../../../core/hooks/use-async-form-handler";
import useToast from "../../../core/hooks/use-toast";
import { useAuth } from "../../authentication";

import { CardText, CardForm } from "./../../../components";

const BillingPortalCard = () => {
  const [isWaiting, setIsWaiting] = React.useState(false);
  const { customerId, isCustomer } = useAuth();
  const { handleError } = useToast();
  const goToBillingPortal = React.useCallback(
    ({ url }) => {
      setIsWaiting(true);
      window.location.href = url;
    },
    [setIsWaiting]
  );
  const { handleSubmit: getBillingPortal, isProcessing } = useAsyncFormHandler({
    url: `/api/v1/payments/billing-portal?customerId=${customerId}`,
    onSuccess: goToBillingPortal,
    onError: handleError,
  });

  if (!customerId || !isCustomer) return null;

  return (
    <CardForm
      hideRequiredHint
      title="Billing portal"
      buttonText="Go to billing portal"
      waitingText="Loading..."
      isProcessing={isProcessing || isWaiting}
      onSubmit={getBillingPortal}
    >
      <CardText>
        You can check your billing status, change your payment method, or cancel
        your subscription at the billing portal.
      </CardText>
    </CardForm>
  );
};

export { BillingPortalCard };
