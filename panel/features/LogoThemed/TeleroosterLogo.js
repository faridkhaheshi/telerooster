import React from "react";
import PropTypes from "prop-types";

export const TeleroosterLogo = () => <div>teleRooster</div>;

TeleroosterLogo.propTypes = {
  checkBackground: PropTypes.bool,
  className: PropTypes.string,
};
