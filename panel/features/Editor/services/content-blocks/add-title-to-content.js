export const addTitleToContent = ({ title, blocks }) => {
  blocks.unshift({
    type: "poster",
    content: {
      payload: {
        text_color: "white",
        background_color: "#a83232",
        title: title,
      },
      template: "public-title",
      alt: title,
    },
  });
};
