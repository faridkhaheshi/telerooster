import { convertHtmlToMarkdown } from "../parsing";

export const convertNodeToContentBlock = ({
  nodeData,
  nodeInfo,
  reason,
  innerHtml,
  element,
  options = [],
}) => {
  const { nodeName, tagName } = nodeData;
  const type = identifyBlockType({ tagName, nodeData, nodeInfo, reason });
  const content = extractBlockContent({
    type,
    element,
    innerHtml,
    reason,
    nodeInfo,
    nodeData,
    tagName,
  });
  return {
    type,
    content,
    source: {
      element,
      nodeName,
      nodeData,
      nodeInfo,
      reason,
      innerHtml,
    },
    options,
  };
};

function identifyBlockType({ tagName, nodeInfo }) {
  if (nodeInfo.isTextNode) return "p";
  if (["h1", "h2", "h3", "h4", "h5", "h6"].indexOf(tagName) > -1) return "h";
  if (["img", "figure"].indexOf(tagName) > -1) return "image";
  if (tagName === "hr") return "divider";
  if (["p", "a", "div"].indexOf(tagName) > -1) return "p";
  if (tagName === "ul") return "ul";
  if (tagName === "ol") return "ol";
  if (tagName === "small") return "small";
  if (tagName === "button") return "button";
  if (tagName === "br") return "p";
  return "custom";
}

function extractBlockContent({ type, element, innerHtml, nodeInfo, tagName }) {
  // if (tagName === "br") return { markdown: "‎" };
  if (tagName === "br") return { markdown: "\n\n" };
  if (type === "h") return { markdown: convertHtmlToMarkdown(innerHtml) };
  if (type === "hr") return { markdown: "\n\n" };
  if (type === "image") {
    const imageContent = { image: nodeInfo.image };
    if (nodeInfo.caption) imageContent.caption = nodeInfo.caption;

    return imageContent;
  }
  return { markdown: convertHtmlToMarkdown(element) };
}
