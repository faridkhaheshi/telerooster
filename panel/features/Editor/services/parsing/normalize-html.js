import { convertHtmlToMarkdown } from "./convert-html-to-markdown";
import { convertMarkdownToHtml } from "./convert-markdown-to-html";

export const normalizeHtml = (html) => {
  const markdown = convertHtmlToMarkdown(html);
  console.log(markdown);
  return convertMarkdownToHtml(markdown);
};
