import { convertHtmlToMarkdown } from "../convert-html-to-markdown";

export const mergeTextLeaves = (leaves, createElement) => {
  const combinedElement = `<p>${leaves
    .map((leaf) => {
      return createElement(leaf);
    })
    .join("")}</p>`;
  return {
    type: "p",
    content: { markdown: convertHtmlToMarkdown(combinedElement) },
    source: {
      element: combinedElement,
      nodeName: "p",
      nodeData: { tagName: "p" },
      nodeInfo: { isTextNode: true },
    },
    options: [],
  };
};
