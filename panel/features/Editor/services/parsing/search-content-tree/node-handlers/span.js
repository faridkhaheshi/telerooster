import { hasOnlyTextChildren } from "../utils";

export const span = ({ node, options, nodeInfo }) => {
  if (hasOnlyTextChildren(node))
    return {
      action: "accept",
      reason: { hasOnlyTextChildren: true },
      options,
      nodeInfo,
    };
  return { action: "pass", reason: {}, options, nodeInfo };
};
