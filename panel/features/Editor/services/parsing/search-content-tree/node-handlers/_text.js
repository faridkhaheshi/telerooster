export const _text = ({ node, options, nodeInfo }) => {
  return {
    action: nodeInfo.isNonEmptyTextNode ? "accept" : "ignore",
    reason: { leaf: true },
    options,
    nodeInfo,
  };
};
