import { extractNodeAttrs } from "../utils";

export const img = ({ node, options, nodeInfo }) => {
  const image = extractNodeAttrs(node);
  nodeInfo.image = image;
  return {
    action: "accept",
    reason: { isTarget: true },
    options,
    nodeInfo,
  };
};
