import { hasOnlyTextChildren } from "../utils";

export const p = ({ node, options, nodeInfo }) => {
  const textChildrenOnly = hasOnlyTextChildren(node);

  return {
    action: textChildrenOnly ? "accept" : "pass",
    reason: textChildrenOnly
      ? { isTarget: true, hasOnlyTextChildren: textChildrenOnly }
      : {},
    options,
    nodeInfo,
  };
};
