import { hasOnlyTextChildren } from "../utils";

export const __textRelatedNode = ({ node, options, nodeInfo }) => {
  const textOnlyChildren = hasOnlyTextChildren(node);

  if (textOnlyChildren)
    return {
      action: "accept",
      reason: { hasOnlyTextChildren: true, isTarget: true },
      options,
      nodeInfo,
    };

  return {
    action: "pass",
    reason: {},
    options: [...options, { tag: node.tagName }],
    nodeInfo,
  };
};
