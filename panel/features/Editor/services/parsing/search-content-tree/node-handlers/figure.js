import { extractNodeAttrs } from "../utils";

export const figure = ({ node, options, nodeInfo }) => {
  node.childNodes.forEach((n) => {
    if (n.tagName === "img") {
      nodeInfo.image = extractNodeAttrs(n);
    } else if (n.tagName === "figcaption") {
      nodeInfo.caption = n.childNodes[0].value;
    }
  });
  return {
    action: "accept",
    reason: { isTarget: true },
    options,
    nodeInfo,
  };
};
