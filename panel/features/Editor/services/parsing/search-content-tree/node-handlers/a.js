import { extractNodeAttrs, hasOnlyTextChildren } from "../utils";

export const a = ({ node, options, nodeInfo }) => {
  return {
    action: "pass",
    reason: {},
    options: [...options, { tag: node.tagName, attrs: extractNodeAttrs(node) }],
    nodeInfo,
  };
};
