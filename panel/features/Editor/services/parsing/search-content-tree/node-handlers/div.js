import { hasOnlyTextChildren } from "../utils";

export const div = ({ node, options, nodeInfo }) => {
  const { attrs = [] } = node;
  const isGmailQuote =
    attrs.filter(
      ({ name, value }) => name === "class" && value === "gmail_attr"
    ).length > 0;

  if (isGmailQuote)
    return {
      action: "ignore",
      options,
      nodeInfo,
    };
  if (hasOnlyTextChildren(node)) {
    return {
      action: "accept",
      reason: { hasOnlyTextChildren: true },
      options: options,
      nodeInfo,
    };
  }
  return {
    action: isGmailQuote ? "ignore" : "pass",
    options,
    nodeInfo,
  };
};
