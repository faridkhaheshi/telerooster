import * as nodeHandlers from "./node-handlers";
import { extractNodeInfo } from "./utils";
import ignoredTags from "./ignored-tags";
import targetTags from "./target-tags";
import textRelatedNodeNames from "./text-related-node-names";
import textNodeNames from "./utils/text-node-names";

export const processNode = ({ node, options }) => {
  const nodeInfo = extractNodeInfo(node);
  const { childNodes = [] } = node;
  const nodeHandler = findNodeHandler(node);
  if (nodeHandler) return nodeHandler({ node, options, nodeInfo });

  if (targetTags.indexOf(node.tagName) > -1) {
    return {
      action: "accept",
      reason: { isTarget: true },
      options,
      nodeInfo,
    };
  }

  if (
    nodeInfo.isCommentNode ||
    (nodeInfo.isElementNode && ignoredTags.indexOf(node.tagName) > -1)
  ) {
    return { action: "ignore" };
  }

  if (textNodeNames.indexOf(node.nodeName) > -1 && childNodes.length === 0) {
    return { action: "ignore" };
  }

  if (childNodes.length === 0) {
    return {
      action: "accept",
      reason: { leaf: true },
      options,
      nodeInfo,
    };
  }

  return {
    action: "pass",
    reason: {},
    options,
    nodeInfo,
  };
};

function findNodeHandler(node) {
  let handlerKey = node.nodeName.replace(/\#/g, "_");
  let hasHandler = Object.keys(nodeHandlers).indexOf(handlerKey) > -1;
  if (!hasHandler && textRelatedNodeNames.indexOf(node.nodeName) > -1) {
    handlerKey = "__textRelatedNode";
    hasHandler = Object.keys(nodeHandlers).indexOf(handlerKey) > -1;
  }
  if (hasHandler) return nodeHandlers[handlerKey];
}
