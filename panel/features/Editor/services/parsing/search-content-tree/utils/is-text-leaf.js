import textRelatedNodeNames from "../text-related-node-names";

export const isTextLeaf = ({ nodeName }) =>
  textRelatedNodeNames.indexOf(nodeName) > -1;
