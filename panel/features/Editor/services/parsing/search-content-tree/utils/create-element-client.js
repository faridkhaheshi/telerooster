import serializer from "xmlserializer";

export const createElementClient = ({ element, options = [], nodeData }) => {
  const resultingElement = options
    .reverse()
    .reduce((acc, { tag, attrs = {} }) => {
      const newElement = document.createElement(tag);
      Object.keys(attrs).forEach((attr) => {
        newElement.setAttribute(attr, attrs[attr]);
      });
      newElement.innerHTML = acc;
      return serializer.serializeToString(newElement);
    }, element);
  return resultingElement;
};
