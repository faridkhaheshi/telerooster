import { searchContentTree } from "../search-content-tree";
import textRelatedNodeNames from "./../text-related-node-names";
import searchAllNodes from "./search-all-nodes";

export const hasOnlyTextChildren = (node) => {
  const { childNodes = [] } = node;
  const nonTextRelatedChildren = childNodes.filter(
    (n) => textRelatedNodeNames.indexOf(n.nodeName) === -1
  );
  let hasImageChild = false;
  searchAllNodes(node, ({ nodeName }) => {
    if (nodeName === "img") hasImageChild = true;
  });
  return !hasImageChild && nonTextRelatedChildren.length < 1;
};
