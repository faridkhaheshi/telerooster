export const extractNodeAttrs = (node) => {
  try {
    const nodeAttrs = node.attrs || node.attributes || [];
    const attributes = {};
    for (let i = 0; i < nodeAttrs.length; i++) {
      attributes[nodeAttrs[i].name] = nodeAttrs[i].value;
    }
    return attributes;
  } catch (err) {
    return {};
  }
};
