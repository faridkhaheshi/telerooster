const searchAllNodes = (tree, processor = () => {}) => {
  const { childNodes } = tree;
  processor(tree);
  if (childNodes) {
    childNodes.forEach((node) => searchAllNodes(node, processor));
  }
};

export default searchAllNodes;
