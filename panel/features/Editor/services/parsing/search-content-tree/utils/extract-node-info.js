import TreeAdapter from "parse5/lib/tree-adapters/default";

export const extractNodeInfo = (node) => {
  const isTextNode = TreeAdapter.isTextNode(node);
  return {
    isCommentNode: TreeAdapter.isCommentNode(node),
    isDocumentTypeNode: TreeAdapter.isDocumentTypeNode(node),
    isTextNode,
    isElementNode: TreeAdapter.isElementNode(node),
    isNonEmptyTextNode: isTextNode && node.value.replace(/\s/g, "").length > 0,
  };
};
