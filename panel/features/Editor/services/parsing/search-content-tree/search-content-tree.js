import parser from "parse5";
import serializer from "xmlserializer";
import { processNode } from "./process-node";

export const searchContentTree = (
  tree,
  processAccepted = () => ({}),
  options = []
) => {
  const { action, reason, options: newOptions, nodeInfo } = processNode({
    node: tree,
    options,
  });
  if (action === "ignore") return;
  if (action === "accept") {
    const { childNodes, parentNode, ...nodeData } = tree;
    return processAccepted({
      nodeData,
      nodeInfo,
      reason,
      innerHtml: parser.serialize(tree),
      element: serializer.serializeToString(tree),
      options: newOptions,
    });
  }
  if (tree.childNodes) {
    tree.childNodes.forEach((node) => {
      searchContentTree(node, processAccepted, newOptions);
    });
  }
};
