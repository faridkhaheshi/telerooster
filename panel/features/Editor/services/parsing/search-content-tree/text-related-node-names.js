const textRelatedNodeNames = [
  "#text",
  "em",
  "b",
  "strong",
  "i",
  "a",
  "big",
  "small",
  "strike",
  "u",
  "span",
  "s",
];

export default textRelatedNodeNames;
