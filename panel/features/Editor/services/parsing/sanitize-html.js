import sanitize from "sanitize-html";

const options = {
  allowedTags: sanitize.defaults.allowedTags.concat([
    "img",
    "figure",
    "video",
    "audio",
    "big",
  ]),
  allowedAttributes: false,
};

export const sanitizeHtml = (html) => sanitize(html, options);
