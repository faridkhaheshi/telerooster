import marked from "marked";

// export const convertMarkdownToHtml = (markdown) => converter.makeHtml(markdown);
// export const convertMarkdownToHtml = (markdown) => marked.parseInline(markdown);
export const convertMarkdownToHtml = (markdown) => marked(markdown);
