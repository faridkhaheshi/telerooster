import TurndownService from "turndown";
import { gfm } from "turndown-plugin-gfm";

const turndownService = new TurndownService({
  headingStyle: "atx",
  bulletListMarker: "-",
});

turndownService.use(gfm);

export const convertHtmlToMarkdown = (html) => turndownService.turndown(html);
