import parser from "parse5";
import { convertNodeToContentBlock } from "./../content-blocks";
import { finalizeContentBlocks } from "./finalize-content-blocks";
import { sanitizeHtml } from "./sanitize-html";
import {
  searchContentTree,
  isTextLeaf,
  mergeTextLeaves,
} from "./search-content-tree";

export const convertHtmlToContentBlocks = (html, { createElement } = {}) => {
  const sanitized = sanitizeHtml(html);
  const docTree = parser.parse(sanitized);
  const blocks = [];
  let waitingLeaves = [];

  const processAcceptedNodes = (newNode) => {
    if (isTextLeaf(newNode.nodeData)) {
      waitingLeaves.push(newNode);
    } else if (waitingLeaves.length > 0) {
      const textBlock = mergeTextLeaves(waitingLeaves, createElement);
      waitingLeaves = [];
      blocks.push(textBlock);
      const newBlock = convertNodeToContentBlock(newNode);
      blocks.push(newBlock);
    } else {
      const newBlock = convertNodeToContentBlock(newNode);
      blocks.push(newBlock);
    }
  };
  searchContentTree(docTree, processAcceptedNodes);
  if (waitingLeaves.length > 0) {
    const lastBlock = mergeTextLeaves(waitingLeaves, createElement);
    blocks.push(lastBlock);
  }
  // return blocks;
  return finalizeContentBlocks(blocks);
};
