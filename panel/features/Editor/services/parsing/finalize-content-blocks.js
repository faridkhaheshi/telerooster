const MAX_BLOCK_SIZE = 2950;

const mergableBlockTypes = ["p", "ul", "ol"];

export const finalizeContentBlocks = (blocks) => {
  const newBlocks = [];
  let waitingBlocks = [];

  blocks.forEach((block) => {
    try {
      if (!isMergableBlock(block)) {
        newBlocks.push(...waitingBlocks);
        newBlocks.push(block);
        waitingBlocks = [];
      } else if (canMergeWithWaitingBlocks(block, waitingBlocks)) {
        if (waitingBlocks.length > 0) {
          const lastWaitingBlock = waitingBlocks[waitingBlocks.length - 1];
          lastWaitingBlock.content.markdown =
            lastWaitingBlock.content.markdown + "\n\n" + block.content.markdown;
        } else {
          waitingBlocks.push(block);
        }
      } else if (block.content.markdown.length <= MAX_BLOCK_SIZE) {
        newBlocks.push(...waitingBlocks);
        waitingBlocks = [block];
      } else {
        newBlocks.push(...waitingBlocks);
        waitingBlocks = breakLargeBlock(block);
      }
    } catch (err) {
      newBlocks.push(...waitingBlocks);
      newBlocks.push(block);
      waitingBlocks = [];
    }
  });

  if (waitingBlocks.length > 0) newBlocks.push(...waitingBlocks);
  return newBlocks.map(({ type, content, options }) => ({
    type,
    content,
    options,
  }));
};

function isMergableBlock({ type }) {
  return mergableBlockTypes.indexOf(type) > -1;
}

function canMergeWithWaitingBlocks(block, prevBlocks) {
  try {
    const prevBlockLen =
      prevBlocks.length === 0
        ? 0
        : prevBlocks[prevBlocks.length - 1].content.markdown.length;
    const currentBlockLen = block.content.markdown.length;
    return currentBlockLen + prevBlockLen + 2 < MAX_BLOCK_SIZE;
  } catch (err) {
    return false;
  }
}

function breakLargeBlock(block) {
  const brokenBlocks = block.content.markdown.split("\n");
  const normalizedBlocks = [];
  brokenBlocks.forEach((blk) => {
    if (blk.length <= MAX_BLOCK_SIZE) {
      normalizedBlocks.push({ type: block.type, content: { markdown: blk } });
      return;
    }
    const chunks = blk.match(new RegExp(".{1," + MAX_BLOCK_SIZE + "}", "g"));
    normalizedBlocks.push(
      ...chunks.map((chunk) => ({
        type: block.type,
        content: { markdown: chunk },
      }))
    );
  });
  return finalizeContentBlocks(normalizedBlocks);
}
