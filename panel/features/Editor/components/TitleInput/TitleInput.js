import React from "react";
import PropTypes from "prop-types";

import "./TitleInput.css";

const TitleInput = React.forwardRef(({ onEnter = () => ({}) }, ref) => {
  const handleKeyPress = React.useCallback(
    (e) => {
      const keyCode = e.which || e.charCode || e.keyCode;
      if (keyCode === 13) {
        e.preventDefault();
        onEnter(e);
      }
    },
    [onEnter]
  );

  const handleKeyDown = React.useCallback(
    (e) => {
      const keyCode = e.which || e.charCode || e.keyCode;
      if (keyCode === 40) {
        onEnter(e);
      }
    },
    [onEnter]
  );
  return (
    <div
      ref={ref}
      placeholder="Title"
      contentEditable={true}
      className="title-input quill"
      onKeyPress={handleKeyPress}
      onKeyDown={handleKeyDown}
    />
  );
});

TitleInput.propTypes = {
  onEnter: PropTypes.func,
};

TitleInput.defaultProps = {
  onEnter: () => ({}),
};

export { TitleInput };
