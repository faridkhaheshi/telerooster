import React from "react";
import PropTypes from "prop-types";
import { useDropzone } from "react-dropzone";
import classNames from "classnames";

import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Input,
  Label,
} from "./../../../../components";

const MediaPickerModal = ({ onPick, toggle, isOpen }) => (
  <Modal isOpen={isOpen} toggle={toggle} size="lg">
    <ModalHeader tag="h5" toggle={toggle}>
      Add a picture
    </ModalHeader>
    <ModalBody>
      <p className="mt-4 mb-4">Add the link to an image</p>
      <ImageUrlForm onSubmit={onPick} />
      <hr />
      <p className="text-center mt-4 mb-4">or upload one</p>
      <ImagePicker onPick={onPick} />
    </ModalBody>
  </Modal>
);

MediaPickerModal.propTypes = {
  onPick: PropTypes.func,
  isOpen: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
};

MediaPickerModal.defaultProps = {
  onPick: () => ({}),
};

export { MediaPickerModal, ImagePicker };

function ImagePicker({ onPick }) {
  const onDrop = React.useCallback((files) => {
    onPick(files.map((file) => ({ type: "image", file })));
  }, []);

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop,
    accept: "image/jpeg, image/png, image/gif",
    // maxFiles: 50,
    multiple: false,
  });
  const dropzoneClass = classNames(
    {
      "dropzone--active": isDragActive,
    },
    "dropzone"
  );

  return (
    <div {...getRootProps()} className={dropzoneClass}>
      <input {...getInputProps()} />
      <DropzoneDialog />
    </div>
  );
}

ImagePicker.propTypes = {
  onPick: PropTypes.func,
};

ImagePicker.defaultProps = {
  onPicker: () => ({}),
};

function DropzoneDialog() {
  return (
    <>
      <i className="fa fa-cloud-upload fa-fw fa-3x mb-3"></i>
      <h5 className="mt-0">Upload Your files</h5>
      <p className="mt-4">
        Drag a file here or <span className="text-primary">browse</span> for a
        file to upload.
      </p>
      <p className="small">JPEG, GIF, and PNG.</p>
    </>
  );
}

function ImageUrlForm({ onSubmit }) {
  const [url, setUrl] = React.useState("");
  const handleSubmit = React.useCallback(
    (e) => {
      e.preventDefault();
      onSubmit([{ type: "image", image: { src: url } }]);
    },
    [url]
  );
  return (
    <Form onSubmit={handleSubmit}>
      <FormGroup>
        <Label>Image url:</Label>
        <Input
          type="url"
          value={url}
          onChange={(e) => {
            setUrl(e.target.value);
          }}
          placeholder="for example https://mywebsite.com/images/sample.png"
        />
      </FormGroup>
      <div className="d-flex justify-content-end">
        <Button color="primary">Add</Button>
      </div>
    </Form>
  );
}

ImageUrlForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};
