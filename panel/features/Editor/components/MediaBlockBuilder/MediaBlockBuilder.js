import React from "react";
import PropTypes from "prop-types";
import { MediaPickerModal } from "./MediaPickerModal";

import "./MediaBlockBuilder.css";

const MediaBlockBuilder = React.forwardRef(
  ({ float, selectionBound = {}, onMediaPick = () => ({}) }, ref) => {
    const [modalOpen, setModalOpen] = React.useState(false);
    const toggleModal = React.useCallback(() => {
      setModalOpen((old) => !old);
    }, [selectionBound]);

    const handleMedia = React.useCallback(
      (media) => {
        setModalOpen(false);
        const mediaArray = Array.isArray(media) ? media : [media];
        onMediaPick(mediaArray);
      },
      [onMediaPick]
    );

    let style = {};
    if (selectionBound) {
      style.top = selectionBound.bottom;
      style.height = 200 - selectionBound.height;
    }
    if (!float || !selectionBound) return <div ref={ref} />;

    return (
      <div className="media-adder-container" style={style} ref={ref}>
        <button size="lg" className="add-media-button" onClick={toggleModal}>
          <i className="fa fa-fw fa-plus" aria-hidden="true"></i>
        </button>
        <p className="mt-2">add a picture</p>
        <MediaPickerModal
          isOpen={modalOpen}
          onPick={handleMedia}
          toggle={toggleModal}
        />
      </div>
    );
  }
);

MediaBlockBuilder.propTypes = {
  float: PropTypes.bool.isRequired,
};

MediaBlockBuilder.defaultProps = {};

export { MediaBlockBuilder };
