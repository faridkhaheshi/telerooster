import React from "react";
import PropTypes from "prop-types";

import { useRouter } from "next/router";

import {
  Form,
  FormGroup,
  Label,
  Input,
  ThreeStateButton,
  SimpleTextInput,
  Link,
} from "./../../../../components";
import useServerResource from "../../../../core/hooks/use-server-resource";
import useAsyncFormHandler from "../../../../core/hooks/use-async-form-handler";
import useToast from "../../../../core/hooks/use-toast";

const MessageSender = ({
  buildMessage,
  readyToSend = true,
  onSuccess = () => ({}),
}) => {
  const { success, danger } = useToast();
  const {
    query: { poolId: defaultPoolId, action = "send" } = {},
  } = useRouter();
  const prepareMessage = React.useCallback(
    ({ target }) => ({
      ...buildMessage(),
      poolId: target["editor-pool-selector"].value,
      action,
      event: target["editor-journey-event"]
        ? target["editor-journey-event"].value
        : undefined,
    }),
    [buildMessage, action]
  );

  const handleSuccessfulSubmission = React.useCallback(
    (result) => {
      success({ message: "message sent to your channel." });
      onSuccess();
    },
    [onSuccess]
  );
  const { handleSubmit, isProcessing } = useAsyncFormHandler({
    url: "/api/v1/messages",
    requestOptions: { method: "POST" },
    parseForm: prepareMessage,
    onSuccess: handleSuccessfulSubmission,
    onError: (errorMessage) => {
      danger({ title: "Sending Failed", message: errorMessage });
    },
  });

  return (
    <MessageSenderForm
      includeEvent={action !== "send"}
      onSubmit={handleSubmit}
      disabled={!readyToSend}
      isProcessing={isProcessing}
      defaultPoolId={defaultPoolId}
    />
  );
};

MessageSender.propTypes = {
  buildMessage: PropTypes.func.isRequired,
  readyToSend: PropTypes.bool,
  onSuccess: PropTypes.func,
};

MessageSender.defaultProps = {
  readyToSend: true,
  onSuccess: () => ({}),
};

export { MessageSender };

function MessageSenderForm({
  onSubmit = () => ({}),
  isProcessing,
  defaultPoolId,
  disabled = false,
  includeEvent = false,
}) {
  const { data: pools, error, isLoading } = useServerResource("/api/v1/pools");
  if (isLoading)
    return (
      <div className="d-flex justify-content-between mt-5">Loading...</div>
    );
  if (error)
    return (
      <div className="d-flex justify-content-between mt-5">
        Failed to load your channels.
      </div>
    );

  if (pools.length === 0)
    return (
      <div>
        You don't have any channels. Create one{" "}
        <strong>
          <Link to="/mychannels">here</Link>
        </strong>
        .
      </div>
    );

  return (
    <Form
      inline
      className="d-flex justify-content-between mt-5"
      onSubmit={onSubmit}
    >
      <FormGroup>
        <Label for="editor-pool-selector" className="mr-4">
          Destination:
        </Label>
        <Input
          type="select"
          name="pool"
          defaultValue={defaultPoolId}
          id="editor-pool-selector"
        >
          {pools.map(({ id, title }) => (
            <option key={id} value={id}>
              {title}
            </option>
          ))}
        </Input>
      </FormGroup>
      {includeEvent && (
        <SimpleTextInput label="event" name="event" id="editor-journey-event" />
      )}
      <ThreeStateButton
        color="primary"
        isProcessing={isProcessing}
        disabled={disabled}
        waitingContent="Sending..."
      >
        Send Content
      </ThreeStateButton>
    </Form>
  );
}

MessageSenderForm.propTypes = {
  onSubmit: PropTypes.func,
  isProcessing: PropTypes.bool,
  disabled: PropTypes.bool,
};

MessageSenderForm.defaultProps = {
  onSubmit: () => ({}),
  isProcessing: false,
  disabled: false,
};
