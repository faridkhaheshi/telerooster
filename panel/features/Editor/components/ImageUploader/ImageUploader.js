import React from "react";
import PropTypes from "prop-types";

import { Progress } from "./../../../../components";
import useImageUploader from "./use-image-uploader";

import "./ImageUploader.css";

const ImageUploader = ({
  file,
  alt = "image uploading",
  onUpload = () => ({}),
}) => {
  const [hideProgress, setHideProgress] = React.useState(false);
  const { progress, image, errorMessage } = useImageUploader(file, onUpload);
  const timer = React.useRef();

  React.useEffect(() => {
    if (progress >= 100 && !hideProgress) {
      timer.current = setTimeout(() => {
        setHideProgress(true);
      }, 500);
    }
    return () => {
      if (timer.current) {
        clearTimeout(timer.current);
      }
    };
  }, [progress, hideProgress, timer.current]);

  return (
    <div>
      {image && image.src ? (
        <img src={image.src} alt={alt} />
      ) : (
        <img src={URL.createObjectURL(file)} alt={alt} />
      )}
      {!hideProgress && (
        <Progress
          value={progress}
          style={{ height: "4px", borderRadius: 0 }}
          color={errorMessage ? "danger" : "primary"}
        />
      )}
    </div>
  );
};

ImageUploader.propTypes = {
  file: PropTypes.shape({}).isRequired,
  alt: PropTypes.string,
  onUpload: PropTypes.func,
};

ImageUploader.defaultProps = {
  alt: "image uploading",
  onUpload: () => ({}),
};

export { ImageUploader };
