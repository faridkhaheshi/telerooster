import React from "react";
import { sendApiRequest } from "../../../../adapters/api/send-api-request";

const useImageUploader = (file, onUpload = () => ({})) => {
  const [progress, setProgress] = React.useState(null);
  const [image, setImage] = React.useState(null);
  const [errorMessage, setErrorMessage] = React.useState(null);
  const uploadRequest = React.useRef();

  const updateProgress = React.useCallback((newProgress) => {
    setProgress(newProgress);
  }, []);

  const handleSuccess = React.useCallback((response) => {
    setImage({
      src: response.secure_url,
      format: response.format,
      bytes: response.bytes,
      data: response,
    });
    onUpload({
      src: response.secure_url,
      format: response.format,
      bytes: response.bytes,
      data: response,
    });
  }, []);

  const handleFailure = React.useCallback((error) => {
    console.error(error);
    setErrorMessage(error.message || "Upload failed");
  }, []);

  const startUpload = React.useCallback(async () => {
    try {
      const { params, url } = await sendApiRequest("/api/v1/images/upload");
      const formData = buildFormData({ file, params });
      const xhr = startUploadRequest({
        url,
        body: formData,
        onProgress: updateProgress,
        onSuccess: handleSuccess,
        onFailure: handleFailure,
      });
      uploadRequest.current = xhr;
    } catch (err) {
      console.error(err);
    }
  }, [updateProgress, uploadRequest, handleSuccess, handleFailure]);

  React.useEffect(() => {
    startUpload();
  }, []);
  return { progress, image, errorMessage };
};

export default useImageUploader;

function buildFormData({ file, params }) {
  const formData = new FormData();
  formData.append("file", file);
  Object.keys(params).forEach((key) => {
    formData.append(key, params[key]);
  });
  return formData;
}

function startUploadRequest({ onProgress, url, onSuccess, onFailure, body }) {
  const xhr = new XMLHttpRequest();
  xhr.open("POST", url, true);
  xhr.upload.addEventListener("progress", (e) => {
    const progress = parseInt(Math.round((e.loaded * 100.0) / e.total));
    onProgress(progress);
  });

  xhr.onreadystatechange = (e) => {
    if (xhr.readyState == 4 && xhr.status == 200) {
      const response = JSON.parse(xhr.responseText);
      onSuccess(response);
    } else if (xhr.readyState == 4) {
      onFailure(xhr.statusText);
    }
  };
  xhr.send(body);
  return xhr;
}
