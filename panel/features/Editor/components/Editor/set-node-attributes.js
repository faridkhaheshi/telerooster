export const setNodeAttributes = (node, attrs) => {
  Object.keys(attrs).forEach((attr) => {
    if (attrs[attr]) {
      node.setAttribute(attr, attrs[attr]);
    }
  });
};
