import React from "react";
import ReactDOM from "react-dom";
import ReactQuill from "react-quill";
import ImageUploader from "../ImageUploader";
import { setNodeAttributes } from "./set-node-attributes";
const Quill = ReactQuill.Quill;
const BlockEmbed = Quill.import("blots/block/embed");

let filesCache = [];
let dispatch = () => ({});

class ImageBlot extends BlockEmbed {
  static create(value) {
    const fileId = value["data-file-id"];
    if (fileId) {
      return createImageUploader({ fileId, value });
    } else {
      const node = super.create();
      setNodeAttributes(node, value);
      const container = document.createElement("div");
      container.className = "editor-image-container";
      container.appendChild(node);
      return container;
    }
  }

  static value(domNode) {
    return {
      alt: domNode.getAttribute("alt"),
      src: domNode.getAttribute("src"),
      srcset: domNode.getAttribute("srcset"),
      width: domNode.getAttribute("width"),
      height: domNode.getAttribute("height"),
      style: domNode.getAttribute("style"),
    };
  }
}

ImageBlot.blotName = "image";
ImageBlot.tagName = "img";
export default ImageBlot;

export const updateFilesCache = (files, editorDispatch) => {
  filesCache = files;
  dispatch = editorDispatch;
};

function createImageUploader({ fileId, value }) {
  const theFile = filesCache.find((f) => f.id === fileId);
  const container = document.createElement("div");
  container.className = "editor-image-container";
  if (!theFile) return container;
  ReactDOM.render(
    <ImageUploader
      file={theFile.file}
      alt={value.alt || "uploading"}
      onUpload={(image) => {
        dispatch({ type: "upload-finished", payload: { fileId, image } });
      }}
    />,
    container
  );
  dispatch({ type: "upload-started", payload: { fileId } });
  return container;
}
