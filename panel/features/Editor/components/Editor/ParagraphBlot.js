import { Quill } from "react-quill";
import nextId from "react-id-generator";

const Block = Quill.import("blots/block");

const ID_PREFIX = "editor-block-";

class ParagraphBlot extends Block {
  static create(value) {
    const node = super.create(value);
    node.setAttribute("id", nextId(ID_PREFIX));
    return node;
  }

  split(index, force = false) {
    // if (force && (index === 0 || index >= this.length() - 1)) {
    //   const clone = this.clone();
    //   clone.domNode.id = nextId(ID_PREFIX);
    //   clone.domNode.classList.remove("active-block");
    //   if (index === 0) {
    //     this.parent.insertBefore(clone, this);
    //     return this;
    //   }
    //   this.parent.insertBefore(clone, this.next);
    //   return clone;
    // }

    this.domNode.classList.remove("active-block");
    const next = super.split(index, force);
    next.domNode.id = nextId(ID_PREFIX);
    this.cache = {};
    return next;
  }
}

export default ParagraphBlot;
