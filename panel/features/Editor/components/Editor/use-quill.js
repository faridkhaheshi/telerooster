import React from "react";
import { Quill } from "react-quill";
import nextId from "react-id-generator";
import ImageBlot, { updateFilesCache } from "./ImageBlot";
import ParagraphBlot from "./ParagraphBlot";
import filesReducer from "./files-reducer";
import useKeyboard from "./use-keyboard";

Quill.register(ImageBlot);
Quill.register("formats/block", ParagraphBlot);

const Block = Quill.import("blots/block");

const toolbar = [
  [{ header: [1, 2, 3, false] }],
  ["bold", "italic", "underline", "strike"], // toggled buttons
  ["blockquote"],
  [{ list: "ordered" }, { list: "bullet" }],
  // [{ size: ["small", false, "large", "huge"] }],
  ["link"],
  ["code-block"],
  ["clean"],
];

const formats = [
  "header",
  "bold",
  "italic",
  "code-block",
  "underline",
  "strike",
  "blockquote",
  "image",
  "size",
  "list",
  "bullet",
  "link",
];

const useQuill = ({ blurEditor, extraContainerRef, ...otherProps } = {}) => {
  const keyboard = useKeyboard({ blurEditor });
  const [files, dispatch] = React.useReducer(filesReducer, []);
  const [currentSelection, setCurrentSelection] = React.useState({
    index: 0,
    length: 0,
  });
  const [selectedEmptyP, setSelectedEmptyP] = React.useState(false);
  const [selectionBound, setSelectionBound] = React.useState(null);
  const [currentBlockId, setCurrentBlockId] = React.useState(null);
  const editorRef = React.useRef();

  const insertMedia = React.useCallback(
    (media) => {
      let insertPosition = currentSelection.index;
      media.forEach(({ type, file, image }) => {
        if (type !== "image") return;
        setSelectedEmptyP(false);
        const prevBlock = document.getElementById(currentBlockId);
        if (prevBlock) {
          prevBlock.classList.remove("active-block");
        }
        if (image) {
          editorRef.current.editor.insertEmbed(insertPosition, "image", image);
        } else {
          const fileId = nextId("file-");
          dispatch({ type: "add-file", payload: { id: fileId, file } });
          editorRef.current.editor.insertEmbed(insertPosition, "image", {
            id: fileId,
            "data-file-id": fileId,
          });
        }
        insertPosition = insertPosition + 1;
      });
    },
    [editorRef, currentSelection, currentBlockId]
  );

  const handleSelectionChange = React.useCallback(
    (range, source, editor) => {
      if (range) setCurrentSelection(range);
      if (range == null || range.length !== 0) return;
      const [block, offset] = editorRef.current.editor.scroll.descendant(
        Block,
        range.index
      );

      if (!block) {
        setCurrentBlockId(null);
        setSelectionBound(null);
        const prevBlock = document.getElementById(currentBlockId);
        if (prevBlock) {
          prevBlock.classList.remove("active-block");
        }
        return;
      } else if (block.domNode.id && block.domNode.id !== currentBlockId) {
        const prevBlock = document.getElementById(currentBlockId);
        if (prevBlock) {
          prevBlock.classList.remove("active-block");
        }
        block.domNode.classList.add("active-block");
        setCurrentBlockId(block.domNode.id);
      }

      if (
        block != null &&
        block.domNode.firstChild instanceof HTMLBRElement &&
        block.domNode.tagName === "P"
      ) {
        const lineBounds = editorRef.current.editor.getBounds(range);
        setSelectionBound(lineBounds);
        setSelectedEmptyP(true);
        block.domNode.classList.add("empty-editor-p");
      } else {
        setSelectionBound(null);
        setSelectedEmptyP(false);
        block.domNode.classList.remove("empty-editor-p");
      }
    },
    [editorRef, currentBlockId]
  );

  updateFilesCache(files, dispatch);

  React.useEffect(() => {
    if (extraContainerRef.current && editorRef.current) {
      editorRef.current.editor.addContainer(extraContainerRef.current);
    }
  }, []);

  return {
    editorConfigs: {
      modules: { toolbar, keyboard },
      formats,
      ref: editorRef,
      ...otherProps,
      onChangeSelection: handleSelectionChange,
    },
    mediaBlockConfigs: {
      float: selectedEmptyP,
      selectionBound,
      onMediaPick: insertMedia,
    },
    files,
    editorRef,
  };
};

export default useQuill;
