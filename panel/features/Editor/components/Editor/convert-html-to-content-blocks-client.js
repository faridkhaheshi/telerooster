import { convertHtmlToContentBlocks } from "../../services/parsing";
import { createElementClient } from "../../services/parsing/search-content-tree/utils";

export const convertHtmlToContentBlocksClient = (html) =>
  convertHtmlToContentBlocks(html, { createElement: createElementClient });
