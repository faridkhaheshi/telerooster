export const moveCursorToContentEditableEnd = (node) => {
  let sel;
  const titleLen = node.textContent.length;
  if (document.selection) {
    sel = document.selection.createRange();
    sel.moveStart("character", titleLen);
    sel.select();
  } else {
    sel = window.getSelection();
    sel.collapse(node.firstChild, titleLen);
  }
};
