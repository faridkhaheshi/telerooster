import React from "react";

const useKeyboard = ({ blurEditor }) => {
  const handleKeydown = React.useCallback(
    ({ index, length }) => {
      if (index === 0 && length === 0) {
        blurEditor();
        return false;
      }
      return true;
    },
    [blurEditor]
  );
  return {
    bindings: {
      backspace: { key: 8, handler: handleKeydown },
      up: { key: 38, handler: handleKeydown },
      left: { key: 37, handler: handleKeydown },
    },
  };
};

export default useKeyboard;
