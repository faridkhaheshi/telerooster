const filesReducer = (files, { type, payload }) => {
  switch (type) {
    case "add-file":
      return [...files, { id: payload.id, file: payload.file }];
    case "upload-finished":
      return uploadFinished(files, payload);
    default:
      return files;
  }
};

export default filesReducer;

function uploadFinished(files, { fileId, image }) {
  return files.map((f) => {
    if (f.id === fileId) return { ...f, image };
    return f;
  });
}
