import React from "react";
import ReactQuill from "react-quill";

import MediaBlockBuilder from "../MediaBlockBuilder";
import MessageSender from "../MessageSender";
import { convertHtmlToContentBlocksClient } from "./convert-html-to-content-blocks-client";
import { TitleInput } from "../TitleInput/TitleInput";
import { addTitleToContent } from "../../services/content-blocks";
import { moveCursorToContentEditableEnd } from "./move-cursor-to-contenteditable-end";
import { convertHtmlToMarkdown } from "../../services/parsing";

import useQuill from "./use-quill";
import "./Editor.css";

const MESSAGE_TEXT_MAX_LEN = 500;

const Editor = () => {
  const mediaBlockBuilderRef = React.useRef();
  const titleRef = React.useRef();
  const blurEditor = React.useCallback(() => {
    titleRef.current.focus();
    moveCursorToContentEditableEnd(titleRef.current);
  }, [titleRef]);

  const { editorConfigs, mediaBlockConfigs, files, editorRef } = useQuill({
    theme: "bubble",
    bounds: ".container",
    placeholder: "start writing...",
    extraContainerRef: mediaBlockBuilderRef,
    blurEditor,
  });

  const handleBuildMessage = React.useCallback(() => {
    const title = titleRef.current.textContent || "";
    const blocks = convertHtmlToContentBlocksClient(
      editorRef.current.editor.root.innerHTML
    );
    if (title.length > 0) {
      addTitleToContent({ blocks, title });
    }
    const message = {
      blocks,
      text: convertHtmlToMarkdown(
        editorRef.current.editor.root.innerHTML
      ).slice(0, MESSAGE_TEXT_MAX_LEN),
    };
    if (title) message.title = title;
    return message;
  }, [editorRef, titleRef]);

  const clearEditor = React.useCallback(() => {
    editorRef.current.editor.setText("");
    titleRef.current.innerHTML = "";
  }, [editorRef]);

  const focusOnEditor = React.useCallback(() => {
    editorRef.current.focus();
  }, [editorRef]);

  React.useEffect(() => {
    editorRef.current.focus();
  }, []);

  return (
    <section className="editor-container">
      <TitleInput ref={titleRef} onEnter={focusOnEditor} />
      <ReactQuill {...editorConfigs} />
      <MediaBlockBuilder ref={mediaBlockBuilderRef} {...mediaBlockConfigs} />
      <MessageSender
        buildMessage={handleBuildMessage}
        readyToSend={files.every((file) => file.image)}
        onSuccess={clearEditor}
      />
    </section>
  );
};

export { Editor };
