const DebugWindow = ({ show = false, user, contextValues }) => {
  if (!show) return null;
  return (
    <div className="d-flex flex-column align-items-center">
      <pre style={{ maxHeight: 100, overflow: "scroll" }}>
        {JSON.stringify(user, null, 2)}
      </pre>
      <p>Plan type: {contextValues.planType}</p>
      <p>isCustomer: {contextValues.isCustomer ? "YES" : "NO"}</p>
      <p>customerId: {contextValues.customerId}</p>
    </div>
  );
};

export default DebugWindow;
