import Cookies from "js-cookie";
import decodeJWT from "jwt-decode";

export const loadToken = () => {
  try {
    const token = Cookies.get("token");
    const payload = decodeJWT(token);
    if (Date.now() >= payload.exp * 1000) throw new Error("token expired");
    return token;
  } catch (e) {
    return null;
  }
};
