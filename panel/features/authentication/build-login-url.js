import { websiteUrl } from "./../../params";

export const buildLoginUrl = (ref) =>
  ref
    ? `${websiteUrl}/auth/login?ref=${encodeURIComponent(ref)}`
    : `${websiteUrl}/auth/login`;
