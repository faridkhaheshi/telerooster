import React from "react";
import { loadToken } from "./load-token";
import { buildLoginUrl } from "./build-login-url";
import { buildLogoutUrl } from "./build-logout-url";
import useServerResource from "../../core/hooks/use-server-resource";
import { FullPageWaiting } from "./../../components";
import useToast from "../../core/hooks/use-toast";
import useAsyncFormHandler from "../../core/hooks/use-async-form-handler";
import DebugWindow from "./DebugWindow";
import { findPlanType } from "./find-plan-type";
import { checkCustomerStatus } from "./check-customer-status";
import { findActiveCustomerId } from "./find-active-customer.id";

export const AuthContext = React.createContext();
export const useAuth = () => React.useContext(AuthContext);

export const AuthContextProvider = ({ pageProps, children }) => {
  const { danger } = useToast();
  const {
    data: user,
    isLoading: isUserLoading,
    error: userFetchError,
    refresh: updateUserInfo,
  } = useServerResource("/api/me", { shouldRetryOnError: false });
  const [token, setToken] = React.useState(null);

  const sendToLogin = React.useCallback(() => {
    window.location.replace(buildLoginUrl(window.location.href));
  }, []);

  const updateToken = React.useCallback(() => {
    const theToken = loadToken();
    if (theToken) {
      setToken(theToken);
    } else {
      sendToLogin();
    }
  }, [loadToken, sendToLogin]);

  const logout = React.useCallback(() => {
    window.location.replace(buildLogoutUrl());
  }, []);

  const {
    handleSubmit: deactivate,
    isProcessing: isDeactivating,
  } = useAsyncFormHandler({
    url: "/api/me",
    requestOptions: { method: "DELETE" },
    onSuccess: logout,
    onError: (errMsg) =>
      danger({ title: "Deactivation failed", message: errMsg }),
  });

  const contextValues = {
    user,
    token,
    logout,
    logoutUrl: buildLogoutUrl(),
    updateUserInfo,
    deactivate,
    isDeactivating,
    planType: findPlanType(user),
    isCustomer: checkCustomerStatus(user),
    customerId: findActiveCustomerId(user),
  };

  React.useEffect(() => {
    updateToken();
  }, []);

  React.useEffect(() => {
    if (!isUserLoading && (userFetchError || !user)) {
      sendToLogin();
    }
  }, [user, isUserLoading, userFetchError, sendToLogin]);

  const waiting = !user;

  return (
    <AuthContext.Provider value={contextValues}>
      <DebugWindow user={user} contextValues={contextValues} />
      {waiting ? <FullPageWaiting /> : children}
    </AuthContext.Provider>
  );
};
