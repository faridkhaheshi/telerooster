export const findActiveCustomerId = (user) => {
  try {
    const { customers } = user;
    const activeCustomerProfiles = customers.filter((cp) => cp.active === true);
    if (activeCustomerProfiles.length === 0) return;
    return activeCustomerProfiles[0].id;
  } catch (err) {
    return;
  }
};
