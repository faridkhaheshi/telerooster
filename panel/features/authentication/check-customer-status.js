export const checkCustomerStatus = (user) => {
  try {
    const { customers } = user;
    const activeCustomerProfiles = customers.filter((cp) => cp.active === true);
    return activeCustomerProfiles.length > 0;
  } catch (err) {
    return false;
  }
};
