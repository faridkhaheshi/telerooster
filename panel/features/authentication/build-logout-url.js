import { websiteUrl } from "./../../params";

export const buildLogoutUrl = (ref) => `${websiteUrl}/auth/logout`;
