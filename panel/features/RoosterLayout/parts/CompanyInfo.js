import React from "react";
import moment from "moment";

import {
  Button,
  Sidebar,
  UncontrolledPopover,
  PopoverBody,
} from "./../../../components";

export const CompanyInfo = () => (
  <Sidebar.Section>
    {/* START DESKTOP View */}
    <Sidebar.HideSlim>
      <p className="small text-muted">
        <FooterText />
      </p>
    </Sidebar.HideSlim>
    {/* END DESKTOP View */}
    {/* START SLIM Only View */}
    <Sidebar.ShowSlim>
      <div className="text-center">
        <Button
          color="link"
          id="UncontrolledSidebarPopoverFooter"
          className="sidebar__link p-0"
        >
          <i className="fa fa-fw fa-question-circle-o" />
        </Button>
        <UncontrolledPopover
          placement="left-end"
          target="UncontrolledSidebarPopoverFooter"
        >
          <PopoverBody>
            <FooterText />
          </PopoverBody>
        </UncontrolledPopover>
      </div>
    </Sidebar.ShowSlim>
  </Sidebar.Section>
);

export function FooterText({ sidebar = true }) {
  return (
    <>
      (C) {moment().format("YYYY")} All Rights Reserved. <br /> This panel is
      part of{" "}
      <a
        href="https://telerooster.com"
        target="_blank"
        rel="noopener noreferrer"
        className={sidebar ? "sidebar__link" : ""}
      >
        teleRooster
      </a>
      . It is designed and implemented by{" "}
      <a
        href="https://dailysay.net"
        target="_blank"
        rel="noopener noreferrer"
        className={sidebar ? "sidebar__link" : ""}
      >
        Daily Say LLC.
      </a>
    </>
  );
}
