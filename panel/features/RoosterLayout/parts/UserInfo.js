import React from "react";

import {
  Sidebar,
  UncontrolledButtonDropdown,
  Avatar,
  AvatarAddOn,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Link,
} from "../../../components";
import { useAuth } from "../../authentication";
import { LogoutModal } from "./LogoutModel";

const avatarImgPlaceholder = "/static/images/avatars/blank.png";

export const UserInfo = () => {
  const { user, planType } = useAuth();

  return (
    <>
      {/* START: Sidebar Default */}
      <Sidebar.HideSlim>
        <Sidebar.Section className="pt-0">
          <DetailedUserInfo
            avatarLink="/profile"
            avatar={avatarImgPlaceholder}
            userType={planType}
            name={user.display_name || user.email}
          />
        </Sidebar.Section>
      </Sidebar.HideSlim>
      {/* END: Sidebar Default */}

      {/* START: Sidebar Slim */}
      <Sidebar.ShowSlim>
        <Sidebar.Section>
          <UserAvatar avatar={avatarImgPlaceholder} size="sm" />
        </Sidebar.Section>
      </Sidebar.ShowSlim>

      {/* END: Sidebar Slim */}
    </>
  );
};

function DetailedUserInfo({
  avatarLink = "/",
  avatar,
  userType = "normal user",
  name = "unknown",
}) {
  return (
    <>
      <Link to={avatarLink} className="d-block">
        <Sidebar.HideSlim>
          <UserAvatar avatar={avatar} size="lg" />
        </Sidebar.HideSlim>
      </Link>

      <UserMenu name={name} />
      <div className="small sidebar__link--muted text-capitalize">
        {userType}
      </div>
    </>
  );
}

function UserMenu({ name }) {
  return (
    <UncontrolledButtonDropdown>
      <DropdownToggle
        color="link"
        className="pl-0 pb-0 btn-profile sidebar__link"
      >
        {name}
        <i className="fa fa-angle-down ml-2"></i>
      </DropdownToggle>
      <DropdownMenu persist>
        <DropdownItem header>{name}</DropdownItem>
        <DropdownItem divider />
        <DropdownItem tag={Link} to="/profile">
          My Profile
        </DropdownItem>
        <DropdownItem tag={Link} to="/settings">
          Settings
        </DropdownItem>
        <DropdownItem tag={Link} to="/billings">
          Billings
        </DropdownItem>
        <DropdownItem divider />
        <DropdownItem id="logout-button-in-dropdown">
          <i className="fa fa-fw fa-sign-out mr-2"></i>
          Log Out
          <LogoutModal target="logout-button-in-dropdown" />
        </DropdownItem>
      </DropdownMenu>
    </UncontrolledButtonDropdown>
  );
}

function UserAvatar({ avatar, size }) {
  return (
    <Avatar.Image
      size={size}
      src={avatar}
      addOns={[
        <AvatarAddOn.Icon
          className="fa fa-circle"
          color="white"
          key="avatar-icon-bg"
        />,
        <AvatarAddOn.Icon
          className="fa fa-circle"
          color="success"
          key="avatar-icon-fg"
        />,
      ]}
    />
  );
}
