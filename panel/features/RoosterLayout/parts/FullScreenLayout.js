// import React from "react";
// import { EmptyLayout, ThemeProvider } from "../../../components";

// export const FullScreenLayout = ({ children }) => (
//   <ThemeProvider initialStyle="dark" initialColor="pink">
//     <EmptyLayout>
//       <EmptyLayout.Section>{children}</EmptyLayout.Section>
//     </EmptyLayout>
//   </ThemeProvider>
// );

import React from "react";

import {
  Form,
  FormGroup,
  FormText,
  Input,
  InputGroupAddon,
  InputGroup,
  Button,
  Label,
  EmptyLayout,
  ThemeConsumer,
  ThemeProvider,
} from "./../../../components";

export const FullScreenLayout = () => (
  <EmptyLayout>
    <EmptyLayout.Section center>
      {/* START Header */}
      {/* END Header */}
      <ul className="list-inline my-5 text-center">
        <li className="list-inline-item text-center mr-2">
          <h2 className="mb-0">16</h2>
          <div>Days</div>
        </li>
        <li className="list-inline-item text-center mr-2">
          <h2 className="mb-0">34</h2>
          <div>Hours</div>
        </li>
        <li className="list-inline-item text-center mr-2">
          <h2 className="mb-0">10</h2>
          <div>Min</div>
        </li>
        <li className="list-inline-item text-center">
          <h2 className="mb-0">3</h2>
          <div>Sec</div>
        </li>
      </ul>
      {/* START Form */}
      <Form className="mb-3">
        <FormGroup>
          <Label for="email">Enter email</Label>
          <InputGroup>
            <Input
              type="email"
              name="email"
              id="email"
              placeholder="Enter here..."
            />
          </InputGroup>
          <FormText className="muted">
            If you want to be informed about the start, please subscribe to the
            newsletter
          </FormText>
        </FormGroup>
      </Form>
      {/* END Form */}
      {/* START Bottom Links */}

      {/* END Bottom Links */}
      {/* START Footer */}

      {/* END Footer */}
    </EmptyLayout.Section>
  </EmptyLayout>
);
