import React from "react";
import PropTypes from "prop-types";

import {
  UncontrolledModal,
  ModalHeader,
  ModalBody,
  NavItem,
  NavLink,
} from "./../../../components";
import { LogoutModal } from "./LogoutModel";

const ShutdownButton = (props) => {
  return (
    <NavItem {...props}>
      <NavLink id="confirm-logout-modal">
        <i className="fa fa-power-off"></i>
      </NavLink>
      <LogoutModal target="confirm-logout-modal" />
    </NavItem>
  );
};

ShutdownButton.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
};

export { ShutdownButton };
