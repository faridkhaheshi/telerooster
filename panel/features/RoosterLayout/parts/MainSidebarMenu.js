import React from "react";

import { SidebarMenu, Badge } from "../../../components";

export const MainSidebarMenu = () => (
  <SidebarMenu>
    <SidebarMenu.Item
      icon={<i className="fa fa-fw fa-home"></i>}
      title="Dashboard"
      to="/"
    />

    <SidebarMenu.Item
      icon={<i className="fa fa-fw fa-bullhorn"></i>}
      title="Channels you own"
      to="/mychannels"
    />

    <SidebarMenu.Item
      icon={<i className="fa fa-fw fa-pencil"></i>}
      title="Post new content"
      to="/post"
    />

    <SidebarMenu.Item
      icon={<i className="fa fa-fw fa-group"></i>}
      title="Your audience"
      to="/audience"
    />

    <SidebarMenu.Item
      icon={<i className="fa fa-fw fa-newspaper-o"></i>}
      title="Channels you follow"
      to="/subscriptions"
    />

    <SidebarMenu.Item
      icon={<i className="fa fa-fw fa-sitemap"></i>}
      title="Your teams"
      to="/teams"
    />

    <SidebarMenu.Item
      icon={<i className="fa fa-fw fa-cog"></i>}
      title="Settings"
      to="/settings"
    />

    <SidebarMenu.Item
      icon={<i className="fa fa-fw fa-credit-card"></i>}
      title="Billings"
      to="/billings"
    />

    <SidebarMenu.Item
      icon={<i className="fa fa-fw fa-user"></i>}
      title="Profile"
      to="/profile"
    />
  </SidebarMenu>
);
