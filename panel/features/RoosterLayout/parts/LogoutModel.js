import React from "react";
import PropTypes from "prop-types";

import {
  UncontrolledModal,
  ModalHeader,
  ModalBody,
} from "./../../../components";
import { useAuth } from "../../authentication";

const LogoutModal = ({ target }) => {
  const { logoutUrl } = useAuth();
  return (
    <UncontrolledModal target={target} className="modal-danger">
      <ModalHeader className="py-3" />
      <ModalBody className="table-danger text-center px-5">
        <h6>Logging out?</h6>
        <p className="modal-text">
          This panel will be closed. You will have to log in again to return
          here.
        </p>
        <UncontrolledModal.Close
          color="danger"
          className="mr-2 btn btn-danger"
          tag="a"
          href={logoutUrl}
        >
          Yes - log out
        </UncontrolledModal.Close>
        <UncontrolledModal.Close color="link" className="text-secondary">
          No - return
        </UncontrolledModal.Close>
      </ModalBody>
    </UncontrolledModal>
  );
};

LogoutModal.propTypes = {
  target: PropTypes.string,
};

export { LogoutModal };
