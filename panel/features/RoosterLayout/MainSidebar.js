import React from "react";

import { Link, Sidebar, SidebarTrigger } from "../../components";

import { TeleroosterLogo } from "../LogoThemed/TeleroosterLogo";
import { CompanyInfo } from "./parts/CompanyInfo";
import { MainSidebarMenu } from "./parts/MainSidebarMenu";
import { UserInfo } from "./parts/UserInfo";
import { websiteUrl } from "./../../params";

export const MainSidebar = () => (
  <Sidebar>
    <SidebarCloseButton />

    {/* START SIDEBAR: Only for Desktop */}
    <SidebarLogo />
    {/* END SIDEBAR: Only for Desktop */}

    {/* scrollable on mobile */}
    <Sidebar.MobileFluid>
      <UserInfo />
      <Sidebar.Section fluid cover>
        <MainSidebarMenu />
      </Sidebar.Section>
      <CompanyInfo />
    </Sidebar.MobileFluid>
  </Sidebar>
);

function SidebarCloseButton() {
  return (
    <Sidebar.Close>
      <SidebarTrigger tag="a" href="javascript:;">
        <i className="fa fa-times-circle fa-fw"></i>
      </SidebarTrigger>
    </Sidebar.Close>
  );
}

function SidebarLogo() {
  return (
    <Sidebar.HideSlim>
      <Sidebar.Section>
        <Link to={websiteUrl} className="sidebar__brand">
          <TeleroosterLogo />
        </Link>
      </Sidebar.Section>
    </Sidebar.HideSlim>
  );
}
