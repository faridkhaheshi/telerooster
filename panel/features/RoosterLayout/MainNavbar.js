import React from "react";

import { Link, Navbar, Nav, NavItem, SidebarTrigger } from "../../components";

import { ShutdownButton } from "./parts/ShutdownButton";
import { TeleroosterLogo } from "../LogoThemed/TeleroosterLogo";
import { websiteUrl } from "./../../params";

export const MainNavbar = () => (
  <Navbar light expand="xs" fluid>
    <Nav navbar>
      <NavItem className="mr-3">
        <SidebarTrigger />
      </NavItem>
      <NavItem className="navbar-brand h5 mb-0 d-lg-none">
        <Link to={websiteUrl}>
          <TeleroosterLogo />
        </Link>
      </NavItem>
    </Nav>
    <Nav navbar className="ml-auto">
      <ShutdownButton className="ml-2" to="/logout" />
    </Nav>
  </Navbar>
);
