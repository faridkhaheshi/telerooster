import React from "react";
import PropTypes from "prop-types";
import { Layout, ThemeProvider } from "../../components";

import { favIcons } from "./favIcons";
import { MainNavbar } from "./MainNavbar";
import { MainSidebar } from "./MainSidebar";

export const MainLayout = ({ isMobile, children }) => (
  <ThemeProvider initialStyle="dark" initialColor="pink">
    <Layout sidebarSlim favIcons={favIcons} isMobile={isMobile}>
      {/* --------- Navbar ----------- */}
      <Layout.Navbar>
        <MainNavbar />
      </Layout.Navbar>

      {/* -------- Sidebar ------------*/}
      <Layout.Sidebar>
        <MainSidebar />
      </Layout.Sidebar>

      {/* -------- Content ------------*/}
      <Layout.Content>{children}</Layout.Content>
    </Layout>
  </ThemeProvider>
);

MainLayout.protoTypes = {
  children: PropTypes.node,
  isMobile: PropTypes.bool,
};
