import { makeErrFromCode } from "restify-errors";
import { websiteUrl } from "./../../params";

const defaultHeaders = {
  Accept: "application/json",
  // "Content-Type": "application/json",
};

export const sendApiRequest = async (url, options = {}) => {
  const finalOptions = buildFinalOptions(options);
  const response = await fetch(`${websiteUrl}${url}`, finalOptions);
  if (response.status === 200) return response.json();
  throw makeErrFromCode(response.status, response.statusText);
};

function buildFinalOptions({
  method = "GET",
  body,
  headers = {},
  ...otherOptions
}) {
  const finalOptions = {
    method,
    headers: { ...defaultHeaders, ...headers },
    credentials: "include",
    ...otherOptions,
  };
  if (body) {
    finalOptions.body = JSON.stringify(body);
    finalOptions.headers["Content-Type"] = "application/json";
  }
  return finalOptions;
}
