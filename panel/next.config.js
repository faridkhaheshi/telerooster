const withSass = require("@zeit/next-sass");
const withCss = require("@zeit/next-css");
const withFonts = require("next-fonts");
const withImages = require("next-images");

module.exports = withImages(
  withFonts(
    withSass(
      withCss({
        env: {
          NEXT_PUBLIC_STRIPE_PUBLIC_KEY:
            process.env.NEXT_PUBLIC_STRIPE_PUBLIC_KEY,
          NEXT_PUBLIC_GOOGLE_RECAPTCHA_SITE_KEY:
            process.env.NEXT_PUBLIC_GOOGLE_RECAPTCHA_SITE_KEY,
          NEXT_PUBLIC_WEBSITE_BASE_URL:
            process.env.NEXT_PUBLIC_WEBSITE_BASE_URL,
          NEXT_PUBLIC_WEBSITE_PORT: process.env.NEXT_PUBLIC_WEBSITE_PORT,
          NEXT_PUBLIC_PANEL_PORT: process.env.NEXT_PUBLIC_PANEL_PORT,
          NEXT_PUBLIC_PANEL_BASE_URL: process.env.NEXT_PUBLIC_PANEL_BASE_URL,
        },
      })
    )
  )
);
