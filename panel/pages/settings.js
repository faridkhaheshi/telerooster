import React from "react";
import { SettingsList } from "../features/Settings";

import { Container, HeaderMain, CustomHead } from "./../components";

const Settings = () => (
  <Container>
    <CustomHead title="Settings @teleRooster" />
    <HeaderMain title="Settings" className="mb-5 mt-4" />
    <SettingsList />
  </Container>
);

export default React.memo(Settings);
