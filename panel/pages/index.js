import React from "react";
import { CardGrid } from "../features/Dashboard";

import { Container, CustomHead } from "./../components";

const Index = () => (
  <Container>
    <CustomHead title="Dashboard @teleRooster" />
    <CardGrid />
  </Container>
);

export default React.memo(Index);
