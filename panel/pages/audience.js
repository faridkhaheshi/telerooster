import React from "react";

import { Container, CustomHead, HeaderMain } from "../components";
import { AudienceList } from "../features/Audience";

const Audience = () => (
  <Container>
    <CustomHead title="Your audience @teleRooster" />
    <HeaderMain title="Your audience" className="mb-5 mt-4" />
    <AudienceList />
  </Container>
);

export default React.memo(Audience);
