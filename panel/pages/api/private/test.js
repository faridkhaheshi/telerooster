import { JSDOM } from "jsdom";
import { processLinks } from "../../../features/link-processing";
import getBase64Signatures from "../../../features/link-processing/utils/get-base64-signatures";
import { removeContentmail } from "../../../features/remove-contentmail";

const handleReqs = async (req, res) => {
  try {
    return res.send("ok");
    // const processed = processLinks(req.body);
    // const processed = removeContentmail(req.body);
    // return res.send(processed);
    // const signatures = getBase64Signatures(req.body);
    // return res.json(signatures);
  } catch (err) {
    console.error(err);
    return res.json({ error: err.message || "something went wrong" });
  }
};

export default handleReqs;
