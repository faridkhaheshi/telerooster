import {
  handleParseDocumentJsonReq,
  handleParseDocumentReq,
} from "../../../services/content-blocks/controllers";
import { makeRoutePrivate } from "./../../../services/auth/controllers";

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case "GET":
      return handleParseDocumentReq(req, res);
    case "POST":
      return handleParseDocumentJsonReq(req, res);
    default:
      return res.status(405).send("The method is not supported");
  }
};

export default makeRoutePrivate(handleReqs);
