const handleReqs = async (req, res) => {
  try {
    return res.json({ someenv: process.env.NEXT_PUBLIC_WEBSITE_PORT });
  } catch (err) {
    return res.json({ error: err.message || "something went wrong" });
  }
};

export default handleReqs;
