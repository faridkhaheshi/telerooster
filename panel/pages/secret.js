import React from "react";

import { Container, CustomHead, HeaderMain, Link } from "../components";

const Secret = () => (
  <Container>
    <CustomHead title="teleRooster secrets" />
    <HeaderMain title="Secrets" className="mb-5 mt-4" />
    <p>
      To add events to your pool:{" "}
      <Link to="/post?action=schedule"> go here</Link>{" "}
    </p>
  </Container>
);

export default Secret;
