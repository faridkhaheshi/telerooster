import React from "react";
import dynamic from "next/dynamic";

import { Container, CustomHead } from "../components";

const Editor = dynamic(() => import("../features/Editor/components/Editor"), {
  ssr: false,
});

const Post = () => (
  <Container>
    <CustomHead title="Post new content @teleRooster" />
    <Editor />
  </Container>
);

export default Post;
