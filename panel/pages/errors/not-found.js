import React from "react";

import { EmptyLayout, Link } from "./../../components";

import { websiteUrl } from "./../../params";

import { HeaderAuth } from "../../features/HeaderAuth";
import { FooterAuth } from "../../features/FooterAuth";

const Error404 = () => (
  <EmptyLayout>
    <EmptyLayout.Section center>
      <HeaderAuth title="404 - Page not found" icon="exclamation" />
      <div className="d-flex mb-5 mt-5">
        <Link to="/">Back to Home</Link>
        <Link to={`${websiteUrl}/support`} className="ml-auto">
          Support
        </Link>
      </div>
      <FooterAuth />
    </EmptyLayout.Section>
  </EmptyLayout>
);

export default Error404;
