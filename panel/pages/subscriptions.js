import React from "react";

import { Container, CustomHead, HeaderMain } from "../components";
import { SubscriptionsList } from "../features/Subscriptions";

const Subscriptions = () => (
  <Container>
    <CustomHead title="Channels you follow @teleRooster" />
    <HeaderMain title="Subscriptions" className="mb-5 mt-4" />
    <SubscriptionsList />
  </Container>
);

export default React.memo(Subscriptions);
