import React from "react";
import { Elements as StripeElements } from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";

import { BillingPortalCard, UpgradeCard } from "../features/Payment";
import { Container, HeaderMain, CustomHead } from "./../components";

import { stripe } from "./../params";

const stripePromise = loadStripe(stripe.publicKey);

const Billings = () => (
  <StripeElements stripe={stripePromise}>
    <Container>
      <CustomHead title="Billings @teleRooster" />
      <HeaderMain title="Billings" className="mb-5 mt-4" />
      <UpgradeCard />
      <BillingPortalCard />
    </Container>
  </StripeElements>
);

export default Billings;
