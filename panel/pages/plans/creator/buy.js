import React from "react";

import { Elements as StripeElements } from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";

import { PaymentCard } from "../../../features/Payment";
import { Container, CustomHead, HeaderMain } from "./../../../components";
import { stripe } from "./../../../params";

const stripePromise = loadStripe(stripe.publicKey);

const BuyCreator = () => (
  <StripeElements stripe={stripePromise}>
    <Container>
      <CustomHead title="Buy Creator plan @teleRooster" />
      <HeaderMain title="Buying creator plan" className="mb-5 mt-4" />
      <PaymentCard plan="creator" />
    </Container>
  </StripeElements>
);

export default BuyCreator;
