import React from "react";
import { useRouter } from "next/router";
import { ChannelEditForm } from "../../features/Channels/ChannelEditForm";

import { Container, CustomHead, HeaderMain } from "./../../components";

const EditChannel = () => {
  const {
    query: { id },
  } = useRouter();
  if (!id) return null;

  return (
    <Container>
      <CustomHead title="Edit channel @teleRooster" />
      <HeaderMain title="Edit Channel" className="mb-5 mt-4" />
      <ChannelEditForm id={id} />
    </Container>
  );
};

export default React.memo(EditChannel);
