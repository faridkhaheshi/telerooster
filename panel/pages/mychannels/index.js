import React from "react";
import { ChannelList } from "../../features/Channels";

import { Container, CustomHead, HeaderMain } from "../../components";

const MyChannels = () => (
  <Container>
    <CustomHead title="My channels @teleRooster" />
    <HeaderMain title="My Channels" className="mb-5 mt-4" />
    <ChannelList resource="pools" />
  </Container>
);

export default MyChannels;
