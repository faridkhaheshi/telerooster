import React from "react";

import { TeamsList } from "../features/Teams";
import { Container, CustomHead, HeaderMain } from "./../components";

const Teams = () => (
  <Container>
    <CustomHead title="Your teams @teleRooster" />
    <HeaderMain title="Your teams" className="mb-5 mt-4" />
    <TeamsList />
  </Container>
);

export default React.memo(Teams);
