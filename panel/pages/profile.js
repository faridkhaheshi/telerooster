import React from "react";
import { EditPasswordCard, ProfileEditForm } from "../features/Profile";

import { Container, CustomHead, HeaderMain } from "./../components";

const Profile = () => (
  <Container>
    <CustomHead title="Edit profile @teleRooster" />
    <HeaderMain title="Profile" className="mb-5 mt-4" />
    <ProfileEditForm />
    <EditPasswordCard />
  </Container>
);

export default React.memo(Profile);
