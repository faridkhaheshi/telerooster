import React from "react";
import PropTypes from "prop-types";
import App from "next/app";
import Router from "next/router";
import MobileDetect from "mobile-detect";
import NProgress from "nprogress";
import { ToastContainer } from "react-toastify";

import { MainLayout } from "../features/RoosterLayout";
import { AuthContextProvider } from "../features/authentication";

import "bootstrap/dist/css/bootstrap.min.css";
import "react-quill/dist/quill.bubble.css";
import "./../styles/bootstrap.scss";
import "./../styles/main.scss";
import "./../styles/plugins/plugins.scss";
import "./../styles/plugins/plugins.css";

Router.events.on("routeChangeStart", () => NProgress.start());
Router.events.on("routeChangeComplete", () => NProgress.done());
Router.events.on("routeChangeError", () => NProgress.done());

export default class DashboardApp extends App {
  static propTypes = {
    isMobile: PropTypes.bool,
  };

  static async getInitialProps({ Component, ctx }) {
    const { req } = ctx;
    const pageProps = Component.getInitialProps
      ? await Component.getInitialProps(ctx)
      : {};
    const commonProps = {
      pageProps,
    };

    if (req) {
      const md = new MobileDetect(req.headers["user-agent"]);
      return {
        ...commonProps,
        isMobile: !!md.mobile(),
      };
    }

    return {
      ...commonProps,
      isMobile: false,
    };
  }

  render() {
    const { Component, pageProps, isMobile } = this.props;
    const LayoutComponent = Component.layoutComponent || MainLayout;

    return (
      <AuthContextProvider>
        <LayoutComponent isMobile={isMobile}>
          <Component {...pageProps} />
        </LayoutComponent>
        <ToastContainer autoClose={10000} draggable hideProgressBar />
      </AuthContextProvider>
    );
  }
}
