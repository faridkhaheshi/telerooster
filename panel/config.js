export default {
  siteTitle: "Admin panel | teleRooster",
  siteDescription:
    "We enable content creators to deliver their contents using business messengers",
  siteCannonicalUrl: process.env.NEXT_PUBLIC_PANEL_BASE_URL,
  siteKeywords: "teleRooster admin panel management dashboard",
};
