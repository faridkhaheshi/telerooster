const cleanHtml = (html) => html.replace(/>[\n\s\t]+</g, "><");

module.exports = cleanHtml;
