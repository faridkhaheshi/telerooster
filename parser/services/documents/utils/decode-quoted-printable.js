const libqp = require("libqp");
const { Readable } = require("stream");

const decodeQuotedPrintable = (str) => {
  const inputStream = Readable.from(str, { encoding: "utf8" });
  const decoder = new libqp.Decoder();
  inputStream.pipe(decoder);
  return decoder;
};

module.exports = decodeQuotedPrintable;
