const TreeAdapter = require("parse5/lib/tree-adapters/default");

const findNodeInfo = (node) => {
  const isTextNode = TreeAdapter.isTextNode(node);
  return {
    isCommentNode: TreeAdapter.isCommentNode(node),
    isDocumentTypeNode: TreeAdapter.isDocumentTypeNode(node),
    isTextNode,
    isElementNode: TreeAdapter.isElementNode(node),
    isNonEmptyTextNode: isTextNode && node.value.replace(/\s/g, "").length > 0,
  };
};

module.exports = findNodeInfo;
