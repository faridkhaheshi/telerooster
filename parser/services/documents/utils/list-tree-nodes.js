const buildFinalNodeData = require("./build-final-node-data");
const searchTreeDepthFirst = require("./search-tree-depth-first");

const listTreeNodes = (tree) => {
  const nodes = [];
  searchTreeDepthFirst(tree, (node) => {
    nodes.push(buildFinalNodeData(node));
  });
  return nodes;
};

module.exports = listTreeNodes;
