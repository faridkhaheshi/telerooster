const extractNodeAttributes = (node) => {
  const attributes = {};
  for (let i = 0; i < node.attributes.length; i++) {
    attributes[node.attributes[i].nodeName] = node.attributes[i].nodeValue;
  }
  return attributes;
};

module.exports = extractNodeAttributes;
