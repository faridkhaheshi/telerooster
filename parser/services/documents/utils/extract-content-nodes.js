const searchContentNodes = require("./search-content-nodes");

const extractContentNodes = (tree) => {
  const nodes = [];

  searchContentNodes(tree, (nodeObject) => {
    nodes.push(nodeObject);
  });

  return nodes;
};

module.exports = extractContentNodes;
