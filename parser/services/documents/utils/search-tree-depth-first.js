const searchTreeDepthFirst = (rootNode, process = () => {}) => {
  rootNode.childNodes.forEach((node) => {
    const { childNodes } = node;
    if (childNodes) {
      searchTreeDepthFirst(node, process);
    } else {
      process(node);
    }
  });
  process(rootNode);
};

module.exports = searchTreeDepthFirst;
