const findNodeInfo = require("./find-node-info");

const buildFinalNodeData = (node) => {
  const { childNodes, parentNode, ...nodeData } = node;
  return {
    ...nodeData,
    ...findNodeInfo(node),
  };
};

module.exports = buildFinalNodeData;
