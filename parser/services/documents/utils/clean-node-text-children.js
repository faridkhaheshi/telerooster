const textNodeNames = require("./text-node-names");
const serializer = require("xmlserializer");
const parser = require("parse5");

const cleanNodeTextChildren = (nodes) => {
  const cleanChildren = [];
  let togetherTextNodes = [];

  nodes.forEach((node) => {
    if (textNodeNames.indexOf(node.nodeName) > -1) {
      togetherTextNodes.push(node);
    } else if (togetherTextNodes.length > 0) {
      const mergedNode = mergeTextChildren(togetherTextNodes);
      cleanChildren.push(mergedNode);
      togetherTextNodes = [];
      cleanChildren.push(node);
    } else {
      cleanChildren.push(node);
    }
  });

  if (togetherTextNodes.length > 0) {
    const lastNode = mergeTextChildren(togetherTextNodes);
    cleanChildren.push(lastNode);
  }

  return cleanChildren;
};

module.exports = cleanNodeTextChildren;

function mergeTextChildren(nodes) {
  if (nodes.length === 1) return nodes[0];
  const mergedElements = nodes
    .map((node) => serializer.serializeToString(node))
    .join("");
  const newElement = `<p>${mergedElements}</p>`;
  return parser.parseFragment(newElement);
}
