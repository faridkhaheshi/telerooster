const parser = require("parse5");
const serializer = require("xmlserializer");
const cleanNodeTextChildren = require("./clean-node-text-children");
const findNodeInfo = require("./find-node-info");
const ignoredTags = require("./ignored-tags");
const targetTags = require("./target-tags");

const searchContentNodes = (tree, processAccepted = () => {}) => {
  try {
    const nodeInfo = findNodeInfo(tree);
    const { childNodes = [] } = tree;
    const validLeaf = isValidLeaf(tree, nodeInfo);
    const isTarget = isTargetNode(tree, nodeInfo);
    const nonTextChildren = childNodes.filter((n) => n.tagName);
    const textChildren = findValidTextChildren(tree);
    const hasOnlyTextChildren =
      textChildren.length > 0 && nonTextChildren.length === 0;

    if (isIgnoredNode(tree, nodeInfo)) return;
    if (validLeaf || isTarget || hasOnlyTextChildren) {
      const { childNodes, parentNode, ...nodeData } = tree;
      return processAccepted({
        nodeData,
        nodeInfo,
        reason: { validLeaf, isTarget, hasOnlyTextChildren },
        innerHtml: parser.serialize(tree),
        element: serializer.serializeToString(tree),
      });
    }

    const cleanChildren = cleanNodeTextChildren(childNodes);

    cleanChildren.forEach((node) => searchContentNodes(node, processAccepted));
  } catch (err) {
    if (tree.childNodes) {
      const cleanChildren = cleanNodeTextChildren(tree.childNodes);
      cleanChildren.forEach((node) =>
        searchContentNodes(node, processAccepted)
      );
    }
  }
};

module.exports = searchContentNodes;

function isIgnoredNode(node, { isCommentNode, isElementNode }) {
  try {
    const isGmailQuote =
      (node.attrs || []).filter(
        ({ name, value }) => name === "class" && value === "gmail_attr"
      ).length > 0;
    return (
      isGmailQuote ||
      isCommentNode ||
      (isElementNode && ignoredTags.indexOf(node.tagName) > -1)
    );
  } catch (err) {
    return false;
  }
}

function findValidTextChildren(node) {
  if (!node.childNodes) return [];
  const textChildren = node.childNodes.filter(
    (n) => findNodeInfo(n).isNonEmptyTextNode
  );
  return textChildren;
}

function hasValidTextChild(node) {
  const textChildren = findValidTextChildren(node);
  return textChildren.length > 0;
}

function isTargetNode(node, { isElementNode }) {
  if (node.nodeName === "p" && node.childNodes.length === 1) {
    if (["a", "img", "figure"].indexOf(node.childNodes[0].nodeName) > -1)
      return false;
  }
  return isElementNode && targetTags.indexOf(node.tagName) > -1;
}

function isValidLeaf(node, { isTextNode, isNonEmptyTextNode }) {
  return !node.childNodes && (!isTextNode || isNonEmptyTextNode);
}
