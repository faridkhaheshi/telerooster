module.exports = {
  handleParseDocumentRequest: require("./handle-parse-document-request"),
  handleDecodeQuotedPrintableReqs: require("./handle-decode-quoted-printable-reqs"),
};
