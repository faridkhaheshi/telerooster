const decodeQuotedPrintable = require("./../utils/decode-quoted-printable");

const handleDecodeQuotedPrintableReqs = async (req, res, next) => {
  try {
    decodeQuotedPrintable(req.body).pipe(res);
  } catch (err) {
    next(err);
  }
};

module.exports = handleDecodeQuotedPrintableReqs;
