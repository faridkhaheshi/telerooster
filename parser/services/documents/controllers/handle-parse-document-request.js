const { parseHtml } = require("../processors");

const handleParseDocumentRequest = async (req, res, next) => {
  try {
    const doc = req.getContentType() === "text/plain" ? req.body : req.body.doc;
    const blocks = parseHtml(doc);
    res.json({ blocks });
    next();
  } catch (err) {
    next(err);
  }
};

module.exports = handleParseDocumentRequest;
