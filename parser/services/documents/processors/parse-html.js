const parser = require("parse5");
const extractContentBlocks = require("../../content-blocks/processors/extract-content-blocks");
const cleanHtml = require("./../utils/clean-html");

const parseHtml = (html) => extractContentBlocks(parser.parse(cleanHtml(html)));

module.exports = parseHtml;
