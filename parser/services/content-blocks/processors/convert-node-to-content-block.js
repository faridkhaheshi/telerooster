const parseToMarkdown = require("./parse-to-markdown");

const convertNodeToContentBlock = ({
  nodeData,
  nodeInfo,
  reason,
  innerHtml,
  element,
}) => {
  const { nodeName, tagName } = nodeData;
  const type = identifyBlockType({ tagName, nodeData, nodeInfo, reason });
  return {
    type,
    content: extractBlockContent({
      type,
      element,
      innerHtml,
      reason,
      nodeInfo,
      nodeData,
    }),
    source: {
      element,
      nodeName,
      nodeData,
      nodeInfo,
      reason,
      innerHtml,
    },
  };
};

module.exports = convertNodeToContentBlock;

function identifyBlockType({ tagName, nodeData, nodeInfo, reason }) {
  if (nodeInfo.isTextNode) return "p";
  if (["h1", "h2", "h3", "h4", "h5", "h6"].indexOf(tagName) > -1) return "h";

  if (["img", "figure"].indexOf(tagName) > -1) return "image";
  if (["hr"].indexOf(tagName) > -1) return "divider";
  if (tagName === "p") return "p";
  if (tagName === "ul") return "ul";
  if (tagName === "ol") return "ol";
  if (tagName === "small") return "small";
  if (tagName === "a") return "icon";
  if (tagName === "button") return "button";
  return "custom";
}

function extractBlockContent({
  type,
  element,
  innerHtml,
  reason,
  nodeInfo,
  nodeData,
}) {
  if (type === "h") return { markdown: parseToMarkdown(innerHtml) };
  if (type === "image") return JSON.parse(parseToMarkdown(element));
  if (type === "icon") {
    const markdown = parseToMarkdown(element);
    try {
      return JSON.parse(markdown);
    } catch (err) {
      return { markdown };
    }
  }
  return { markdown: parseToMarkdown(element) };
}
