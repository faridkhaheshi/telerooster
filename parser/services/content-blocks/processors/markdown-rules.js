const extractNodeAttributes = require("../../documents/utils/extract-node-attributes");

module.exports = {
  figures: {
    filter: (node, options) =>
      options.imageStyle === "JSON" && node.nodeName === "FIGURE",
    replacement: (content, node) => {
      const [imageChild] = node.childNodes.filter((n) => n.nodeName === "IMG");
      const [figCaption] = node.childNodes.filter(
        (n) => n.nodeName === "FIGCAPTION"
      );
      const imageAttrs = extractNodeAttributes(imageChild);
      const caption = figCaption.innerHTML;
      return JSON.stringify({ image: imageAttrs, caption });
    },
  },
  images: {
    filter: (node, options) =>
      options.imageStyle === "JSON" && node.nodeName === "IMG",
    replacement: (content, node) => {
      const image = extractNodeAttributes(node);
      return JSON.stringify({ image });
    },
  },
  icons: {
    filter: (node, options) => {
      if (options.imageStyle === "JSON" && node.nodeName === "A") {
        const [imageChild] = node.childNodes.filter(
          (n) => n.nodeName === "IMG"
        );
        return imageChild && node.getAttribute("href");
      }
    },
    replacement: (content, node) => {
      const [imageChild] = node.childNodes.filter((n) => n.nodeName === "IMG");
      const image = extractNodeAttributes(imageChild);
      const link = extractNodeAttributes(node);
      return JSON.stringify({ image, link });
    },
  },
};
