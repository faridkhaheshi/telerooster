const TurndownService = require("turndown");
const { gfm } = require("turndown-plugin-gfm");
const rules = require("./markdown-rules");

const turndownService = new TurndownService({
  headingStyle: "atx",
  bulletListMarker: "-",
  imageStyle: "JSON",
});

turndownService.use(gfm);

Object.keys(rules).forEach((rule) =>
  turndownService.addRule(rule, rules[rule])
);

const parseToMarkdown = (html) => {
  return turndownService.turndown(html);
};

module.exports = parseToMarkdown;
