module.exports = {
  extractContentBlocks: require("./extract-content-blocks"),
  convertNodeToContentBlock: require("./convert-node-to-content-block"),
};
