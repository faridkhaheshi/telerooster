const searchContentNodes = require("../../documents/utils/search-content-nodes");
const convertNodeToContentBlock = require("./convert-node-to-content-block");

const extractContentBlocks = (tree) => {
  const blocks = [];

  searchContentNodes(tree, (nodeObject) => {
    try {
      const block = convertNodeToContentBlock(nodeObject);
      if (block) blocks.push(block);
    } catch (err) {}
  });

  return blocks;
};

module.exports = extractContentBlocks;
