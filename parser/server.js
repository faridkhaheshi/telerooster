const restify = require("restify");
const routes = require("./routes");

const server = restify.createServer();
routes(server);

server.listen(3000, () => {
  console.log(`${server.name} listening at ${server.url}`);
});
