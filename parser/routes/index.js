const restify = require("restify");

module.exports = (server) => {
  server.use(restify.plugins.bodyParser({ maxBodySize: 1024 * 1024 * 10 }));
  require("./api")(server);
  require("./papi")(server);
};
