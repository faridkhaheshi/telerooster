const handleParseDocumentRequest = require("../../services/documents/controllers/handle-parse-document-request");

module.exports = (server) => {
  server.post("/api/documents", handleParseDocumentRequest);
};
