const {
  handleDecodeQuotedPrintableReqs,
} = require("../../services/documents/controllers");

module.exports = (server) => {
  server.post("/api/decode-quoted-printable", handleDecodeQuotedPrintableReqs);
};
