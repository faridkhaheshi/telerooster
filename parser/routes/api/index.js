module.exports = (server) => {
  require("./documents")(server);
  require("./decode-quoted-printable")(server);
};
