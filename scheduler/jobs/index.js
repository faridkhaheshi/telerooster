module.exports = (scheduler) => {
  require("./update-channels")(scheduler);
  require("./update-teams")(scheduler);
  require("./process-daily-journey-events")(scheduler);
};
