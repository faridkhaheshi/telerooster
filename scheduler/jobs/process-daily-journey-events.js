const raiseEvent = require("../adapters/dispatcher/raise-event");
const { schedulerOptions } = require("../config");

module.exports = (scheduler) => {
  scheduler.schedule(
    "00 00 10 * * *",
    async () => {
      try {
        console.log('running "process-daily-journey-events" job:');
        await raiseEvent({ event: "process-daily-journey-events" });
        await raiseEvent({
          event: "send-email",
          payload: {
            to: "farid.khaheshi@gmail.com",
            subject: `processing daily journey events ${new Date()}`,
            text: "started",
          },
        });
        console.log("process-journey-events raised");
      } catch (err) {
        console.log("process-journey-events failed:");
        console.error(err);
      }
    },
    schedulerOptions
  );
};
