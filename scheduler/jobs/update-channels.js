const raiseEvent = require("../adapters/dispatcher/raise-event");
const { schedulerOptions } = require("./../config");

module.exports = (scheduler) => {
  scheduler.schedule(
    "05 04 03 * * *",
    async () => {
      try {
        console.log("running update channels event...");
        await raiseEvent({ event: "update-channels" });
        await raiseEvent({
          event: "send-email",
          payload: {
            to: "farid.khaheshi@gmail.com",
            subject: `updating channels ${new Date()}`,
            text: "started",
          },
        });
        console.log(`update channels event successfully raised`);
      } catch (err) {
        console.log("update channels event failed:");
        console.error(err);
      }
    },
    schedulerOptions
  );
};
