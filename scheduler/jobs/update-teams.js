const raiseEvent = require("../adapters/dispatcher/raise-event");
const { schedulerOptions } = require("./../config");

module.exports = (scheduler) => {
  scheduler.schedule(
    "04 03 02 * * *",
    async () => {
      try {
        console.log("running update teams event...");
        await raiseEvent({ event: "update-teams" });
        await raiseEvent({
          event: "send-email",
          payload: {
            to: "farid.khaheshi@gmail.com",
            subject: `updating teams ${new Date()}`,
            text: "started",
          },
        });
        console.log(`update teams event successfully raised`);
      } catch (err) {
        console.log("update teams event failed:");
        console.error(err);
      }
    },
    schedulerOptions
  );
};
