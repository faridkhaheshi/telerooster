const fetch = require("node-fetch");
const { InternalServerError } = require("restify-errors");

const defaultHeaders = {
  Accept: "application/json",
  "Content-Type": "application/json",
  Authorization: `Bearer ${process.env.INTERNAL_API_KEY}`,
};

const sendInternalApiRequest = async (url, options = {}) => {
  const fullOptions = buildFullRequestOptions(options);
  const response = await fetch(url, fullOptions);
  if (response.status !== 200)
    throw new InternalServerError(
      "internal api request failed for unknown reasons"
    );
  return response.json();
};

module.exports = sendInternalApiRequest;

function buildFullRequestOptions({ method = "GET", body, headers = {} }) {
  const finalOptions = {
    method,
    headers: { ...defaultHeaders, ...headers },
  };
  if (body) finalOptions.body = JSON.stringify(body);
  return finalOptions;
}
