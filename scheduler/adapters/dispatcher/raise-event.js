const sendInternalApiRequest = require("../api/send-internal-api-request");

const raiseEvent = ({ event, payload = {} }) =>
  sendInternalApiRequest(process.env.DISPATCHER_URL, {
    method: "POST",
    body: { event, payload },
  });

module.exports = raiseEvent;
