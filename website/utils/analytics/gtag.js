import { google } from "./../../config";

export const pageview = (url) => {
  if (process.env.NODE_ENV !== "production") return;
  window.gtag("config", google.gaTrackingId, {
    page_path: url,
  });
};

export const event = ({ action, category, label, value }) => {
  if (process.env.NODE_ENV !== "production") return;
  window.gtag("event", action, {
    event_category: category,
    event_label: label,
    value,
  });
};
