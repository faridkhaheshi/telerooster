import { buildSubscribeLink } from "../../components/widgets/website-embed-button";

export const prepareContentPoolForVitrin = ({
  id,
  title,
  poster,
  is_free,
  price_sm,
  price_md,
  price_lg,
  has_free_trial,
  description,
  creator: { display_name, id: creatorId } = {},
}) => ({
  title,
  linkText: "Subscribe",
  linkAddress: buildSubscribeLink({ poolId: id }),
  subtitle: `Creator: ${display_name}`,
  priceTag: buildPriceTag({
    is_free,
    price_sm,
    price_md,
    price_lg,
    has_free_trial,
  }),
  priceDetail: buildPriceDetail({ is_free }),
  id,
  description,
  image: { src: poster, alt: title },
});

function buildPriceTag({
  is_free,
  price_sm,
  price_md,
  price_lg,
  has_free_trial,
}) {
  if (is_free) return "Free";
  return `$${price_sm / 100} /month /user`;
  return "price";
}

function buildPriceDetail({ is_free }) {
  if (is_free) return null;
  return "(discount for larger teams)";
}
