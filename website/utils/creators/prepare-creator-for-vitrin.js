export const prepareCreatorForVitrin = ({
  id,
  linkText = `See creator's page`,
  linkAddress = `/creators/${id}`,
  name,
  description,
  image,
}) => ({
  id,
  title: name,
  linkText,
  linkAddress,
  description,
  image,
});
