import Cookies from 'js-cookie';
import { cookieOptions } from './../../config';

export const saveToken = (token) => {
  if (process.env.NODE_ENV === 'production')
    Cookies.set('token', token, cookieOptions);
  else Cookies.set('token', token);
};
