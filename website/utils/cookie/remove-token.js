import Cookies from 'js-cookie';
import { cookieOptions } from './../../config';

export const removeToken = (token) => {
  if (process.env.NODE_ENV === 'production')
    Cookies.remove('token', cookieOptions);
  else Cookies.remove('token');
};
