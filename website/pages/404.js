import { NextSeo } from "next-seo";

export default function Custom404() {
  return (
    <main className="full-page-centered bg-sky-blue text-white">
      <NextSeo title="Page not found | teleRooster" />
      <h5>404 - Page not found</h5>
      <a href="/">Back to Home</a>
    </main>
  );
}
