import { protectRoute } from "../../../services/auth/controllers/protect-route";
import { handleMessagePost } from "../../../services/messages/controllers";

function handleReqs(req, res) {
  const { method } = req;
  switch (method) {
    case "POST":
      return protectRoute(handleMessagePost)(req, res);
    default:
      return res.status(405).send("The method is not supported");
  }
}

export default handleReqs;
