import { cors } from "../../../../adapters/api/cors";
import { protectRoute } from "../../../../services/auth/controllers";
import { getUserTeams } from "../../../../services/teams/controllers";

const handleReqs = async (req, res) => {
  await cors(req, res);
  const { method } = req;
  switch (method) {
    case "GET":
      return protectRoute(getUserTeams)(req, res);
    default:
      return res.status(405).send("The method is not supported");
  }
};

export default handleReqs;
