import { cors } from "../../../../../adapters/api/cors";
import { protectRoute } from "../../../../../services/auth/controllers";
import {
  getUserPool,
  handleDeletePoolByAdminRequest,
  updateUserPool,
} from "../../../../../services/pools/controllers";

const handleReqs = async (req, res) => {
  await cors(req, res);
  const { method } = req;
  switch (method) {
    case "PUT":
      return protectRoute(updateUserPool)(req, res);
    case "GET":
      return protectRoute(getUserPool)(req, res);
    case "DELETE":
      return protectRoute(handleDeletePoolByAdminRequest)(req, res);
    default:
      return res.status(405).send("The method is not supported");
  }
};

export default handleReqs;
