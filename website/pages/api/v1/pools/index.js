import { cors } from "../../../../adapters/api/cors";
import { protectRoute } from "../../../../services/auth/controllers";
import { getUserPools } from "../../../../services/pools/controllers";

const handleReqs = async (req, res) => {
  await cors(req, res);

  const { method } = req;
  switch (method) {
    case "GET":
      return protectRoute(getUserPools)(req, res);
    case "DELETE":
      return res.json({ done: true });
    default:
      return res.status(405).send("The method is not supported");
  }
};

export default handleReqs;
