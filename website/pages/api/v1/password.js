import { cors } from "../../../adapters/api/cors";
import { protectRoute } from "../../../services/auth/controllers";
import { changeUserPassword } from "../../../services/users/controllers";

const handleReqs = async (req, res) => {
  await cors(req, res);
  const { method } = req;
  switch (method) {
    case "PUT":
      return protectRoute(changeUserPassword)(req, res);
    default:
      return res.status(405).send("The method is not supported");
  }
};

export default handleReqs;
