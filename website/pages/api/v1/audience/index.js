import { cors } from "../../../../adapters/api/cors";
import { protectRoute } from "../../../../services/auth/controllers";
import { getUserAudience } from "../../../../services/teams/controllers";

const handleReqs = async (req, res) => {
  await cors(req, res);
  const { method } = req;
  switch (method) {
    case "GET":
      return protectRoute(getUserAudience)(req, res);
    default:
      return res.status(405).send("The method is not supported");
  }
};

export default handleReqs;
