import { cors } from "../../../../adapters/api/cors";
import { protectRoute } from "../../../../services/auth/controllers";
import { handleImageUploadRequest } from "../../../../services/images/controllers";

const handleReqs = async (req, res) => {
  await cors(req, res);
  const { method } = req;
  switch (method) {
    case "GET":
      return protectRoute(handleImageUploadRequest)(req, res);
    default:
      return res.status(405).send("The method is not supported");
  }
};

export default handleReqs;
