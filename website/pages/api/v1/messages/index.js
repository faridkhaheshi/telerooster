import { cors } from "../../../../adapters/api/cors";
import { protectRoute } from "../../../../services/auth/controllers";
import { handlePostMessage } from "../../../../services/messages/controllers";

const handleReqs = async (req, res) => {
  await cors(req, res);

  const { method } = req;
  switch (method) {
    case "POST":
      return protectRoute(handlePostMessage)(req, res);
    default:
      return res.status(405).send("The method is not supported");
  }
};

export default handleReqs;
