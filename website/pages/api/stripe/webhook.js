import { handleStripeEvents } from "../../../services/payments/controllers";

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case "POST":
      return handleStripeEvents(req, res);
    default:
      return res.status(405).send("The method is not supported");
  }
};

export default handleReqs;

export const config = {
  api: {
    bodyParser: false,
  },
};
