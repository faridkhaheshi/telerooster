import { handleIncomingEmails } from "../../services/contentmails/controllers";

const handleReqs = async (req, res) => {
  const { method } = req;
  switch (method) {
    case "POST":
      return handleIncomingEmails(req, res);
    default:
      return res.send("live");
  }
};

export default handleReqs;
