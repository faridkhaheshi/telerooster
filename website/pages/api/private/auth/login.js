import {
  makeRoutePrivate,
  handlePrivateLoginRequest,
} from "./../../../../services/auth/controllers";

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case "GET":
      return handlePrivateLoginRequest(req, res);
    default:
      return res.status(405).send("The method is not supported");
  }
};

export default makeRoutePrivate(handleReqs);
