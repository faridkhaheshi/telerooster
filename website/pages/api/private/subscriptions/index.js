import { handlePrivateAddSubscriptionRequest } from "../../../../services/subscriptions/controllers";
import { makeRoutePrivate } from "./../../../../services/auth/controllers";

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case "POST":
      return handlePrivateAddSubscriptionRequest(req, res);
    default:
      return res.status(405).send("The method is not supported");
  }
};

export default makeRoutePrivate(handleReqs);
