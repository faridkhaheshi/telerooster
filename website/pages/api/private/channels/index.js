import { makeRoutePrivate } from "../../../../services/auth/controllers";
import { handleCreateChannelRequest } from "../../../../services/channels/controllers";

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case "POST":
      return handleCreateChannelRequest(req, res);
    default:
      return res.status(405).send("The method is not supported");
  }
};

export default makeRoutePrivate(handleReqs);
