import { makeRoutePrivate } from "../../../../services/auth/controllers";
import {
  handleUpdateAllChannelsInfoRequest,
  handleUpdateChannelRequest,
} from "../../../../services/channels/controllers";

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case "PUT":
      return handleUpdateChannelRequest(req, res);
    case "POST":
      return handleUpdateAllChannelsInfoRequest(req, res);
    default:
      return res.status(405).send("The method is not supported");
  }
};

export default makeRoutePrivate(handleReqs);
