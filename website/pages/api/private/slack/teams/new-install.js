import {
  addUserMiddleware,
  makeRoutePrivate,
} from "../../../../../services/auth/controllers";
import { handleSlackNewInstall } from "../../../../../services/teams/controllers";

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case "POST":
      return addUserMiddleware(handleSlackNewInstall, "cookies")(req, res);
    default:
      return res.status(405).send("The method is not supported");
  }
};

export default makeRoutePrivate(handleReqs);
