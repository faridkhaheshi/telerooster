import { limitRouteToSuperadmin } from "../../../../services/auth/controllers";
import { handleRaiseEventRequest } from "../../../../services/events/controllers";

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case "POST":
      return handleRaiseEventRequest(req, res);
    default:
      return res.status(405).send("The method is not supported");
  }
};

export default limitRouteToSuperadmin(handleReqs);
