import { limitRouteToSuperadmin } from "../../../../../services/auth/controllers";
import { handleUpdateUserRequest } from "../../../../../services/users/controllers";

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case "PATCH":
      return handleUpdateUserRequest(req, res);
    default:
      return res.status(405).send("The method is not supported");
  }
};

export default limitRouteToSuperadmin(handleReqs);
