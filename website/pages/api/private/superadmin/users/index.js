import { limitRouteToSuperadmin } from "../../../../../services/auth/controllers";
import { handleAddUserRequest } from "../../../../../services/users/controllers";

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case "POST":
      return handleAddUserRequest(req, res);
    default:
      return res.status(405).send("The method is not supported");
  }
};

export default limitRouteToSuperadmin(handleReqs);
