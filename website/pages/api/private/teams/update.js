import { makeRoutePrivate } from "../../../../services/auth/controllers";
import { handleUpdateAllTeamsInfoRequest } from "../../../../services/teams/controllers";

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case "POST":
      return handleUpdateAllTeamsInfoRequest(req, res);
    default:
      return res.status(405).send("The method is not supported");
  }
};

export default makeRoutePrivate(handleReqs);
