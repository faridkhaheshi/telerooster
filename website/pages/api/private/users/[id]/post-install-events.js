import { makeRoutePrivate } from "../../../../../services/auth/controllers";
import { getPostInstallEventsForUser } from "../../../../../services/users/controllers";

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case "GET":
      return getPostInstallEventsForUser(req, res);
    default:
      return res.status(405).send("The method is not supported");
  }
};

export default makeRoutePrivate(handleReqs);
