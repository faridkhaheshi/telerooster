import { makeRoutePrivate } from "../../../../../services/auth/controllers";
import { handlePaidInvoice } from "../../../../../services/payments/controllers";

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case "POST":
      return handlePaidInvoice(req, res);
    default:
      return res.status(405).send("The method is not supported");
  }
};

export default makeRoutePrivate(handleReqs);
