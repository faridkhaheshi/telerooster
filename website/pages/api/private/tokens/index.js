import { makeRoutePrivate } from "../../../../services/auth/controllers";
import { handleGetTokenRequests } from "../../../../services/tokens/controllers";

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case "GET":
      return handleGetTokenRequests(req, res);
    default:
      return res.status(405).send("The method is not supported");
  }
};

export default makeRoutePrivate(handleReqs);
