import { makeRoutePrivate } from "../../../../services/auth/controllers";
import { handlePrivateSaveMessage } from "../../../../services/messages/controllers";

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case "POST":
      return handlePrivateSaveMessage(req, res);
    default:
      return res.status(405).send("The method is not supported");
  }
};

export default makeRoutePrivate(handleReqs);

export const config = {
  api: {
    bodyParser: {
      sizeLimit: "10mb",
    },
  },
};
