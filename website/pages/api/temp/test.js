import { sendAnEmail } from '../../../services/emails/processors';

export default async function handleReqs(req, res) {
  const { method } = req;
  switch (method) {
    case 'GET':
      return doSomething(req, res);
    default:
      return res.status(405).send('The method is not supported');
  }
}

async function doSomething(req, res) {
  try {
    const result = await sendAnEmail();
    return res.json({ ok: 'ok', result });
  } catch (error) {
    return res.json({ error });
  }
}
