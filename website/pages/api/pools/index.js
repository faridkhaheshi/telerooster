import {
  handleCreatePoolRequest,
  getUserPools,
} from "../../../services/pools/controllers";
import { protectRoute } from "../../../services/auth/controllers/protect-route";

function handleReqs(req, res) {
  const { method } = req;
  switch (method) {
    case "GET":
      return protectRoute(getUserPools)(req, res);
    case "POST":
      return protectRoute(handleCreatePoolRequest)(req, res);
    default:
      return res.status(405).send("The method is not supported");
  }
}

export default handleReqs;
