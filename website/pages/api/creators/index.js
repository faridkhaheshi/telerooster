export default async function handleReqs(req, res) {
  const { method } = req;
  switch (method) {
    case 'GET':
      return res.json(getTopCreators());
    default:
      return res.status(405).send('The method is not supported');
  }
}

function getTopCreators() {
  return [
    {
      id: 'c1',
      name: 'Daily Say',
      image: {
        src: '/images/daily-say.png',
        alt: 'office exercise tips',
      },
      description: `Daily Say is the company behind teleRooster.
      Its contents are mainly short inspiring messages. 
      Daily Say focuses on encouraging people to look at problems from different perspectives.
      `,
    },
  ];
}
