import { protectRoute } from "../../../services/auth/controllers";
import { addPoolSubscription } from "../../../services/subscriptions/controllers";

function handleReqs(req, res) {
  const { method } = req;
  switch (method) {
    case "POST":
      return protectRoute(addPoolSubscription)(req, res);
    default:
      return res.status(405).send("The method is not supported");
  }
}

export default handleReqs;
