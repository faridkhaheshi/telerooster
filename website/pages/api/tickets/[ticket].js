import { handleTicketVerificationRequest } from '../../../services/tickets/controllers';

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case 'POST':
      return handleTicketVerificationRequest(req, res);
    default:
      return res.status(405).send('The method is not supported');
  }
};

export default handleReqs;
