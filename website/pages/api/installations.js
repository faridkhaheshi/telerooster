import { addPostInstallationEvent } from "../../services/installations/controllers";
import { protectRoute } from "./../../services/auth/controllers";

const handleReqs = async (req, res) => {
  const { method } = req;
  switch (method) {
    case "POST":
      return addPostInstallationEvent(req, res);
    default:
      return res.send("live");
  }
};

export default protectRoute(handleReqs);
