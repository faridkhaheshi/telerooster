import { protectRoute } from "../../../services/auth/controllers";
import { getUserTeams } from "../../../services/teams/controllers";

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case "GET":
      return getUserTeams(req, res);
    default:
      return res.status(405).send("The method is not supported");
  }
};

export default protectRoute(handleReqs);
