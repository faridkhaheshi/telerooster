import { cors } from "../../adapters/api/cors";
import { protectRoute } from "../../services/auth/controllers";
import {
  getTokenUser,
  handleDeactivationRequest,
  handleEditProfileRequest,
} from "../../services/users/controllers";

const handleReqs = async (req, res) => {
  await cors(req, res);

  const { method } = req;
  switch (method) {
    case "GET":
      return protectRoute(getTokenUser)(req, res);
    case "PUT":
      return protectRoute(handleEditProfileRequest)(req, res);
    case "DELETE":
      return protectRoute(handleDeactivationRequest)(req, res);
    default:
      return res.status(405).send("The method is not supported");
  }
};

export default handleReqs;
