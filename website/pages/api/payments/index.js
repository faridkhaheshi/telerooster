import { handlePaymentResult } from "../../../services/payments/controllers";
import { protectRoute } from "./../../../services/auth/controllers";

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case "POST":
      return protectRoute(handlePaymentResult)(req, res);
    default:
      return res.status(405).send("The method is not supported");
  }
};

export default handleReqs;
