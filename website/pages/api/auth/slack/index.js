import { handleSlackLoginRequest } from "../../../../services/auth/slack/controllers";

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case "GET":
      return handleSlackLoginRequest(req, res);
    default:
      return res.status(405).send("The method is not supported");
  }
};

export default handleReqs;
