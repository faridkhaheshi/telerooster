import { handleGoogleLoginRequest } from "../../../../services/auth/google/controllers";

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case "POST":
      return handleGoogleLoginRequest(req, res);
    default:
      return res.status(405).send("The method is not supported");
  }
};

export default handleReqs;
