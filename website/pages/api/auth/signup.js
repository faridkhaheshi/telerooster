import { handleSignupRequest } from '../../../services/auth/controllers';

function handleReqs(req, res) {
  const { method } = req;
  switch (method) {
    case 'POST':
      return handleSignupRequest(req, res);
    default:
      return res.status(405).send('The method is not supported');
  }
}

export default handleReqs;
