import { addUserMiddleware } from "../../../../services/auth/controllers";
import { handleMicrosoftLoginResult } from "../../../../services/auth/microsoft/controllers";

const handleReqs = (req, res) => {
  const { method } = req;
  switch (method) {
    case "GET":
      return addUserMiddleware(handleMicrosoftLoginResult)(req, res);
    default:
      return res.status(405).send("The method is not supported");
  }
};

export default handleReqs;
