import { generateContentmailForPool } from "../../services/contentmails/processors";

const handleReqs = async (req, res) => {
  try {
    const contentmail = await generateContentmailForPool(req.body.poolId);
    return res.json({ ok: true, contentmail });
  } catch (error) {
    console.log(error);
    return res.status(error.status || 500).json({ error });
  }
};

export default handleReqs;
