import LogoutHandler from '../../components/auth/logout-handler';

function LogoutPage() {
  return (
    <div className='full-page-centered bg-sky-blue'>
      <LogoutHandler />
    </div>
  );
}

export default React.memo(LogoutPage);
