import { NextSeo } from "next-seo";
import LoginForm from "./../../features/social-login/LoginForm";

function LoginPageInternal() {
  return (
    <main className="full-page-centered bg-sky-blue">
      <NextSeo title="Log in to teleRooster" />
      <LoginForm />
    </main>
  );
}

const LoginPage = React.memo(LoginPageInternal);
LoginPage.layoutOptions = { noLayout: true };

export default LoginPage;
