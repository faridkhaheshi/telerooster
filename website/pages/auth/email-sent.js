import { useRouter } from "next/router";
import LinkTo from "../../components/common/link-to";
import BackToHomeButton from "../../features/social-login/common/BackToHomeButton";

function EmailSentPageInternal() {
  const {
    query: { email, type },
  } = useRouter();
  if (!email) return null;
  return (
    <div className="full-page-centered bg-sky-blue">
      <main className="centered-form d-flex flex-column justify-content-between">
        <div>
          <h1>Almost there...</h1>
          <p className="bigger-p-text mt-5">
            We sent an email to <strong className="text-sharp">{email}</strong>.
            Please follow the link in the email to{" "}
            {type === "forgot"
              ? "reset your password"
              : "complete your registration"}
            .
          </p>
        </div>
        <p className="light-text">
          No email?{" "}
          <LinkTo to={type === "forgot" ? "/auth/forgot" : "/auth/signup"}>
            Try again
          </LinkTo>
        </p>
      </main>
      <BackToHomeButton />
    </div>
  );
}

const EmailSentPage = React.memo(EmailSentPageInternal);
EmailSentPage.layoutOptions = { noLayout: true };

export default EmailSentPage;
