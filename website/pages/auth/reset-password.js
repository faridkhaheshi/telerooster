import { NextSeo } from "next-seo";
import { protectPage } from "../../contexts/auth";
import BackToHomeButton from "../../features/social-login/common/BackToHomeButton";
import ResetPasswordForm from "../../features/forgot-password/ResetPasswordForm";

const ResetPasswordInternal = () => (
  <main className="full-page-centered bg-sky-blue">
    <NextSeo title="Reset password | teleRooster" nofollow noindex />
    <ResetPasswordForm />
    <BackToHomeButton />
  </main>
);

const ResetPassword = React.memo(ResetPasswordInternal);
ResetPassword.layoutOptions = { noLayout: true };

export default protectPage(ResetPassword);
