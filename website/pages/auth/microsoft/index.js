import dynamic from "next/dynamic";

const MicrosoftAuthDialogClientOnly = dynamic(
  () => import("../../../components/auth/microsoft-auth-dialog"),
  {
    ssr: false,
  }
);

export default MicrosoftAuthDialogClientOnly;

//
// import { MicrosoftAuthContextProvider } from "../../../contexts/auth";

// function AuthWithMicrosoft() {
//   return (
//     <MicrosoftAuthContextProvider>
//       <MicrosoftAuthDialog />
//     </MicrosoftAuthContextProvider>
//   );
// }

// export default React.memo(AuthWithMicrosoft);
