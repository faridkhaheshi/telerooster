function MicrosoftAuthLogoutPage() {
  return <div>Microsoft Logged out</div>;
}

export default React.memo(MicrosoftAuthLogoutPage);
