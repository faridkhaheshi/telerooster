import MicrosoftConnector from "../../../components/auth/microsoft-connector";

function ConnectMicrosoftAccount({ accessToken }) {
  return (
    <div className="full-page-centered bg-sky-blue">
      <MicrosoftConnector accessToken={accessToken} />
    </div>
  );
}

export async function getServerSideProps({ query: { accessToken } }) {
  return {
    props: { accessToken },
  };
}

export default React.memo(ConnectMicrosoftAccount);
