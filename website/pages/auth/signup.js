import { NextSeo } from "next-seo";
import SignupForm from "../../features/social-login/SignupForm";

function SignupPageInternal() {
  return (
    <main className="full-page-centered bg-sky-blue">
      <NextSeo title="Sign up | teleRooster" />
      <SignupForm />
    </main>
  );
}

const SignupPage = React.memo(SignupPageInternal);
SignupPage.layoutOptions = { noLayout: true };

export default SignupPage;
