import { useRouter } from "next/router";
import { useAuth } from "../../contexts/auth";
import WaitingSpinner from "../../components/common/waiting-spinner";

function AuthenticatePage({ token, refUrl }) {
  const { login } = useAuth();
  const router = useRouter();

  React.useEffect(() => {
    if (!token) router.push("/auth/login");
  }, [token]);

  React.useEffect(() => {
    if (token) {
      login({ token, ref: refUrl });
    }
  }, [token, login, refUrl]);

  return (
    <main className="full-page-centered bg-sky-blue">
      <WaitingSpinner color="white" />
    </main>
  );
}

export default AuthenticatePage;

export const getServerSideProps = ({ query: { ref, token } }) => {
  const props = {};
  if (ref) props.refUrl = ref;
  if (token) props.token = token;
  return { props };
};
