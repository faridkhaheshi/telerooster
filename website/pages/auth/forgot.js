import { NextSeo } from "next-seo";
import ForgotPasswordForm from "../../features/forgot-password/ForgotPasswordForm";
import BackToHomeButton from "../../features/social-login/common/BackToHomeButton";

const ForgotInternal = () => {
  return (
    <main className="full-page-centered bg-sky-blue">
      <NextSeo title="Forgot password | teleRooster" nofollow noindex />
      <ForgotPasswordForm />
      <BackToHomeButton />
    </main>
  );
};

const Forgot = React.memo(ForgotInternal);
Forgot.layoutOptions = { noLayout: true };

export default Forgot;
