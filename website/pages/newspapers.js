import {
  MainPoster,
  LiveChannelCreationForm,
  LinkingPoster,
  AddToPoster,
} from "../features/landing-page";
import {
  mainPosterContent,
  addToPosterContent,
} from "../features/newspapers-page";
import FeaturedPoolsVitrin from "../features/pool-vitrin/FeaturedPoolsVitrin";

function Newspapers() {
  return (
    <main>
      <MainPoster {...mainPosterContent} />
      <LiveChannelCreationForm />
      <LinkingPoster />
      <AddToPoster {...addToPosterContent} />
      <FeaturedPoolsVitrin />
    </main>
  );
}

Newspapers.layoutOptions = {
  showNavbar: true,
  showFooter: true,
};

export default Newspapers;
