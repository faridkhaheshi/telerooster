import { buildRequestOptions, sendApiRequest } from "../../adapters/api";
import WaitingSpinner from "../../components/common/waiting-spinner";
import { microsoft } from "./../../config";

const InstallMsTeamsAppPageInternal = ({ event, ...payload }) => {
  const [proceed, setProceed] = React.useState(false);
  const [errorMessage, setErrorMessage] = React.useState(null);

  const registerPostInstallEvent = React.useCallback(async () => {
    try {
      const options = buildRequestOptions({
        method: "POST",
        body: { payload, event, platform: "microsoft" },
      });
      await sendApiRequest("/api/installations", options);
      setProceed(true);
    } catch (err) {
      console.error(err);
      setErrorMessage("Something went wrong");
    }
  }, [event, payload]);

  React.useEffect(() => {
    if (proceed) window.location.href = microsoft.appInstallLink;
  }, [proceed]);

  React.useEffect(() => {
    if (event) {
      registerPostInstallEvent();
    } else {
      setProceed(true);
    }
  }, []);
  return <Dialog errorMessage={errorMessage} />;
};

const InstallMsTeamsAppPage = React.memo(InstallMsTeamsAppPageInternal);
InstallMsTeamsAppPage.layoutOptions = { noLayout: true };

export default InstallMsTeamsAppPage;

export const getServerSideProps = ({ req, res, query = {} }) => {
  return { props: { ...query } };
};

function Dialog({ errorMessage }) {
  if (errorMessage)
    return (
      <main className="full-page-centered bg-sky-blue">
        <p className="error">{errorMessage}</p>
      </main>
    );

  return (
    <main className="full-page-centered bg-sky-blue">
      <p>
        Loading the installation page for <strong>Microsoft Teams</strong>
      </p>
      <WaitingSpinner color="white" />
    </main>
  );
}
