import { NextSeo } from "next-seo";
import SupportPoster from "../features/support-page/SupportPoster";

const Support = () => (
  <main>
    <NextSeo title="Support | teleRooster" />
    <SupportPoster />
  </main>
);

Support.layoutOptions = {
  showNavbar: true,
  showFooter: true,
  navbarColor: "white",
};

export default Support;
