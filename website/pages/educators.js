import {
  MainPoster,
  LiveChannelCreationForm,
  LinkingPoster,
  AddToPoster,
} from "../features/landing-page";
import {
  mainPosterContent,
  linkingPosterContent,
} from "../features/educators-page";
import FeaturedPoolsVitrin from "../features/pool-vitrin/FeaturedPoolsVitrin";

function Educators() {
  return (
    <main>
      <MainPoster {...mainPosterContent} />
      <LiveChannelCreationForm />
      <LinkingPoster {...linkingPosterContent} />
      <AddToPoster />
      <FeaturedPoolsVitrin />
    </main>
  );
}

Educators.layoutOptions = {
  showNavbar: true,
  showFooter: true,
};

export default Educators;
