import { NextSeo } from "next-seo";
import { sendApiRequest } from "../../../adapters/api";
import UserOnboarder from "../../../features/onboarding/UserOnboarder";

function AddPoolToTeamsPageInternal({ pool, platform }) {
  return (
    <main className="full-page-centered bg-sky-blue">
      <NextSeo
        title={`Adding channel "${pool.title}" @teleRooster`}
        description={pool.description}
        noindex
        nofollow
      />
      <UserOnboarder pool={pool} platform={platform} />
    </main>
  );
}

const AddPoolToTeamsPage = React.memo(AddPoolToTeamsPageInternal);

export default AddPoolToTeamsPage;

export async function getServerSideProps({ query: { id, platform = null } }) {
  const pool = await sendApiRequest(`/api/pools/${id}`, { local: true });
  return { props: { pool, platform } };
}
