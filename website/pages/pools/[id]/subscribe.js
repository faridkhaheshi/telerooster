import { NextSeo } from "next-seo";
import { sendApiRequest } from "../../../adapters/api";
import PoolSubscriber from "../../../components/pools/pool-subscriber";
import { protectPage } from "../../../contexts/auth";

const PoolSubscribePage = ({ pool, platform }) => (
  <main className="full-page-centered bg-sky-blue">
    <NextSeo
      title={`Adding channel "${pool.title}" @teleRooster`}
      description={pool.description}
      noindex
      nofollow
    />
    <PoolSubscriber pool={pool} platform={platform} />
  </main>
);

export default protectPage(PoolSubscribePage);

export async function getServerSideProps({ query: { id, platform = null } }) {
  const pool = await sendApiRequest(`/api/pools/${id}`, { local: true });
  return { props: { pool, platform } };
}
