import { NextSeo } from "next-seo";
import { baseUrl } from "./../../../config";
import { sendApiRequest } from "../../../adapters/api/send-api-request";
import PoolCard from "../../../features/pool-page/PoolCard/PoolCard";

function PoolPage({ pool }) {
  return (
    <main className="full-page-centered">
      <NextSeo
        title={`${pool.title} @teleRooster`}
        description={pool.description}
        openGraph={{
          title: `${pool.title} @teleRooster`,
          description: pool.description,
          type: "website",
          url: `${baseUrl}/pools/${pool.id}`,
          images: pool.poster
            ? [
                {
                  url: pool.poster,
                  width: 800,
                  height: 600,
                  alt: pool.title,
                },
              ]
            : [],
          site_name: `${pool.title} @teleRooster`,
        }}
      />
      <PoolCard pool={pool} shadow />
    </main>
  );
}

const PoolPageMemo = React.memo(PoolPage);
PoolPageMemo.layoutOptions = { noLayout: true };

export default PoolPageMemo;

export async function getServerSideProps({ query: { id } }) {
  const pool = await sendApiRequest(`/api/pools/${id}`, { local: true });
  return { props: { pool } };
}
