import { NextSeo } from "next-seo";
import { sendApiRequest } from "../../../adapters/api/send-api-request";
import LinkTo from "../../../components/common/link-to";
import { buildSubscribeLink } from "../../../components/widgets/website-embed-button";
import { panelBaseUrl } from "../../../config";
import { NextSteps } from "../../../features/embed-page";
import RoosterLogo from "../../../features/embed-page/common/RoosterLogo";
import PoolCard from "../../../features/pool-page/PoolCard/PoolCard";
import BackToHomeButton from "../../../features/social-login/common/BackToHomeButton";

function EmbedPoolPage({ pool }) {
  return (
    <main className="d-flex flex-column align-items-center">
      <NextSeo
        title={`Embed "${pool.title}" @teleRooster`}
        description={pool.description}
        noindex
        nofollow
      />
      <RoosterLogo />
      <h2 className="mt-5 mb-5">
        <LinkTo to={buildSubscribeLink({ poolId: pool.id })} target="_blank">
          Your channel
        </LinkTo>{" "}
        is ready:
      </h2>
      <PoolCard pool={pool} hideBackButton shadow />
      <NextSteps pool={pool} />
      <BackToHomeButton
        forward
        text="proceed to dashboard"
        color="black"
        className="mb-5"
        to={panelBaseUrl}
      />
    </main>
  );
}

const EmbedPoolPageMemo = React.memo(EmbedPoolPage);
EmbedPoolPageMemo.layoutOptions = { noLayout: true };

export default EmbedPoolPageMemo;

export async function getServerSideProps({ query: { id } }) {
  const pool = await sendApiRequest(`/api/pools/${id}`, { local: true });
  return {
    props: { pool },
  };
}
