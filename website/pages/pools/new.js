import { NextSeo } from "next-seo";
import CreatePoolForm from "../../components/pools/create-pool-form";
import BackToHomeButton from "./../../features/social-login/common/BackToHomeButton";

function NewPoolInternal() {
  return (
    <div className="full-page-centered bg-sky-blue">
      <NextSeo title="Create a channel in teleRooster" />
      <CreatePoolForm />
      <BackToHomeButton className="mb-5" />
    </div>
  );
}

const NewPool = React.memo(NewPoolInternal);

NewPool.layoutOptions = { noLayout: true };

export default NewPool;
