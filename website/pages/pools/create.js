import { NextSeo } from "next-seo";

import WaitingSpinner from "../../components/common/waiting-spinner";
import { protectPage } from "../../contexts/auth";
import { useRouter } from "next/router";
import PoolCreator from "../../components/pools/pool-creator";

function CreatePool() {
  const [poolData, setPoolData] = React.useState(null);
  const router = useRouter();

  React.useEffect(() => {
    if (router?.query?.poolStr) {
      const pool = JSON.parse(router?.query?.poolStr);
      setPoolData(pool);
    }
  }, [router.query]);

  return (
    <div className="full-page-centered bg-sky-blue">
      <NextSeo title="Creating a channel in teleRooster" />
      {poolData ? (
        <PoolCreator poolData={poolData} />
      ) : (
        <>
          <p>Processing...</p>
          <WaitingSpinner />
        </>
      )}
    </div>
  );
}

const CreatePoolPage = React.memo(CreatePool);
CreatePoolPage.layoutOptions = { noLayout: true };

export default protectPage(CreatePoolPage);
