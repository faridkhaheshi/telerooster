import { DefaultSeo } from "next-seo";
import { ToastContainer } from "react-toastify";
import { AuthContextProvider } from "../contexts/auth";
import { config as fontawesomeConfig } from "@fortawesome/fontawesome-svg-core";
import useGoogleAnalytics from "../hooks/use-google-analytics";
import { Layout } from "../features/layout";
import SEO from "./../next-seo.config";

import "../styles/globals.css";
import "@fortawesome/fontawesome-svg-core/styles.css";
import "react-toastify/dist/ReactToastify.css";
import "bootstrap/dist/css/bootstrap.min.css";

fontawesomeConfig.autoAddCss = false;

function MyApp({ Component, pageProps }) {
  useGoogleAnalytics();

  return (
    <AuthContextProvider>
      <DefaultSeo {...SEO} />
      <Layout options={Component.layoutOptions}>
        <Component {...pageProps} />
      </Layout>
      <ToastContainer autoClose={10000} draggable hideProgressBar />
    </AuthContextProvider>
  );
}

export default MyApp;
