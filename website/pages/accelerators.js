import {
  MainPoster,
  LiveChannelCreationForm,
  LinkingPoster,
  AddToPoster,
} from "../features/landing-page";
import {
  mainPosterContent,
  linkingPosterContent,
  addToPosterContent,
} from "../features/accelerators-page";
import FeaturedPoolsVitrin from "../features/pool-vitrin/FeaturedPoolsVitrin";

function Accelerators() {
  return (
    <main>
      <MainPoster
        title={mainPosterContent.title}
        content={mainPosterContent.content}
      />
      <LiveChannelCreationForm />
      <LinkingPoster {...linkingPosterContent} />
      <AddToPoster {...addToPosterContent} />
      <FeaturedPoolsVitrin />
    </main>
  );
}

Accelerators.layoutOptions = {
  showNavbar: true,
  showFooter: true,
};

export default Accelerators;
