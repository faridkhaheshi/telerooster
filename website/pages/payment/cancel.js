import { NextSeo } from "next-seo";
import { PaymentResultProcessor } from "../../features/payment";

const CancelPage = ({ sessionId }) => (
  <main className="full-page-centered bg-sky-blue">
    <NextSeo title="Payment canceled" nofollow noindex />
    <PaymentResultProcessor sessionId={sessionId} result="cancel" />
  </main>
);

export default React.memo(CancelPage);

export const getServerSideProps = ({ query: { session_id } }) => ({
  props: { sessionId: session_id },
});
