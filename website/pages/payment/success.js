import { NextSeo } from "next-seo";
import { PaymentResultProcessor } from "../../features/payment";

const SuccessPage = ({ sessionId }) => (
  <main className="full-page-centered bg-sky-blue">
    <NextSeo title="Payment successful" nofollow noindex />
    <PaymentResultProcessor sessionId={sessionId} result="success" />
  </main>
);

export default React.memo(SuccessPage);

export const getServerSideProps = ({ query: { session_id } }) => ({
  props: { sessionId: session_id },
});
