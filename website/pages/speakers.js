import {
  MainPoster,
  LiveChannelCreationForm,
  LinkingPoster,
  AddToPoster,
} from "../features/landing-page";
import {
  mainPosterContent,
  linkingPosterContent,
} from "../features/speakers-page";
import FeaturedPoolsVitrin from "../features/pool-vitrin/FeaturedPoolsVitrin";

function Speakers() {
  return (
    <main>
      <MainPoster {...mainPosterContent} />
      <LiveChannelCreationForm />
      <LinkingPoster {...linkingPosterContent} />
      <AddToPoster />
      <FeaturedPoolsVitrin />
    </main>
  );
}

Speakers.layoutOptions = {
  showNavbar: true,
  showFooter: true,
};

export default Speakers;
