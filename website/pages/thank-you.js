function ThankYou() {
  return (
    <div
      className="full-page-centered bigger-p-text"
      style={{ backgroundColor: "#DA3E52", color: "white" }}
    >
      <img
        style={{ maxHeight: "70vh", maxWidth: "100%", marginBottom: 20 }}
        src={getRandomThankYouGif()}
        alt="thank you"
      />
      Thank you for your response. We appreciate your time.
    </div>
  );
}

export default React.memo(ThankYou);

const gifLinks = [
  "https://media.giphy.com/media/3oEduJnper1UdNqreg/giphy.gif",
  "https://media.giphy.com/media/5ArJanyCfxgiY/giphy.gif",
  "https://media.giphy.com/media/3oz8xIsloV7zOmt81G/giphy.gif",
  "https://media.giphy.com/media/3oEdva9BUHPIs2SkGk/giphy.gif",
  "https://media.giphy.com/media/WnIu6vAWt5ul3EVcUE/giphy.gif",
  "https://media.giphy.com/media/KGZCDBoaOPILHJc7RT/giphy.gif",
  "https://media.giphy.com/media/xIJLgO6rizUJi/giphy.gif",
  "https://media.giphy.com/media/KB8C86UMgLDThpt4WT/giphy.gif",
  "https://media.giphy.com/media/3ohs4kI2X9r7O8ZtoA/giphy.gif",
  "https://media.giphy.com/media/WS09ex6XM9yYDCeH5c/giphy.gif",
  "https://media.giphy.com/media/13qctMBrrgbwJi/giphy.gif",
  "https://media.giphy.com/media/eyGs1FYIYgka4/giphy.gif",
];

export const getRandomThankYouGif = () => {
  return gifLinks[Math.floor(Math.random() * gifLinks.length)];
};
