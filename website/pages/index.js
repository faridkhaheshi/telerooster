import {
  MainPoster,
  mainPosterContent,
  LiveChannelCreationForm,
  LinkingPoster,
  AddToPoster,
  linkingPosterContent,
  addToPosterContent,
} from "../features/landing-page";
import FeaturedPoolsVitrin from "../features/pool-vitrin/FeaturedPoolsVitrin";

function HomeInternal() {
  return (
    <main>
      <MainPoster {...mainPosterContent} />
      <LiveChannelCreationForm />
      <LinkingPoster {...linkingPosterContent} />
      <AddToPoster {...addToPosterContent} />
      <FeaturedPoolsVitrin />
    </main>
  );
}

const Home = React.memo(HomeInternal);
Home.layoutOptions = {
  showNavbar: true,
  showFooter: true,
};

export default Home;
