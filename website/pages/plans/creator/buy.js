import { NextSeo } from "next-seo";

const BuyCreatorPlan = () => (
  <main className="full-page-centered bg-sky-blue">
    <NextSeo title="Buying creator plan | teleRooster" />
    Buy creator plan
  </main>
);

export default React.memo(BuyCreatorPlan);
