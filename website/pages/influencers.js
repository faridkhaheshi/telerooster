import {
  MainPoster,
  LiveChannelCreationForm,
  LinkingPoster,
  AddToPoster,
} from "../features/landing-page";
import {
  mainPosterContent,
  linkingPosterContent,
  addToPosterContent,
} from "../features/influencers-page";
import FeaturedPoolsVitrin from "../features/pool-vitrin/FeaturedPoolsVitrin";

function Influencers() {
  return (
    <main>
      <MainPoster {...mainPosterContent} />
      <LiveChannelCreationForm />
      <LinkingPoster {...linkingPosterContent} />
      <AddToPoster {...addToPosterContent} />
      <FeaturedPoolsVitrin />
    </main>
  );
}

Influencers.layoutOptions = {
  showNavbar: true,
  showFooter: true,
};

export default Influencers;
