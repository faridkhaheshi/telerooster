import { NextSeo } from "next-seo";
import { PrivacyPolicy } from "../features/privacy-page";
import TextPageContainer from "../features/text-pages/TextPageContainer";

const Privacy = () => (
  <main>
    <NextSeo title="Privacy policy | teleRooster" />
    <TextPageContainer title="Privacy Policy">
      <PrivacyPolicy />
    </TextPageContainer>
  </main>
);

Privacy.layoutOptions = {
  showNavbar: true,
  showFooter: true,
  navbarColor: "var(--sky-blue)",
};

export default Privacy;
