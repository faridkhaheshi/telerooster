import { NextSeo } from "next-seo";
import { useRouter } from "next/router";
import TicketChecker from "./../../components/auth/ticket-checker";

function TicketPageInternal() {
  const {
    query: { ticket, email },
  } = useRouter();

  return (
    <div className="full-page-centered bg-sky-blue">
      <NextSeo title="Checking your ticket | teleRooster" noindex nofollow />
      {ticket && <TicketChecker ticket={ticket} email={email} />}
    </div>
  );
}

const TicketPage = React.memo(TicketPageInternal);
TicketPage.layoutOptions = { noLayout: true };

export default TicketPage;
