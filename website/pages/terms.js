import { NextSeo } from "next-seo";
import TermsAndConditions from "../features/terms-page/TermsAndConditions";
import TextPageContainer from "../features/text-pages/TextPageContainer";

const Terms = () => (
  <main>
    <NextSeo title="Terms and conditions | teleRooster" />
    <TextPageContainer title="Terms and Conditions">
      <TermsAndConditions />
    </TextPageContainer>
  </main>
);

Terms.layoutOptions = {
  showNavbar: true,
  showFooter: true,
  navbarColor: "var(--sky-blue)",
};

export default Terms;
