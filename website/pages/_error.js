import { NextSeo } from "next-seo";

function Error({ statusCode }) {
  return (
    <main className="full-page-centered bg-sky-blue text-white">
      <NextSeo title="Error page | teleRooster" />
      <h5>
        {statusCode
          ? `An error ${statusCode} occurred on server`
          : `An error occurred on client`}
      </h5>
      <a href="/">Back to Home</a>
    </main>
  );
}

Error.getInitialProps = ({ res, err }) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
  return { statusCode };
};

export default Error;
