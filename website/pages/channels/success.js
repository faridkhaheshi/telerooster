import { NextSeo } from "next-seo";
import ChannelInfo from "../../components/channels/channel-info";
import { useRouter } from "next/router";

function ChannelPageInternal() {
  const {
    query: { name, webUrl },
  } = useRouter();
  if (!name || !webUrl) return null;
  return (
    <main className="full-page-centered bg-sky-blue">
      <NextSeo
        title={`Successfully added ${name} to your team`}
        noindex
        nofollow
      />
      <ChannelInfo name={name} webUrl={webUrl} />
    </main>
  );
}

const ChannelPage = React.memo(ChannelPageInternal);
ChannelPage.layoutOptions = { noLayout: true };

export default ChannelPage;
