import { NextSeo } from "next-seo";
import ChannelCreator from "../../components/channels/channel-creator";
import { protectPage } from "../../contexts/auth";
import { useRouter } from "next/router";
import WaitingSpinner from "../../components/common/waiting-spinner";

function CreateChannelPageInternal() {
  const router = useRouter();
  const {
    query: { teamId, subscriptionId, platform, profileId },
  } = router;

  if (!teamId || !subscriptionId)
    return (
      <main className="full-page-centered bg-sky-blue">
        <WaitingSpinner color="white" />
      </main>
    );

  return (
    <div className="full-page-centered bg-sky-blue">
      <NextSeo title="Add channel to a team | teleRooster" noindex nofollow />
      <ChannelCreator
        teamId={teamId}
        subscriptionId={subscriptionId}
        profileId={profileId}
        platform={platform}
      />
    </div>
  );
}

const CreateChannelPage = React.memo(CreateChannelPageInternal);
CreateChannelPage.layoutOptions = { noLayout: true };

export default protectPage(CreateChannelPage);
