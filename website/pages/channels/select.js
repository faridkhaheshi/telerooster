import { NextSeo } from "next-seo";
import { sendApiRequest } from "../../adapters/api";
import TeamSelector from "../../components/channels/team-selector";
import { protectPage } from "../../contexts/auth";

function SelectChannelPageInternal({ teams, platform, subscriptionId }) {
  if (!subscriptionId) return null;
  return (
    <div className="full-page-centered bg-sky-blue">
      <NextSeo title="Select a team | teleRooster" noindex nofollow />
      <TeamSelector
        teams={teams}
        subscriptionId={subscriptionId}
        platform={platform}
      />
    </div>
  );
}

const SelectChannelPage = React.memo(SelectChannelPageInternal);

export default protectPage(SelectChannelPage);

export async function getServerSideProps({
  req,
  query: { subscriptionId, platform },
}) {
  const token = req.cookies.token;
  if (!token || !subscriptionId) return { props: {} };
  const apiUrl = platform
    ? `/api/v1/teams?platform=${platform}`
    : `/api/v1/teams`;
  const teams = await sendApiRequest(apiUrl, {
    local: true,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
  });
  return { props: { subscriptionId, platform: platform || null, teams } };
}
