import { slack } from "./../../config";

export default () => {
  React.useEffect(() => {
    window.location.href = slack.authUrl;
  }, []);
  return <div />;
};

// export const getServerSideProps = ({ req, res }) => {
//   console.log(slack.authUrl);
//   if (res?.writeHead) {
//     res.writeHead(302, { Location: slack.authUrl });
//     res.end();
//   } else {
//     return { props: {} };
//   }
// };
