import { NextSeo } from "next-seo";
import { PricingPlansRow, PricingPoster } from "../features/pricing-page";

const Pricing = () => (
  <main>
    <NextSeo title="Pricing plans | teleRooster" />
    <PricingPoster />
    <PricingPlansRow />
  </main>
);

Pricing.layoutOptions = {
  showNavbar: true,
  showFooter: true,
  navbarColor: "var(--pork-pink)",
  logoAccent: "white",
};

export default Pricing;
