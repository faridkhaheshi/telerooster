import { NextSeo } from "next-seo";

const Faq = () => (
  <main className="full-page-centered bg-sky-blue">
    <NextSeo title="Frequently asked questions | teleRooster" />
    FAQ
  </main>
);

Faq.layoutOptions = {
  showNavbar: true,
  showFooter: true,
};

export default Faq;
