import styles from "./PoolCard.module.css";
import PoolImage from "./PoolImage";
import AddToSlackButton from "./../AddToSlackButton";
import AddToTeamsButton from "./../AddToTeamsButton";
import BackToHomeButton from "./../../social-login/common/BackToHomeButton";
import PoolInfo from "./PoolInfo";
import { pickCardColor } from "./pick-card-color";

const PoolCard = ({
  pool,
  width = 350,
  height = 700,
  shadow = false,
  zoom = false,
  color = pickCardColor(`0x${pool.id.split("-").pop()}`),
  hideBackButton = false,
  disableButton = false,
  link = false,
}) => (
  <>
    <div
      className={findCardClassName({ zoom, shadow })}
      style={{
        width,
        height,
        borderRadius: Math.round(width / 25),
        backgroundColor: color,
      }}
    >
      <PoolImage src={pool.poster} alt={pool.title} cardWidth={width} />
      <PoolInfo pool={pool} link={link} />
      <div className={styles.cardFooter}>
        <AddToSlackButton
          poolId={pool.id}
          className="mb-2"
          disabled={disableButton}
        />
        {/* <AddToTeamsButton poolId={pool.id} /> */}
      </div>
    </div>
    {!hideBackButton && (
      <BackToHomeButton text="telerooster.com" color={color} forward />
    )}
  </>
);

export default React.memo(PoolCard);

function findCardClassName({ zoom = false, shadow = false }) {
  if (zoom && shadow)
    return `${styles.container} ${styles.poolCardShadow} ${styles.zoom}`;
  else if (shadow) {
    return `${styles.container} ${styles.poolCardShadow}`;
  } else {
    return styles.container;
  }
}
