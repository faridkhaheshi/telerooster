import { Row, Col } from "reactstrap";
import LinkTo from "../../../components/common/link-to";

import styles from "./PoolInfo.module.css";
import PriceTag from "./PriceTag";

const PoolInfo = ({ pool, link = false }) => (
  <>
    <div className={styles.fixedInfoContainer}>
      {link ? (
        <LinkTo to={`/pools/${pool.id}`}>
          <h3 className="mb-4">{pool.title}</h3>
        </LinkTo>
      ) : (
        <h3 className="mb-4">{pool.title}</h3>
      )}
      <Row className="m-0 p-0">
        <Col xs={8} className={styles.subtitleContainer}>
          <p
            className={styles.substitle}
          >{`Creator: ${pool.creator.display_name}`}</p>
        </Col>
        <Col sm={4} className={styles.priceTagContainer}>
          <p className={styles.priceTag}>
            <PriceTag {...pool} />
          </p>
        </Col>
      </Row>
    </div>
    <div className={styles.scrollableInfoContainer}>
      <p>{pool.description}</p>
    </div>
  </>
);

export default React.memo(PoolInfo);
