export const parsePrice = (price) => ({
  dollars: Math.floor(price / 100),
  cents: price % 100,
  get tag() {
    return `$ ${this.dollars}.${this.cents}`;
  },
});
