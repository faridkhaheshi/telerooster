const colors = [
  "#04A777",
  "#FFBA08",
  "#3F88C5",
  "#D00000",
  "#6A7FDB",
  "#8ED081",
  "#B57F50",
  "#136F63",
  "#DA3E52",
  "#613DC1",
  "#F06543",
  "#9DC4B5",
  "#BC4749",
  "#885A89",
  "#57B8FF",
  "#F991CC",
  "#7ec850",
  "#F09D51",
];

export const pickCardColor = (index = -1) => {
  const intIndex = parseInt(index) % colors.length;
  if (intIndex >= 0 && intIndex < colors.length) return colors[intIndex];
  return colors[Math.floor(Math.random() * colors.length)];
};
