import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faImage } from "@fortawesome/free-regular-svg-icons";
import styles from "./PoolImage.module.css";

const PoolImage = ({ src, alt, cardWidth = 250 }) => (
  <div className={styles.imageContainer}>
    {src ? (
      <img src={src} alt={alt || "image"} />
    ) : (
      <FontAwesomeIcon
        icon={faImage}
        size={calculateImagePlacehoderSize(cardWidth)}
      />
    )}
  </div>
);

export default React.memo(PoolImage);

function calculateImagePlacehoderSize(cardWidth) {
  if (cardWidth > 300) return "3x";
  if (cardWidth > 100) return "2x";
  return "1x";
}
