import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faQuestionCircle } from "@fortawesome/free-solid-svg-icons";
import { parsePrice } from "./parse-price";
import { UncontrolledTooltip } from "reactstrap";

import styles from "./PriceTag.module.css";

const PriceTag = ({
  id,
  is_free: isFree,
  price_sm: priceSm,
  price_md: priceMd,
  price_lg: priceLg,
}) => {
  if (isFree) return "Free";
  const minPrice = Math.min(priceSm, priceMd, priceLg);
  const minPriceInfo = parsePrice(minPrice);
  const priceSmInfo = parsePrice(priceSm);
  const priceMdInfo = parsePrice(priceMd);
  const priceLgInfo = parsePrice(priceLg);

  return (
    <>
      <span>{`from ${minPriceInfo.tag}`}</span>
      <FontAwesomeIcon
        icon={faQuestionCircle}
        className="ml-2"
        id={`pool-price-tag-${id}`}
      />
      <UncontrolledTooltip
        placement="auto"
        target={`pool-price-tag-${id}`}
        innerClassName={styles.tooltipContainer}
      >
        <p className="mt-2">
          Prices are <strong>per member per month</strong>.
        </p>
        Details:
        <ul>
          <li>
            small teams (under 10 users): <br />
            <strong>{priceSmInfo.tag}</strong>
          </li>
          <li>
            medium-sized teams (10 to 500 users):
            <br />
            <strong>{priceMdInfo.tag}</strong>
          </li>
          <li>
            large teams (501 users and more):
            <br /> <strong>{priceLgInfo.tag}</strong>
          </li>
        </ul>
      </UncontrolledTooltip>
    </>
  );
};

export default React.memo(PriceTag);
