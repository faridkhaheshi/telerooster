import { buildSubscribeLink } from "../../components/widgets/website-embed-button";

const AddToTeamsButton = ({ poolId, className }) => (
  <a
    href={buildSubscribeLink({ poolId, platform: "microsoft" })}
    target="_blank"
    style={{ display: "block" }}
    className={className || ""}
  >
    <img
      src="/images/add-to-teams.png"
      alt="Add to Microsoft Teams"
      width="140"
      height="41"
      style={{ display: "block", marginLeft: "auto", marginRight: "auto" }}
    />
  </a>
);

export default AddToTeamsButton;
