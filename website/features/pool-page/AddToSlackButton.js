import { buildSubscribeLink } from "../../components/widgets/website-embed-button";

const AddToSlackButtons = ({ poolId, className, disabled = false }) => (
  <a
    href={disabled ? "#" : buildSubscribeLink({ poolId, platform: "slack" })}
    target={disabled ? "" : "_blank"}
    style={{ display: "block" }}
    className={className || ""}
  >
    <img
      src="https://cdn.brandfolder.io/5H442O3W/as/pl54cs-bd9mhs-czsxst/btn-add-to-slack.svg"
      alt="Add to Slack"
      width="140"
      height="41"
      style={{ display: "block", marginLeft: "auto", marginRight: "auto" }}
    />
  </a>
);

export default React.memo(AddToSlackButtons);
