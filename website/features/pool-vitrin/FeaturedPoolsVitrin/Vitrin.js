import WaitingSpinner from "../../../components/common/waiting-spinner";
import PoolCard from "../../pool-page/PoolCard/PoolCard";

import styles from "./FeaturedPoolsVitrin.module.css";

const Vitrin = ({ isLoading, error, pools }) => {
  if (isLoading) return <WaitingSpinner color="var(--grass-green)" />;

  if (error)
    return <p className="error">{error.message || "Loading failed"}</p>;

  return (
    <div className={styles.vitrinContainer}>
      {pools.map((pool) => (
        <PoolCard
          key={pool.id}
          pool={pool}
          width={300}
          height={700}
          zoom
          hideBackButton
          link
        />
      ))}
    </div>
  );
};

export default Vitrin;
