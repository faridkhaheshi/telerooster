import useServerResource from "../../../hooks/use-server-resource";
import styles from "./FeaturedPoolsVitrin.module.css";
import Vitrin from "./Vitrin";

const FeaturedPoolsVitrin = () => {
  const { data: pools, error, isLoading } = useServerResource(
    "/api/pools/featured"
  );
  return (
    <section className={styles.RowContainer} id="featured-pools-vitrin">
      <div className={styles.titleContainer}>
        <h1 className="large-heading-margin">Want to experience it first?</h1>
        <p className="bigger-p-text">
          Subscribe to a channel to see how teleRooster works in action.
        </p>
      </div>
      <Vitrin isLoading={isLoading} error={error} pools={pools} />
    </section>
  );
};

export default React.memo(FeaturedPoolsVitrin);
