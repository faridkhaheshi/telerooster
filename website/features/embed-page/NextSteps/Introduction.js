import LinkTo from "../../../components/common/link-to";
import styles from "./NextSteps.module.css";
import { panelBaseUrl } from "./../../../config";

const Introduction = ({ poolId }) => (
  <section className={`${styles.introContainer} ${styles.sectionContainer}`}>
    <h1>What's next?</h1>
    <p>
      Was it hard? We hope it was not. You can{" "}
      <LinkTo to={`${panelBaseUrl}/post?poolId=${poolId}`} target="_blank">
        start posting
      </LinkTo>{" "}
      to your channel right now.
    </p>
    <p>But you can do more by following the steps below:</p>
  </section>
);

export default React.memo(Introduction);
