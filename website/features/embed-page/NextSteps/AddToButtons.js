import LinkTo from "../../../components/common/link-to";
import EmailEmbedButton, {
  emailEmbedButtonText,
} from "../../../components/widgets/email-embed-button";
import WebsiteEmbedButton, {
  buildSubscribeLink,
  websiteEmbedButtonText,
} from "../../../components/widgets/website-embed-button";
import EmbedRow from "../EmbedRow";
import styles from "./NextSteps.module.css";

const AddToButtons = ({ pool }) => (
  <>
    <section className={`${styles.sectionContainer} ${styles.lastSection}`}>
      <h2>4. Get your "Add to ..." buttons</h2>
      <p>
        Everyone with{" "}
        <LinkTo to={`/pools/${pool.id}`} target="_blank">
          this link
        </LinkTo>{" "}
        can add your channel to their team quite easily. But you can make the
        process even easier by giving your audience the option to join by
        clicking on a button.
      </p>
      <p>
        Use the following code blocks to add this button to your website or your
        email newsletters:
      </p>
    </section>
    <EmbedRow
      title="Add to your website"
      code={websiteEmbedButtonText(pool.id)}
    >
      <WebsiteEmbedButton pool={pool} />
    </EmbedRow>
    <EmbedRow title="Add to your emails" code={emailEmbedButtonText(pool.id)}>
      <EmailEmbedButton pool={pool} />
    </EmbedRow>
    <EmbedRow
      title="Simple link to use anywhere"
      code={buildSubscribeLink({ poolId: pool.id })}
      light
      hideLight
    >
      <p>Anyone can subscribe to your channel by clicking on this link:</p>
      <a href={buildSubscribeLink({ poolId: pool.id })} target="_blank">
        {buildSubscribeLink({ poolId: pool.id })}
      </a>
    </EmbedRow>
  </>
);

export default React.memo(AddToButtons);
