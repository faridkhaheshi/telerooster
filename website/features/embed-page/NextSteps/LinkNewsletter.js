import ContentMailBox from "./ContentMailBox";
import styles from "./NextSteps.module.css";

const LinkNewslettter = ({ pool }) => {
  return (
    <section className={styles.sectionContainer}>
      <h2>3. Link your email newsletter</h2>
      <p>
        In addition to using the editor to post contents, you can link this
        channel to your email newsletters. Every message you send via your email
        newsletters will be automatically posted in the channel.
      </p>
      <p>
        To do this, just add the following email address to your newsletter. We
        will handle the rest.
      </p>
      <ContentMailBox pool={pool} />
    </section>
  );
};

export default React.memo(LinkNewslettter);
