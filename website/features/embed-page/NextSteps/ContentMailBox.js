import { Spinner } from "reactstrap";
import CustomButton from "../../../components/common/custom-button";
import { useAuth } from "../../../contexts/auth/auth-context";
import useServerResource from "../../../hooks/use-server-resource";
import CopiableCodeBox from "../common/CopiebleCodeBox";
import { baseUrl } from "./../../../config";

const ContentMailBox = ({ pool }) => {
  const { isAuthenticated, user } = useAuth();
  const loadContentMail =
    isAuthenticated && user && user.id === pool.creator_id;
  const { data: poolInfo, error, isLoading } = useServerResource(
    loadContentMail && `/api/v1/pools/${pool.id}`
  );

  if (!isAuthenticated) return <LoginButton poolId={pool.id} />;

  if (!loadContentMail || error)
    return (
      <p className="error">
        Only the owner of this channel can see this information.
      </p>
    );

  if (isLoading)
    return (
      <Spinner
        color="secondary"
        size="sm"
        className="ml-auto mr-auto d-block"
      />
    );

  if (
    poolInfo &&
    poolInfo.Contentmails &&
    Array.isArray(poolInfo.Contentmails) &&
    poolInfo.Contentmails.length > 0
  )
    return (
      <>
        <CopiableCodeBox code={poolInfo.Contentmails[0].address} />
        <p className="mt-4">
          Simply subscribe this email to your newsletter. Click the copy button
          above and paste the address into your newsletter's subscription box.
        </p>
      </>
    );

  return (
    <p className="error">
      Only the owner of this channel can see this information.
    </p>
  );
};

export default React.memo(ContentMailBox);

function LoginButton({ poolId }) {
  return (
    <>
      <p className="error">
        Only the owner of this channel can see this information.
      </p>
      <div className="d-flex justify-content-center mb-5">
        <CustomButton
          link
          href={`/auth/login?ref=${encodeURIComponent(
            `${baseUrl}/pools/${poolId}/embed`
          )}`}
        >
          Login
        </CustomButton>
      </div>
    </>
  );
}
