import LinkTo from "../../../components/common/link-to";
import styles from "./NextSteps.module.css";
import { panelBaseUrl } from "./../../../config";
import CustomButton from "../../../components/common/custom-button";

const SendFirstContent = ({ poolId }) => (
  <section className={styles.sectionContainer}>
    <h2>2. Send your first content</h2>
    <p>
      Use{" "}
      <LinkTo to={`${panelBaseUrl}/post?poolId=${poolId}`} target="_blank">
        our editor
      </LinkTo>{" "}
      to post your first content to your channel. See how it feels.
    </p>
    <div className="d-flex justify-content-center">
      <CustomButton
        link
        href={`${panelBaseUrl}/post?poolId=${poolId}`}
        className="mt-5 mb-4"
        target="_blank"
      >
        Go to the editor
      </CustomButton>
    </div>
  </section>
);

export default React.memo(SendFirstContent);
