import styles from "./NextSteps.module.css";
import AddToSlackButton from "../../pool-page/AddToSlackButton";
import AddToTeamsButton from "../../pool-page/AddToTeamsButton";

const AddToTeam = ({ poolId }) => (
  <section className={styles.sectionContainer}>
    <h2>1. Add it to your own team</h2>
    <p>
      See how it works in action. Just click on the "Add to Slack" button above.
      It actually works!
    </p>
    <p>
      The button helps your audience to add this channel to their teams. Here it
      is again:
    </p>
    <AddToSlackButton poolId={poolId} className="mb-2 mt-5" />
    {/* <AddToTeamsButton poolId={poolId} className="mb-5" /> */}
  </section>
);

export default React.memo(AddToTeam);
