import Introduction from "./Introduction";
import AddToTeam from "./AddToTeam";
import SendFirstContent from "./SendFirstContent";
import LinkNewsletter from "./LinkNewsletter";
import AddToButtons from "./AddToButtons";

const NextSteps = ({ pool }) => {
  return (
    <>
      <Introduction poolId={pool.id} />
      <AddToTeam poolId={pool.id} />
      <SendFirstContent poolId={pool.id} />
      <LinkNewsletter pool={pool} />
      <AddToButtons pool={pool} />
    </>
  );
};

export default React.memo(NextSteps);
