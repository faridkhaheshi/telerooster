import { Col, Row } from "reactstrap";
import CopiableCodeBox from "../common/CopiebleCodeBox";
import LightSwitch from "../common/LightSwitch/LightSwitch";

import styles from "./EmbedRow.module.css";

const EmbedRow = ({
  code,
  hideLight = false,
  light = false,
  title,
  children,
}) => {
  const [isLight, setIsLight] = React.useState(light);
  return (
    <Row className={styles.embedRow}>
      <Col md={4} className={styles.embedTitle}>
        <h3>{title}</h3>
      </Col>
      <Col md={8} className={styles.embedPreviewCol}>
        <div
          className={`${styles.showRoom} ${
            isLight ? styles.light : styles.dark
          }`}
        >
          {!hideLight && (
            <LightSwitch
              initiallyOn={light}
              onChange={setIsLight}
              top={20}
              left={20}
            />
          )}
          {children}
        </div>
        <CopiableCodeBox code={code} />
      </Col>
    </Row>
  );
};

export default React.memo(EmbedRow);
