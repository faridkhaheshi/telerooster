import { Col, Row } from "reactstrap";
import CopiableCodeBox from "../common/CopiebleCodeBox";
import LightSwitch from "../common/LightSwitch/LightSwitch";

import styles from "./EmbedRow.module.css";

const PreviewVitrin = ({
  code,
  hideLight = false,
  light = false,
  className = "",
  children,
}) => {
  const [isLight, setIsLight] = React.useState(light);
  return (
    <div className={styles.embedPreviewCol} className={className}>
      <div
        className={`${styles.showRoom} ${isLight ? styles.light : styles.dark}`}
      >
        {!hideLight && (
          <LightSwitch
            initiallyOn={light}
            onChange={setIsLight}
            top={20}
            left={20}
          />
        )}
        {children}
      </div>
      <CopiableCodeBox code={code} />
    </div>
  );
};

export default PreviewVitrin;
