import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import { faCopy } from "@fortawesome/free-regular-svg-icons";

import styles from "./CopiableCodeBox.module.css";
import { Button } from "reactstrap";
import useCopyToClipboard from "../../../../hooks/use-copy-to-clipboard";

const CopiableCodeBox = ({ code }) => {
  const { copy, copied } = useCopyToClipboard(code);
  return (
    <div className={styles.boxContainer}>
      <pre className={styles.codeContainer}>
        <code>{code}</code>
      </pre>
      <Button
        color="link"
        className={styles.copyButton}
        onClick={copy}
        disabled={copied}
      >
        {copied ? (
          <FontAwesomeIcon icon={faCheck} className={styles.copyIcon} />
        ) : (
          <FontAwesomeIcon icon={faCopy} className={styles.copyIcon} />
        )}
      </Button>
    </div>
  );
};

export default CopiableCodeBox;
