import LinkTo from "../../../../components/common/link-to";
import styles from "./RoosterLogo.module.css";

const RoosterLogo = () => (
  <LinkTo to="/" className={styles.logoContainer}>
    <img src="/images/rooster-2.png" alt="telerooster" />
  </LinkTo>
);

export default React.memo(RoosterLogo);
