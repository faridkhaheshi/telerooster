import ToggleSwitch from "../../../../components/common/toggle-switch";
import styles from "./LightSwitch.module.css";

const LightSwitch = ({
  initiallyOn = false,
  left = 0,
  top = 0,
  onChange = () => ({}),
}) => {
  return (
    <div className={styles.lightSwitchContainer} style={{ left, top }}>
      <ToggleSwitch initiallyChecked={initiallyOn} onChange={onChange} />
      <div className={styles.lightSwitchText}>Light</div>
    </div>
  );
};

export default LightSwitch;
