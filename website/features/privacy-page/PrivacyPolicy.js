const PrivacyPolicy = () => (
  <>
    <p>
      We create tools that help creators use the Internet to make their work
      easier. To do this, we need to have some information about our users.
    </p>
    <p>
      Here we describe how we collect and use information about you when you use
      our services. If you are one of our customers, you should read this policy
      in conjunction with our{" "}
      <a href="/terms" target="_blank">
        Terms and Conditions
      </a>
      .
    </p>
    <h3>1. Who are we?</h3>
    <p>
      Telerooster is a product of Daily Say LLC. We are software developers. In
      this document all references to "Telerooster", "we", "us" or "our" should
      be interpreted accordingly and refer to the Daily Say LLC.
    </p>
    <h3>2. How do we collect your data?</h3>
    <p>
      We collect information about you when you fill in a signup or contact form
      on our website or send us an email. We may also receive your personal data
      from third parties, when you explicitly allow us to do so. For example
      when you choose to sign in with your Google® account, you are allowing
      Google® to share some of your information with us.
    </p>
    <p>
      We collect information by automated means. When you visit our website or
      read our marketing emails, we automatically collect information about you
      via cookies, web beacons and other similar technologies. These are small
      files associated with information that your web browser or our servers
      will save and return as part of your use of the website and the services
      for purposes such as saving your login session between visits, remembering
      your display preferences, tracking your use of the website, and for
      audience measurement purposes.
    </p>
    <h3>3. What data do we collect?</h3>
    <p>
      We collect three types of information about you: personal data, team data,
      and usage data.
    </p>
    <p>
      <strong>Personal data.</strong> This is information that lets us know who
      you are. This includes the information you provide us when registering to
      use the service (i.e. your name, your email address, other contact
      information you share with us, credit card information). Your login
      credentials are also personal data.
    </p>
    <p>
      <strong>Team data.</strong> When you add a channel to your team via
      Telerooster, we collect information about your team. This information is
      limited to what we need to send content to the channel or to charge you
      properly for the content you are receiving. It includes the number of
      members in your team and the name of your team. We also collect
      information about the name of other channels in your team in order to
      avoid creating a new channel with the same name.
    </p>
    <p>
      <strong>Usage data.</strong> This is information that does not let us
      determine your identity, but helps us provide better service to you. For
      example we collect information about the locations on our website that you
      have visited, or clicked on.
    </p>
    <h3>4. How do we use your data?</h3>
    <p>
      If you are a visitor to our website, we use your personal and usage data
      to engage with you in order to suggest the best offer to you.
    </p>
    <p>
      When you are one of our customers, we use the information we collect about
      you to provide the service to you. As part of that purpose, we use your
      data:
    </p>
    <ol>
      <li>to create and maintain your account, and to control access to it;</li>
      <li>
        to provide our services and facilitate performance, including
        verifications relating to you;
      </li>
      <li>
        for billing and collection purposes, if you have subscribed to one of
        our paid plans;
      </li>
      <li>
        for the investigation and prevention of fraud and breaches of the Terms
        of service;
      </li>
      <li>to enable third parties to provide services to us;</li>
      <li>to enable our content creators to provide service to you;</li>
      <li>to comply with applicable laws to which we are subject.</li>
    </ol>
    <p>
      We may use your usage data to enhance the services, for instance through
      web analytics for troubleshooting. We may also use aggregated or
      depersonalized information to promote our services, such as by citing
      usage statistics.
    </p>
    <h3>
      5. What are our purposes and legal basis for collecting your personal
      data?
    </h3>
    <p>
      We collect your personal data because we need it to perform a contract we
      have signed with you or because you have taken steps to enter into a
      contract with us (for instance, when you fill in a contact form to request
      information about our services or when you sign up for an account).
      Otherwise, we collect personal data based on your consent for that
      specific purpose.
    </p>
    <h3>6. With whom do we share personal or team data?</h3>
    <p>
      Except for the limited circumstances we describe here or in an applicable
      agreement or our Terms of Service, we do not share your data with third
      parties. When we need to provide your data to third parties, we will only
      share it to the extent necessary to provide you with our services, and we
      ensure that we have in place data protection requirements with these third
      parties (including standard contractual clauses as well as the requisite
      technical and organizational measures).
    </p>
    <p>
      We may also share your data as required or permitted by law and as to
      optimally provide our services through third-party providers as described
      below.
    </p>
    <p>
      <em>Hosting Services:</em> We host the website and operate the platform
      using third parties, including AWS®. Your panel will be hosted from their
      data centers throughout the United States or Europe.
    </p>
    <p>
      <em>Payment Providers:</em> We use Stripe® to process subscription
      payments, and therefore provide them with the data required to charge your
      credit card and maintain any payment mandate information as the law
      requires.
    </p>
    <p>
      <em>Website functionalities:</em> We may use third-party services either
      embedded into our website (such as Google® Analytics) or outside of it to
      enhance the function of the website and our services.
    </p>
    <p>
      While we provide these third parties with no more information than what is
      necessary to enable them to provide the services to us, any information
      that you provide these services providers independently is subject to
      their respective privacy policies and practices.
    </p>
    <p>
      In no case do we sell, share or rent out your contacts to third parties,
      nor use them for any purpose other than those set forth in this policy.
    </p>
    <p>
      In certain situations, we may be required to disclose personal data in
      response to lawful requests by public authorities or regulatory bodies,
      including to meet law enforcement requirements, in the case of a court
      order, a summons to appear in court or any other similar requisition from
      a government or the judiciary, or to establish or defend a legal
      application.
    </p>
    <p>
      Additionally, we will provide information to a third party in the event of
      any reorganization, merger, sale, joint venture, assignment, transfer or
      other disposition of all or any portion of our business, assets or stock
      (including in connection with any bankruptcy or similar proceedings).
    </p>
    <h3>7. Accessing, modifying, or deleting your personal data</h3>
    <p>
      We keep your personal data for as long as is necessary to provide our
      services to you (unless otherwise required by law).
    </p>
    <p>
      If you would like us to cease all of the described uses of your personal
      data, you may delete your account at any time from the Settings section of
      our Panel (
      <a href="https://panel.telerooster.com" target="_blank">
        https://panel.telerooster.com
      </a>
      ). This will delete your personal data from our records, and we will make
      no further use of it.
    </p>
    <p>
      To prevent the risk of unintentional loss of data, the removal of data
      takes place 30 days after the request is received. Your service will be
      deactivated immediately, but your data will remain in our systems for 30
      days after which the data will be removed completely from our operations.
      We may, however, retain copies of your data in backups for legal retention
      purposes and/or for our own legitimate business purposes.
    </p>
    <p>
      If you are unable to deactivate your account from the Panel, or you need a
      customized action regarding your personal data, you can send an email to{" "}
      <a href="mailto:support@dailysay.net">support@dailysay.net</a>. We will
      respond within 7 working days with procedures for accessing, modifying, or
      deleting your data that fits your particular need.
    </p>
    <h3>8. Security</h3>
    <p>
      The security and integrity of your personal information are very important
      to us. We follow industry-accepted standards to protect the personal
      information submitted to us, both during transmission and once it is
      received. We ensure the appropriate electronic, physical and managerial
      procedures are in place with a view to safeguarding and preserving all the
      data handled. Our infrastructure is located in top-tier data centers. Each
      of these locations adheres to strict physical and procedural controls
      which are frequently audited. Our applications are routinely scanned for
      vulnerabilities.
    </p>
    <h3>9. Changes</h3>
    <p>
      The information provided in this policy may be modified to address new
      issues or changes. If we make significant changes, we may notify you by
      other means (for instance, by email or with a banner on the website) prior
      to the change becoming effective. Any changes we make will take effect 30
      days after the update date noted above. If you object to the changes,
      email us at <a href="mailto:support@dailysay.net">support@dailysay.net</a>{" "}
      before the new effective date to delete your information from our records.
    </p>
  </>
);

// const PrivacyPolicy = () => (
//   <>
//     <p>
//       We create tools that help creators use the Internet to make their work
//       easier.
//     </p>
//     <p>
//       The purpose of this document is to explain the processes and working
//       practices that we are committed to in order to:
//     </p>
//     <ol>
//       <li>
//         design and create our products and services in a way that they do what
//         they must with the least amount of data possible,
//       </li>
//       <li>
//         tell our customers transparently what data is received and stored from
//         them,
//       </li>
//       <li>
//         use the latest and most up-to-date technologies to take care of our
//         customers' data,
//       </li>
//       <li>let our customers remove their data at any time they want.</li>
//     </ol>
//     <h2>Section 1: what types of data are used?</h2>
//     <p>User data can be categorized in three categories:</p>
//     <ol>
//       <li>
//         <strong>Service-specific data:</strong> This is the data that is needed
//         for our services to work properly.
//       </li>
//       <li>
//         <strong>General user data:</strong> This data is not necessarily
//         required for providing the service, but storing it makes it easier for
//         users to use our service. General data is usually stored on a user's
//         mobile or browser to make it easier to use our services later.
//       </li>
//       <li>
//         <strong>Usage Data and Statistics:</strong> This data is not necessarily
//         given by the user. We obtain it by processing how users use our service.
//       </li>
//     </ol>
//     <h2>Section 2: Collecting data</h2>
//     <p>
//       The required data may be available in various forms. But we always adhere
//       to the following rules for accessing this data:
//     </p>
//     <ol>
//       <li>
//         <strong>User-informed access:</strong> We only use data that we are
//         directly and explicitly authorized to use.
//       </li>
//       <li>
//         <strong>Access to the minimum amount of data possible:</strong> We
//         always design and develop our services in a way that can be implemented
//         with the least amount of data possible. This "necessity of access" must
//         be clear to the user as well.
//       </li>
//     </ol>
//     <p>
//       The method of collecting data for each of the three categories described
//       in section 1 is as follows:
//     </p>
//     <ol>
//       <li>
//         <strong>Collecting service-specific data:</strong> This data is always
//         received directly from the user. Once given by the user, this data is
//         always maintained solely on our servers. No copy of this data is stored
//         on user's device.
//       </li>
//       <li>
//         <strong>Collecting general user data:</strong> This data is given by the
//         user but is stored on user's device to be used again.
//       </li>
//       <li>
//         <strong>Collecting user usage data and statistics:</strong> This data is
//         collected to improve our service. We also store and process data about
//         how our websites and applications are used to detect abusive or
//         suspicious behaviors or to detect bugs that may occur in the software.
//       </li>
//     </ol>
//     <p>
//       In all cases where the information is directly received from the user, we
//       use secure protocols (SSL). These protocols assure us that user data
//       cannot be accessed by a third-party.
//     </p>
//     <h2>Section 3: Using the data</h2>
//     <p>
//       We only store data that is crucial for providing our service. In all
//       cases, the user who uses this data is our user, not us.
//     </p>
//     <p>
//       We never use type 1 and 2 of data ourselves. We use Usage Data and
//       Statistics (type 3) only as described. That is, to improve our services,
//       to detect abandoned user accounts, or to make our systems more secure.
//     </p>
//     <h2>Section 4: Data protection</h2>
//     <p>
//       All business-specific data is stored on systems that are fully under our
//       control. We use the latest technologies to protect this data and keep it
//       secure.
//     </p>
//     <p>
//       To review our services and improve them we may use services provided by
//       other companies. For these services we will always select reputable
//       partners. These services are only used to process the User Usage Data and
//       Statistics (type 3).
//     </p>
//     <h2>Section 5: Accessing or deleting user data</h2>
//     <p>
//       Users can deactivate their accounts from their management panel available
//       at{" "}
//       <a href="https://panel.telerooster.com" target="_blank">
//         https://panel.telerooster.com
//       </a>
//       . After deactivation, we will delete all data stored from the user on our
//       servers.
//     </p>
//     <h2>Section 6: Deactivating accounts</h2>
//     <p>
//       In the process of deactivating user accounts, we must consider the
//       possibility that the user may have made a mistake or may have realized
//       that he/she has vital information in the account.
//     </p>
//     <p>
//       To handle such cases, we provide a 30-day deadline after deactivation. In
//       this time the user can reactivate the account by contacting our support.
//       In this period the account is deactivated and all services are stopped,
//       but the data is still on our server. After this deadline, all data related
//       to the deactivated account will be removed from our system.
//     </p>
//     <p>
//       So when you choose to deactivate an account in one of our services, the
//       complete removal of your data from our system will take place after 30
//       days.
//     </p>
//     <h2>Section 7: Changes to this document</h2>
//     <p>
//       This document is one of the key documents of our business. Changes to this
//       document may occur over time. You can always find the latest version of
//       our privacy policy on our website (
//       <a href="https://telerooster.com/privacy">
//         https://telerooster.com/privacy
//       </a>
//       ). We will also notify our users in other ways, such as email or SMS, if
//       there is a fundamental change in our policies.
//     </p>
//     <h2>Section 8: Contacting us about privacy</h2>
//     <p>
//       Our users can use the following email address to contact us about their
//       privacy concerns:
//       <br />
//       <a href="mailto:support@dailysay.net">support@dailysay.net</a>
//     </p>
//   </>
// );

export default React.memo(PrivacyPolicy);
