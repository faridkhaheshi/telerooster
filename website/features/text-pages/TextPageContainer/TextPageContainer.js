import styles from "./TextPageContainer.module.css";

const TextPageContainer = ({ title, children }) => (
  <section className={styles.pageContainer}>
    <article className={`${styles.textContainer} text-column text-left`}>
      <h1 className="mb-5 text-center">{title}</h1>
      {children}
    </article>
  </section>
);

export default React.memo(TextPageContainer);
