export default {
  title: "3. Let your audience onboard their teams",
  subtitle: `
  Share your Slack channel's URL link with your audience.`,
};
