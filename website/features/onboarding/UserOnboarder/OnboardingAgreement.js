import { baseUrl, panelBaseUrl } from "../../../config";
import LinkTo from "../../../components/common/link-to";

const OnboardingAgreement = () => (
  <div className="mt-2 text-justify text-muted">
    <small>
      This service is enabled by{" "}
      <a href={baseUrl} target="_blank">
        telerooster.com
      </a>
      . By clicking on the button above you agree to the{" "}
      <LinkTo to="/terms" target="_blank">
        Terms and Conditions
      </LinkTo>{" "}
      and{" "}
      <LinkTo to="/privacy" target="_blank">
        Privacy Policy
      </LinkTo>
      .
    </small>
  </div>
);

export default React.memo(OnboardingAgreement);
