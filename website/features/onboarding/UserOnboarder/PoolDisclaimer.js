import { panelBaseUrl } from "../../../config";
import parsePlatform from "./parse-platform";

const PoolDisclaimer = ({ platform, pool }) => (
  <div className="mb-5">
    <h4 className="mb-5">
      Adding a new channel to your {parsePlatform(platform)}
    </h4>
    <p>
      Channel: <strong>{pool.title}</strong>
      <br />
      Creator: <strong>{pool.creator.display_name}</strong>
    </p>
    <p>
      This will add a new channel to your {parsePlatform(platform)} team. You
      can always delete a channel to stop receiving content. You can also manage
      your subscriptions in your panel{" "}
      <a href={panelBaseUrl} target="_blank">
        here
      </a>
      .
    </p>
  </div>
);

export default React.memo(PoolDisclaimer);
