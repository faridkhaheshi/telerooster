import { useRouter } from "next/router";
import WaitingSpinner from "../../../components/common/waiting-spinner";
import { useAuth } from "../../../contexts/auth";
import ConfirmDialog from "./ConfirmDialog";

const UserOnboarder = ({ pool, platform }) => {
  const router = useRouter();
  const { isReadyForPlatform, authFinished } = useAuth();
  const [showConfirmDialog, setShowConfirmDialog] = React.useState(false);

  React.useEffect(() => {
    if (!router || !authFinished) return;
    if (isReadyForPlatform(platform)) {
      router.push(`/pools/${pool.id}/subscribe?platform=${platform}`);
    } else {
      setShowConfirmDialog(true);
    }
  }, [
    platform,
    isReadyForPlatform,
    setShowConfirmDialog,
    pool.id,
    authFinished,
    router,
  ]);

  if (showConfirmDialog)
    return <ConfirmDialog pool={pool} platform={platform} />;

  return <WaitingSpinner color="white" />;
};

export default UserOnboarder;
