const parsePlatform = (platform) =>
  platform === "slack"
    ? "Slack"
    : platform === "microsoft"
    ? "Microsoft Teams"
    : "";

export default parsePlatform;
