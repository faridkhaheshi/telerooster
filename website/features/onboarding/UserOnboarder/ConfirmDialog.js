import CustomButton from "../../../components/common/custom-button";
import { baseUrl } from "../../../config";

import { useAuth } from "../../../contexts/auth";
import RoosterLogo from "../../embed-page/common/RoosterLogo";
import OnboardingAgreement from "./OnboardingAgreement";
import parsePlatform from "./parse-platform";
import PoolDisclaimer from "./PoolDisclaimer";

const MAX_CHANNEL_TITLE_LENGTH = 20;

const ConfirmDialog = ({ pool, platform }) => {
  const { getAuthPageForPlatform } = useAuth();
  return (
    <>
      <RoosterLogo />
      <section className="centered-form mt-5 d-flex flex-column justify-content-between">
        <PoolDisclaimer pool={pool} platform={platform} />
        <div>
          <CustomButton
            block
            link
            href={`${getAuthPageForPlatform(platform)}?ref=${encodeURIComponent(
              `${baseUrl}/pools/${pool.id}/subscribe?platform=${platform}`
            )}`}
          >
            Add "{polishChannelName(pool.title)}" to {parsePlatform(platform)}
          </CustomButton>
          <OnboardingAgreement />
        </div>
      </section>
    </>
  );
};

export default React.memo(ConfirmDialog);

function polishChannelName(channelName) {
  if (channelName.length < MAX_CHANNEL_TITLE_LENGTH) return channelName;
  return channelName.slice(0, MAX_CHANNEL_TITLE_LENGTH) + "...";
}
