const TermsAndConditions = () => (
  <>
    <p>
      The following terms and conditions ("Terms of Service") apply to the
      Services to be delivered by Daily Say LLC ("Telerooster" or "we", "us") to
      the customer ordering the Services ("Customer" or "you").
    </p>
    <p>
      PLEASE CAREFULLY READ THESE TERMS. BY SUBSCRIBING TO OUR SERVICES, YOU
      AGREE THAT YOU HAVE READ AND AGREE, WITHOUT RESERVATION, TO BE BOUND BY
      THE LATEST VERSION OF THIS AGREEMENT.
    </p>
    <p>
      The Services are exclusively reserved for professional use and are not
      available to minors under the age of 18.
    </p>
    <h4>1. DEFINED TERMS</h4>
    <p>The following words, when capitalized, have the meaning stated:</p>
    <p>
      "Affiliate" means any entity that is owned or that owns a party, or that
      is under its common control.
    </p>
    <p>
      "Agreement" means, collectively, these Terms and Conditions, and each of
      the other documents referenced in these Terms and Conditions.
    </p>
    <p>
      "Confidential Information" means non-public information disclosed by one
      party to the other in any form that (i) is designated as "Confidential";
      (ii) a reasonable person knows or reasonably should understand to be
      confidential; or (iii) includes either party's products, customers,
      marketing and promotions, know-how, or the negotiated terms of the
      Agreement; and which is not independently developed by the other party
      without reference to the other's Confidential Information or otherwise
      known to the other party on a non-confidential basis prior to disclosure.
    </p>
    <p>
      "Configuration Requirements" means those specifications as required to
      perform the Services, such as a required reference architecture,
      documentation or software version.
    </p>
    <p>
      "Control Panel" or "Panel" means the customer portal accessible to
      Customer containing, among other information, the pricing terms applicable
      to Customer’s use of the Services and users designated to receive support.
      The Panel is available to Customers at{" "}
      <a href="https://panel.telerooster/com" target="_blank">
        https://panel.telerooster/com
      </a>
      .
    </p>
    <p>
      "Deliverables" means the tangible or intangible materials that are
      prepared for your use in the course of performing the Services and that
      are specifically identified in a Service Order as Deliverables and
      described therein.
    </p>
    <p>
      "Intellectual Property" means patents, copyrights, trademarks, trade
      secrets, software and source code, specifications and ancillary
      documentation and any other proprietary intellectual property rights.
    </p>
    <p>
      "Personal Data" means any: (i) personally identifiable information or
      information that is referred to as personal data (including sensitive
      personal data), PII (or other like term) under applicable data protection
      or privacy law and includes information that by itself or combined with
      other information can be used to identify a person, (ii) trade secrets,
      (iii) financial records (iv) other sensitive, regulated, or confidential
      information.
    </p>
    <p>
      "Representatives" means a party’s respective service providers, officers,
      directors, employees, contractors, Affiliates, suppliers, agents.
    </p>
    <p>
      "Services" means the services identified in a specific Service Order.
      Services that are provided on an on-going basis over a defined term are
      referred to as "Recurring Service" and Services that are provided on a
      one-off basis are referred to as "One Time Services".
    </p>
    <p>
      "SLA" means any provision providing a specified credit remedy for an
      identified failure to deliver or provide the Services to the identified
      standard.
    </p>
    <p>
      "Service Order" means the document which describes the Services you are
      purchasing, including any online order, process, or tool through which you
      request or provision Services.
    </p>
    <h4>2. SERVICES AND SUPPORT</h4>
    <p>
      2.1 General. Telerooster will provide the Services in accordance with the
      Agreement and all applicable laws. Telerooster shall have no obligation to
      provide Services for Customer Configurations that do not meet the
      Configuration Requirements. Telerooster will provide support only to those
      individuals designated in the Panel.
    </p>
    <p>
      2.2 Service Level Agreement. Telerooster guarantees that the Services
      (meaning any or all of the API, SMTP and Outbound Delivery services listed
      on our status page) will be available 99% of the time in any given monthly
      billing period, excluding maintenance. You are entitled to a credit of 5%
      of your given monthly fee for the Services for every 30 minutes of
      Services unavailability (after the first 1%) in a given monthly period.
    </p>
    <p>
      2.3 Credit Limitations. You are not entitled to any credit if you are in
      breach of the terms governing your use of the Services until you have
      cured the breach. You are not entitled to any credit if downtime would not
      have occurred but for your breach of these Terms of Service. You are not
      entitled to any credit for downtime or outages resulting from denial of
      service attacks, virus activity, hacking attempts, or any other
      circumstances that are not within our control. In addition, to receive a
      credit, you must make the request by sending an email to{" "}
      <a href="mailto:support@dailysay.net">support@dailysay.net</a> within
      thirty (30) days following the end of the downtime. You must show that
      your use of the Services was adversely affected in some way as a result of
      the downtime to be eligible for the credit. Notwithstanding anything in
      these Terms of Service to the contrary, the maximum total credit for the
      monthly billing period, including all guaranties, shall not exceed 100% of
      the fees for that billing period. Credits that would be available but for
      this limitation will not be carried forward to future billing periods.
    </p>
    <p>
      2.4 Delivery of Messages. Telerooster will use commercially reasonable
      efforts to deliver your messages in business messengers, but cannot
      guarantee delivery. Policies of recipient services may prevent successful
      delivery of your messages. Furthermore, You acknowledge that Telerooster
      does not control the transfer of Data via the internet, and cannot be held
      responsible for delays or delivery problems arising from internet or other
      outside connection issues.
    </p>
    <p>
      2.5 Unsupported & Test Services. Telerooster may designate Services as
      "non-standard", "reasonable endeavors" or "best efforts", or with like
      designation (collectively "Unsupported Services"). Telerooster makes no
      representation or warranty with respect to Unsupported Services except
      that it will use good faith efforts as may be expected of technicians
      having generalized knowledge and training in information technology
      systems. Telerooster has no guarantee of results in this regard.
      Telerooster shall not be liable to you for any loss or damage arising from
      the provision of Unsupported Services and SLAs shall not apply to
      Unsupported Services, or any other aspect of the Customer Configuration
      that is adversely affected by Unsupported Services.
    </p>
    <p>
      2.6 General Disclaimers. Telerooster makes no commitment to provide any
      services other than the Services stated in the Service Order. Telerooster
      is not responsible to you or any third party for unauthorized access to
      your Customer Data or for unauthorized use of the Services that is not
      solely caused by Telerooster’s failure to meet its security obligations in
      Section 4 (Security and Data Privacy). Telerooster and its Representatives
      disclaim any and all warranties not expressly stated in the Agreement to
      the maximum extent permitted by law including implied warranties such as
      merchantability, satisfactory quality, fitness for a particular purpose
      and non-infringement. Telerooster expressly reserves the right, at any
      time during the term of the Agreement, to adapt, arrange and/or modify any
      of the components granting access and use rights to the Service and the
      associated documentation, provided that the maintenance and support
      commitments are complied with for these operations. Similarly, Telerooster
      may, at any time, discontinue providing a platform deemed undesirable
      and/or obsolete and migrate services to a new infrastructure; in which
      case, Telerooster will endeavor to inform you as early as possible.
    </p>
    <h4>3. CUSTOMER OBLIGATIONS</h4>
    <p>
      3.1 Generally. You may use the Services for commercial and professional
      purposes only and may not use the Services in any situation where failure
      or fault of the Services or the Customer Configuration could lead to death
      or serious bodily injury of any person or physical or environmental
      damage. You will enable Telerooster’s reasonable method for access to the
      Customer Configuration for the purpose of performing the Services and
      invoicing. You must cooperate with Telerooster’s reasonable investigation
      of outages, security problems, and any suspected breach of the Agreement.
      You are responsible for keeping your account permissions, billing, and
      other account information up to date. You agree that you are solely
      responsible for the suitability of the Services and your compliance with
      any applicable laws, including export laws and data privacy laws. You also
      agree to ensure that your own users comply with this Agreement.
    </p>
    <p>
      3.2 Documentation. You agree to comply with the Telerooster documentation
      found on the respective website and agree that Telerooster may establish
      new procedures for your use of the Services as it deems necessary for the
      optimal performance of the Services.
    </p>
    <p>
      3.3 Data Backup. Telerooster shall only back up data to the extent stated
      on a Service Order. It is your responsibility to ensure the integrity and
      security of Customer Data and to regularly backup and validate the
      integrity of backups of Customer Data on an environment separate from the
      Customer Configuration.
    </p>
    <p>
      3.4 Suspension of Services. Telerooster may suspend the Services without
      liability if: (i) Telerooster reasonably believes that the Services are
      being used in violation of the Agreement; (ii) you don’t cooperate with
      our reasonable investigation of any suspected violation of the Agreement;
      (iii) there is an attack on the Services or your Services are accessed or
      manipulated by a third party without your consent, (iv) Telerooster is
      required by law or by a regulatory or government body to suspend the
      Services, or (v) there is another event for which Telerooster reasonably
      believes that the suspension of the Services is necessary to protect the
      Telerooster network or our other customers.
    </p>
    <h4>4. SECURITY AND DATA PRIVACY</h4>
    <p>
      4.1 Generally. Telerooster shall provide the Services in accordance with
      the security and privacy practices set forth at{" "}
      <a href="/privacy" target="_blank">
        https://telerooster.com/privacy
      </a>{" "}
      and any additional security specifications identified in the Service Order
      or these Terms of Service. You must use reasonable security precautions in
      connection with your use of the Services, including appropriately securing
      and encrypting Personal Data stored on or transmitted using the Customer
      Configuration. Customer Data is, and at all times shall remain, your
      exclusive property. Telerooster will not use or disclose Customer Data
      except as materially required to perform the Services or as required by
      law.
    </p>
    <p>
      4.2 Content Privacy. You acknowledge and understand that the Services may
      include the transmission of unencrypted messages in plain text over the
      public internet. You are responsible for encrypting any Personal Data you
      use in conjunction with the Services. Messages sent using the Services may
      be unsecured, may be intercepted by other users of the public internet,
      and may be stored and disclosed by third parties (such as a recipient’s
      messenger). Although Services include support for TLS, the content may be
      transmitted even if the recipient does not also support TLS, resulting in
      an unencrypted transmission.
    </p>
    <p>
      4.2 Data Protection Addendum. In the course of providing the Services
      under this Agreement, Telerooster may process certain Personal Data on
      your behalf.
    </p>
    <h4>5. INTELLECTUAL PROPERTY</h4>
    <p>
      5.1 Pre-Existing. Each party shall retain exclusive ownership of
      Intellectual Property created, authored, or invented by it prior to the
      commencement of the Services. If you provide Telerooster with your
      pre-existing Intellectual Property ("Customer IP"), then you hereby grant
      to Telerooster, during the term of the applicable Service Order, a
      limited, worldwide, non-transferable, royalty-free, right and license
      (with right of sub-license where required to perform the Services) to use
      the Customer IP solely for the purpose of providing the Services. You
      represent and warrant that you have all rights in the Customer IP
      necessary to grant this license, and that Telerooster’s use of such
      Customer IP shall not infringe on the Intellectual Property rights of any
      third party.
    </p>
    <p>
      5.2 Created by Telerooster. Unless otherwise specifically stated in the
      applicable Service Order, and excluding any Customer IP, Telerooster shall
      own all Intellectual Property created as part of providing the Services or
      contained in the Deliverables. Unless otherwise specifically stated in the
      Agreement, and subject to your payment in full for the applicable
      Services, Telerooster grants to you a limited, non-exclusive,
      non-transferable, royalty-free right and license (without the right to
      sublicense) to use any Deliverables, and during the term of the Service
      Order any Intellectual Property (excluding any Third Party Software and
      any Open Source Software), provided to you by Telerooster as part of the
      Services for your internal use as necessary for you to enjoy the benefit
      of the Services.
    </p>
    <p>
      5.3 Open Source. In the event Telerooster distributes any open source
      software to you as part of the Services (for example Linux, OpenStack, and
      software licensed under the Apache, GPL, MIT or other open source
      licenses, collectively "Open Source Software") then such Open Source
      Software is subject to the terms of the applicable open source license. To
      the extent there is a conflict with these Terms of Service, the terms of
      the applicable open source license shall control.
    </p>
    <p>
      5.4 Third-Party Software. Telerooster may provide third-party software for
      your use as part of the Services or to assist in our delivery of the
      Services ("Third-Party Software"). Unless otherwise permitted by the terms
      of the applicable license you may not (i) assign, grant or transfer any
      interest in the Third-Party Software to another individual or entity, (ii)
      reverse engineer, decompile, copy or modify the Third-Party Software,
      (iii) modify or obscure any copyright, trademark or other proprietary
      rights notices that are contained in or on the Third-Party Software, or
      (iv) exercise any of the reserved Intellectual Property rights provided
      under the laws governing this Agreement. You may only use Third-Party
      Software provided for your use as part of the Services (identified on the
      Service Order) on the Customer Configuration on which it was originally
      installed, subject to any additional restrictions identified in these
      Terms of Service or Service Order. You are prohibited from using
      Third-Party Software that Telerooster installs in order to assist our
      delivery of the Services. Upon termination of the Service Order, you will
      permit the removal of the Third-Party Software. Telerooster makes no
      representation or warranty regarding Third-Party Software except that
      Telerooster has the right to use or provide the Third-Party Software and
      that Telerooster is in material compliance with the applicable license.
    </p>
    <p>
      5.5 Infringement. If the delivery of the Services infringes the
      intellectual property rights of a third party and Telerooster determines
      that it is not reasonably or commercially practicable to obtain the right
      to use the infringing element, or modify the Services or Deliverable such
      that they do not infringe, then Telerooster may terminate the Service
      Order on ninety days’ notice and will not have any liability on account of
      such termination except to refund amounts paid for unused Services
      (prorated as to portions of Deliverables deemed infringing).
    </p>
    <h4>6. FEES</h4>
    <p>
      6.1 Fees. You agree to pay the fees for the Services based on the rates
      and charges set forth in your Panel or Service Order, as applicable. From
      time to time, Telerooster may impose additional fees or surcharges on
      certain Services provided to you, which such additional fees and
      surcharges shall in each case be communicated to you through your Control
      Panel or an update to your Service Order, as applicable. Fees are due
      within thirty days from the invoice date. If you have arranged for payment
      by credit card, Telerooster may charge your card or account on or after
      the invoice date. If your undisputed payment is overdue for more than
      thirty days, Telerooster may suspend the Services and any other services
      you receive from Telerooster on written notice. Telerooster shall
      undertake collection efforts prior to any suspension. Invoices that are
      not disputed within one hundred and twenty days of the invoice date are
      conclusively deemed accurate. Fees must be paid in the currency identified
      in your Panel or Service Order, as applicable. You are solely responsible
      for all wire transfers and other bank fees associated with the delivery of
      payments to Telerooster. Telerooster may charge interest on overdue
      amounts at the greater of 1.5% per month or the maximum legal rate, and
      may charge you for any cost or expense arising out of our collection
      efforts.
    </p>
    <p>
      6.2 Fee Increases. Telerooster may revise the pricing terms that apply to
      your use of the Services at any time by providing you with notice pursuant
      to Section 11 of these Terms of Service. With respect to Customers that
      are not party to a then-effective Service Order, such rate changes will be
      effective immediately upon the posting of an update to your Panel. With
      respect to Customers that are party to a then-effective Service Order, the
      revised rates, charges and fees assessed to you for Services under such
      Service Order will not become effective until the commencement of the next
      renewal term of such Service Order and in any event may not increase by
      more than five percent (5%) (as compared to the rates and charges in
      effect for the immediately preceding term) effective upon the commencement
      of each renewal term (and the Service Order will be deemed amended to
      reflect such increased rates and charges). If at any time during either an
      initial or renewal term of a Service Order a third party license provider
      directly or indirectly increases the fee they charge Telerooster for your
      use of Third-Party Software, Telerooster may increase your fees by the
      same percentage amount on sixty (60) days’ advance written notice.
    </p>
    <p>
      6.3 Taxes. All amounts due to Telerooster under the Agreement are
      exclusive of any value-added, goods and services, sales, use, and like
      taxes, (collectively, "Tax"). You must pay Telerooster the Tax that is due
      or provide Telerooster with satisfactory evidence of your exemption from
      the Tax in advance of invoicing. You must provide Telerooster with
      accurate and adequate documentation sufficient to permit Telerooster to
      determine if any Tax is due. All payments to Telerooster shall be made
      without any withholding or deduction for any taxes except for withholding
      (or similar) taxes imposed on income that may be attributable to
      Telerooster in connection with its provision of the Services that you are
      legally required to withhold and remit to the applicable governmental or
      taxing authority ("Local Withholding Taxes"). You agree to timely provide
      Telerooster with accurate factual information and documentation of your
      payment of any such Local Withholding Taxes. Telerooster shall remit such
      cost to you in the form of a credit on your outstanding account balance
      following receipt of sufficient evidence of payment of any such Local
      Withholding Taxes.
    </p>
    <p>
      6.4 Reimbursement for Expenses. Unless otherwise agreed in the Service
      Order or otherwise provided for in the Control Panel, if any of the
      Services are performed at your premises you agree to reimburse Telerooster
      for the actual substantiated out-of-pocket expenses of its
      Representatives.
    </p>
    <h4>7. TERM & TERMINATION</h4>
    <p>
      7.1 Term. The Agreement shall continue until terminated in accordance with
      its terms or the termination of the final Service Order, whichever is the
      later. Unless otherwise stated in the applicable Service Order, each
      Service Order has an initial 30-day term and shall automatically renew on
      initial term expiry on a rolling thirty-day basis unless either party
      provides the other with written notice of non-renewal at least thirty days
      prior to the expiration of the then-current term.
    </p>
    <p>
      7.2 Termination for Convenience. For Recurring Services, unless otherwise
      stated in the Agreement, you may terminate all or part of any Service
      Order for convenience at any time by giving Telerooster at least ninety
      (90) days advance written notice; subject to an early termination fee
      equal to the monthly recurring fee times the number of months remaining in
      the then-current term of the Service Order for the Services that have been
      terminated.
    </p>
    <p>
      7.3 Termination for Cause. Either party may terminate the Agreement or the
      affected Service Order(s) for cause on written notice if the other party
      materially breaches the Agreement and, where the breach is remediable,
      does not remedy the breach within forty-five (45) days of the
      non-breaching party’s written notice describing the breach.
    </p>
    <p>
      7.3.1 If following suspension of your Services for non-payment, your
      payment of any invoiced undisputed amount remains overdue for a further
      ten (10) days, Telerooster may terminate the Agreement or the applicable
      Service Order(s) for breach on written notice.
    </p>
    <p>
      7.3.2 Either party may terminate the Agreement and the Service Order(s) on
      written notice if the other enters into compulsory or voluntary
      liquidation, or ceases for any reason to carry on business, or takes or
      suffers any similar action which the other party reasonably believes means
      that it may be unable to pay its debts. Notwithstanding anything to the
      contrary in the Agreement, the fees for the Services shall become due
      immediately on such an occurrence.
    </p>
    <h4>8. CONFIDENTIAL INFORMATION</h4>
    <p>
      8.1 Generally. Each party agrees not to use the other’s Confidential
      Information except in connection with the performance or use of the
      Services, the exercise of its legal rights under this Agreement, or as
      required by law, and will use reasonable care to protect Confidential
      Information from unauthorized disclosure. Each party agrees not to
      disclose the other’s Confidential Information to any third party except:
      (i) to its Representatives, provided that such Representatives agree to
      confidentiality measures that are at least as stringent as those stated in
      these Terms of Service; (ii) as required by law; or (iii) in response to a
      subpoena or court order or other compulsory legal processes, provided that
      the party subject to such process shall give the other written notice of
      at least seven days prior to disclosing Confidential Information unless
      the law forbids such notice.
    </p>
    <p>
      8.2 Routing Data. Your messages and other items sent or received via the
      Service will include information that is created by the systems and
      networks that are used to create and transmit the message including
      information such as server hostnames, IP addresses, timestamps, mail queue
      file identifiers, and spam filtering information ("message routing data").
      You agree that Telerooster may view and use the message routing data for
      our general business purposes, including maintaining and improving
      security, improving our services, and developing products. In addition,
      you agree that Telerooster may disclose message routing data to third
      parties in aggregate statistical form, provided that Telerooster does not
      include any information that could be used to identify you.
    </p>
    <p>
      8.3 Usage Data. Telerooster collects and stores information related to
      your use of the Services, such as the use of the Website, and APIs. You
      agree that Telerooster may use this information for our general business
      purposes and may disclose the information to third parties in aggregate
      statistical form, provided that Telerooster does not include any
      information that could be used to identify you.
    </p>
    <h4>9. LIMITATIONS ON DAMAGES</h4>
    <p>
      9.1 Direct Damages. Notwithstanding anything in the Agreement to the
      contrary, except for liability arising from death or personal injury
      caused by negligence, willful misconduct, fraudulent misrepresentation or
      any other loss or damages for which such limitation is expressly
      prohibited by applicable law, the maximum aggregate monetary liability of
      Telerooster and any of its Representatives in connection with the Services
      or the Agreement under any theory of law shall not exceed the total amount
      paid for the Services that are the subject of the claim in the twelve
      months immediately preceding the event(s) that gave rise to the claim.
    </p>
    <p>
      9.2 Indirect Damages. Neither party (nor any of our Affiliates or
      Representatives) is liable to the other for any indirect, special,
      incidental, exemplary or consequential loss or damages of any kind.
      Neither of us is liable for any loss that could have been avoided by the
      damaged party’s use of reasonable diligence, even if the party responsible
      for the damages has been advised or should be aware of the possibility of
      such damages. In no event shall either of us be liable to the other for
      any punitive damages or for any loss of profits, data, revenue, business
      opportunities, customers, contracts, goodwill or reputation.
    </p>
    <p>
      9.3 SLA Credits. The credits stated in any applicable SLA are your sole
      and exclusive remedy for Telerooster’s failure to meet those guarantees
      for which credits are provided. The maximum total credit(s) for failure to
      meet any applicable SLA for any calendar month shall not exceed one
      hundred percent of the then-current monthly recurring fee for the
      Services. Credits that would be available but for this limitation will not
      be carried forward to future months. You are not entitled to a credit if
      you are in breach of the Agreement at the time of the occurrence of the
      event giving rise to the credit until you have remedied the breach. No
      credit will be due if the credit would not have accrued but for your
      action or omission.
    </p>
    <h4>10. INDEMNIFICATION</h4>
    <p>
      10.1 If Telerooster, its Affiliates, or any of its or their
      Representatives (the "Indemnitees") is faced with a legal claim by a third
      party arising out of your actual or alleged: willful misconduct, breach of
      applicable law, failure to meet the security obligations required by the
      Agreement, breach of your agreement with your customers or end users, , or
      your breach of Section 5 (Intellectual Property) then you will pay the
      cost of defending the claim (including reasonable legal fees) and any
      damages award, fine or other penalties that are imposed on the Indemnitees
      as a result of the claim. Your obligations under this Section include
      claims arising out of the acts or omissions of your employees or agents,
      any other person to whom you have given access to the Customer
      Configuration, and any person who gains access to the Customer
      Configuration as a result of your failure to use reasonable security
      precautions, even if the acts or omissions of such persons were not
      authorized by you.
    </p>
    <p>
      10.2 Telerooster will choose legal counsel to defend the claim, provided
      that the choice is reasonable and is communicated to you. You must comply
      with our reasonable requests for assistance and cooperation in the defense
      of the claim. Telerooster may not settle the claim without your consent,
      which may not be unreasonably withheld, delayed or conditioned. You must
      pay costs and expenses due under this Section as Telerooster incurs them.
    </p>
    <h4>11. NOTICES</h4>
    <p>
      Your routine communications to Telerooster regarding the Services should
      be sent to <a href="mailto:support@dailysay.net">support@dailysay.net</a>.
    </p>
    <p>
      Telerooster’s routine communications regarding the Services and legal
      notices will be posted on the Panel or sent by email or post to the
      individual(s) you designate as your contact(s) on your account. Notices
      are deemed received as of the time posted or delivered, or if that time
      does not fall on a business day, as of the beginning of the first business
      day following the time posted or delivered. For purposes of counting days
      for notice periods, the business day on which the notice is deemed
      received counts as the first day.
    </p>
    <h4>12. PUBLICITY, USE OF MARKS</h4>
    <p>
      Unless otherwise agreed in the Service Order, you agree that Telerooster
      may publicly disclose that it is providing Services to you and may use
      your name and logo to identify you in promotional materials, including
      press releases. You may not issue any press release or publicity regarding
      the Agreement, use the Telerooster name or logo or other identifying
      indicia, or publicly disclose that it is using the Services without
      Telerooster’s prior written consent.
    </p>
    <h4>13. ASSIGNMENT/SUBCONTRACTORS</h4>
    <p>
      Neither party may assign the Agreement or any Service Orders without the
      prior written consent of the other party except to an Affiliate or
      successor as part of a corporate reorganization or a sale of some or all
      of its business, provided the assigning party notifies the other party of
      such change of control. Telerooster may use its Affiliates or
      subcontractors to perform all or any part of the Services, but Telerooster
      remains responsible under the Agreement for work performed by its
      Affiliates and subcontractors to the same extent as if Telerooster
      performed the Services itself. You acknowledge and agree that Telerooster
      Affiliates and subcontractors may be based outside of the geographic
      jurisdiction in which you have chosen to store Customer Data and if
      legally required the parties will enter into good faith negotiations of
      such agreements as are necessary in order to legitimize the transfer of
      Customer Data.
    </p>
    <h4>14. FORCE MAJEURE</h4>
    <p>
      Neither party will be in violation of the Agreement if the failure to
      perform the obligation is due to an extraordinary event beyond its
      control, such as significant failure of a part of the power grid, Internet
      failure, natural disaster, war, riot, epidemic, strikes or labor action,
      terrorism ("Force Majeure”).
    </p>
    <p>
      If a Force Majeure event occurs, this Agreement shall be automatically
      suspended during the time the Force Majeure event continues, and neither
      Party shall be liable to the other for non-performance or delay in the
      performance of required obligation(s) due to the Force Majeure event,
      provided the non-performing Party gives prompt written notice of its
      inability to perform specified obligation(s) due to the event and uses
      reasonable efforts to resume its performance of its obligation(s) as soon
      as possible. Notwithstanding the foregoing, in no event shall a Force
      Majeure event excuse or delay a Party’s obligation(s) with respect to
      confidentiality or Intellectual Property Rights.
    </p>
    <h4>15. GOVERNING LAW</h4>
    <p>
      15.1 The Agreement is governed by the laws of the State of Wyoming, USA,
      exclusive of any choice of law principle that would require the
      application of the law of a different jurisdiction, and the laws of the
      United States of America including the Federal Arbitration Act, 9 U.S.C.
      §1, et seq. Any dispute or claim relating to or arising out of the
      Agreement shall be submitted to binding arbitration. The arbitration shall
      be conducted in the state and county (or equivalent geographic location)
      of the non-asserting party’s principal business offices in accordance with
      the Commercial Arbitration Rules of the American Arbitration Association
      ("AAA") in effect at the time the dispute or claim arose. The arbitration
      shall be conducted by one arbitrator from AAA or a comparable arbitration
      service. The arbitrator shall issue a reasoned award with findings of fact
      and conclusions of law. Either party may bring an action in any court of
      competent jurisdiction to compel arbitration under this Agreement, or to
      enforce an arbitration award. Neither a party nor an arbitrator may
      disclose the existence, content, or results of any arbitration hereunder
      without the prior written consent of both parties.
    </p>
    <p>
      Either party shall be permitted to appeal the final award under the AAA’s
      Optional Appellate Arbitration Rules in effect at the time the dispute or
      claim arose. Grounds for vacating the award shall include, in addition to
      those enumerated under the Federal Arbitration Act, that the arbitrator
      committed errors of law that are material and prejudicial. The appeal
      shall be determined upon the written documents submitted by the parties,
      with no oral argument. After the appellate rights described herein have
      been exercised or waived, the parties shall have no further right to
      challenge the award.
    </p>
    <p>
      15.2 Notwithstanding the exclusive jurisdiction provision above, you agree
      that Telerooster may seek to enforce any judgment anywhere in the world
      where you may have assets. No claim may be brought as a class or
      collective action, nor may you assert such a claim as a member of a class
      or collective action that is brought by another claimant. Each of us
      agrees that Telerooster will not bring a claim under the Agreement more
      than two years after the time that the claim accrued. The Agreement shall
      not be governed by the United Nations Convention on the International Sale
      of Goods.
    </p>
    <p>
      15.3 The prevailing party in any action or proceeding relating to this
      Agreement shall be entitled to recover reasonable legal fees and costs,
      including attorney’s fees.
    </p>
  </>
);

export default React.memo(TermsAndConditions);
