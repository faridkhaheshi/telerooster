export default {
  title: "2. Send your content",
  subtitle: "",
  content: (
    <>
      <p>
        <strong>Manually:</strong> Use teleRooster's easy-to-use editor for
        posting your content
      </p>
      <p>
        <strong>Automatically:</strong> link your newsletter to your Slack
        channel. Your emails will be posted in your channel automatically.
      </p>
    </>
  ),
};
