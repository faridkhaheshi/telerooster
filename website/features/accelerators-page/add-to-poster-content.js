export default {
  title: "3. Encourage startups to get onboard",
  subtitle: `
  Share your channel's URL link or attach its "Add to slack" button to your newsletter and blog.`,
};
