import LinkTo from "../../components/common/link-to";

export default {
  title: "Send content to startups in Slack®",
  content: (
    <ul className="mt-2 text-column">
      <li>
        <LinkTo to="#create-channel-form">Create</LinkTo> a channel that can be
        added to any team,
      </li>
      <li>
        Any team can add your channel by clicking on a URL link{" "}
        <LinkTo to="#featured-pools-vitrin">(see examples)</LinkTo>,
      </li>
    </ul>
  ),
};
