import LinkTo from "../../components/common/link-to";

export default {
  title: (
    <>
      <h5 className="mb-4">Slack is replacing email for companies </h5>
      Send corporate newsletters in Slack®
    </>
  ),
  content: (
    <ul className="mt-2 text-column">
      <li>
        <LinkTo to="#create-channel-form">Create</LinkTo> a channel that can be
        added to any team,
      </li>
      <li>
        Every team's admin can add your channel by clicking on a button or a URL
        link <LinkTo to="#featured-pools-vitrin">(see examples)</LinkTo>,
      </li>
      <li>
        You can{" "}
        <LinkTo to="#link-newsletter">easily link your email newsletter</LinkTo>
        . Your emails will be sent to your channel automatically.
      </li>
      <li>
        If the Slack team is not a corporate subscriber they won't receive the
        newsletter.
      </li>
    </ul>
  ),
};
