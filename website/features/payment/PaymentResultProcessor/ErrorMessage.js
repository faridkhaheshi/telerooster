import LinkTo from "./../../../components/common/link-to";

const ErrorMessage = ({
  message = (
    <>
      Something went wrong. We were unable to retrieve the result of your
      payment. Plase contact{" "}
      <LinkTo to="/support" target="_blank">
        support
      </LinkTo>
      .
    </>
  ),
}) => <p className="error text-column">{message}</p>;

export default React.memo(ErrorMessage);
