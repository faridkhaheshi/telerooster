import WaitingSpinner from "../../../components/common/waiting-spinner";
import useAsyncFormHandler from "../../../hooks/use-async-form-handler";
import ErrorMessage from "./ErrorMessage";
import SuccessMessage from "./SuccessMessage";

const PaymentResultProcessor = ({ sessionId, result = "cancel" }) => {
  const {
    handleSubmit: reportPaymentResult,
    errorMessage,
    result: serverResult,
  } = useAsyncFormHandler({
    url: "/api/payments",
    requestOptions: {
      method: "post",
    },
    parseForm: () => ({ sessionId, result }),
    onSuccess: (result) => {
      console.log(result);
    },
  });

  React.useEffect(() => {
    if (result === "cancel") {
      reportPaymentResult();
    }
  }, []);

  if (!sessionId || errorMessage)
    return <ErrorMessage message={errorMessage || undefined} />;

  if (serverResult || result === "success")
    return <SuccessMessage result={result} />;

  return <WaitingSpinner color="white" />;
};

export default PaymentResultProcessor;
