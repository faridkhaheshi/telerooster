import LinkTo from "../../../components/common/link-to";
import { panelBaseUrl } from "../../../config";

const SuccessMessage = ({ result = "success" }) => {
  if (result === "success")
    return (
      <div className="centered-form d-flex flex-column justify-content-between">
        <div>
          <h1>Payment successful</h1>
          <p>You have successfully upgraded your account.</p>
        </div>
        <p className="light-text">
          Return to the <LinkTo to={`${panelBaseUrl}/billings`}>panel</LinkTo>{" "}
          to check your billing status.
        </p>
      </div>
    );
  return (
    <div className="centered-form d-flex flex-column justify-content-between">
      <div>
        <h1>Payment Canceled</h1>
      </div>
      <p className="light-text">
        Return to the <LinkTo to={`${panelBaseUrl}/billings`}>panel</LinkTo> to
        check your billing status.
      </p>
    </div>
  );
};

export default React.memo(SuccessMessage);
