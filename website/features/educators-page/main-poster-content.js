import LinkTo from "../../components/common/link-to";

export default {
  title: "Send your content to teams in Slack®",
  content: (
    <ul className="mt-2 text-column">
      <li>
        <LinkTo to="#create-channel-form">Create</LinkTo> a channel that can be
        added to any team,
      </li>
      <li>
        Any team can add your channel by clicking on a button or a URL link{" "}
        <LinkTo to="#featured-pools-vitrin">(see examples)</LinkTo>,
      </li>
      <li>
        Your audience can subscribe to your channel for free or you can charge
        companies for having access to your channel.
      </li>
    </ul>
  ),
};
