import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";

import styles from "./ToggleButton.module.css";

const ToggleButton = ({ className, ...props }) => (
  <button
    color="link"
    {...props}
    className={`${className} ${styles.toggleButton}`}
  >
    <FontAwesomeIcon icon={faBars} />
  </button>
);

export default ToggleButton;
