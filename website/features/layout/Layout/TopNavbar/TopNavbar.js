import { Navbar, NavbarToggler, Nav, Collapse } from "reactstrap";
import Logo from "../Logo";
import LinkTo from "../../../../components/common/link-to";
import ToggleButton from "../ToggleButton";
import AuthAccess from "../../../../components/auth/auth-access";
import SidebarMenu from "../SidebarMenu";
import navLinks from "./navLinks";
import TopMenu from "../TopMenu";

import styles from "./TopNavbar.module.css";
import useRouterEvent from "../../../../hooks/use-router-event";

const TopNavbar = ({ backgroundColor = "var(--cream)", accentColor }) => {
  const [isOpen, setIsOpen] = React.useState(false);
  const toggle = React.useCallback(() => {
    setIsOpen((old) => !old);
  }, []);

  const closeSidebar = React.useCallback(() => setIsOpen(false), []);

  useRouterEvent("routeChangeStart", closeSidebar);

  return (
    <header>
      <Navbar light expand="lg" style={{ backgroundColor }}>
        <div className={styles.leftContainer}>
          <NavbarToggler tag={ToggleButton} onClick={toggle} />
          <Collapse navbar>
            <TopMenu navLinks={navLinks} />
          </Collapse>
          <SidebarMenu
            isOpen={isOpen}
            close={closeSidebar}
            navLinks={navLinks}
          />
        </div>
        <LinkTo to="/" className="navbar-brand">
          <Logo accentColor={accentColor} />
        </LinkTo>
        <div className={styles.rightContainer}>
          <AuthAccess />
        </div>
      </Navbar>
    </header>
  );
};

export default TopNavbar;
