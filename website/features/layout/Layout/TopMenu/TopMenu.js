import { Nav } from "reactstrap";
import NavbarLink from "../NavbarLink/NavbarLink";

const TopMenu = ({ navLinks }) => (
  <Nav className="mr-auto" navbar>
    {navLinks.map(({ text, href }) => (
      <NavbarLink key={`top-menu-${text}`} to={href} type="top">
        {text}
      </NavbarLink>
    ))}
  </Nav>
);

export default TopMenu;
