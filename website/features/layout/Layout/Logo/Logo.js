import styles from "./Logo.module.css";

const Logo = ({ hideText = false, accentColor = "var(--text-lighter)" }) => (
  <div className={styles.logoContainer}>
    <figure className={styles.logoImageContainer}>
      <img src="/images/rooster-2.png" alt="telerooster" />
    </figure>
    {!hideText && (
      <div className={`${styles.brandContainer} d-none d-md-block`}>
        <span style={{ color: accentColor }}>tele</span>Rooster
      </div>
    )}
  </div>
);

export default React.memo(Logo);
