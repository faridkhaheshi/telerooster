import { useRouter } from "next/router";
import { NavItem, NavLink } from "reactstrap";

import LinkTo from "../../../../components/common/link-to";

const NavbarLink = ({ to, type = "top", children }) => {
  const { asPath } = useRouter();

  return (
    <NavItem
      active={asPath === to}
      className={type === "top" ? `${type} px-3` : type}
    >
      <NavLink tag={LinkTo} to={to}>
        {children}
      </NavLink>
    </NavItem>
  );
};

export default NavbarLink;
