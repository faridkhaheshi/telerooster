import CustomButton from "../../../../components/common/custom-button";

import { panelBaseUrl } from "../../../../config";

import styles from "./SidebarMenu.module.css";

const SidebarFooter = () => (
  <div className={styles.sidebarFooter}>
    <CustomButton block external link href={panelBaseUrl}>
      Dashboard
    </CustomButton>
  </div>
);

export default React.memo(SidebarFooter);
