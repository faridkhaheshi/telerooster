import { Button } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes } from "@fortawesome/free-solid-svg-icons";

import styles from "./SidebarMenu.module.css";

const SidebarHeader = ({ close }) => (
  <div className={styles.sidebarHeader}>
    <Button className={styles.closeButton} onClick={close} color="link">
      <FontAwesomeIcon icon={faTimes} />
    </Button>
  </div>
);

export default React.memo(SidebarHeader);
