import { Nav } from "reactstrap";
import NavbarLink from "../NavbarLink/NavbarLink";
import SidebarHeader from "./SidebarHeader";

import useClickOutside from "../../../../hooks/use-click-outside";

import styles from "./SidebarMenu.module.css";
import SidebarFooter from "./SidebarFooter";

const SidebarMenu = ({ isOpen, close, navLinks, hideHome = false }) => {
  const sidebarRef = React.useRef();
  useClickOutside(sidebarRef, () => {
    if (isOpen) close();
  });
  return (
    <div
      id="side-nav"
      ref={sidebarRef}
      className={
        isOpen
          ? `${styles.openSidebar} ${styles.sidebarContainer}`
          : styles.sidebarContainer
      }
    >
      <SidebarHeader close={close} />
      <Nav className={styles.sidebarNav}>
        {!hideHome && (
          <NavbarLink to="/" type="side">
            home
          </NavbarLink>
        )}
        {navLinks.map(({ text, href }) => (
          <NavbarLink key={`side-menu-${text}`} to={href} type="side">
            {text}
          </NavbarLink>
        ))}
      </Nav>
      <SidebarFooter />
    </div>
  );
};

export default SidebarMenu;
