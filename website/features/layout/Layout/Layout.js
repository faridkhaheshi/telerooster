import Footer from "./Footer";
import TopNavbar from "./TopNavbar";

const Layout = ({ children, options = {} }) => {
  const { showNavbar, showFooter, navbarColor, logoAccent } = options;
  if (!showNavbar && !showFooter) return children;

  return (
    <>
      {showNavbar && (
        <TopNavbar backgroundColor={navbarColor} accentColor={logoAccent} />
      )}
      {children}
      {showFooter && <Footer />}
    </>
  );
};

export default Layout;
