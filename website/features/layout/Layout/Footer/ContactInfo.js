const ContactInfo = () => (
  <>
    <p>
      <strong>Street:</strong> 2207-140 Main St W
    </p>
    <p>
      <strong>City:</strong> Hamilton
    </p>
    <p>
      <strong>Province:</strong> Ontario
    </p>
    <p>
      <strong>Zip Code:</strong> L8P0B8
    </p>
    <p>
      <strong>Phone Number:</strong> +1 905-218-7009
    </p>
    {/* <p>
      <strong>Support:</strong>{" "}
      <a href="mailto:support@dailysay.net">support@dailysay.net</a>
    </p> */}
  </>
);

export default React.memo(ContactInfo);
