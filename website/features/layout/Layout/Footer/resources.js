export default [
  { text: "Create a channel", href: "/pools/new", key: "new-pool" },
  // { text: "Frequently asked questions", href: "/faq", key: "faq" },
  { text: "Pricing", href: "/pricing", key: "pricing" },
  { text: "Support", href: "/support", key: "support" },
  { text: "Blog", href: "#", key: "blog" },
  { text: "Terms and conditions", href: "/terms", key: "terms" },
  { text: "Privacy policy", href: "/privacy", key: "privacy" },
];
