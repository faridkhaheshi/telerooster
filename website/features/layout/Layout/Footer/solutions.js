export default [
  { text: "For newsletter creators", href: "/", key: "creators" },
  {
    text: "For LinkedIn influencers",
    href: "/influencers",
    key: "linkedIn-influencers",
  },
  {
    text: "For accelerators",
    href: "/accelerators",
    key: "accelerators",
  },
  {
    text: "For news industry",
    href: "/newspapers",
    key: "newspapers",
  },
  {
    text: "For corporate educators and trainers",
    href: "/educators",
    key: "educators",
  },
  {
    text: "For keynote speakers",
    href: "/speakers",
    key: "speakers",
  },
];
