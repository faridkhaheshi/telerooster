import SocialLink from "./SocialLink";
import socialAddresses from "./social-addresses";

const SocialLinks = () => (
  <div className="mt-4 d-flex justify-content-start">
    {socialAddresses.map(({ platform, href }) => (
      <SocialLink
        key={`footer-social-link-${platform}`}
        platform={platform}
        href={href}
      />
    ))}
  </div>
);

export default React.memo(SocialLinks);
