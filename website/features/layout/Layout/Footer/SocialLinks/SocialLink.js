import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTwitter,
  faFacebookF,
  faYoutube,
  faLinkedin,
  faSlack,
  faMicrosoft,
} from "@fortawesome/free-brands-svg-icons";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";
import { Button } from "reactstrap";

import styles from "./SocialLink.module.css";

const platformIcons = {
  twitter: faTwitter,
  facebook: faFacebookF,
  youtube: faYoutube,
  linkedin: faLinkedin,
  slack: faSlack,
  microsoft: faMicrosoft,
  email: faEnvelope,
};

const supportedPlatforms = Object.keys(platformIcons);

const SocialLink = ({ platform, href = "#" }) => {
  if (supportedPlatforms.indexOf(platform) === -1) return null;
  return (
    <Button
      color="link"
      className={styles.socialButton}
      href={href}
      target="_blank"
    >
      <FontAwesomeIcon icon={platformIcons[platform]} />
    </Button>
  );
};

export default React.memo(SocialLink);
