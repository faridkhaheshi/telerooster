export default [
  { platform: "twitter", href: "https://twitter.com/faridkhaheshi" },
  { platform: "facebook", href: "https://www.facebook.com/farid.khaheshi" },
  { platform: "email", href: "mailto:support@dailysay.net" },
];
