import { Container, Row } from "reactstrap";
import FooterColumn from "./FooterColumn";
import solutions from "./solutions";
import resources from "./resources";
import ContactInfo from "./ContactInfo";
import SocialLinks from "./SocialLinks";

import styles from "./Footer.module.css";

export const MAX_COLS = 3;
export const COL_WIDTH = Math.round(12 / MAX_COLS);

const Footer = () => (
  <footer className={`mt-auto ${styles.footerContainer}`}>
    <Container>
      <Row className={styles.columnsContainer}>
        <FooterColumn title="Solutions" links={solutions} />
        <FooterColumn title="Resources" links={resources} />
        <FooterColumn title="Contact">
          <ContactInfo />
          <SocialLinks />
        </FooterColumn>
      </Row>
    </Container>
    <small className={styles.copyRight}>
      © {Math.max(new Date().getFullYear(), 2021)} All Rights Reserved.
    </small>
  </footer>
);

export default React.memo(Footer);
