import { Col, Container, Row } from "reactstrap";
import LinkTo from "../../../../components/common/link-to";
import { COL_WIDTH } from "./Footer";

import styles from "./Footer.module.css";

const FooterColumn = ({ children, title, links }) => (
  <Col md={COL_WIDTH} className={styles.column}>
    <h5 className="mb-4">{title}</h5>
    {links ? (
      <ul>
        {links.map(({ text, href, key }) => (
          <li key={`footer-link-${key}`}>
            <LinkTo to={href}>{text}</LinkTo>
          </li>
        ))}
      </ul>
    ) : (
      children
    )}
  </Col>
);

export default React.memo(FooterColumn);
