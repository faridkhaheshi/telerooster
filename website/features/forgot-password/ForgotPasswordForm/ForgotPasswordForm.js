import { useRouter } from "next/router";
import AsyncForm from "../../../components/common/async-form";
import EmailInput from "../../social-login/common/EmailInput";
import ReCaptcha from "react-google-recaptcha";
import { google } from "./../../../config";

const ForgotPasswordForm = () => {
  const router = useRouter();
  const [captchaKey, setCaptchaKey] = React.useState(null);

  const moveForward = React.useCallback(
    ({ email }) => {
      if (email) {
        router.push({
          pathname: "/auth/email-sent",
          query: { email, type: "forgot" },
        });
      }
    },
    [router]
  );

  const prepareFormData = React.useCallback(
    (e) => {
      if (!captchaKey) throw new Error("Please check the Captcha box");
      const info = {
        email: e?.target["email"]?.value,
        captchaKey,
      };
      return info;
    },
    [captchaKey]
  );
  return (
    <section className="centered-form d-flex justify-content-between flex-column">
      <h1>Reset password</h1>
      <AsyncForm
        disabled={!captchaKey}
        method="POST"
        url="/api/auth/forgot"
        buttonText="Send me the email"
        extractFormData={prepareFormData}
        onSuccess={moveForward}
      >
        <p className="mb-3">
          Please enter your email address. We will email you a link to reset
          your password.
        </p>
        <EmailInput id="forgot-password-email-input" />
        <ReCaptcha sitekey={google.recaptchaSiteKey} onChange={setCaptchaKey} />
      </AsyncForm>
    </section>
  );
};

export default ForgotPasswordForm;
