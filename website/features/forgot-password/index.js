import ForgotPasswordForm from "./ForgotPasswordForm";
import ResetPasswordForm from "./ResetPasswordForm";

export { ForgotPasswordForm, ResetPasswordForm };
