import AsyncForm from "../../../components/common/async-form";
import { useAuth } from "../../../contexts/auth";
import useToast from "../../../hooks/use-toast";
import PasswordInput from "../../social-login/common/PasswordInput";

const ResetPasswordForm = () => {
  const { logout } = useAuth();
  const { success, danger } = useToast();
  const [pass1, setPass1] = React.useState("");
  const [pass2, setPass2] = React.useState("");
  const handleSuccess = React.useCallback(() => {
    success({ title: "Success", message: "Your password changed." });
    logout("/auth/login");
  }, [success]);
  const prepareReqBody = React.useCallback(() => {
    if (pass1 !== pass2) throw new Error("passwords don't match");
    return { password: pass1 };
  }, [pass1, pass2]);
  return (
    <section className="centered-form d-flex justify-content-between flex-column">
      <h1>Change password</h1>
      <AsyncForm
        disabled={pass1.length < 5 || pass1 !== pass2}
        method="PUT"
        url="/api/v1/password"
        buttonText="Set new password"
        extractFormData={prepareReqBody}
        onSuccess={handleSuccess}
        autoComplete="off"
      >
        <PasswordInput
          label="New password:"
          name="password"
          value={pass1}
          onChange={(e) => setPass1(e.target.value)}
          autoComplete="off"
        />
        <PasswordInput
          label="Repeat new password:"
          name="passwordConfirm"
          value={pass2}
          onChange={(e) => setPass2(e.target.value)}
          valid={pass1 === pass2 && pass1.length > 0}
          autoComplete="off"
        />
      </AsyncForm>
    </section>
  );
};

export default ResetPasswordForm;
