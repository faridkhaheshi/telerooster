import styles from "./SupportPoster.module.css";

const SupportPoster = ({ backgroundColor = "white" }) => (
  <section className="top-poster" style={{ backgroundColor }}>
    <h1 className="mb-5">Need Help? Have a suggestion?</h1>
    <div className="text-column">
      <p>
        You can always reach us via{" "}
        <a href="mailto:support@dailysay.net">support@dailysay.net</a> or send
        us a message on Twitter:
      </p>
      <p>
        Farid:{" "}
        <a href="https://twitter.com/faridkhaheshi" target="_blank">
          @faridkhaheshi
        </a>
      </p>
      <p>
        Zaeem:{" "}
        <a href="https://twitter.com/zaeem29" target="_blank">
          @zaeem29
        </a>
      </p>
    </div>
  </section>
);

export default SupportPoster;
