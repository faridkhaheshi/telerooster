import LinkTo from "../../../components/common/link-to";

const LoginLink = ({ refUrl }) => (
  <p className="text-right">
    Already have an account?{" "}
    <LinkTo
      to={
        refUrl ? `/auth/login?ref=${encodeURIComponent(refUrl)}` : "/auth/login"
      }
    >
      Log in
    </LinkTo>
  </p>
);

export default React.memo(LoginLink);
