import LinkTo from "../../../components/common/link-to";

const Agreement = () => (
  <small className="text-justify">
    By signing up you are creating an account in telerooster.com and you agree
    to{" "}
    <LinkTo to="/terms" target="_blank">
      Terms and Conditions
    </LinkTo>{" "}
    and{" "}
    <LinkTo to="/privacy" target="_blank">
      Privacy Policy.
    </LinkTo>
  </small>
);

export default React.memo(Agreement);
