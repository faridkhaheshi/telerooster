import { useRouter } from "next/router";

import AsyncForm from "../../../components/common/async-form";
import EmailInput from "../common/EmailInput";
import PasswordInput from "../common/PasswordInput";
import SocialLogin from "../SocialLogin";
import Agreement from "./Agreement";
import LoginLink from "./LoginLink";
import BackToHomeButton from "../common/BackToHomeButton";

const SignupForm = () => {
  const router = useRouter();
  const { query: { ref } = {} } = router;

  const prepareSignupData = React.useCallback(
    (e) => {
      const signupInfo = {
        email: e?.target["email"]?.value,
        password: e?.target["password"]?.value,
      };
      if (ref) signupInfo.ref = ref;
      return signupInfo;
    },
    [ref]
  );

  const moveForward = React.useCallback(
    ({ email }) => {
      if (email)
        router.push({
          pathname: "/auth/email-sent",
          query: { email, type: "signup" },
        });
    },
    [router]
  );

  return (
    <>
      <section className="centered-form d-flex flex-column justify-content-between">
        <div className="mb-3">
          <h1>Welcome</h1>
          <p>Start easy:</p>
          <SocialLogin refUrl={ref} />
          <hr />
          <p>or create an account using your email:</p>
          <AsyncForm
            url="/api/auth/signup"
            method="POST"
            buttonText="Sign Up"
            extractFormData={prepareSignupData}
            onSuccess={moveForward}
          >
            <EmailInput id="signup-email-input" />
            <PasswordInput id="signup-password-input" />
            <LoginLink refUrl={ref} />
          </AsyncForm>
        </div>
        <Agreement />
      </section>
      <BackToHomeButton />
    </>
  );
};

export default SignupForm;
