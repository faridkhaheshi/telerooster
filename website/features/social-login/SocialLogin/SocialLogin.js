import WaitingSpinner from "../../../components/common/waiting-spinner";
import { useAuth } from "../../../contexts/auth/auth-context";
import useAsyncFormHandler from "../../../hooks/use-async-form-handler";
import useToast from "../../../hooks/use-toast";
import useGoogleAuth from "../hooks/use-google-auth";

const SocialLogin = ({ refUrl }) => {
  const { danger } = useToast();
  const { login } = useAuth();
  const { GoogleLogin, loginButtonProps } = useGoogleAuth();
  const { handleSubmit, isProcessing } = useAsyncFormHandler({
    url: `/api/auth/google`,
    requestOptions: { method: "POST" },
    parseForm: ({ tokenId }) => ({ idToken: tokenId }),
    onSuccess: ({ token }) => login({ token, ref: refUrl }),
    onError: (errMsg) => danger({ title: "Sign in failed", message: errMsg }),
  });

  const handleFailure = React.useCallback(
    (error) => danger({ title: "Failed", message: error.error }),
    [danger]
  );

  return (
    <div className="d-flex justify-content-center my-4">
      {isProcessing ? (
        <WaitingSpinner color="var(--sky-blue)" />
      ) : (
        <GoogleLogin
          onSuccess={handleSubmit}
          onFailure={handleFailure}
          {...loginButtonProps}
        />
      )}
    </div>
  );
};

export default SocialLogin;
