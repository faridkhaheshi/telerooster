import { FormGroup, Label, Input } from "reactstrap";

const EmailInput = ({ id = "email-input", required = true }) => (
  <FormGroup>
    <Label for={id}>Email:</Label>
    <Input
      required={required}
      type="email"
      name="email"
      id={id}
      autoComplete="on"
    />
  </FormGroup>
);

export default EmailInput;
