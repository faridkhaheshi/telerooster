import { useRouter } from "next/router";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChevronLeft,
  faChevronRight,
} from "@fortawesome/free-solid-svg-icons";
import LinkTo from "../../../components/common/link-to";

const BackToHomeButton = ({
  text = "back",
  color,
  forward = false,
  className,
  to = "/",
}) => {
  const {
    query: { ref },
  } = useRouter();
  return (
    <LinkTo
      to={ref || to}
      className={className ? `${className} back-button` : "back-button"}
      style={color ? { color } : {}}
    >
      {forward ? (
        <>
          {text}
          <FontAwesomeIcon icon={faChevronRight} className="ml-2" />
        </>
      ) : (
        <>
          <FontAwesomeIcon icon={faChevronLeft} className="mr-2" />
          {text}
        </>
      )}
    </LinkTo>
  );
};

export default BackToHomeButton;
