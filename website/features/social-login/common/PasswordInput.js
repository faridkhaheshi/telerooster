import { FormGroup, Label, Input } from "reactstrap";

const PasswordInput = ({
  id = "password-input",
  label = "Password:",
  name = "password",
  required = true,
  autoComplete = "on",
  ...otherProps
}) => (
  <FormGroup>
    <Label for={id}>{label}</Label>
    <Input
      required={required}
      type="password"
      name={name}
      id={id}
      autoComplete={autoComplete}
      {...otherProps}
    />
  </FormGroup>
);

export default PasswordInput;
