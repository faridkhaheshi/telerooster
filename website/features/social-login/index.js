import SocialLogin from "./SocialLogin";
import SignupForm from "./SignupForm";

export { SocialLogin, SignupForm };
