import LinkTo from "../../../components/common/link-to";

const SignupLink = ({ refUrl }) => (
  <p className="text-right">
    Don't have an account?{" "}
    <LinkTo
      to={
        refUrl
          ? `/auth/signup?ref=${encodeURIComponent(refUrl)}`
          : "/auth/signup"
      }
    >
      Create an account
    </LinkTo>
  </p>
);

export default React.memo(SignupLink);
