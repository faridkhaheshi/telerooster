import { useRouter } from "next/router";
import AsyncForm from "../../../components/common/async-form";
import LinkTo from "../../../components/common/link-to";
import { useAuth } from "../../../contexts/auth/auth-context";
import BackToHomeButton from "../common/BackToHomeButton";
import EmailInput from "../common/EmailInput";
import PasswordInput from "../common/PasswordInput";
import SocialLogin from "../SocialLogin";
import SignupLink from "./SignupLink";

const LoginForm = () => {
  const router = useRouter();
  const { login } = useAuth();
  const { query: { ref } = {} } = router;

  const callLogin = React.useCallback(
    ({ token }) => {
      login({ token, ref });
    },
    [login, ref]
  );

  const prepareLoginData = React.useCallback(
    (e) => {
      const loginInfo = {
        email: e?.target["email"]?.value,
        password: e?.target["password"]?.value,
      };
      if (ref) loginInfo.ref = ref;
      return loginInfo;
    },
    [ref]
  );

  return (
    <>
      <section className="centered-form">
        <h1>Log In</h1>
        <SocialLogin refUrl={ref} />
        <hr />
        <p>or log in with your account:</p>
        <AsyncForm
          method="POST"
          url="/api/auth/login"
          buttonText="Log In"
          extractFormData={prepareLoginData}
          onSuccess={callLogin}
        >
          <EmailInput id="login-email-input" />
          <PasswordInput id="login-password-input" />
          <p className="text-right mt-4">
            <LinkTo to="/auth/forgot">Forgot Password?</LinkTo>
          </p>
          <SignupLink refUrl={ref} />
        </AsyncForm>
      </section>
      <BackToHomeButton />
    </>
  );
};

export default LoginForm;
