import React from "react";
import GoogleLogin, { useGoogleLogout } from "react-google-login";
import { google } from "./../../../config";

const useGoogleAuth = () => {
  const handleLogoutSuccess = React.useCallback(() => {
    console.log("logout successful...");
  }, []);
  const handleLogoutFailure = React.useCallback((error) => {
    console.log("logout failed...");
    console.log(error);
  }, []);

  const { signOut: signOutGoogle } = useGoogleLogout({
    onFailure: handleLogoutFailure,
    clientId: google.authClientId,
    scope: google.authScopes,
    onLogoutSuccess: handleLogoutSuccess,
  });

  return {
    GoogleLogin,
    signOutGoogle,
    loginButtonProps: {
      scopes: google.authScopes,
      clientId: google.authClientId,
      responseType: "id_token",
    },
  };
};

export default useGoogleAuth;
