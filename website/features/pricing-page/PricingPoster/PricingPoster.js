import styles from "./PricingPoster.module.css";

const PricingPoster = () => (
  <section className={styles.posterRow}>
    <h1 className="mb-5">We connect creators to teams</h1>
    <div className="text-column">
      <p>
        When you sign up or add Telerooster channels to your team you become a{" "}
        <strong>"Consumer"</strong>. In the <strong>"Consumer"</strong> plan,
        you can also create channels but you can't send more than 10 posts.
      </p>
      <p>
        You can upgrade to the <strong>"Creator"</strong> plan to continue
        posting to your own channels.
      </p>
      <p>
        If you are a creator and want to evaluate teleRooster, just sign up,
        create a channel and send a few messages to see how it works.
      </p>
    </div>
  </section>
);

export default React.memo(PricingPoster);
