import { panelBaseUrl } from "./../../../config";

export default [
  {
    key: "pricing-plan-standard",
    planName: "standard",
    cardColor: "var(--pork-pink)",
    title: "Consumer",
    subtitle:
      "For teams who want to add channels to their business messengers.",
    priceCents: 0,
    benefits: [
      <>
        Add any channel to your teams <br /> (for paid channels you should pay
        the subscription price separately)
      </>,
      "Send maximum 10 posts to evaluate the Creator plan",
    ],
    buttonText: "Sign up for free",
    href: "/auth/signup",
  },
  {
    key: "pricing-plan-creator",
    planName: "creator",
    cardColor: "#885A89",
    title: "Creator",
    subtitle:
      "For creators who want to create channels to engage with their audience in business messengers.",
    priceCents: 2999,
    benefits: [
      "Everything in the Consumer Plan",
      "100 posts per month",
      <>
        No usage fee for up to 2,000 subscribers, <br />
        $5 /month for every 1,000 subscribers above 2,000
      </>,
      <>
        Create up to 2 channels, <br /> $19 /month for every extra channel
      </>,
    ],
    buttonText: "Upgrade",
    href: `${panelBaseUrl}/billings`,
  },
];
