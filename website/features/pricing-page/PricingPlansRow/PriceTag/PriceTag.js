import styles from "./PriceTag.module.css";

const PriceTag = ({ priceCents }) => {
  const cents = priceCents === 0 ? "00" : `${priceCents % 100}`;
  return (
    <div className={styles.container}>
      <span className={styles.secondaryText}>Starting at</span>
      <div className={styles.priceContainer}>
        <span className={styles.dollarSign}>$</span>
        <span className={styles.dollars}>{Math.floor(priceCents / 100)}</span>
        <span className={styles.cents}>{cents}</span>
      </div>
      <span className={`${styles.secondaryText} mt-3`}>/month</span>
    </div>
  );
};

export default PriceTag;
