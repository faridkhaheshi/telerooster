import PriceTag from "./PriceTag";
import PricingPlanButton from "./PricingPlanButton";

import styles from "./PricingPlan.module.css";

const PricingPlan = ({
  cardColor = "var(--liver-red)",
  title = "card title",
  subtitle = "card subtitle",
  priceCents = 1000,
  benefits = [],
  buttonText = "buy",
  href = "/",
  planName = "standard",
}) => (
  <div className={styles.cardContainer} style={{ backgroundColor: cardColor }}>
    <h3 className={styles.cardTitle}>{title}</h3>
    <div className={styles.cardInfo}>
      <div className={styles.topInfo}>{subtitle}</div>
      <PriceTag priceCents={priceCents} />
      <div className={styles.bottomInfo}>
        <ul>
          {benefits.map((b, index) => (
            <li key={`benefit-${planName}-${index}`}>{b}</li>
          ))}
        </ul>
      </div>
    </div>
    <PricingPlanButton
      defaultText={buttonText}
      className={styles.actionButton}
      href={href}
      planName={planName}
    />
  </div>
);

export default PricingPlan;
