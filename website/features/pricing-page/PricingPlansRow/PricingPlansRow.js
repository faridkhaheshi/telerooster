import PricingPlan from "./PricingPlan";
import pricingPlans from "./pricing-plans";

import styles from "./PricingPlansRow.module.css";

const PricingPlansRow = () => {
  return (
    <section className="big-row align-items-center">
      <h1 className="mb-5">Pricing plans</h1>
      <div className="mt-5 d-flex flex-wrap align-items-center justify-content-center">
        {pricingPlans.map((plan) => (
          <PricingPlan key={plan.key} {...plan} />
        ))}
      </div>
    </section>
  );
};

export default React.memo(PricingPlansRow);
