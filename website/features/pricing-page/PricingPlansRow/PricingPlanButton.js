import { panelBaseUrl } from "./../../../config";
import LinkTo from "../../../components/common/link-to";
import { useAuth } from "../../../contexts/auth/auth-context";

const PricingPlanButton = ({
  planName = "standard",
  defaultText = "go to plan page",
  className = "",
  href = "#",
}) => {
  const { planType, isAuthenticated } = useAuth();
  if (!isAuthenticated)
    return (
      <LinkTo className={className} to={href}>
        {defaultText}
      </LinkTo>
    );
  if (planType === planName) {
    return (
      <LinkTo className={className} to={`${panelBaseUrl}/billings`}>
        Your current plan
      </LinkTo>
    );
  } else if (planName === "standard") {
    return (
      <LinkTo className={className} to={`${panelBaseUrl}/billings`}>
        Switch back
      </LinkTo>
    );
  } else {
    return (
      <LinkTo className={className} to={`${panelBaseUrl}/billings`}>
        Upgrade
      </LinkTo>
    );
  }
};

export default PricingPlanButton;
