export default {
  title: "3. Let your audience onboard their teams",
  subtitle: `
  Share your channel's URL link or attach its "Add to slack" button to your newsletter and blog.`,
};
