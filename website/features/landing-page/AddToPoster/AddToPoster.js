import WebsiteEmbedButton from "../../../components/widgets/website-embed-button";
import { PreviewVitrin } from "../../embed-page/EmbedRow";
import AddToSlackButton from "../../pool-page/AddToSlackButton";
import defaultContent from "./add-to-poster-content";
import styles from "./AddToPoster.module.css";

const codeReplacement = `<div>You will get the code for your customized button. <a href="https://telerooster.com"> telerooster.com </a> </div>`;

const AddToPoster = ({
  title = defaultContent.title,
  subtitle = defaultContent.subtitle,
}) => (
  <section className={styles.container}>
    <h2 className="large-heading-margin">{title}</h2>
    {subtitle && <p className="bigger-p-text">{subtitle}</p>}
    <PreviewVitrin className={styles.vitrinContainer} code={codeReplacement}>
      <p>A button customized for you:</p>
      <AddToSlackButton poolId="12" disabled />
    </PreviewVitrin>
  </section>
);

export default React.memo(AddToPoster);
