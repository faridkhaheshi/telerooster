import { FormGroup, Label, Input } from "reactstrap";

const SimpleInput = ({ label, id, ...otherProps }) => (
  <FormGroup>
    <Label for={id} className="bold">
      {label}
    </Label>
    <Input id={id} {...otherProps} />
  </FormGroup>
);

export default SimpleInput;
