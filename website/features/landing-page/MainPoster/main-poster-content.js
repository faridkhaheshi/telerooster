import LinkTo from "../../../components/common/link-to";

export default {
  title: "Send your newsletters in Slack®",
  content: (
    <ul className="mt-2 text-column">
      <li>
        <LinkTo to="#create-channel-form">Create</LinkTo> a channel that can be
        added to any team,
      </li>
      <li>
        Any team can add your channel by clicking on a button{" "}
        <LinkTo to="#featured-pools-vitrin">(see examples)</LinkTo>,
      </li>
      <li>
        You can{" "}
        <LinkTo to="#link-newsletter">easily link your email newsletter</LinkTo>
        . Your emails will be sent to your channel automatically.
      </li>
    </ul>
  ),
};
