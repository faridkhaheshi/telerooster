import LinkTo from "../../../components/common/link-to";
import defaultContent from "./main-poster-content";
import styles from "./MainPoster.module.css";

const MainPoster = ({
  title = defaultContent.title,
  content = defaultContent.content,
}) => {
  return (
    <section className={styles.mainPosterContainer}>
      <h1 className="mb-5">{title}</h1>
      {content}
    </section>
  );
};

export default MainPoster;
