import { Col } from "reactstrap";
import PoolCard from "../../pool-page/PoolCard/PoolCard";

import styles from "./LiveChannelCreationForm.module.css";

export const defaults = {
  title: "Your newsletter",
  creator: "You",
  description:
    "This is what your audience will see. They can click on the button below to add your channel to their team.",
};

const CardColumn = ({ title, description, creator, size = 4 }) => (
  <Col lg={size} className={styles.cardContainer}>
    <PoolCard
      zoom
      shadow
      hideBackButton
      pool={{
        title: title.length > 0 ? title : defaults.title,
        is_free: true,
        id: "sample-15",
        creator: {
          display_name: creator.length > 0 ? creator : defaults.creator,
        },
        description:
          description.length > 0 ? description : defaults.description,
      }}
      disableButton
      width={300}
      height={600}
    />
  </Col>
);

export default React.memo(CardColumn);
