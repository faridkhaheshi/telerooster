import { useRouter } from "next/router";
import { Row } from "reactstrap";
import CardColumn from "./CardColumn";
import FormColumn from "./FormColumn";

import styles from "./LiveChannelCreationForm.module.css";

const LiveChannelCreationForm = () => {
  const router = useRouter();
  const [title, setTitle] = React.useState("");
  const [creator, setCreator] = React.useState("");
  const [description, setDescription] = React.useState("");

  const handleSubmit = React.useCallback(
    (e) => {
      e.preventDefault();
      const channelData = {
        title,
        description,
        creator,
        pricingPlan: "free",
      };
      router.push({
        pathname: "/pools/create",
        query: { poolStr: JSON.stringify(channelData) },
      });
    },
    [title, description, creator]
  );

  return (
    <section className={styles.container} id="create-channel-form">
      <h2 className="pad-x-5 large-heading-margin">1. Create your channel</h2>
      <p className="bigger-p-text">It is not an ordinary channel.</p>
      <Row className={styles.rowContainer}>
        <FormColumn
          size="8"
          title={title}
          description={description}
          creator={creator}
          setTitle={setTitle}
          setDescription={setDescription}
          setCreator={setCreator}
          onSubmit={handleSubmit}
        />
        <CardColumn
          size="4"
          title={title}
          description={description}
          creator={creator}
        />
      </Row>
    </section>
  );
};

export default LiveChannelCreationForm;
