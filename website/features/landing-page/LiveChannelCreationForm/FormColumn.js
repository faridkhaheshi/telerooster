import { Row, Col, FormGroup, Label, Input, Form } from "reactstrap";
import AsyncForm from "../../../components/common/async-form";
import CustomButton from "../../../components/common/custom-button";
import SimpleInput from "../common/SimpleInput";
import { defaults } from "./CardColumn";

import styles from "./LiveChannelCreationForm.module.css";

const FormColumn = ({
  size = 8,
  title,
  description,
  creator,
  setTitle,
  setDescription,
  setCreator,
  onSubmit = () => ({}),
}) => (
  <Col lg={size} className={styles.formColumn}>
    <Form className={styles.formContainer} onSubmit={onSubmit}>
      <SimpleInput
        required
        type="text"
        label="Title:"
        id="live-channel-card-title"
        name="title"
        value={title}
        onChange={(e) => setTitle(e.target.value)}
        placeholder={defaults.title}
        maxLength={50}
      />
      <SimpleInput
        required
        type="text"
        label="Creator name:"
        id="live-channel-card-creator"
        name="creator"
        value={creator}
        onChange={(e) => setCreator(e.target.value)}
        placeholder={defaults.creator}
        maxLength={100}
      />
      <SimpleInput
        required
        type="textarea"
        rows={4}
        label="Description:"
        id="live-channel-card-description"
        name="description"
        value={description}
        onChange={(e) => setDescription(e.target.value)}
        placeholder={defaults.description}
        maxLength={1500}
      />
      <CustomButton block>Create</CustomButton>
    </Form>
  </Col>
);

export default FormColumn;
