import styles from "./LinkingPoster.module.css";
import defaultContent from "./linking-poster-content";

const LinkingPoster = ({
  title = defaultContent.title,
  subtitle = defaultContent.subtitle,
  content = defaultContent.content,
}) => (
  <section className={styles.container} id="link-newsletter">
    <h2 className="large-heading-margin">{title}</h2>
    {subtitle && <p className="bigger-p-text">{subtitle}</p>}
    <div className="mt-5 text-column">{content}</div>
  </section>
);

export default React.memo(LinkingPoster);
