export default {
  title: "2. Connect your email newsletter",
  subtitle: "Your emails will be posted in your channel. Automatically.",
  content: (
    <>
      <p>
        You will get a unique (and secret) email address for your channel. Every
        email sent to this address will be automatically sent to your channel.
        Just add this address to your email newsletter and you are done.
      </p>
      <p>
        Furthermore, you can use our easy-to-use editor to post your contents.
      </p>
    </>
  ),
};
