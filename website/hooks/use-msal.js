import React from "react";
import { PublicClientApplication } from "@azure/msal-browser";
import { microsoft, baseUrl } from "./../config";

const defaultMsalConfig = {
  auth: {
    clientId: microsoft.appId,
    authority: "https://login.microsoftonline.com/common",
    redirectUri: `${baseUrl}/auth/microsoft`,
  },
  cache: {
    cacheLocation: "localStorage",
    storeAuthStateInCookie: false,
  },
};

export const useMsal = ({ config = defaultMsalConfig } = {}) => {
  const [client, setClient] = React.useState();

  React.useEffect(() => {
    const pc = new PublicClientApplication(config);
    setClient(pc);
  }, []);

  return {
    client,
    isReady: client ? true : false,
  };
};
