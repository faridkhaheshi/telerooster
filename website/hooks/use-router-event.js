import React from "react";
import Router from "next/router";

const useRouterEvent = (eventName, handler) => {
  React.useEffect(() => {
    Router.events.on("routeChangeComplete", handler);
    return () => {
      Router.events.off("routeChangeComplete", handler);
    };
  }, []);
};

export default useRouterEvent;
