import React from "react";
import * as gtag from "./../utils/analytics/gtag";
import useRouterEvent from "./use-router-event";

const useGoogleAnalytics = () => {
  const handleRouteChange = React.useCallback((url) => {
    gtag.pageview(url);
  }, []);
  useRouterEvent("routeChangeComplete", handleRouteChange);
};

export default useGoogleAnalytics;
