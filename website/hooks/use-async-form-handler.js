import React from "react";
import { sendApiRequest2 as sendApiRequest } from "../adapters/api/send-api-request";

const useAsyncFormHandler = ({
  url,
  requestOptions = {},
  parseForm,
  onSuccess,
  onError,
}) => {
  const [isProcessing, setIsProcessing] = React.useState(false);
  const [result, setResult] = React.useState(null);
  const [errorMessage, setErrorMessage] = React.useState(null);

  const handleSubmit = React.useCallback(
    async (e) => {
      if (e && e.preventDefault) e.preventDefault();
      try {
        setErrorMessage(null);
        setIsProcessing(true);
        if (parseForm) requestOptions.body = parseForm(e);
        const theResult = await sendApiRequest(url, requestOptions);
        setResult(theResult);
        if (onSuccess) onSuccess(theResult);
      } catch (err) {
        console.error(err);
        const errorMsg =
          err.message || err.statusText || "something went wrong";
        setErrorMessage(errorMsg);
        if (onError) onError(errorMsg);
      } finally {
        setIsProcessing(false);
      }
    },
    [url, requestOptions, onSuccess]
  );

  return { result, errorMessage, handleSubmit, isProcessing };
};

export default useAsyncFormHandler;
