import React from "react";
import { toast } from "react-toastify";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faTimes,
  faCheck,
  faInfo,
  faExclamation,
  faQuestion,
} from "@fortawesome/free-solid-svg-icons";

const defaultOptions = {
  autoClose: 5000,
  hideProgressBar: true,
  position: toast.POSITION.TOP_RIGHT,
  pauseOnHover: true,
};

const useToast = () => {
  const success = React.useCallback((content, options = defaultOptions) => {
    toast.success(buildSuccessMessage.bind(null, content), options);
  }, []);

  const info = React.useCallback((content, options = defaultOptions) => {
    toast.info(buildInfoMessage.bind(null, content));
  }, []);

  const warn = React.useCallback((content, options = defaultOptions) => {
    toast.warn(buildWarnMessage.bind(null, content), options);
  }, []);

  const danger = React.useCallback((content, options = defaultOptions) => {
    toast.error(buildDangerMessage.bind(null, content), options);
  }, []);

  const message = React.useCallback((content, options = defaultOptions) => {
    toast(buildDefaultMessage.bind(null, content), options);
  }, []);

  return { toast, success, info, warn, danger, message };
};

export default useToast;

function buildSuccessMessage(
  { title = "Success!", message = "Done." } = {},
  { closeToast }
) {
  return (
    <div className="d-flex justify-content-start align-items-center">
      <div className="px-2">
        <FontAwesomeIcon icon={faCheck} size="2x" />
      </div>
      <div className="ml-2">
        <h6>{title}</h6>
        <p>{message}</p>
      </div>
    </div>
  );
}

function buildInfoMessage(
  { title = "Information!", message = "Done." } = {},
  { closeToast }
) {
  return (
    <div className="d-flex justify-content-start align-items-center">
      <div className="px-2">
        <FontAwesomeIcon icon={faInfo} size="1x" />
      </div>
      <div className="ml-2">
        <h6>{title}</h6>
        <p>{message}</p>
      </div>
    </div>
  );
}

function buildWarnMessage(
  { title = "Warning!", message = "Done." } = {},
  { closeToast }
) {
  return (
    <div className="d-flex justify-content-start align-items-center">
      <div className="px-2">
        <FontAwesomeIcon icon={faExclamation} size="1x" />
      </div>
      <div className="ml-2">
        <h6>{title}</h6>
        <p>{message}</p>
      </div>
    </div>
  );
}

function buildDangerMessage(
  { title = "Error!", message = "Done." } = {},
  { closeToast }
) {
  return (
    <div className="d-flex justify-content-start align-items-center">
      <div className="ml-2">
        <h6>{title}</h6>
        <p>{message}</p>
      </div>
    </div>
  );
}

function buildDefaultMessage(
  { title = "Attention!", message = "Done." } = {},
  { closeToast }
) {
  return (
    <div className="d-flex justify-content-start align-items-center">
      <div className="px-2">
        <FontAwesomeIcon icon={faQuestion} size="1x" />
      </div>
      <div className="ml-2">
        <h6>{title}</h6>
        <p>{message}</p>
      </div>
    </div>
  );
}
