import React from "react";

const useTimer = (timeMs = 1000) => {
  const [timePassed, setTimePassed] = React.useState(false);

  React.useEffect(() => {
    const timer = setTimeout(() => {
      setTimePassed(true);
    }, timeMs);
    return () => {
      clearTimeout(timer);
    };
  }, []);

  return timePassed;
};

export default useTimer;
