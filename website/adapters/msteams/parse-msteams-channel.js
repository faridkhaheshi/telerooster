export const parseMsTeamsChannel = (
  { id, displayName, webUrl, membershipType, ...otherInfo },
  { conversation_ref }
) => {
  const channelConvRef = conversation_ref;
  channelConvRef.conversation.id = id;
  return {
    platform: "microsoft",
    id_ext: id,
    name: displayName,
    info: otherInfo,
    private: membershipType === "private",
    web_url: webUrl,
    conversation_ref: channelConvRef,
  };
};
