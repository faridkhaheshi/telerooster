export const parseMsTeamsTeam = (
  {
    id,
    name,
    aadGroupId,
    channelCount,
    memberCount,
    tenantId,
    conversationRef,
  },
  profile,
  channels
) => {
  const info = {
    aadGroupId,
    channelCount,
    memberCount,
    tenantId,
  };
  if (channels) info.channels = channels;
  const teamData = {
    id_ext: id,
    platform: "microsoft",
    added_by: profile.id,
    name,
    info,
  };

  if (conversationRef) teamData.conversation_ref = conversationRef;

  return teamData;
};
