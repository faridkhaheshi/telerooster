export const normalizeTeamsChannelName = (name) =>
  name
    .replace(/[^\w-_]+/g, " ")
    .replace(/\s\s+/g, " ")
    .trim();
