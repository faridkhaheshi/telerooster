import { Client } from "@microsoft/microsoft-graph-client";

export const createMicrosoftGraphClient = (token) => {
  const authProvider = (cb) => {
    cb(null, token);
  };
  const options = { authProvider };
  const msGraphClient = Client.init(options);
  return msGraphClient;
};
