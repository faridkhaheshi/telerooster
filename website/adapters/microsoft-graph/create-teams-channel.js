import { suggestChannelNameForPool } from "../../services/pools/processors";
import { normalizeTeamsChannelName } from "../msteams";
import { createMicrosoftGraphClient } from "./create-microsoft-graph-client";

const NUMBER_OF_RETRIES = 5;

export const createTeamsChannel = async ({
  accessToken,
  teamAadGroupId,
  displayName,
  description,
}) => {
  const graphClient = createMicrosoftGraphClient(accessToken);
  const { value: teamChannels } = await graphClient
    .api(`/teams/${teamAadGroupId}/channels`)
    .get();
  const excludeNames = teamChannels.map((tc) => tc.displayName);
  const baseName = normalizeTeamsChannelName(displayName);
  let remainingTries = NUMBER_OF_RETRIES;
  let nameError = false;
  let newChannelName;
  do {
    try {
      newChannelName = suggestChannelNameForPool(baseName, excludeNames);
      const result = await graphClient
        .api(`/teams/${teamAadGroupId}/channels`)
        .post({
          displayName: newChannelName,
          description,
          isFavoriteByDefault: true,
        });
      return result;
    } catch (error) {
      nameError = error.message.includes("name already exist");
      if (nameError) {
        excludeNames.push(newChannelName);
      }
    }
    remainingTries--;
  } while (remainingTries > 0 && nameError);

  throw new Error("Unable to create new channel");
};
