export const raiseEvent = async ({ event, payload }) => {
  const response = await fetch(process.env.DISPATCHER_URL, {
    method: "POST",
    body: JSON.stringify({ event, payload }),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });
  if (response.status !== 200) throw new Error("raising event failed");
  return response.json();
};
