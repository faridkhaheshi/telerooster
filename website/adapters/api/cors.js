import Cors from "cors";
import { runMiddleware } from "../../utils/nextjs/run-middleware";
import { baseUrl, panelBaseUrl } from "./../../config";

const whiteList = [baseUrl, panelBaseUrl];

const options = {
  origin: (origin, cb) => {
    if (whiteList.indexOf(origin) !== -1 || !origin) {
      cb(null, true);
    } else {
      cb(new Error("Not allowed by CORS"));
    }
  },
  methods: ["GET", "HEAD", "PUT", "PATCH", "POST", "DELETE"],
  credentials: true,
  optionsSuccessStatus: 200,
};

const corsMiddleware = Cors(options);

export const cors = async (req, res) => {
  await runMiddleware(req, res, corsMiddleware);
};
