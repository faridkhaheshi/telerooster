import { InternalServerError, makeErrFromCode } from "restify-errors";

const defaultHeaders = {
  Accept: "application/json",
  "Content-Type": "application/json",
  Authorization: `Bearer ${process.env.INTERNAL_API_KEY}`,
};

export const sendInternalApiRequest = async (url, options = {}) => {
  const fullOptions = buildFullRequestOptions(options);
  const response = await fetch(url, fullOptions);
  if (response.status !== 200)
    throw makeErrFromCode(response.status, response.statusText);
  return response.json();
};

function buildFullRequestOptions({ method = "GET", body, headers = {} }) {
  const finalOptions = {
    method,
    headers: { ...defaultHeaders, ...headers },
  };
  if (body) finalOptions.body = JSON.stringify(body);
  return finalOptions;
}
