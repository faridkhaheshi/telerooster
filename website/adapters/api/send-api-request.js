import { makeErrFromCode } from "restify-errors";

const localBaseUrl = "http://website:3000";

const defaultHeaders = {
  Accept: "application/json",
  // "Content-Type": "application/json",
};

export const sendApiRequest = async (url, options = {}) => {
  try {
    const { local = false, ...otherOptions } = options;
    let fullUrl = url;
    if (local) fullUrl = `${localBaseUrl}${url}`;
    const response = await fetch(fullUrl, otherOptions);
    const jsonResponse = await response.json();
    if (response.status === 200) return jsonResponse;
    throw jsonResponse.error;
  } catch (error) {
    // TODO: handle errors
    // console.log("error");
    // console.error(error);
    throw error;
  }
};

export const buildRequestOptions = ({ method = "GET", body } = {}) => ({
  method,
  body: body ? JSON.stringify(body) : undefined,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
});

export const sendApiRequest2 = async (url, options = {}) => {
  const finalOptions = buildFinalOptions(options);
  const response = await fetch(url, finalOptions);
  if (response.status === 200) return response.json();
  throw makeErrFromCode(response.status, response.statusText);
};

function buildFinalOptions({
  method = "GET",
  body,
  headers = {},
  ...otherOptions
}) {
  const finalOptions = {
    method,
    headers: { ...defaultHeaders, ...headers },
    ...otherOptions,
  };
  if (body) {
    finalOptions.body = JSON.stringify(body);
    finalOptions.headers["Content-Type"] = "application/json";
  }
  return finalOptions;
}
