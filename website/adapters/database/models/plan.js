"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Plan extends Model {
    static associate(models) {
      Plan.belongsTo(models.User, {
        foreignKey: "user_id",
        as: "user",
      });
    }
  }
  Plan.init(
    {
      status: {
        type: DataTypes.ENUM(["active", "expired", "canceled"]),
        allowNull: false,
        defaultValue: "active",
      },
      type: {
        type: DataTypes.ENUM(["creator", "content"]),
        allowNull: false,
        defaultValue: "creator",
      },
      user_id: {
        type: DataTypes.UUID,
        allowNull: false,
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
        references: {
          model: "Users",
          key: "id",
        },
      },
      expires_at: {
        type: DataTypes.INTEGER,
      },
      product_id_ext: {
        type: DataTypes.STRING,
      },
      update_ref: {
        type: DataTypes.JSONB,
      },
    },
    {
      sequelize,
      modelName: "Plan",
      schema: "rooster",
    }
  );
  return Plan;
};
