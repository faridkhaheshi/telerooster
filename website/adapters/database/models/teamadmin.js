"use strict";
const { Model, Sequelize } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class TeamAdmin extends Model {
    static associate(models) {}
  }
  TeamAdmin.init(
    {
      id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: Sequelize.literal("uuid_generate_v4()"),
      },
      active: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: true },
      team_id: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: "Teams",
          key: "id",
        },
      },
      profile_id: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: "Profiles",
          key: "id",
        },
      },
    },
    {
      sequelize,
      modelName: "TeamAdmin",
      schema: "rooster",
      indexes: [
        {
          unique: true,
          fields: ["team_id", "profile_id"],
          name: "once_per_profile_per_team",
        },
      ],
    }
  );
  return TeamAdmin;
};
