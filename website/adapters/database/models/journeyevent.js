import { Model, DataTypes } from "sequelize";
export default function (sequelize, Sequelize) {
  class JourneyEvent extends Model {
    static associate(models) {
      JourneyEvent.belongsTo(models.Pool, {
        foreignKey: "pool_id",
        as: "pool",
      });
    }
  }
  JourneyEvent.init(
    {
      pool_id: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: "Pool",
          key: "id",
        },
      },
      event: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      message_id: {
        type: DataTypes.UUID,
        references: {
          model: "Message",
          key: "id",
        },
      },
      active: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: true },
    },
    {
      sequelize,
      modelName: "JourneyEvent",
      schema: "rooster",
    }
  );
  return JourneyEvent;
}
