import { Model, DataTypes } from "sequelize";

export default function (sequelize, Sequelize) {
  class Pool extends Model {
    static associate(models) {
      Pool.belongsTo(models.User, { foreignKey: "creator_id", as: "creator" });
      Pool.hasMany(models.Subscription, {
        foreignKey: "pool_id",
        as: "subscriptions",
      });
      Pool.belongsToMany(models.User, {
        through: models.Subscription,
        foreignKey: "pool_id",
      });
      Pool.hasMany(models.Contentmail, { foreignKey: "pool_id" });
      Pool.hasMany(models.Message, {
        as: "messages",
        foreignKey: "pool_id",
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
      });
      Pool.hasMany(models.JourneyEvent, {
        as: "messageJourneyEvents",
        foreignKey: "pool_id",
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
      });
    }
  }

  Pool.init(
    {
      creator_id: {
        type: DataTypes.UUID,
        references: {
          model: "Users",
          key: "id",
        },
      },
      poster: { type: DataTypes.TEXT },
      title: { type: DataTypes.STRING(100), allowNull: false },
      description: { type: Sequelize.STRING(1500), allowNull: false },
      slug: { type: DataTypes.TEXT, unique: true },
      is_free: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      price_sm: { type: DataTypes.INTEGER, validate: { min: 0 } },
      price_md: { type: DataTypes.INTEGER, validate: { min: 0 } },
      price_lg: { type: DataTypes.INTEGER, validate: { min: 0 } },
      has_free_trial: { type: DataTypes.BOOLEAN, defaultValue: true },
      featured: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      active: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: true },
      has_journey: { type: DataTypes.BOOLEAN, defaultValue: false },
    },
    {
      sequelize,
      modelName: "Pool",
      schema: "rooster",
    }
  );
  return Pool;
}
