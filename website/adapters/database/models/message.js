import { Model, DataTypes } from "sequelize";
export default function (sequelize, Sequelize) {
  class Message extends Model {
    static associate(models) {
      Message.belongsTo(models.Pool, {
        foreignKey: "pool_id",
        as: "pool",
      });
      Message.hasMany(models.Parcel, {
        as: "parcels",
        foreignKey: "message_id",
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
      });
    }
  }
  Message.init(
    {
      pool_id: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: "Pool",
          key: "id",
        },
      },
      text: {
        type: DataTypes.TEXT("long"),
      },
      title: {
        type: DataTypes.TEXT,
      },
      blocks: {
        type: DataTypes.JSONB,
      },
      active: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: true },
    },
    {
      sequelize,
      modelName: "Message",
      schema: "rooster",
    }
  );
  return Message;
}
