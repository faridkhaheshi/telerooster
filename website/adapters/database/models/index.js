import Sequelize from "sequelize";
import userModel from "./user";
import poolModel from "./pool";
import profileModel from "./profile";
import tokenModel from "./token";
import subscriptionModel from "./subscription";
import teamModel from "./team";
import teamAdminModel from "./teamadmin";
import channelModel from "./channel";
import pipeModel from "./pipe";
import contentmailModel from "./contentmail";
import paymentSessionModel from "./paymentsession";
import customerModel from "./customer";
import planModel from "./plan";
import teamReportModel from "./teamreport";
import channelReportModel from "./channelreport";
import messageModel from "./message";
import journeyEventModel from "./journeyevent";
import parcelModel from "./parcel";

const sequelize = new Sequelize(process.env.POSTGRES_URI, {
  dialect: "postgres",
  logging: false,
  operatorAliases: false,
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
});

const db = {};

db.Channel = channelModel(sequelize, Sequelize);
db.Contentmail = contentmailModel(sequelize, Sequelize);
db.Customer = customerModel(sequelize, Sequelize);
db.PaymentSession = paymentSessionModel(sequelize, Sequelize);
db.Pipe = pipeModel(sequelize, Sequelize);
db.Pool = poolModel(sequelize, Sequelize);
db.Profile = profileModel(sequelize, Sequelize);
db.Subscription = subscriptionModel(sequelize, Sequelize);
db.Team = teamModel(sequelize, Sequelize);
db.TeamAdmin = teamAdminModel(sequelize, Sequelize);
db.Token = tokenModel(sequelize, Sequelize);
db.User = userModel(sequelize, Sequelize);
db.Plan = planModel(sequelize, Sequelize);
db.TeamReport = teamReportModel(sequelize, Sequelize);
db.ChannelReport = channelReportModel(sequelize, Sequelize);
db.Message = messageModel(sequelize, Sequelize);
db.JourneyEvent = journeyEventModel(sequelize, Sequelize);
db.Parcel = parcelModel(sequelize, Sequelize);

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

export default db;
