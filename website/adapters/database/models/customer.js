"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Customer extends Model {
    static associate(models) {
      Customer.belongsTo(models.User, {
        foreignKey: "user_id",
        as: "user",
      });
    }
  }
  Customer.init(
    {
      provider: {
        type: DataTypes.ENUM(["stripe"]),
        allowNull: false,
      },
      id_ext: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      user_id: {
        type: DataTypes.UUID,
        allowNull: false,
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
        references: {
          model: "Users",
          key: "id",
        },
      },
      info: {
        type: DataTypes.JSONB,
      },
      active: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: true },
    },
    {
      sequelize,
      modelName: "Customer",
      schema: "rooster",
      indexes: [
        {
          unique: true,
          fields: ["provider", "id_ext"],
          name: "one_customer_id_ext_per_provider",
        },
      ],
    }
  );
  return Customer;
};
