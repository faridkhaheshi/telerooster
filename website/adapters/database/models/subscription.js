import { Model, DataTypes } from "sequelize";
export default function (sequelize, Sequelize) {
  class Subscription extends Model {
    static associate(models) {
      Subscription.belongsTo(models.User, {
        foreignKey: "user_id",
        as: "user",
      });
      Subscription.belongsTo(models.Pool, {
        foreignKey: "pool_id",
        as: "pool",
      });
      Subscription.belongsToMany(models.Channel, {
        through: models.Pipe,
        foreignKey: "subscription_id",
      });
    }
  }

  Subscription.init(
    {
      id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: Sequelize.literal("uuid_generate_v4()"),
      },
      active: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: true },
      status: {
        type: DataTypes.ENUM(["waiting", "ready"]),
        allowNull: false,
        defaultValue: "waiting",
      },
      user_id: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: "Users",
          key: "id",
        },
      },
      pool_id: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: "Pools",
          key: "id",
        },
      },
    },
    {
      sequelize,
      modelName: "Subscription",
      schema: "rooster",
      indexes: [
        {
          unique: true,
          fields: ["user_id", "pool_id"],
          name: "one_pool_subscription_per_user",
        },
      ],
    }
  );
  return Subscription;
}
