"use strict";
const { Model, Sequelize } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class ChannelReport extends Model {
    static associate(models) {
      ChannelReport.belongsTo(models.Channel, {
        foreignKey: "channel_id",
        as: "channel",
      });
    }
  }
  ChannelReport.init(
    {
      channel_id: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: "Channels",
          key: "id",
        },
      },
      info: {
        type: DataTypes.JSONB,
      },
    },
    {
      sequelize,
      modelName: "ChannelReport",
      schema: "rooster",
    }
  );
  return ChannelReport;
};
