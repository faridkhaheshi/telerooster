import { Model, DataTypes } from "sequelize";
export default function (sequelize, Sequelize) {
  class Profile extends Model {
    static associate(models) {
      Profile.belongsTo(models.User, {
        foreignKey: "user_id",
        as: "user",
      });
      Profile.belongsToMany(models.Team, {
        through: models.TeamAdmin,
        foreignKey: "profile_id",
      });
      Profile.hasMany(models.Team, {
        as: "teams",
        foreignKey: "added_by",
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
      });
      Profile.hasOne(models.Token, {
        as: "token",
        foreignKey: "profile_id",
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
      });
    }
  }

  Profile.init(
    {
      provider: {
        type: DataTypes.ENUM(["microsoft", "slack", "google"]),
        allowNull: false,
      },
      id_ext: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      user_id: {
        type: DataTypes.UUID,
        references: {
          model: "Users",
          key: "id",
        },
      },
      info: {
        type: DataTypes.JSONB,
      },
      conversation_ref: {
        type: DataTypes.JSONB,
      },
    },
    {
      sequelize,
      modelName: "Profile",
      schema: "rooster",
      indexes: [
        {
          unique: true,
          fields: ["provider", "id_ext"],
          name: "one_profile_per_provider",
        },
      ],
    }
  );
  return Profile;
}
