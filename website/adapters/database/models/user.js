import { Model, DataTypes } from "sequelize";

export default function (sequelize, Sequelize) {
  class User extends Model {
    static associate(models) {
      User.hasMany(models.Pool, {
        as: "pools",
        foreignKey: "creator_id",
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
      });
      User.hasMany(models.Profile, {
        as: "profiles",
        foreignKey: "user_id",
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
      });
      User.hasMany(models.Token, {
        as: "tokens",
        foreignKey: "user_id",
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
      });
      User.hasMany(models.Customer, {
        as: "customers",
        foreignKey: "user_id",
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
      });
      User.hasMany(models.Plan, {
        as: "plans",
        foreignKey: "user_id",
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
      });
      User.hasMany(models.PaymentSession, {
        as: "paymentSessions",
        foreignKey: "user_id",
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
      });
      User.hasMany(models.Subscription, { foreignKey: "user_id", as: "user" });
      User.belongsToMany(models.Pool, {
        through: models.Subscription,
        foreignKey: "user_id",
      });
    }
  }

  User.init(
    {
      first_name: { type: DataTypes.STRING(50) },
      last_name: { type: DataTypes.STRING(50) },
      display_name: { type: DataTypes.STRING(100) },
      email: {
        type: DataTypes.CITEXT,
        validate: { isEmail: true },
        unique: true,
        allowNull: false,
      },
      password_hash: { type: DataTypes.TEXT },
      role: {
        type: DataTypes.ENUM(["normal", "admin", "superadmin"]),
        allowNull: false,
        defaultValue: "normal",
      },
      active: { type: DataTypes.BOOLEAN, allowNull: false, defaultValue: true },
    },
    {
      sequelize,
      modelName: "User",
      schema: "rooster",
    }
  );

  return User;
}
