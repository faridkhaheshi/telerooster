import { sendInternalApiRequest } from "../api";

export const createSlackChannel = ({ installation, teamId, channelData }) =>
  sendInternalApiRequest(`${process.env.SLACKBOT_API_BASE_URL}/channels`, {
    method: "POST",
    body: { installation, teamId, channelData },
  });
