import { useRouter } from "next/router";
import { removeToken, saveToken } from "../../utils/cookie";
import useServerResource from "../../hooks/use-server-resource";
import FullPageWaiting from "../../components/common/FullPageWaiting";
import findPlanType from "./find-plan-type";
import checkCustomerStatus from "./check-customer-status";
import checkPlatformReadiness from "./check-platform-readiness";
import getAuthPageForPlatform from "./get-auth-page-for-platform";

export const AuthContext = React.createContext();
export const useAuth = () => React.useContext(AuthContext);

export const AuthContextProvider = ({ children }) => {
  const router = useRouter();
  const {
    data: user,
    isLoading: isUserLoading,
    error: userFetchError,
    refresh: updateUserInfo,
  } = useServerResource("/api/me", {
    shouldRetryOnError: false,
  });

  const logout = React.useCallback(
    (ref = "/") => {
      removeToken();
      updateUserInfo();
      window.location.replace(ref);
    },
    [router, updateUserInfo]
  );

  const login = React.useCallback(
    ({ token, ref = "/" }) => {
      saveToken(token);
      updateUserInfo();
      window.location.replace(ref);
      router.push(ref);
    },
    [router, updateUserInfo]
  );

  const isReadyForPlatform = React.useCallback(
    (platform) => checkPlatformReadiness(platform, user),
    [user]
  );

  const contextValues = {
    isAuthenticated: user ? true : false,
    isUserLoading,
    authFinished: user || userFetchError ? true : false,
    user,
    login,
    logout,
    updateUserInfo,
    userFetchError,
    planType: findPlanType(user),
    isCustomer: checkCustomerStatus(user),
    isReadyForPlatform,
    getAuthPageForPlatform,
  };

  return (
    <AuthContext.Provider value={contextValues}>
      <AuthInfo show={false} {...contextValues} />
      <FullPageWaiting show={!contextValues.authFinished} />
      {children}
    </AuthContext.Provider>
  );
};

function AuthInfo({
  show = false,
  isUserLoading,
  isAuthenticated,
  isFetchingUser,
  authFinished,
  updateUserInfo,
  logout,
  userFetchError,
  user,
  planType,
  isCustomer,
}) {
  if (!show) return null;
  return (
    <>
      <button onClick={updateUserInfo}>update</button>
      <button onClick={() => logout()}>logout</button>
      <p>{planType}</p>
      <p>isCustomer: {isCustomer ? "YES" : "NO"}</p>
      <pre>
        <code>{`isAuthenticated: ${isAuthenticated}`}</code>
      </pre>
      <pre>
        <code>{`isFetchingUser: ${isFetchingUser}`}</code>
      </pre>
      <pre>
        <code>{`authFinished: ${authFinished}`}</code>
      </pre>
      <pre>
        <code>{`isUserLoading: ${isUserLoading}`}</code>
      </pre>
      <div
        style={{
          maxHeight: 100,
          overflow: "scroll",
        }}
      >
        <pre>{JSON.stringify(userFetchError, null, 2)}</pre>
        {!userFetchError && user ? (
          <pre>{JSON.stringify(user, null, 2)}</pre>
        ) : (
          "no user"
        )}
      </div>
    </>
  );
}
