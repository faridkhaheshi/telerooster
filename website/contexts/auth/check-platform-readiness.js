const checkPlatformReadiness = (platform, userInfo) => {
  try {
    if (!platform) return userInfo ? true : false;
    const platformTokens = userInfo.tokens.filter(
      ({ provider, active }) => provider === platform && active
    );
    return platformTokens.length > 0;
  } catch (error) {
    return false;
  }
};

export default checkPlatformReadiness;
