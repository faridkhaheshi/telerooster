const findPlanType = (user) => {
  try {
    const { plans = [] } = user;
    if (plans.length === 0) return "standard";
    const creatorPlans = plans.filter(
      (pl) => pl.status === "active" && pl.type === "creator"
    );
    return creatorPlans.length > 0 ? "creator" : "standard";
  } catch (err) {
    return "standard";
  }
};

export default findPlanType;
