import Cookies from 'js-cookie';
import decodeJWT from 'jwt-decode';

export const loadAuthInfo = () => {
  try {
    const token = Cookies.get('token');
    const payload = decodeJWT(token);
    if (Date.now() >= payload.exp * 1000) throw new Error('token expired');
    return { user: payload, token };
  } catch (error) {
    return { token: null, user: null };
  }
};
