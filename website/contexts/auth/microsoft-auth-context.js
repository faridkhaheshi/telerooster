import { useMsal } from "../../hooks/use-msal";

export const MicrosoftAuthContext = React.createContext();
export const useMicrosoftAuth = () => React.useContext(MicrosoftAuthContext);

const defaultScopes = ["https://graph.microsoft.com/.default"];

export const MicrosoftAuthContextProvider = ({ children }) => {
  const { client, isReady } = useMsal({ scopes: defaultScopes });

  const logout = React.useCallback(() => {
    client.logout();
  }, [client]);

  const contextValues = { logout };

  return (
    <MicrosoftAuthContext.Provider value={contextValues}>
      {children}
    </MicrosoftAuthContext.Provider>
  );
};
