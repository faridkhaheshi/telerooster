import { useRouter } from "next/router";
import { useAuth } from "./auth-context";
import WaitingSpinner from "../../components/common/waiting-spinner";

export const protectPage = (Page) => {
  const ProtectedPage = (props) => {
    const { user, authFinished, isAuthenticated } = useAuth();
    const [renderPage, setRenderPage] = React.useState(false);
    const [goToAuthPage, setGoToAuthPage] = React.useState(false);
    const router = useRouter();
    const {
      query: { platform },
    } = router;

    React.useEffect(() => {
      if (authFinished && !isAuthenticated) return setGoToAuthPage(true);
      if (!user) return;
      const hasPlatformInfo = checkUserInfoForPlatform(platform, user);
      if (hasPlatformInfo) return setRenderPage(true);
      return setGoToAuthPage(true);
    }, [authFinished, isAuthenticated, user, platform]);

    React.useEffect(() => {
      if (goToAuthPage) {
        const authPage = getAuthPage(platform);
        const ref = window?.location?.href;
        router.push({
          pathname: authPage,
          query: { ref },
        });
      }
    }, [goToAuthPage, router, platform]);

    if (renderPage) return <Page {...props} />;
    return (
      <div className="full-page-centered bg-sky-blue">
        <WaitingSpinner color="white" />
      </div>
    );
  };

  return ProtectedPage;
};

// function getAuthPage(platform) {
//   if (platform === "microsoft") return "/microsoft/welcome";
//   if (platform === "slack") return "/slack/welcome";
//   return "/auth/login";
// }

function getAuthPage(platform) {
  if (platform === "microsoft") return "/api/auth/microsoft";
  if (platform === "slack") return "/api/auth/slack";
  return "/auth/login";
}

function checkUserInfoForPlatform(platform, userInfo) {
  try {
    if (!platform) return userInfo ? true : false;
    const platformTokens = userInfo.tokens.filter(
      ({ provider, active }) => provider === platform && active
    );
    return platformTokens.length > 0;
  } catch (error) {
    return false;
  }
}
