const getAuthPageForPlatform = (platform) => {
  if (platform === "microsoft") return "/api/auth/microsoft";
  if (platform === "slack") return "/api/auth/slack";
  return "/auth/login";
};

export default getAuthPageForPlatform;
