const defaultConfig = require('./default');
const productionConfig = require('./production');

module.exports =
  process.env.NODE_ENV === 'production' ? productionConfig : defaultConfig;
