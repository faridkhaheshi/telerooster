const baseUrl = process.env.NEXT_PUBLIC_WEBSITE_BASE_URL;

module.exports = {
  baseUrl,
  panelBaseUrl: process.env.NEXT_PUBLIC_PANEL_BASE_URL,
  cookieOptions: {
    path: "/",
    domain: process.env.NEXT_PUBLIC_WEBSITE_COOKIE_DOMAIN,
    sameSite: "None",
    secure: true,
  },
  microsoft: {
    appId: "2bbfea18-0c6a-4992-9051-bcd64bc97b66",
    authRedirectUri: "/api/auth/microsoft/success",
    scopes: "https://graph.microsoft.com/.default offline_access",
    appInstallLink:
      "https://teams.microsoft.com/l/app/1da03173-5bfc-4ac9-88a2-f7db399ac49b",
  },
  slack: {
    authUrl: `${process.env.NEXT_PUBLIC_SLACKBOT_BASE_URL}/auth`,
    appId: process.env.NEXT_PUBLIC_SLACK_APP_ID,
  },
  google: {
    gaTrackingId: "UA-150353421-6",
    authClientId: process.env.NEXT_PUBLIC_GOOGLE_AUTH_CLIENT_ID,
    authScopes: "profile email",
    recaptchaSiteKey: process.env.NEXT_PUBLIC_GOOGLE_RECAPTCHA_SITE_KEY,
  },
};
