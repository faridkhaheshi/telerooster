const baseUrl = `http://localhost:${process.env.NEXT_PUBLIC_TELEROOSTER_WEBSITE_PORT}`;

module.exports = {
  baseUrl,
  panelBaseUrl: `http://localhost:${process.env.NEXT_PUBLIC_PANEL_PORT}`,
  cookieOptions: {
    path: "/",
    sameSite: "None",
    secure: true,
  },
  microsoft: {
    appId: "1b9c72cd-64dc-4e0d-9f0a-9685cf848797",
    authRedirectUri: "/api/auth/microsoft/success",
    scopes: "https://graph.microsoft.com/.default offline_access",
    appInstallLink:
      "https://teams.microsoft.com/l/app/3b23c00b-3fe9-4136-9652-f9e38a0aab69",
  },
  slack: {
    authUrl: `http://localhost:${process.env.NEXT_PUBLIC_SLACKBOT_PORT}/auth`,
    appId: process.env.NEXT_PUBLIC_SLACK_APP_ID,
  },
  google: {
    gaTrackingId: "UA-150353421-6",
    authClientId: process.env.NEXT_PUBLIC_GOOGLE_AUTH_CLIENT_ID,
    authScopes: "profile email",
    recaptchaSiteKey: process.env.NEXT_PUBLIC_GOOGLE_RECAPTCHA_SITE_KEY,
  },
};
