export const suggestChannelNameForPool = (basicName, excludeNames = []) => {
  if (excludeNames.indexOf(basicName) === -1) return basicName;
  let newName;
  do {
    newName = `${basicName}_${randomNumber(10000)}`;
  } while (excludeNames.indexOf(newName) > -1);
  return newName;
};

function randomNumber(max) {
  return Math.floor(Math.random() * max);
}
