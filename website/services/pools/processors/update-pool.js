import { NotFoundError } from "restify-errors";
import db from "../../../adapters/database";

export const updatePool = async (query, updateData) => {
  const pool = await db.Pool.findOne({ where: query });
  if (pool) return pool.update(updateData);
  throw new NotFoundError("Pool not found");
};
