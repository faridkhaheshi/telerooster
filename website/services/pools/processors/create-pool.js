import db from "../../../adapters/database";

export const createPool = async (poolData) => db.Pool.create(poolData);
