import db from "../../../adapters/database";
import pool from "../../../adapters/database/models/pool";

export const findPools = async (query) =>
  db.Pool.findAll({
    where: query,
    include: [
      {
        model: db.User,
        as: "creator",
        attributes: [
          "id",
          "display_name",
          "first_name",
          "last_name",
          "createdAt",
        ],
      },
    ],
  });

export const findPoolsPure = async (options) => db.Pool.findAll(options);

export const findUserPoolsWithSubscribers = async ({ userId }) => {
  const pools = await db.Pool.findAll({
    where: { creator_id: userId, active: true },
    include: [
      {
        model: db.Subscription,
        as: "subscriptions",
        where: { active: true },
        attributes: ["id"],
        required: false,
        include: {
          model: db.Channel,
          required: false,
          where: { active: true },
          through: { model: db.Pipe, attributes: ["id"] },
          attributes: ["id", "platform", "team_id", "info"],
          include: {
            model: db.Team,
            as: "team",
            where: { active: true },
            attributes: ["id", "info", "name", "platform"],
          },
        },
      },
      {
        model: db.Contentmail,
        required: false,
        attributes: ["id", "address"],
        where: { active: true },
      },
    ],
  });

  return pools.map((pool) => {
    const { subscriptions, ...poolData } = pool.toJSON();
    const uniqueTeams = findUniqueTeams(subscriptions);
    poolData.teamsCount = uniqueTeams.length;
    poolData.membersCount = uniqueTeams.reduce(
      (total, team) =>
        total + (team.channelMemberCount || team.teamMemberCount || 0),
      0
    );
    return poolData;
  });
};

function findUniqueTeams(poolSubscriptions) {
  const uniqueTeams = [];
  const uniqueTeamIds = new Set();
  poolSubscriptions.forEach((sub) => {
    sub.Channels.forEach(
      ({
        info: channelInfo,
        team: {
          id: teamId,
          info: { memberCount: teamMemberCount },
        },
      }) => {
        if (!uniqueTeamIds.has(teamId)) {
          uniqueTeamIds.add(teamId);
          uniqueTeams.push({
            id: teamId,
            channelMemberCount: channelInfo.memberCount,
            teamMemberCount,
          });
        }
      }
    );
  });
  return uniqueTeams;
}
