import db from "../../../adapters/database";

export const findPoolById = (id) =>
  db.Pool.findByPk(id, {
    include: [
      {
        model: db.User,
        as: "creator",
        attributes: ["display_name", "first_name", "last_name", "createdAt"],
      },
    ],
  });

export const findPoolByIdPure = async (id, options = {}) =>
  db.Pool.findByPk(id, options);

export const findPoolByIdWithContentMails = async (id) =>
  db.Pool.findByPk(id, {
    include: {
      model: db.Contentmail,
      attributes: ["id", "address"],
      where: { active: true },
    },
  });
