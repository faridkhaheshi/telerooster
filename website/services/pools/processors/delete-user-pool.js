import db from "../../../adapters/database";
import { NotFoundError } from "restify-errors";

export const deleteUserPool = async ({ userId, poolId }) => {
  const pool = await db.Pool.findOne({
    where: { creator_id: userId, id: poolId },
  });
  if (pool) return pool.update({ active: false });
  throw new NotFoundError("Pool not found");
};
