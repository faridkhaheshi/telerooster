import { updatePool } from "../processors";
import { parsePoolDataFromUserInput } from "../utils";

export const updateUserPool = async (req, res) => {
  try {
    const {
      user: { id: userId },
      query: { id: poolId },
      body,
    } = req;
    const updateData = parsePoolDataFromUserInput(body);
    const pool = await updatePool(
      { id: poolId, creator_id: userId, active: true },
      updateData
    );
    return res.json(pool);
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
