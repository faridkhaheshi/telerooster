import { UnauthorizedError } from "restify-errors";
import { findPoolByIdWithContentMails } from "../processors";

export const getUserPool = async (req, res) => {
  try {
    const {
      user: { id: userId } = {},
      query: { id: poolId },
    } = req;
    const pool = await findPoolByIdWithContentMails(poolId);
    if (pool.creator_id === userId) return res.json(pool);
    throw new UnauthorizedError("Not your channel");
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
