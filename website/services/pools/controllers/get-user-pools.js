import { findUserPoolsWithSubscribers } from "../processors";

export const getUserPools = async (req, res) => {
  try {
    const {
      user: { id: userId },
    } = req;
    const pools = await findUserPoolsWithSubscribers({ userId });
    return res.json(pools);
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
