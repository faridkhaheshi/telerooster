import { findPoolById } from "../processors";

export const getPool = async (req, res) => {
  try {
    const {
      query: { id },
      user,
    } = req;
    const pool = await findPoolById(id);
    if (pool) return res.json(pool);
    return res.status(404).json({ error: { message: "Not found" } });
  } catch (err) {
    return res
      .status(500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
