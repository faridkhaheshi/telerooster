import { generateContentmailForPool } from "../../contentmails/processors";
import { updateUserDisplayName } from "../../users/processors";
import { createPool } from "../processors";
import { parsePoolDataFromUserInput } from "../utils/parse-pool-data-from-user-input";

export const handleCreatePoolRequest = async (req, res) => {
  try {
    const poolData = parsePoolDataFromUserInput(req.body);
    const userDisplayName = req.body.creator;
    const [pool] = await Promise.all([
      createPool({ ...poolData, creator_id: req.user.id }),
      updateUserDisplayName(req.user.id, userDisplayName),
    ]);
    const contentmail = await generateContentmailForPool(pool.id);
    res.json(pool);
  } catch (err) {
    return res
      .status(500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
