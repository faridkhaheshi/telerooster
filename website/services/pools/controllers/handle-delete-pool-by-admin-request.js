import { findPoolByIdPure } from "../processors";
import { deleteUserPool } from "../processors/delete-user-pool";

export const handleDeletePoolByAdminRequest = async (req, res) => {
  try {
    const {
      user: { id: userId },
      query: { id: poolId },
    } = req;
    const pool = await deleteUserPool({ userId, poolId });
    return res.json({ done: true, pool });
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
