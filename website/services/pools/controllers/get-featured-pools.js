import { findPools } from "../processors";

export const getFeaturedPools = async (req, res) => {
  try {
    const pools = await findPools({ featured: true });
    return res.json(pools);
  } catch (err) {
    return res
      .status(err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
