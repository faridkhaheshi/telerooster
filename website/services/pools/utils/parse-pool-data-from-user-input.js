export const parsePoolDataFromUserInput = ({
  title,
  description,
  poster,
  pricingPlan,
  prices = {},
}) => {
  const basePoolData = { title, description, is_free: pricingPlan === "free" };
  if (poster) basePoolData.poster = poster;
  if (pricingPlan === "free") return basePoolData;
  basePoolData.price_sm = prices.smallTeams;
  basePoolData.price_md = prices.mediumTeams;
  basePoolData.price_lg = prices.largeTeams;
  basePoolData.has_free_trial = prices.hasTrial;
  return basePoolData;
};
