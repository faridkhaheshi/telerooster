import { raiseEvent } from "../../../adapters/dispatcher";

export const handleRaiseEventRequest = async (req, res) => {
  try {
    const result = await raiseEvent(req.body);
    return res.json(result);
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
