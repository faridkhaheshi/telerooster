import { saveMessage } from "../processors";

export const handlePrivateSaveMessage = async (req, res) => {
  try {
    const { body } = req;
    const message = await saveMessage(body);
    return res.json(message);
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
