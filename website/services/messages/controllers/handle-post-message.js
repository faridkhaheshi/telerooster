import { BadRequestError, NotAuthorizedError } from "restify-errors";
import { raiseEvent } from "../../../adapters/dispatcher";
import { saveJourneyEvent } from "../../journeyevents/processors";
import { findPoolByIdPure } from "../../pools/processors";
import { saveMessage } from "../processors/save-message";

export const handlePostMessage = async (req, res) => {
  try {
    const {
      user,
      body: { poolId, blocks, title, text, action, event },
    } = req;
    const {
      creator_id: poolOwnerId,
      has_journey: hasJourney,
    } = await findPoolByIdPure(poolId);
    if (user.id !== poolOwnerId) throw new NotAuthorizedError("Not authorized");
    if (!poolId) throw new BadRequestError("poolId is required");
    const message = await saveMessage({
      pool_id: poolId,
      blocks,
      title,
      text,
    });

    if (action === "send") {
      await raiseEvent({
        event: "send-pool-message",
        payload: { poolId, message },
      });
    } else if (
      action === "schedule" &&
      hasJourney &&
      event &&
      event.length > 0
    ) {
      const journeyEvent = await saveJourneyEvent({
        poolId,
        messageId: message.id,
        event,
      });
    }

    return res.json({ done: true });
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
