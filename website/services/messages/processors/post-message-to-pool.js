import { raiseEvent } from "../../../adapters/dispatcher";

export const postMessageToPool = ({ poolId, message }) =>
  raiseEvent({
    event: "send-pool-message",
    payload: { poolId, message },
  });
