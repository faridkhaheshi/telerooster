import { getTeamToken } from "./get-team-token";
import { getValidProfileToken } from "./get-valid-profile-token";

export const getValidToken = async ({ profileId, teamId, platform }) => {
  if (platform === "microsoft") return getValidProfileToken(profileId);
  if (platform === "slack") {
    const token = await getTeamToken(teamId);
    return token.info;
  }
  return null;
};
