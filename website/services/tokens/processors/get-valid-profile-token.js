import { getProfileToken } from "./get-profile-token";
import { refreshToken } from "./refresh-token";

const REFRESH_TOKEN_MINS_BEFORE_EXP = 2;

export const getValidProfileToken = async (profileId) => {
  const currentToken = await getProfileToken(profileId);
  if (!currentToken) return null;
  const { expires_in, access_token } = currentToken;
  const now = Math.floor(Date.now() / 1000);

  if (!expires_in || now < expires_in - REFRESH_TOKEN_MINS_BEFORE_EXP * 60) {
    return access_token;
  }

  await refreshToken(currentToken);
  return currentToken.access_token;
};
