import {
  getMicrosoftAuthInfo,
  parseMicrosoftToken,
} from "../../auth/microsoft/processors";

export const refreshToken = async (token) => {
  if (token.provider === "microsoft") {
    const newAuthInfo = await getMicrosoftAuthInfo({
      refresh_token: token.refresh_token,
    });
    const newTokenData = parseMicrosoftToken(newAuthInfo);
    token.access_token = newTokenData.access_token;
    token.refresh_token = newTokenData.refresh_token;
    token.expires_in = newTokenData.expires_in;
    token.info = newTokenData.info;
    token.active = true;
    await token.save();
    await token.reload();
  }
  return token;
};
