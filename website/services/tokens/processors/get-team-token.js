import db from "../../../adapters/database";

export const getTeamToken = async (teamId) =>
  db.Token.findOne({ where: { team_id: teamId, active: true } });
