import db from "../../../adapters/database";

export const getProfileToken = async (profileId) =>
  db.Token.findOne({ where: { profile_id: profileId, active: true } });
