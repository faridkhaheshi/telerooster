import db from "../../../adapters/database";

export const addProfileToken = async ({ profile, token }) => {
  const { id, user_id } = profile;
  token.user_id = user_id;
  const [theToken, created] = await db.Token.findOrCreate({
    where: { profile_id: id },
    defaults: token,
  });
  if (created) return theToken;
  const updatedToken = await theToken.update(token);
  return updatedToken;
};
