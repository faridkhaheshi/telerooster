import db from "../../../adapters/database";

export const addTeamToken = async ({ team, token, user }) => {
  token.user_id = user.id;
  const [theToken, created] = await db.Token.findOrCreate({
    where: { team_id: team.id },
    defaults: token,
  });
  if (created) return theToken;
  const updatedToken = await theToken.update(token);
  return updatedToken;
};
