import db from "../../../adapters/database";

export const findToken = async (query) => {
  query.active = true;
  return db.Token.findOne({ where: query });
};
