import { findToken } from "../processors";

export const handleGetTokenRequests = async (req, res) => {
  try {
    const { query } = req;
    const token = await findToken(query);
    return res.json(token);
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
