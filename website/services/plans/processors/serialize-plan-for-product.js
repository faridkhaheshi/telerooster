import db from "../../../adapters/database";

export const serializePlanForProduct = async ({
  productIdExt,
  expiresAt,
  userId,
  updateRef,
}) => {
  const [plan, created] = await db.Plan.findOrCreate({
    where: {
      user_id: userId,
      product_id_ext: productIdExt,
    },
    defaults: {
      status: "active",
      type: "creator",
      expires_at: expiresAt,
      update_ref: updateRef,
    },
  });

  if (!created)
    await plan.update({
      status: "active",
      type: "creator",
      expires_at: expiresAt,
      update_ref: updateRef,
    });
  return plan;
};
