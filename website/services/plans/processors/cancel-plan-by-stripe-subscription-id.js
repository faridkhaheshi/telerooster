import { NotFoundError } from "restify-errors";
import db from "../../../adapters/database";

export const cancelPlanByStripeSubscriptionId = async (
  stripeSubscriptionId
) => {
  const plan = await db.Plan.findOne({
    where: { "update_ref.subscriptionId": stripeSubscriptionId },
  });
  if (!plan)
    throw new NotFoundError(
      `No plan found for subscription id ${stripeSubscriptionId}`
    );
  return plan.update({ status: "canceled" });
};
