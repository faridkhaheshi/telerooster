import { updateAllChannelsInfo } from "../processors/update-all-channels-info";

export const handleUpdateAllChannelsInfoRequest = async (req, res) => {
  try {
    const total = await updateAllChannelsInfo();
    return res.json({ done: true, total });
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
