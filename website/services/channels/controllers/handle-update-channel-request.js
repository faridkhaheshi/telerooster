import { NotFoundError } from "restify-errors";
import { findChannel } from "../processors";

export const handleUpdateChannelRequest = async (req, res) => {
  try {
    const {
      body: { query, updateData },
    } = req;

    const channel = await findChannel({ where: query });
    if (channel) {
      const updatedChannel = await channel.update(updateData);
      return res.json({ done: true, channel: updatedChannel });
    }

    throw new NotFoundError("Channel not found");
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
