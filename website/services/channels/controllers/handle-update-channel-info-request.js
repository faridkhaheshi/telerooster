import { updateChannelInfo } from "../processors";

export const handleUpdateChannelInfoRequest = async (req, res) => {
  try {
    const {
      query: { id },
    } = req;
    const updatedChannel = await updateChannelInfo(id);
    return res.json(updatedChannel);
  } catch (err) {
    console.error(err);
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
