import { getValidToken } from "../../tokens/processors";
import {
  addSubscriptionChannelForTeam,
  updateChannelInfo,
} from "../processors";
import { raiseEvent } from "./../../../adapters/dispatcher";

export const handleCreateChannelRequest = async (req, res) => {
  try {
    const {
      body: { subscriptionId, teamId, profileId, platform },
    } = req;
    const token = await getValidToken({ teamId, profileId, platform });
    if (!token) throw new Error("No permission to add new channel to team");
    const channel = await addSubscriptionChannelForTeam({
      token,
      subscriptionId,
      teamId,
      platform,
    });

    await updateChannelInfo(channel.id);
    await raiseEvent({
      event: "channel-created",
      payload: {
        channelId: channel.id,
        subscriptionId,
        teamId,
      },
    });
    const { conversation_ref, ...publicInfo } = channel.toJSON();
    return res.json(publicInfo);
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
