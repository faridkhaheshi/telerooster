import { findChannel } from "../processors";

export const getChannelInfo = async (req, res) => {
  try {
    const {
      query: { id },
    } = req;
    const channel = await findChannel({
      where: { id },
      attributes: { exclude: ["conversation_ref"] },
    });
    return res.json(channel);
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
