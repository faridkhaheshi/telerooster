import channel from "../../../adapters/database/models/channel";
import { createTeamsChannel } from "../../../adapters/microsoft-graph";
import { parseMsTeamsChannel } from "../../../adapters/msteams";
import { createSlackChannel } from "../../../adapters/slackbot/create-slack-channel";
import { serializeChannel } from "./serialize-channel";

export const addChannelForTeam = async ({
  channelData,
  team,
  platform,
  token,
}) => {
  if (platform === "microsoft") {
    const teamsChannel = await createTeamsChannel({
      accessToken: token,
      teamAadGroupId: team.info.aadGroupId,
      displayName: channelData.name,
      description: channelData.description,
    });
    const channelInfo = parseMsTeamsChannel(teamsChannel, team);
    channelInfo.team_id = team.id;
    const channel = await serializeChannel(channelInfo);
    return channel;
  }
  if (platform === "slack") {
    const channelInfo = await createSlackChannel({
      installation: token,
      teamId: team.id_ext,
      channelData,
    });
    channelInfo.team_id = team.id;
    const channel = await serializeChannel(channelInfo);
    return channel;
  }
  return null;
};
