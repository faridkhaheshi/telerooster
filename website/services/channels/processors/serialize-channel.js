import db from "../../../adapters/database";

export const serializeChannel = async (channelData) => {
  const { platform, id_ext, ...otherData } = channelData;
  const [channel] = await db.Channel.findOrCreate({
    where: { platform, id_ext },
    defaults: otherData,
  });

  return channel;
};
