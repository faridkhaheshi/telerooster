import db from "../../../adapters/database";

export const saveChannelReport = (report) => db.ChannelReport.create(report);
