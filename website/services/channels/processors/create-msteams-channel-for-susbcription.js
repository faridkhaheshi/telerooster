import { createTeamsChannel } from "../../../adapters/microsoft-graph";
import { parseMsTeamsChannel } from "../../../adapters/msteams";
import { connectSubscriptionToChannel } from "../../pipes/processors";
import { serializeChannel } from "./serialize-channel";

export const createMsTeamsChannelForSubscription = async ({
  subscription,
  team,
  accessToken,
}) => {
  const teamsChannel = await createTeamsChannel({
    accessToken,
    teamAadGroupId: team.info.aadGroupId,
    displayName: subscription.pool.title,
    description:
      subscription?.pool?.description ||
      `This channel is created by teleRooster for receiving messages from ${subscription.pool.title}`,
  });
  const channelData = parseMsTeamsChannel(teamsChannel, team);
  channelData.team_id = team.id;
  const channel = await serializeChannel(channelData);
  const pipe = await connectSubscriptionToChannel({
    channelId: channel.id,
    subscriptionId: subscription.id,
  });
  subscription.status = "ready";
  await subscription.save();
  return channel;
};
