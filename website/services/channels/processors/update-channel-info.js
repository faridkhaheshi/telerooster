import {
  NotFoundError,
  NotImplementedError,
  UnauthorizedError,
} from "restify-errors";
import { sendInternalApiRequest } from "../../../adapters/api";
import { getTeamToken } from "../../tokens/processors";
import { findChannelById } from "./find-channel-by-id";
import { saveChannelReport } from "./save-channel-report";

export const updateChannelInfo = async (channelId) => {
  const channel = await findChannelById(channelId);
  if (!channel) throw new NotFoundError("channel not found");
  if (channel.platform !== "slack")
    throw new NotImplementedError(
      "platforms other than slack are not supported yet"
    );
  const token = await getTeamToken(channel.team_id);
  if (!token) throw new UnauthorizedError("slack token not found");
  let reportInfo;
  try {
    const updatedChannelStatus = await sendInternalApiRequest(
      `${process.env.SLACKBOT_API_BASE_URL}/channels/${channel.id_ext}?token=${token.info.bot.token}`
    );
    reportInfo = updatedChannelStatus.info;
  } catch (error) {
    if (error instanceof NotFoundError) {
      reportInfo = { active: false };
    } else throw error;
  }

  await saveChannelReport({
    channel_id: channel.id,
    info: reportInfo,
  });
  const newInfo = { ...channel.info, ...reportInfo };
  const updatedChannel = await channel.update({
    info: newInfo,
    active: reportInfo.active === false ? false : true,
  });
  return updatedChannel;
};
