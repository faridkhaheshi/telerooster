import Sequelize from "sequelize";
import db from "../../../adapters/database";
import { updateChannelInfo } from "./update-channel-info";

const Op = Sequelize.Op;

export const updateAllChannelsInfo = async () => {
  const startTime = new Date();
  let total = 0;
  const pageSize = 1;
  let pageNumber = 0;
  while (true) {
    const channels = await db.Channel.findAll({
      where: {
        active: true,
        createdAt: {
          [Op.lt]: startTime,
        },
      },
      offset: pageNumber * pageSize,
      limit: pageSize,
      order: [["createdAt", "DESC"]],
    });
    pageNumber++;
    if (!Array.isArray(channels) || channels.length === 0) break;
    try {
      await updateChannelInfo(channels[0].id);
    } catch (err) {}

    total += channels.length;
  }

  return total;
};
