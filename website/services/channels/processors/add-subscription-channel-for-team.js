import { connectSubscriptionToChannel } from "../../pipes/processors";
import { findSubscriptionById } from "../../subscriptions/processors";
import { findTeamById } from "../../teams/processors";
import { addChannelForTeam } from "./add-channel-for-team";

export const addSubscriptionChannelForTeam = async ({
  token,
  subscriptionId,
  teamId,
  platform,
}) => {
  const subscription = await findSubscriptionById(subscriptionId);
  const team = await findTeamById(teamId);
  const channelData = {
    name: subscription.pool.title,
    description:
      subscription?.pool?.description ||
      `This channel is created by teleRooster for receiving messages from ${subscription.pool.title}`,
  };
  const channel = await addChannelForTeam({
    channelData,
    team,
    platform,
    token,
  });
  const pipe = await connectSubscriptionToChannel({
    channelId: channel.id,
    subscriptionId,
  });
  subscription.status = "ready";
  await subscription.save();
  return channel;
};
