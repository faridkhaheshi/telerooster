import db from "../../../adapters/database";

export const findChannel = (options) => db.Channel.findOne(options);
