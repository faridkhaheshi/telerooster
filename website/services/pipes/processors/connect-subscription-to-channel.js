import db from "../../../adapters/database";

export const connectSubscriptionToChannel = async ({
  subscriptionId,
  channelId,
}) => {
  const [pipe] = await db.Pipe.upsert(
    { subscription_id: subscriptionId, channel_id: channelId, active: true },
    { returning: true }
  );
  return pipe;
};
