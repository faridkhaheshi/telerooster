import db from "../../../adapters/database";

export const serializeCustomer = async ({ provider, customerInfo, userId }) => {
  const { id: idExt } = customerInfo;
  const [customer, created] = await db.Customer.findOrCreate({
    where: { provider, id_ext: idExt },
    defaults: { user_id: userId, info: customerInfo },
  });
  if (!created)
    await customer.update({
      active: true,
      user_id: userId,
      info: customerInfo,
    });
  return customer;
};
