import db from "../../../adapters/database";

export const findCustomerByIdExt = (idExt) =>
  db.Customer.findOne({ where: { id_ext: idExt, active: true } });
