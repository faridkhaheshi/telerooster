import { v2 as cloudinary } from "cloudinary";

const baseParams = {};

export const generateUploadSignature = (params = {}) => {
  const timestamp = Math.round(new Date().getTime() / 1000);
  const finalParams = { ...params, ...baseParams };
  const signature = cloudinary.utils.api_sign_request(
    { timestamp, ...finalParams },
    process.env.CLOUDINARY_API_SECRET
  );
  const uploadUrl = `https://api.cloudinary.com/v1_1/${process.env.CLOUDINARY_CLOUD_NAME}/image/upload`;
  return {
    url: uploadUrl,
    params: {
      signature,
      timestamp,
      api_key: process.env.CLOUDINARY_API_KEY,
      ...finalParams,
    },
  };
};
