import { generateUploadSignature } from "../processors";

export const handleImageUploadRequest = async (req, res) => {
  try {
    const { user, query } = req;
    const result = generateUploadSignature(query);
    return res.json(result);
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
