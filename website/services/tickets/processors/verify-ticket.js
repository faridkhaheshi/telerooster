import redisClient from "../../../adapters/redis/redis-client";
import { getDataRedisKey, getTicketRedisKey } from "./redis-info";

export const verifyTicket = async ({ ticket, prefix }) => {
  let output;
  const ticketRedisKey = getTicketRedisKey(prefix);
  const dataRedisKey = getDataRedisKey({ prefix, ticket });
  try {
    const savedTicket = await redisClient.getAsync(ticketRedisKey);
    if (savedTicket != ticket) throw new Error("wrong ticket");
    output = await redisClient.getAsync(dataRedisKey);
    if (!output) throw new Error("empty ticket");
  } catch (error) {
    output = null;
  } finally {
    await redisClient.delAsync(ticketRedisKey);
    await redisClient.delAsync(dataRedisKey);
    return output;
  }
};
