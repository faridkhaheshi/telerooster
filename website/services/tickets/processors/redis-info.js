export const ticketExpirationHours = 12;
export const ticketExpirationSec = ticketExpirationHours * 60 * 60;
export const getTicketRedisKey = (prefix) => `TICKET:${prefix}`;
export const getDataRedisKey = ({ prefix, ticket }) =>
  `TICKET:${prefix}:${ticket}`;
export const getStateRedisKey = (key) => `STATE:${key}`;
