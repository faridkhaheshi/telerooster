import { v4 as uuidv4 } from "uuid";
import redisClient from "../../../adapters/redis/redis-client";
import { getStateRedisKey, ticketExpirationSec } from "./redis-info";

export const generateState = async (secretObject) => {
  const uuid = uuidv4();
  const ticket = Buffer.from(uuid).toString("base64");
  await redisClient.setexAsync(
    getStateRedisKey(ticket),
    ticketExpirationSec,
    JSON.stringify(secretObject)
  );
  return ticket;
};

export const fetchState = async (ticket) => {
  const secretString = await redisClient.getAsync(getStateRedisKey(ticket));
  if (!secretString) return null;
  return JSON.parse(secretString);
};
