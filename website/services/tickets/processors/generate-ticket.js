import redisClient from "../../../adapters/redis/redis-client";
import { generateRandomCode } from "../utils";
import {
  getDataRedisKey,
  getTicketRedisKey,
  ticketExpirationSec,
} from "./redis-info";

export const generateTicket = async ({ prefix = "general", secretString }) => {
  const ticket = generateRandomCode();
  await redisClient.setexAsync(
    getTicketRedisKey(prefix),
    ticketExpirationSec,
    `${ticket}`
  );
  await redisClient.setexAsync(
    getDataRedisKey({ prefix, ticket }),
    ticketExpirationSec,
    `${secretString}`
  );

  return ticket;
};
