export const generateRandomCode = (numberOfDigits = 6) => {
  const smallest = Math.pow(10, numberOfDigits - 1);
  const largest = Math.pow(10, numberOfDigits);

  return Math.floor(smallest + Math.random() * (largest - smallest)).toString();
};
