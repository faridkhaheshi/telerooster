import { loginUser } from "../../auth/processors";
import { runJob } from "../../jobs/processors";
import { verifyTicket } from "../processors";

export const handleTicketVerificationRequest = async (req, res) => {
  try {
    const {
      query: { ticket },
      body: { email },
    } = req;
    if (!ticket || !email)
      return res
        .status(400)
        .json({ error: { message: "missing required data" } });
    const secretString = await verifyTicket({ prefix: email, ticket });
    if (!secretString)
      return res
        .status(401)
        .json({ error: { message: "incorrect authentication info" } });

    const { job, payload } = JSON.parse(secretString);

    let redirectTo = "/";
    if (job) {
      redirectTo = await runJob(job, payload);
    }
    return res.json({ ok: true, redirectTo });
  } catch (error) {
    return res.status(error.status || 500).json({ error });
  }
};
