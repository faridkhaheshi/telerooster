import db from "../../../adapters/database";

export const findTeamByIdExt = async ({ idExt, platform }) =>
  db.Team.findOne({
    where: { id_ext: idExt, platform, active: true },
  });

export const findTeamByIdExtWithToken = async ({
  platform,
  idExt,
  profileIdExt,
}) =>
  db.Team.findOne({
    where: { id_ext: idExt, platform, active: true },
    attributes: { exclude: ["info", "conversation_ref"] },
    include: [
      {
        model: db.Token,
        as: "token",
        attributes: ["id", "info"],
        where: { active: true },
      },
      {
        model: db.Profile,
        required: false,
        where: { id_ext: profileIdExt },
        attributes: ["id", "id_ext"],
        through: {
          model: db.TeamAdmin,
          where: {
            active: true,
          },
          attributes: [],
        },
      },
    ],
  });
