import db from "../../../adapters/database";

export const serializeTeam = async (teamData) => {
  const [team] = await db.Team.upsert(teamData, { returning: true });
  return team;
};
