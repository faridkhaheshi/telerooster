import moment from "moment";
import db from "../../../adapters/database";

export const findUserAudience = async ({ userId }) => {
  const activePipes = await db.Pipe.findAll({
    where: { active: true },
    include: [
      {
        model: db.Subscription,
        as: "subscription",
        where: { active: true },
        include: {
          model: db.Pool,
          as: "pool",
          where: { active: true, creator_id: userId },
          attributes: ["id", "creator_id", "title", "featured"],
        },
      },
      {
        model: db.Channel,
        as: "channel",
        attributes: ["id", "platform", "name", "team_id", "info", "createdAt"],
        where: { active: true },
        include: {
          model: db.Team,
          as: "team",
          where: { active: true },
          attributes: ["id", "platform", "name", "added_by", "info"],
        },
      },
    ],
  });
  const subscriptionChannels = {};
  activePipes.forEach(
    ({
      subscription: {
        id: subscriptionId,
        pool: { id: poolId, title },
      },
      channel: {
        id: channelId,
        platform,
        name: channelName,
        createdAt: channelCreationTimestamp,
        team: {
          id: teamId,
          name: teamName,
          info: { memberCount: teamMemberCount },
        },
        info: channelInfo,
      },
    }) => {
      const channelCreationTime = moment(channelCreationTimestamp).unix();
      const channelMemberCount = channelInfo.memberCount || teamMemberCount;
      if (subscriptionId in subscriptionChannels) {
        const prevChannelInfo = subscriptionChannels[subscriptionId];
        if (prevChannelInfo.channelCreationTime >= channelCreationTime) {
          return;
        }
      }
      subscriptionChannels[subscriptionId] = {
        id: subscriptionId,
        platform,
        channelCreationTime,
        name: teamName,
        teamMemberCount,
        channelMemberCount,
        pool: { id: poolId, title },
      };
    }
  );

  return Object.keys(subscriptionChannels).map(
    (subId) => subscriptionChannels[subId]
  );
};
