import db from "../../../adapters/database";

export const saveTeamReport = (report) => db.TeamReport.create(report);
