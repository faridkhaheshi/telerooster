import db from "../../../adapters/database";

export const addAdminToTeam = async ({ team, profile }) => {
  const [rel] = await db.TeamAdmin.upsert(
    { team_id: team.id, profile_id: profile.id, active: true },
    { returning: true }
  );
  return rel;
};
