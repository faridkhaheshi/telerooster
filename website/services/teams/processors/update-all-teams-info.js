import Sequelize from "sequelize";
import db from "../../../adapters/database";
import { updateTeamInfo } from "./update-team-info";

const Op = Sequelize.Op;

export const updateAllTeamsInfo = async () => {
  const startTime = new Date();
  let total = 0;
  const pageSize = 1;
  let pageNumber = 0;
  while (true) {
    const teams = await db.Team.findAll({
      where: {
        active: true,
        createdAt: {
          [Op.lt]: startTime,
        },
      },
      offset: pageNumber * pageSize,
      limit: pageSize,
      order: [["createdAt", "DESC"]],
    });
    pageNumber++;
    if (!Array.isArray(teams) || teams.length === 0) break;
    try {
      await updateTeamInfo(teams[0].id);
    } catch (error) {}

    total += teams.length;
  }

  return total;
};
