import {
  NotFoundError,
  NotImplementedError,
  UnauthorizedError,
} from "restify-errors";
import { sendInternalApiRequest } from "../../../adapters/api";
import { getTeamToken } from "../../tokens/processors";
import { findTeamById } from "./find-team-by-id";
import { saveTeamReport } from "./save-team-report";

export const updateTeamInfo = async (teamId) => {
  const team = await findTeamById(teamId);
  if (!team) throw new NotFoundError("team not found");
  if (team.platform !== "slack")
    throw new NotImplementedError(
      "platforms other than slack are not supported yet"
    );
  const token = await getTeamToken(team.id);
  if (!token) throw new UnauthorizedError("slack token not found");
  const updatedTeamStatus = await sendInternalApiRequest(
    `${process.env.SLACKBOT_API_BASE_URL}/teams/${team.id_ext}?token=${token.info.bot.token}`
  );
  await saveTeamReport({
    team_id: team.id,
    info: updatedTeamStatus.info,
  });

  const newInfo = { ...team.info, ...updatedTeamStatus.info };
  const updatedTeam = await team.update({ info: newInfo });

  return updatedTeam;
};
