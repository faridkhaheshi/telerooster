import { NotFoundError } from "restify-errors";
import db from "../../../adapters/database";

export const deactivateUserTeam = async ({ userId, teamId }) => {
  const team = await db.Team.findByPk(teamId, {
    include: { model: db.Profile, as: "added_by_profile" },
  });
  if (team && team.added_by_profile.user_id === userId)
    return team.update({ active: false });
  throw new NotFoundError("Team not found");
};
