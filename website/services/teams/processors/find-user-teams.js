import db from "../../../adapters/database";

export const findUserTeams = async ({ userId, query = {} }) => {
  const profiles = await db.Profile.findAll({
    where: { user_id: userId },
    attributes: ["id", "provider", "info"],
    include: {
      model: db.Team,
      attributes: ["id", "name", "platform", "createdAt"],
      where: {
        active: true,
        ...query,
      },
      through: {
        model: db.TeamAdmin,
        where: {
          active: true,
        },
        attributes: ["id", "profile_id"],
      },
    },
  });
  const teams = profiles.reduce(
    (acc, prof) => [
      ...acc,
      ...prof.Teams.map((t) => ({
        ...t.toJSON(),
        TeamAdmin: {
          profile_id: prof.id,
          info: { displayName: prof.info.displayName, email: prof.info.email },
        },
      })),
    ],
    []
  );
  return teams;
};
