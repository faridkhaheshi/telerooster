import { findUserAudience } from "../processors";

export const getUserAudience = async (req, res) => {
  try {
    const teams = await findUserAudience({ userId: req.user.id });
    const total = teams.reduce(
      (acc, { channelMemberCount }) => acc + channelMemberCount,
      0
    );
    return res.json({ total, teams });
  } catch (err) {
    console.error(err);
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
