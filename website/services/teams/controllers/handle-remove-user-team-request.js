import { BadRequestError } from "restify-errors";
import { deactivateUserTeam } from "../processors";

export const handleRemoveUserTeamRequest = async (req, res) => {
  try {
    const {
      user: { id: userId },
      query: { id: teamId },
    } = req;
    if (!userId) throw new BadRequestError("authentication is required...");
    await deactivateUserTeam({ userId, teamId });
    return res.json({ done: true });
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
