import { createMsTeamsChannelForSubscription } from "../../channels/processors";
import { findSubscriptions } from "../../subscriptions/processors";
import { getValidProfileToken } from "../../tokens/processors";

export const addMsTeamsTeamSubscriptionChannels = async (req, res) => {
  try {
    const {
      body: { team, profile },
    } = req;
    const accessToken = await getValidProfileToken(profile.id);

    if (!accessToken) return res.json([]);
    const waitingSubscriptions = await findSubscriptions({
      active: true,
      status: "waiting",
      user_id: profile.user.id,
    });
    if (!waitingSubscriptions) return res.json([]);
    const channels = await Promise.all(
      waitingSubscriptions.map((subscription) =>
        createMsTeamsChannelForSubscription({
          subscription,
          team,
          accessToken,
        })
      )
    );
    return res.json(channels);
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
