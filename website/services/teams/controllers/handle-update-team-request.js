import { NotFoundError } from "restify-errors";
import { findTeamById } from "../processors";

export const handleUpdateTeamRequest = async (req, res) => {
  try {
    const {
      body: updateData,
      query: { id: teamId },
    } = req;
    const team = await findTeamById(teamId);
    if (!team) throw new NotFoundError("Team not found");
    const updatedTeam = await team.update(updateData);
    return res.json({ done: true, team: updatedTeam });
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
