import { updateAllTeamsInfo } from "../processors";

export const handleUpdateAllTeamsInfoRequest = async (req, res) => {
  try {
    const total = await updateAllTeamsInfo();
    return res.json({ done: true, total });
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
