import { findTeamByIdExtWithToken } from "../processors";

export const fetchSlackTeamTokenInfo = async (req, res) => {
  try {
    const {
      query: { idExt, userId: profileIdExt },
    } = req;
    const {
      id: teamId,
      name: teamName,
      added_by: creatorProfileId,
      token: { info: installation },
      Profiles: [adminProfile],
    } = await findTeamByIdExtWithToken({
      platform: "slack",
      idExt,
      profileIdExt,
    });
    return res.json({
      ...installation,
      context: {
        roosterInfo: {
          teamId,
          teamName,
          creatorProfileId,
          adminProfile: adminProfile || null,
        },
        isAdmin: adminProfile ? true : false,
      },
    });
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
