import { parseMsTeamsTeam } from "../../../adapters/msteams/parse-msteams-team";
import { parseMicrosoftProfile } from "../../auth/microsoft/processors";
import { serializeProfile } from "../../profiles/processors";
import { addAdminToTeam, serializeTeam } from "../processors";

export const handleMsTeamsNewInstall = async (req, res) => {
  try {
    const {
      body: {
        team,
        teamConversationRef,
        channels,
        admin,
        adminConversationRef,
      },
    } = req;

    const profile = await serializeProfile(
      parseMicrosoftProfile(
        { ...admin, conversationRef: adminConversationRef },
        "bot-install"
      )
    );
    const teamData = parseMsTeamsTeam(
      { ...team, conversationRef: teamConversationRef },
      profile,
      channels
    );
    teamData.active = true;
    const theTeam = await serializeTeam(teamData);
    await addAdminToTeam({
      profile: profile,
      team: theTeam,
    });
    return res.json({ team: theTeam, profile });
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
