import { BadRequestError } from "restify-errors";
import { findUserTeams } from "../processors";

export const getUserTeams = async (req, res) => {
  try {
    const {
      user: { id: userId },
      query,
    } = req;
    if (!userId) throw new BadRequestError("authentication is required...");
    const teams = await findUserTeams({
      userId,
      query,
    });
    return res.json(teams);
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
