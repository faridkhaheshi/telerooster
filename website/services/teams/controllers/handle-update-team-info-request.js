import { updateTeamInfo } from "../processors";

export const handleUpdateTeamInfoRequest = async (req, res) => {
  try {
    const {
      query: { id },
    } = req;
    const updatedTeam = await updateTeamInfo(id);
    return res.json(updatedTeam);
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
