import { raiseEvent } from "../../../adapters/dispatcher/raise-event";
import { loginUser } from "../../auth/processors";
import { serializeProfile } from "../../profiles/processors";
import { addTeamToken } from "../../tokens/processors";
import { addAdminToTeam, serializeTeam } from "../processors";

export const handleSlackNewInstall = async (req, res) => {
  try {
    const {
      body: { profileData, teamData, tokenData },
      user,
    } = req;
    profileData.user = user;
    const profile = await serializeProfile(profileData);

    teamData.added_by = profile.id;
    teamData.active = true;
    const team = await serializeTeam(teamData);
    await addAdminToTeam({ profile, team });
    await addTeamToken({
      user: profile.user,
      team,
      token: tokenData,
    });

    const token = await loginUser(profile.user);
    await raiseEvent({
      event: "new-team",
      payload: {
        platform: "slack",
        teamId: team.id,
        profileId: profile.id,
        userId: profile.user.id,
      },
    });
    return res.json({ team, profile, token });
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
