import { findUserById } from "./find-user-by-id";

const fieldMap = {
  firstName: "first_name",
  lastName: "last_name",
  displayName: "display_name",
  passwordHash: "password_hash",
  active: "active",
};

const supportedFields = Object.keys(fieldMap);

export const updateUserById = async (id, updateObject = {}) => {
  const user = await findUserById(id);
  const update = {};
  Object.keys(updateObject)
    .filter((key) => supportedFields.indexOf(key) > -1)
    .forEach((key) => {
      update[fieldMap[key]] = updateObject[key];
    });
  const updated = await user.update(update);
  return updated;
};
