import db from "../../../adapters/database";

export const createUser = async ({ email, passwordHash }) => {
  try {
    const user = await db.User.create({ email, password_hash: passwordHash });
    const output = user.toJSON();
    delete output["password_hash"];
    return output;
  } catch (error) {
    throw handleCreateUserError(error);
  }
};

export function handleCreateUserError(error) {
  const {
    errors: [err],
  } = error;
  const { type, message } = err;
  let status = 500;
  if (type === "Validation error") status = 400;
  else if (type === "unique violation") status = 409;
  return { status, message };
}
