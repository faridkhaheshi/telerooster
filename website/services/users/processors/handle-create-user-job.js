import { loginUser } from "../../auth/processors";
import { createUser } from "./create-user";

export const handleCreateUserJob = async ({ email, passwordHash, ref }) => {
  const user = await createUser({ email, passwordHash });
  const token = await loginUser(user);
  if (ref) {
    return `/auth?token=${token}&ref=${encodeURIComponent(ref)}`;
  }
  return `/auth?token=${token}`;
};
