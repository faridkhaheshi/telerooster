import redisClient from "../../../adapters/redis/redis-client";
import { getPostInstallEventsRedisKey } from "../utils/post-install-events-redis-info";

export const findUserPostInstallEvents = ({ userId, platform }) =>
  new Promise((resolve, reject) => {
    {
      const redisKey = getPostInstallEventsRedisKey({ userId, platform });
      redisClient
        .multi()
        .lrange(redisKey, 0, -1)
        .del(redisKey)
        .exec((err, replies) => {
          if (err) return reject(err);
          const events = replies[0];
          resolve(events.map((e) => JSON.parse(e)));
        });
    }
  });
