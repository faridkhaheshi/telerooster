import db from "../../../adapters/database";

export const fillMissingUserInfo = async (id, userInfo) => {
  const user = await db.User.findByPk(id);
  const updateData = buildUpdateData(user, userInfo);
  return await user.update(updateData);
};

function buildUpdateData(user, userInfo) {
  const updateData = {};
  Object.keys(userInfo).forEach((key) => {
    if (user[key]) return;
    updateData[key] = userInfo[key];
  });
  return updateData;
}
