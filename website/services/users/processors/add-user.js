import db from "../../../adapters/database";
import { handleCreateUserError } from "./create-user";

export const addUser = async (userInfo) => {
  try {
    const user = await db.User.create(userInfo);
    const output = user.toJSON();
    delete output["password_hash"];
    return output;
  } catch (error) {
    throw handleCreateUserError(error);
  }
};
