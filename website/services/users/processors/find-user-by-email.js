import db from "../../../adapters/database";

export const findUserByEmail = async (email) => {
  try {
    const user = await db.User.findOne({
      where: { email, active: true },
    });
    return user.toJSON() || null;
  } catch (error) {
    return null;
  }
};
