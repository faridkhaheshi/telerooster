import db from "../../../adapters/database";

const defaultInclude = [
  {
    model: db.Profile,
    as: "profiles",
    attributes: ["id", "provider", "createdAt"],
  },
  {
    model: db.Token,
    as: "tokens",
    attributes: ["id", "provider", "active", "createdAt"],
  },
  {
    model: db.Customer,
    as: "customers",
    attributes: ["id", "provider", "active", "createdAt"],
  },
  {
    model: db.Plan,
    as: "plans",
    attributes: ["id", "status", "type", "expires_at", "createdAt"],
  },
];

export const findUserById = async (id, include = defaultInclude) =>
  db.User.findOne({
    where: { id, active: true },
    attributes: { exclude: ["password_hash"] },
    include,
  });
