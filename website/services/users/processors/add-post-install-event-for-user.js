import redisClient from "../../../adapters/redis/redis-client";
import {
  expirationSecs,
  getPostInstallEventsRedisKey,
} from "../utils/post-install-events-redis-info";

export const addPostInstallEventForUser = ({
  userId,
  event,
  payload,
  platform,
}) =>
  new Promise((resolve, reject) => {
    const redisKey = getPostInstallEventsRedisKey({ userId, platform });
    redisClient
      .multi()
      .lpush(redisKey, JSON.stringify({ event, payload }))
      .expire(redisKey, expirationSecs)
      .exec((err, replies) => {
        if (err) return reject(err);
        return resolve(replies[0]);
      });
  });
