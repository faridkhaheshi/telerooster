import db from "../../../adapters/database";

export const updateUserDisplayName = async (userId, displayName) => {
  const user = await db.User.findByPk(userId, {
    attributes: { exclude: ["password_hash"] },
  });
  const updated = await user.update({ display_name: displayName });

  return updated;
};
