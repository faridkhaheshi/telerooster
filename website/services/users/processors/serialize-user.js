import db from "../../../adapters/database";

export const serializeUser = async (userData) => {
  const { email, ...otherData } = userData;
  const [user] = await db.User.findOrCreate({
    where: { email, active: true },
    defaults: otherData,
  });
  return user;
};
