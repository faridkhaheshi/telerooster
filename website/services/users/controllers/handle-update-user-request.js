import { hashPassword } from "../../auth/processors";
import { updateUserById } from "../processors";

export const handleUpdateUserRequest = async (req, res) => {
  try {
    const {
      body: { password, ...otherInfo },
      query: { id: userId },
    } = req;
    if (password) otherInfo.passwordHash = await hashPassword(password);
    const updatedUser = await updateUserById(userId, otherInfo);
    return res.json(updatedUser);
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
