import { findUserPostInstallEvents } from "../processors";

export const getPostInstallEventsForUser = async (req, res) => {
  try {
    const {
      query: { id: userId, platform },
    } = req;
    const events = await findUserPostInstallEvents({ userId, platform });
    return res.json(events);
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
