import { hashPassword } from "../../auth/processors";
import { updateUserById } from "../processors";

export const changeUserPassword = async (req, res) => {
  try {
    const {
      body: { password },
      user: { id: userId },
    } = req;
    const passwordHash = await hashPassword(password);
    const updatedUser = await updateUserById(userId, { passwordHash });

    return res.json({ done: true, user: updatedUser });
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
