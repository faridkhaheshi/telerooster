import { updateUserById } from "../processors";

export const handleEditProfileRequest = async (req, res) => {
  try {
    const {
      user: { id: userId },
      body: { firstName, lastName, displayName },
    } = req;

    const updatedUser = await updateUserById(userId, {
      firstName,
      lastName,
      displayName,
    });
    return res.json({ done: true, user: updatedUser });
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
