import { NotFoundError } from "restify-errors";
import { findUserById } from "../processors";

export const getTokenUser = async (req, res) => {
  try {
    const { user: { id: userId } = {} } = req;
    if (!userId) throw new NotFoundError("user not found");
    const user = await findUserById(userId);
    if (!user) throw new NotFoundError("user not found");
    return res.json(user);
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
