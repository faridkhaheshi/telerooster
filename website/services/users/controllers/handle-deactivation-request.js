import { updateUserById } from "../processors";

export const handleDeactivationRequest = async (req, res) => {
  try {
    const {
      user: { id: userId },
    } = req;
    await updateUserById(userId, { active: false });
    return res.json({ done: true });
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
