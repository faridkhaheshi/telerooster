import { BadRequestError, ConflictError } from "restify-errors";
import { hashPassword } from "../../auth/processors";
import { normalizeEmail } from "../../auth/utils";
import { createUser, findUserByEmail } from "../processors";

export const handleAddUserRequest = async (req, res) => {
  try {
    const {
      body: { email, password },
    } = req;
    if (!email || !password)
      throw new BadRequestError("email and password are required");

    const normalizedEmail = normalizeEmail(email);
    const user = await findUserByEmail(normalizedEmail);

    if (user) throw new ConflictError("already exist");
    const passwordHash = await hashPassword(password);
    const theUser = await createUser({ email: normalizedEmail, passwordHash });

    return res.json(theUser);
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
