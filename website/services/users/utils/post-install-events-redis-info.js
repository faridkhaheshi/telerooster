export const getPostInstallEventsRedisKey = ({ userId, platform }) =>
  `POST_INSTALL:${platform}:${userId}`;

export const expirationHours = 12;
export const expirationSecs = expirationHours * 60 * 60;
