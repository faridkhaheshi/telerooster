import mailgun from "mailgun-js";

const mg = mailgun({
  apiKey: process.env.MAILGUN_API_KEY,
  domain: process.env.MAILGUN_DOMAIN,
});

export const sendAnEmail = ({
  to,
  subject = undefined,
  text = undefined,
  html = undefined,
}) => {
  const mailData = {
    from: process.env.MAILGUN_SENDER,
    to,
    subject,
    text,
    html,
  };
  return mg.messages().send(mailData);
};
