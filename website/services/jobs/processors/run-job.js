import { jobHandlers } from "./job-handlers";

export const runJob = async (job, payload) => {
  try {
    if (Object.keys(jobHandlers).indexOf(job) > -1) {
      const result = await jobHandlers[job](payload);
      return result;
    }
    return "/";
  } catch (error) {
    return "/";
  }
};
