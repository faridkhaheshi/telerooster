import { handleLoginUserJob } from "../../auth/processors";
import { handleCreateUserJob } from "../../users/processors";

export const jobHandlers = {
  "create-user": handleCreateUserJob,
  "login-user": handleLoginUserJob,
};
