import { findContentmail } from "../processors";

export const parseContentmail = async (req, res) => {
  try {
    const {
      query: { address },
    } = req;
    const contentmail = await findContentmail(address);
    if (contentmail) return res.json(contentmail);
    return res.status(404).json({ error: { message: "not found" } });
  } catch (err) {
    return res
      .status(err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
