import { raiseEvent } from "../../../adapters/dispatcher";

export const handleIncomingEmails = async (req, res) => {
  try {
    const {
      body: { mandrill_events },
    } = req;
    const events = JSON.parse(mandrill_events);
    events.forEach(async (e) => {
      await raiseEvent({ event: "content-email-received", payload: e });
    });

    return res.send("ok");
  } catch (err) {
    return res.send("ok");
  }
};
