import randomString from "crypto-random-string";
import db from "../../../adapters/database";

const emailDomain = process.env.RECEPTION_EMAIL_DOMAIN;
const startingLen = 32;
const attemptsForEachLen = 3;

export const generateContentmailForPool = async (poolId) => {
  let addressLen = startingLen;
  let result;
  let attempsForLen = attemptsForEachLen;
  do {
    const randomAddress = randomString({
      length: addressLen,
      type: "alphanumeric",
    });
    const address = `${randomAddress.toLowerCase()}@${emailDomain}`;
    result = await db.Contentmail.findOrCreate({
      where: { address },
      defaults: { active: true, pool_id: poolId, type: "pool" },
    });
    if (attempsForLen > 0) {
      attempsForLen = attempsForLen - 1;
    } else {
      addressLen = addressLen + 1;
      attempsForLen = attemptsForEachLen;
    }
  } while (!result[1]);

  return result[0];
};
