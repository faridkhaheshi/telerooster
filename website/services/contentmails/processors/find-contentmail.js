import db from "../../../adapters/database";

export const findContentmail = async (address) => {
  try {
    const contentmail = await db.Contentmail.findOne({
      where: { address, active: true },
    });
    return contentmail.toJSON() || null;
  } catch (err) {
    return null;
  }
};
