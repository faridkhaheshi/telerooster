import db from "../../../adapters/database";

export const findProviderProfile = async ({ provider, id_ext }) => {
  try {
    const profile = await db.Profile.findOne({
      where: { provider, id_ext },
      include: [
        {
          model: db.User,
          as: "user",
          attributes: [
            "id",
            "display_name",
            "first_name",
            "last_name",
            "email",
            "role",
            "createdAt",
          ],
        },
      ],
    });
    return profile;
  } catch (error) {
    return null;
  }
};
