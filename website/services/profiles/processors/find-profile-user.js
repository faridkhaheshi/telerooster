import db from "../../../adapters/database";

export const findProfileUser = (profileId) =>
  db.Profile.findByPk(profileId, {
    include: {
      model: db.User,
      as: "user",
      where: { active: true },
    },
  });
