import {
  fillMissingUserInfo,
  findUserById,
  serializeUser,
} from "../../users/processors";
import { addProfile } from "./add-profile";
import { findProviderProfile } from "./find-provider-profile";

export const serializeProfile = async ({ userInfo, profileInfo, user }) => {
  const { provider, id_ext } = profileInfo;
  let profile;
  let theUser;
  if (user?.id) theUser = await findUserById(user.id);
  if (theUser) theUser = await fillMissingUserInfo(user.id, userInfo);
  profile = await findProviderProfile({ provider, id_ext });
  if (profile && theUser) {
    profile.user_id = user.id;
    await profile.save();
    const updatedProfile = await findProviderProfile({ provider, id_ext });
    return updatedProfile.toJSON();
  } else if (profile) {
    if (!profile.conversation_ref) {
      profile.conversation_ref = profileInfo.conversation_ref;
      await profile.save();
    }
    return profile.toJSON();
  }
  if (!theUser) {
    theUser = await serializeUser(userInfo);
  }
  profileInfo.user_id = theUser.id;
  profile = await addProfile(profileInfo);
  const exportedProfile = profile.toJSON();
  exportedProfile.user = theUser.toJSON();
  return exportedProfile;
};
