import db from "../../../adapters/database";

export const addProfile = async (profileData) => {
  try {
    const profile = await db.Profile.create(profileData);
    return profile;
  } catch (error) {
    throw handleAddProfileError(error);
  }
};

export function handleAddProfileError(error) {
  const {
    errors: [err],
  } = error;
  const { type, message } = err;
  let status = 500;
  if (type === "Validation error") status = 400;
  else if (type === "unique violation") status = 409;
  return { status, message };
}
