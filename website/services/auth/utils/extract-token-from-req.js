export const extractTokenFromRequest = (req) => {
  try {
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      const tokenInCookie = req.cookies.token;
      return tokenInCookie || null;
    }
    const authHeaderParts = authHeader.split(" ");
    if (authHeaderParts[0] !== "Bearer") throw new Error("");
    return authHeaderParts[1];
  } catch (err) {
    return null;
  }
};

export const extractBearerTokenFromRequest = (req) => {
  const authHeader = req.headers.authorization;
  const authHeaderParts = authHeader.split(" ");
  if (authHeaderParts[0] !== "Bearer")
    throw new Error("Wrong authorization header");
  return authHeaderParts[1];
};

export const extractTokenFromCookies = (req) => {
  try {
    return req.cookies.token;
  } catch (err) {
    return null;
  }
};
