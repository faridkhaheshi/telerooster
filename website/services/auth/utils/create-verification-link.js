import { baseUrl } from './../../../config';

export const createVerificationLink = ({ ticket, email }) =>
  `${baseUrl}/tickets/${ticket}?email=${encodeURIComponent(email)}`;
