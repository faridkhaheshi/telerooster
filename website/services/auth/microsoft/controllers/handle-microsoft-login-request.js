import { createMicrosoftAuthorizationUrl } from "./../processors";
import { addUserMiddleware } from "./../../controllers";
import { generateState } from "../../../tickets/processors";

const handleAuthenticatedLoginRequest = async (req, res) => {
  try {
    const {
      user,
      query: { ref },
    } = req;
    // TODO: check to see of we have a microsoft account for the user.

    const state = ref ? await generateState({ ref }) : undefined;
    const urlParams = state ? { state } : {};

    const authorizationUrl = createMicrosoftAuthorizationUrl(urlParams);

    res.writeHead(302, { Location: authorizationUrl });
    return res.end();
  } catch (err) {
    return res
      .status(err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};

export const handleMicrosoftLoginRequest = addUserMiddleware(
  handleAuthenticatedLoginRequest
);
