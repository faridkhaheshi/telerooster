import { serializeProfile } from "../../../profiles/processors";
import { fetchState } from "../../../tickets/processors";
import { addProfileToken } from "../../../tokens/processors";
import { loginUser } from "../../processors";
import {
  getMicrosoftAuthInfo,
  getMicrosoftProfile,
  parseMicrosoftProfile,
  parseMicrosoftToken,
  processLoginError,
} from "../processors";

export const handleMicrosoftLoginResult = async (req, res) => {
  try {
    const {
      user,
      query: { error, error_description, code, state },
    } = req;
    processLoginError({ error, error_description });
    const authInfo = await getMicrosoftAuthInfo({ code });
    const profileData = await getMicrosoftProfile(authInfo.access_token);
    const parsedProfile = parseMicrosoftProfile(profileData, "oauth", user);
    const profile = await serializeProfile(parsedProfile);
    await addProfileToken({
      profile,
      token: parseMicrosoftToken(authInfo),
    });

    let redirectTo = "/";
    if (state) {
      const stateObject = await fetchState(state);
      redirectTo = stateObject?.ref || "/";
    }

    if (user) {
      res.writeHead(302, {
        Location: redirectTo,
      });
      return res.end();
    }
    const token = await loginUser(profile.user);
    res.writeHead(302, {
      Location: `/auth?token=${token}&ref=${encodeURIComponent(redirectTo)}`,
    });
    return res.end();
  } catch (err) {
    console.log(err);
    return res
      .status(err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
