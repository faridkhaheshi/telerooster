import { createMicrosoftGraphClient } from "./../../../../adapters/microsoft-graph";

export const getMicrosoftProfile = async (accessToken) => {
  const graphClient = createMicrosoftGraphClient(accessToken);
  return graphClient.api("/me").get();
};
