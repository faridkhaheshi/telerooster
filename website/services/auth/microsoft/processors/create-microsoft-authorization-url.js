import queryString from "querystring";
import { microsoft, baseUrl } from "../../../../config";

const defaultParams = {
  client_id: microsoft.appId,
  response_type: "code",
  redirect_uri: `${baseUrl}${microsoft.authRedirectUri}`,
  response_mode: "query",
  scope: microsoft.scopes,
};

export const createMicrosoftAuthorizationUrl = ({
  tenant = "common",
  ...params
} = {}) =>
  `https://login.microsoftonline.com/${tenant}/oauth2/v2.0/authorize?${queryString.encode(
    { ...defaultParams, ...params }
  )}`;
