import { microsoft, baseUrl } from "../../../../config";
import { processLoginError } from "./process-login-error";

const tenant = "common";
const defaultParams = {
  client_id: microsoft.appId,
  scope: microsoft.scopes,
  redirect_uri: `${baseUrl}${microsoft.authRedirectUri}`,
  client_secret: process.env.MICROSOFT_CLIENT_SECRET,
};

export const getMicrosoftAuthInfo = async (params) => {
  const tokenUrl = `https://login.microsoftonline.com/${tenant}/oauth2/v2.0/token`;
  const grantType = params.refresh_token
    ? "refresh_token"
    : "authorization_code";
  const response = await fetch(tokenUrl, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
    },
    body: buildRequestBody({
      ...defaultParams,
      grant_type: grantType,
      ...params,
    }),
  });
  const authInfo = await response.json();
  processLoginError(authInfo);

  return authInfo;
};

function buildRequestBody(params) {
  const formBody = [];
  for (let property in params) {
    const encodedKey = encodeURIComponent(property);
    const encodedValue = encodeURIComponent(params[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  return formBody.join("&");
}
