import jwtDecode from "jwt-decode";

export const parseMicrosoftToken = (tokenInfo) => {
  const { access_token, refresh_token, expires_in, ...otherData } = tokenInfo;
  const { exp, tid, appid } = jwtDecode(access_token);
  return {
    provider: "microsoft",
    expires_in: exp,
    access_token,
    refresh_token,
    info: { ...otherData, tid, appid },
  };
};
