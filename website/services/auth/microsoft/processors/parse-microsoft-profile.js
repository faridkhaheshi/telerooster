export const parseMicrosoftProfile = (profile, source = "unknown", user) => {
  const displayName =
    profile.displayName ||
    profile.name ||
    `${profile.givenName} ${profile.surname}`;
  const email = profile.mail || profile.email;
  const { conversationRef, ...otherProfileInfo } = profile;
  const profileInfo = {
    provider: "microsoft",
    id_ext: profile.aadObjectId || profile.id,
    info: { source, ...otherProfileInfo, email, displayName },
  };
  if (profile.conversationRef)
    profileInfo.conversation_ref = profile.conversationRef;
  return {
    userInfo: {
      first_name: profile.givenName,
      last_name: profile.surname,
      display_name: displayName,
      email,
    },
    profileInfo,
    user,
  };
};
