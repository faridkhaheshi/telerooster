export const processLoginError = ({ error, error_description }) => {
  if (error) {
    const theError = new Error(
      error_description || `authorization failed with error code ${error}`
    );
    theError.status = 400;
    throw theError;
  }
};
