import { extractBearerTokenFromRequest } from "../utils";

export const makeRoutePrivate = (handler) => (req, res) => {
  try {
    const token = extractBearerTokenFromRequest(req);
    if (token !== process.env.INTERNAL_API_KEY)
      throw new Error("wrong auth token provided");
    return handler(req, res);
  } catch (err) {
    return res.status(401).json({
      error: {
        message: "Not allowed",
      },
    });
  }
};
