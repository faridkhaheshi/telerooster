import { verifyToken } from "../processors";
import { extractTokenFromRequest } from "../utils";

export const protectRoute = (reqHandler) => async (req, res) => {
  try {
    const token = extractTokenFromRequest(req);
    if (!token)
      return res.status(401).json({ error: { message: "Not allowed" } });
    const payload = await verifyToken(token);
    req.user = payload;
    return reqHandler(req, res);
  } catch (error) {
    return res.status(401).json({
      error: {
        message: "Not allowed",
      },
    });
  }
};
