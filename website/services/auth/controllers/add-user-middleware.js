import { verifyToken } from "../processors";
import { extractTokenFromCookies, extractTokenFromRequest } from "../utils";

export const addUserMiddleware = (reqHandler, source = "both") => async (
  req,
  res
) => {
  try {
    const token =
      source === "both"
        ? extractTokenFromRequest(req)
        : extractTokenFromCookies(req);
    const payload = await verifyToken(token);
    req.user = payload;
    return req.reqHandler(req, res);
  } catch (error) {
    return reqHandler(req, res);
  }
};
