import { extractBearerTokenFromRequest } from "../utils";

export const limitRouteToSuperadmin = (handler) => (req, res) => {
  try {
    const token = extractBearerTokenFromRequest(req);
    if (token !== process.env.SUPERADMIN_API_KEY) throw new Error("no");
    return handler(req, res);
  } catch (err) {
    return res.status(401).json({
      error: {
        message: "Not allowed",
      },
    });
  }
};
