import { BadRequestError } from "restify-errors";
import { findUserByEmail } from "../../users/processors";
import { verifyRecaptchaKey } from "../google/processors";
import { sendForgotPasswordEmail } from "../processors";
import { normalizeEmail } from "../utils";

export const handleForgotPasswordRequest = async (req, res) => {
  try {
    const {
      body: { email, captchaKey },
    } = req;
    if (!email || !captchaKey)
      throw new BadRequestError("required fields are missings");
    const isCaptchaValid = await verifyRecaptchaKey(captchaKey);
    if (!isCaptchaValid) throw new BadRequestError("wrong captcha");
    const normalizedEmail = normalizeEmail(email);
    const user = await findUserByEmail(normalizedEmail);
    if (user) {
      await sendForgotPasswordEmail({ email: normalizedEmail, user });
    }
    return res.json({ done: true, email: normalizedEmail });
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
