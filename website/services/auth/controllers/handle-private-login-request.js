import { findProfileUser } from "../../profiles/processors";
import { loginUser } from "../processors";

export const handlePrivateLoginRequest = async (req, res) => {
  try {
    const {
      query: { profileId },
    } = req;
    const profile = await findProfileUser(profileId);
    if (!profile || !profile.user) throw new Error("user not found");
    const token = await loginUser(profile.user.toJSON());
    return res.json({ token });
  } catch (err) {
    return res.json({});
  }
};
