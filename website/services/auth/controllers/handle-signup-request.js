import { findUserByEmail } from "../../users/processors";
import { sendVerificationEmail } from "../processors";
import { normalizeEmail } from "../utils";

export const handleSignupRequest = async (req, res) => {
  try {
    const {
      body: { email, password, ref },
    } = req;

    if (!email || !password)
      return res
        .status(400)
        .json({ error: { message: "missing required data" } });

    const normalizedEmail = normalizeEmail(email);

    const user = await findUserByEmail(normalizedEmail);
    if (user)
      return res
        .status(409)
        .json({ error: { message: "email already registered" } });

    await sendVerificationEmail({ email: normalizedEmail, password, ref });
    return res.json({ done: true, email: normalizedEmail });
  } catch (err) {
    return res
      .status(err.status || 500)
      .json({ error: err.message || "something went wrong" });
  }
};
