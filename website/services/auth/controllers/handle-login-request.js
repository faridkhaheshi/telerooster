import { NotFoundError } from "restify-errors";
import { checkLoginInfo, loginUser } from "../processors";

export const handleLoginRequest = async (req, res) => {
  try {
    const {
      body: { email, password },
    } = req;
    const user = await checkLoginInfo({ email, password });
    if (!user) throw new NotFoundError("user not found");
    const token = await loginUser(user);
    res.json({ token });
  } catch (err) {
    return res
      .status(500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
