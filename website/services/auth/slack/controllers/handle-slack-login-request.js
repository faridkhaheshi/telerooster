import { slack } from "./../../../../config";

export const handleSlackLoginRequest = async (req, res) => {
  const {
    query: { ref },
  } = req;
  res.writeHead(302, {
    Location: ref
      ? `${slack.authUrl}?ref=${encodeURIComponent(ref)}`
      : slack.authUrl,
  });
  return res.end();
};
