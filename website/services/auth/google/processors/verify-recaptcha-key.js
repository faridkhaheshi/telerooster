import { sendApiRequest2 } from "../../../../adapters/api/send-api-request";

export const verifyRecaptchaKey = async (key) => {
  try {
    const response = await fetch(
      "https://www.google.com/recaptcha/api/siteverify",
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/x-www-form-urlencoded; charset=utf-8",
        },
        body: `secret=${process.env.GOOGLE_RECAPTCHA_SECRET_KEY}&response=${key}`,
      }
    );
    const json = await response.json();
    return json.success;
  } catch (err) {
    return false;
  }
};
