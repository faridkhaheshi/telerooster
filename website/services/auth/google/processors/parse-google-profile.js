export const parseGoogleProfile = (profile, source = "unknown", user) => {
  const displayName =
    profile.name || `${profile.given_name} ${profile.family_name}`;
  const email = profile.email;
  const profileInfo = {
    provider: "google",
    id_ext: profile.id || profile.sub,
    info: { source, ...profile, email, displayName },
  };
  return {
    userInfo: {
      first_name: profile.given_name,
      last_name: profile.family_name,
      display_name: displayName,
      email,
    },
    profileInfo,
    user,
  };
};
