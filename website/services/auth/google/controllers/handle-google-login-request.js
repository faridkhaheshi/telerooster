import { serializeProfile } from "../../../profiles/processors";
import { loginUser } from "../../processors";
import { parseGoogleProfile, verifyIdToken } from "../processors";

export const handleGoogleLoginRequest = async (req, res) => {
  try {
    const {
      body: { idToken },
    } = req;
    const googleProfile = await verifyIdToken(idToken);
    const parsedProfile = parseGoogleProfile(googleProfile, "oauth");
    const profile = await serializeProfile(parsedProfile);
    const token = await loginUser(profile.user);
    return res.json({ token });
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
