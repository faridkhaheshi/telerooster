import { sendAnEmail } from "../../emails/processors";
import { generateTicket } from "../../tickets/processors";
import { createVerificationLink } from "../utils";
import { hashPassword } from "./hash-password";

export const sendVerificationEmail = async ({ email, password, ref }) => {
  const passwordHash = await hashPassword(password);
  const action = {
    job: "create-user",
    payload: { email, passwordHash, ref },
  };
  const ticket = await generateTicket({
    prefix: email,
    secretString: JSON.stringify(action),
  });
  const verificationLink = createVerificationLink({ ticket, email });
  if (process.env.NODE_ENV !== "production") console.log(verificationLink);
  await sendAnEmail({
    to: email,
    subject: "teleRooster: verify your email",
    text: `click on this link:

${verificationLink}`,
  });
};
