import { generateToken } from './generate-token';

export const loginUser = (user) => generateToken(user);
