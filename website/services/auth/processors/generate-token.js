import jwt from "jsonwebtoken";

const jwtSecret = process.env.JWT_SECRET;

const defaultOptions = { expiresIn: "60 days", issuer: "telerooster.com" };

export const generateToken = (user, options = defaultOptions) =>
  new Promise((resolve, reject) => {
    const { updatedAt, password_hash, ...rest } = user;
    jwt.sign(rest, jwtSecret, options, (err, token) => {
      if (err) reject(new Error("error in generating token"));
      else resolve(token);
    });
  });
