import { loginUser } from "./log-in-user";

export const handleLoginUserJob = async ({ user, ref }) => {
  const token = await loginUser(user);
  if (ref) {
    return `/auth?token=${token}&ref=${encodeURIComponent(ref)}`;
  }
  return `/auth?token=${token}`;
};
