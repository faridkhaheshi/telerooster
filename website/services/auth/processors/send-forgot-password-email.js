import { sendAnEmail } from "../../emails/processors";
import { generateTicket } from "../../tickets/processors";
import { createVerificationLink } from "../utils";
import { baseUrl } from "./../../../config";

export const sendForgotPasswordEmail = async ({ email, user }) => {
  const action = {
    job: "login-user",
    payload: {
      user,
      ref: `${baseUrl}/auth/reset-password`,
    },
  };
  const ticket = await generateTicket({
    prefix: email,
    secretString: JSON.stringify(action),
  });

  const verificationLink = createVerificationLink({ ticket, email });
  if (process.env.NODE_ENV !== "production") console.log(verificationLink);
  await sendAnEmail({
    to: email,
    subject: "teleRooster: reset your password",
    text: `Click on this link to reset your password:

${verificationLink}
`,
  });
};
