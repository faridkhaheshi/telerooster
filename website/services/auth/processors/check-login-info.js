import { BadRequestError, NotFoundError } from "restify-errors";
import { findUserByEmail } from "../../users/processors";
import { normalizeEmail } from "../utils";
import { verifyPassword } from "./hash-password";

export const checkLoginInfo = async ({ email, password }) => {
  if (!email) throw new Error('required field "email" not provided');
  if (!password) throw new Error('required field "password" not provided');
  const normalizedEmail = normalizeEmail(email);
  const user = await findUserByEmail(normalizedEmail);
  if (!user) throw new NotFoundError("user not found");
  if (!user.password_hash)
    throw new BadRequestError("password is not set for the user");
  const passwordMatch = await verifyPassword(password, user.password_hash);
  if (passwordMatch) return user;
  throw new Error("wrong password");
};
