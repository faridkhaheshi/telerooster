import { raiseEvent } from "../../../adapters/dispatcher";
import { findPoolByIdPure } from "../../pools/processors";

export const handleContentPost = async (req, res) => {
  try {
    const {
      user,
      body: { poolId, content },
    } = req;
    const { creator_id: creatorId } = await findPoolByIdPure(poolId);
    if (creatorId !== user.id)
      return res.status(401).json({ error: { message: "Not authorized" } });
    await raiseEvent({
      event: "send-pool-message",
      payload: { poolId, message: content },
    });
    return res.json({ done: true });
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
