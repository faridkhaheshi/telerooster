import db from "../../../adapters/database";

const defaultInclude = [
  {
    model: db.Pool,
    as: "pool",
    attributes: ["id", "title", "slug", "active", "description"],
  },
];

export const findSubscriptionById = async (id, include = defaultInclude) =>
  db.Subscription.findByPk(id, { include });

export const findSubscriptionByIdWithCreator = async (id) =>
  db.Subscription.findByPk(id, {
    include: [
      {
        model: db.Pool,
        as: "pool",
        attributes: ["id", "title", "slug", "active", "description"],
        include: {
          model: db.User,
          as: "creator",
          attributes: {
            exclude: ["password_hash"],
          },
        },
      },
    ],
  });
