import db from "../../../adapters/database";

export const subscribeUserToPool = async ({ userId, poolId }) => {
  const [sub] = await db.Subscription.upsert(
    { user_id: userId, pool_id: poolId, status: "ready" },
    { returning: true }
  );

  return sub;
};
