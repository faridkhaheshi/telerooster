import { NotFoundError } from "restify-errors";
import db from "../../../adapters/database";

export const deactivateSubscription = async ({ userId, subscriptionId }) => {
  const subscription = await db.Subscription.findOne({
    where: { user_id: userId, id: subscriptionId },
  });
  if (subscription) return subscription.update({ active: false });
  throw new NotFoundError("Subscription not found");
};
