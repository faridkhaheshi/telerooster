import db from "../../../adapters/database";

export const findUserSubscriptions = ({ userId }) =>
  db.Subscription.findAll({
    where: { user_id: userId, active: true },
    include: {
      model: db.Pool,
      as: "pool",
      where: { active: true },
    },
    limit: 100,
  });
