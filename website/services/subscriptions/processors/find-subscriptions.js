import db from "../../../adapters/database";

const defaultInclude = [
  {
    model: db.Pool,
    as: "pool",
    attributes: ["id", "title", "slug", "active", "description"],
    where: { active: true },
  },
];

export const findSubscriptions = (
  query,
  { include = defaultInclude, limit = 10 } = {}
) =>
  db.Subscription.findAll({
    where: query,
    include,
    limit,
  });
