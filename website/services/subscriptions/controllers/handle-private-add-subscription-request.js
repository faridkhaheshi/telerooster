import { findProfileUser } from "../../profiles/processors";
import { subscribeUserToPool } from "../processors";

export const handlePrivateAddSubscriptionRequest = async (req, res) => {
  try {
    const {
      body: { userId, poolId },
    } = req;
    const subscription = await subscribeUserToPool({ userId, poolId });
    return res.json(subscription);
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
