import { BadRequestError } from "restify-errors";
import { deactivateSubscription } from "./../processors";

export const handleUnsubscribeRequest = async (req, res) => {
  try {
    const {
      user: { id: userId },
      query: { id: subscriptionId },
    } = req;
    if (!userId || !subscriptionId)
      throw new BadRequestError("missing required info.");
    const subscription = deactivateSubscription({ userId, subscriptionId });
    return res.json({ done: true, subscription });
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
