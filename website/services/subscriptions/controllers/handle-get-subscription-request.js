import { findSubscriptionByIdWithCreator } from "../processors";

export const handleGetSubscriptionRequest = async (req, res) => {
  try {
    const subscription = await findSubscriptionByIdWithCreator(req.query.id);
    return res.json(subscription);
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
