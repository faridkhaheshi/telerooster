import { findUserSubscriptions } from "../processors";

export const getUserSubscriptions = async (req, res) => {
  try {
    const {
      user: { id: userId },
    } = req;
    const subscriptions = await findUserSubscriptions({ userId });
    return res.json(subscriptions);
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
