import { subscribeUserToPool } from "../processors";

export const addPoolSubscription = async (req, res) => {
  try {
    const {
      body: { poolId, userId },
      user: { role, id },
    } = req;
    // TODO: enable admins to add subscriptions for any user;

    const sub = await subscribeUserToPool({ userId: id, poolId });
    return res.json(sub);
  } catch (err) {
    return res
      .status(err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
