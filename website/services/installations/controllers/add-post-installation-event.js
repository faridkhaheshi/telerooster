import { addPostInstallEventForUser } from "../../users/processors";

export const addPostInstallationEvent = async (req, res) => {
  try {
    const {
      user: { id: userId },
      body: { payload, event, platform },
    } = req;
    await addPostInstallEventForUser({ userId, event, payload, platform });
    res.json({ done: true });
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
