import db from "../../../adapters/database";

export const saveJourneyEvent = async ({ poolId, event, messageId }) => {
  const [journeyEvent, created] = await db.JourneyEvent.findOrCreate({
    where: { pool_id: poolId, event },
    defaults: { message_id: messageId },
  });
  if (!created) journeyEvent.update({ message_id: messageId });
  return journeyEvent;
};
