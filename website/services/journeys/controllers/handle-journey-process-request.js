import { processPipesForJourneys } from "../processors";

export const handleJourneyProcessRequest = async (req, res) => {
  try {
    const total = await processPipesForJourneys();
    return res.json({ done: true, total });
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
