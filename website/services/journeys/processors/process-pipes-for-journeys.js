import Sequelize from "sequelize";

import db from "../../../adapters/database";
import { raiseEvent } from "../../../adapters/dispatcher";

const Op = Sequelize.Op;

export const processPipesForJourneys = async () => {
  const startTime = new Date();
  let total = 0;
  const pageSize = 1;
  let pageNumber = 0;
  while (true) {
    const pipes = await db.Pipe.findAll({
      where: {
        active: true,
        createdAt: {
          [Op.lt]: startTime,
        },
      },
      include: [
        {
          model: db.Subscription,
          as: "subscription",
          required: true,
          where: { active: true },
          attributes: ["id", "user_id", "pool_id", "createdAt"],
          include: {
            model: db.Pool,
            required: true,
            as: "pool",
            attributes: ["id", "has_journey", "createdAt"],
            where: { active: true, has_journey: true },
          },
        },
      ],
      offset: pageNumber * pageSize,
      limit: pageSize,
      order: [["createdAt", "DESC"]],
    });
    pageNumber++;
    if (!Array.isArray(pipes) || pipes.length === 0) break;
    try {
      await raiseEvent({
        event: "process-daily-journey-item",
        payload: {
          channelId: pipes[0].channel_id,
          poolId: pipes[0].subscription.pool_id,
          pipe: pipes[0],
        },
      });
    } catch (err) {}
    total += pipes.length;
  }

  return total;
};
