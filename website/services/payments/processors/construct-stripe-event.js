import getRawBody from "raw-body";
import { BadRequestError } from "restify-errors";
import { stripe } from "./load-stripe";

export const constructStripeEvent = async (req) => {
  try {
    const sig = req.headers["stripe-signature"];
    const bodyBuffer = await getRawBody(req);
    const rawBody = bodyBuffer.toString("utf8");

    const event = stripe.webhooks.constructEvent(
      rawBody,
      sig,
      process.env.STRIPE_WEBHOOK_SIGNING_SECRET
    );
    return event;
  } catch (err) {
    throw new BadRequestError(`webhook error: ${err.message}`);
  }
};
