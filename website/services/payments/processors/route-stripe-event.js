import { raiseEvent } from "./../../../adapters/dispatcher";
import supportedStripeEvents from "./supported-stripe-events";

export const routeStripeEvent = async (event) => {
  if (supportedStripeEvents.indexOf(event.type) > -1) {
    await raiseEvent({ event: convertEventName(event.type), payload: event });
  }
};

function convertEventName(eventType) {
  return "stripe_" + eventType.split(".").join("-");
}
