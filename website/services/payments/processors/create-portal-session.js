import { stripe } from "./load-stripe";
import { panelBaseUrl } from "../../../config";

export const createPortalSession = async ({ customer }) => {
  const portalSession = await stripe.billingPortal.sessions.create({
    customer: customer.id_ext,
    return_url: `${panelBaseUrl}/billings`,
  });
  return portalSession;
};
