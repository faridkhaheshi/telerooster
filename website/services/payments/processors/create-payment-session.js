import db from "../../../adapters/database";
import { stripe } from "./load-stripe";
import { baseUrl, panelBaseUrl } from "../../../config";

export const createPaymentSession = async ({ userId, intent, customer }) => {
  const options = { client_reference_id: userId };
  if (customer && customer.id_ext) options.customer = customer.id_ext;

  const stripeSession = await stripe.checkout.sessions.create({
    success_url: `${baseUrl}/payment/success?session_id={CHECKOUT_SESSION_ID}`,
    cancel_url: `${baseUrl}/payment/cancel?session_id={CHECKOUT_SESSION_ID}`,
    mode: "subscription",
    payment_method_types: ["card"],
    line_items: [
      {
        price: process.env.STRIPE_CREATOR_PLAN_BASE_PRICE_ID,
        quantity: 1,
      },
    ],
    ...options,
  });

  const session = await db.PaymentSession.create({
    provider: "stripe",
    id_ext: stripeSession.id,
    user_id: userId,
    info: stripeSession,
    intent,
    status: "unpaid",
  });

  return session;
};
