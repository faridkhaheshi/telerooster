import { BadRequestError } from "restify-errors";
import db from "../../../adapters/database";
import { stripe } from "./load-stripe";

export const checkPaymentSessionResult = async ({ sessionId, userId }) => {
  const paymentSession = await db.PaymentSession.findOne({
    where: {
      provider: "stripe",
      id_ext: sessionId,
      user_id: userId,
    },
  });
  if (!paymentSession) throw new BadRequestError("payment verification failed");
  const stripeSession = await stripe.checkout.sessions.retrieve(sessionId, {
    expand: ["customer", "subscription"],
  });
  if (
    stripeSession.payment_status === "paid" &&
    stripeSession?.subscription?.plan?.product ===
      paymentSession.intent.productId &&
    stripeSession?.subscription?.status === "active"
  ) {
    await paymentSession.update({
      status: "paid",
      info: stripeSession,
    });
    return paymentSession;
  }
  throw new BadRequestError("payment verification failed");
};
