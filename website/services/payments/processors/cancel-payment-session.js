import db from "../../../adapters/database";

export const cancelPaymentSession = async ({ userId, sessionId }) => {
  const paymentSession = await db.PaymentSession.findOne({
    where: {
      provider: "stripe",
      status: "unpaid",
      id_ext: sessionId,
      user_id: userId,
    },
  });
  if (!paymentSession) throw new BadRequestError("payment verification failed");
  return paymentSession.update({ status: "canceled" });
};
