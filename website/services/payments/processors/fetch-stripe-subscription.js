import { stripe } from "./load-stripe";

export const fetchStripeSubscription = (subscriptionId) =>
  stripe.subscriptions.retrieve(subscriptionId);
