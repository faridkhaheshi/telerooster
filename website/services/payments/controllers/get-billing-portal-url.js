import { NotFoundError } from "restify-errors";
import { createPortalSession } from "../processors";
import { fetchCustomerById } from "./../../customers/processors";

export const getBillingPortalUrl = async (req, res) => {
  try {
    const {
      user: { id: userId },
      query: { customerId },
    } = req;

    const customer = await fetchCustomerById(customerId);
    if (customer.user_id !== userId || !customer.active)
      throw new NotFoundError("Customer not found");
    const portalSession = await createPortalSession({ customer });
    return res.json({ url: portalSession.url });
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
