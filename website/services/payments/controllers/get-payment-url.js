import { BadRequestError } from "restify-errors";
import { verifyRecaptchaKey } from "../../auth/google/processors";
import { createPaymentSession, savePaymentSession } from "../processors";
import { fetchCustomerById } from "./../../customers/processors";

export const getPaymentUrl = async (req, res) => {
  try {
    const {
      user: { id: userId },
      query: { captcha, customerId },
    } = req;
    if (!userId || !captcha)
      throw new BadRequestError("required fields are missing");
    const isCaptchaValid = await verifyRecaptchaKey(captcha);
    if (!isCaptchaValid) throw new BadRequestError("wrong captcha");
    const customer = customerId ? await fetchCustomerById(customerId) : null;
    const session = await createPaymentSession({
      userId,
      customer,
      intent: {
        productId: process.env.STRIPE_CREATOR_PLAN_PRODUCT_ID,
        paymentStatus: "paid",
      },
    });
    return res.json({ id: session.id_ext });
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
