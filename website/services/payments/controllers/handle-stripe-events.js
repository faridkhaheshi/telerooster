import { constructStripeEvent, routeStripeEvent } from "../processors";

export const handleStripeEvents = async (req, res) => {
  try {
    const event = await constructStripeEvent(req);
    await routeStripeEvent(event);
    return res.json({ received: true });
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
