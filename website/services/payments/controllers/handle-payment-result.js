import { cancelPaymentSession } from "../processors";

export const handlePaymentResult = async (req, res) => {
  try {
    const {
      user: { id: userId },
      body: { sessionId, result },
    } = req;
    if (result === "cancel") {
      await cancelPaymentSession({ sessionId, userId });
    }

    return res.json({ done: true });
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
