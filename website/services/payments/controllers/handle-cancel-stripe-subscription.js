import { cancelPlanByStripeSubscriptionId } from "../../plans/processors";

export const handleCancelStripeSubscription = async (req, res) => {
  try {
    const {
      query: { id: stripeSubscriptionId },
    } = req;
    const plan = await cancelPlanByStripeSubscriptionId(stripeSubscriptionId);
    return res.json({ ok: true, plan });
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
