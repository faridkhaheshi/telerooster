import { BadRequestError } from "restify-errors";
import { findCustomerByIdExt } from "../../customers/processors";
import { serializePlanForProduct } from "../../plans/processors";
import { fetchStripeSubscription } from "../processors";

export const handlePaidInvoice = async (req, res) => {
  try {
    const {
      body: { subscriptionId, customerId },
    } = req;
    const subscription = await fetchStripeSubscription(subscriptionId);
    const customer = await findCustomerByIdExt(customerId);
    if (!subscription || subscription.status !== "active" || !customer)
      throw new BadRequestError("the subscription is not active");
    const plan = await serializePlanForProduct({
      userId: customer.user_id,
      productIdExt: subscription?.plan?.product,
      expiresAt: subscription?.current_period_end,
      updateRef: {
        subscriptionId: subscription?.id,
      },
    });
    return res.json({ done: true, plan });
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
