import { serializeCustomer } from "../../customers/processors";
import { checkPaymentSessionResult } from "../processors";

export const processPaymentResult = async (req, res) => {
  try {
    const {
      body: { userId },
      query: { id: sessionId },
    } = req;
    const paymentSession = await checkPaymentSessionResult({
      sessionId,
      userId,
    });

    await serializeCustomer({
      provider: "stripe",
      customerInfo: paymentSession.info.customer,
      userId,
    });

    return res.json({ done: true });
  } catch (err) {
    return res
      .status(err.statusCode || err.status || 500)
      .json({ error: { message: err.message || "something went wrong" } });
  }
};
