import { MicrosoftAuthContextProvider } from "../../contexts/auth";
import MicrosoftAuthenticator from "./microsoft-authenticator";

function MicrosoftAuthDialog() {
  return (
    <MicrosoftAuthContextProvider>
      <div className="full-page-centered bg-sky-blue">
        <MicrosoftAuthenticator />
      </div>
    </MicrosoftAuthContextProvider>
  );
}

export default MicrosoftAuthDialog;
