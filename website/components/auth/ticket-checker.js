import { useRouter } from "next/router";
import { Spinner } from "reactstrap";
import {
  buildRequestOptions,
  sendApiRequest,
} from "../../adapters/api/send-api-request";
import LinkTo from "../common/link-to";

function TicketChecker({ ticket, email }) {
  const [errorMessage, setErrorMessage] = React.useState(null);
  const [destination, setDestination] = React.useState(null);
  const router = useRouter();

  const verifyTicket = React.useCallback(async () => {
    try {
      const { redirectTo } = await sendApiRequest(
        `/api/tickets/${ticket}`,
        buildRequestOptions({ method: "POST", body: { email } })
      );
      setDestination(redirectTo);
    } catch (error) {
      setErrorMessage(
        error.message || "Verification failed. Please try sign up again."
      );
    }
  }, [ticket, email]);

  React.useEffect(() => {
    if (!ticket || !email) return;
    verifyTicket();
  }, [ticket, email]);

  React.useEffect(() => {
    if (!destination) return;
    router.push(destination);
  }, [destination, router]);

  return <DialogInfo errorMessage={errorMessage} />;
}

export default React.memo(TicketChecker);

function DialogInfo({ errorMessage }) {
  if (!errorMessage) return <WaitingDialog />;

  return <ErrorDialog errorMessage={errorMessage} />;
}

function WaitingDialog() {
  return (
    <main className="centered-form d-flex flex-column justify-content-between">
      <div className=" d-flex flex-column flex-grow-1">
        <h1>Checking your ticket</h1>
        <p>This may take few seconds...</p>
        <WaitingSpinner />
      </div>
    </main>
  );
}

function ErrorDialog({ errorMessage }) {
  return (
    <main className="centered-form d-flex flex-column justify-content-between">
      <div className=" d-flex flex-column flex-grow-1">
        <h1>Something went wrong</h1>
        <p>
          Please keep in mind that our activation links can only be used once.
        </p>
        <p>
          <LinkTo to="/auth/signup">Sign up</LinkTo>
        </p>
      </div>
      <div className="error">{errorMessage}</div>
    </main>
  );
}

function WaitingSpinner() {
  return (
    <div className="d-flex flex-column justify-content-center align-items-center flex-grow-1">
      <Spinner
        style={{
          color: "var(--sky-blue)",
        }}
      />
    </div>
  );
}
