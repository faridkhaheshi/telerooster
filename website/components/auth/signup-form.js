import { useRouter } from "next/router";
import AsyncForm from "../common/async-form";
import LinkTo from "./../common/link-to";
import { EmailInput, PasswordInput, getLoginInfo } from "./login-form";

function SignupForm() {
  const router = useRouter();
  const { query: { ref } = {} } = router;

  const moveForward = React.useCallback(
    ({ email }) => {
      if (email)
        router.push({
          pathname: "/auth/email-sent",
          query: { email },
        });
    },
    [router]
  );

  const prepareSignupData = React.useCallback(
    (e) => {
      const signupInfo = getLoginInfo(e);
      if (ref) signupInfo.ref = ref;
      return signupInfo;
    },
    [ref]
  );

  return (
    <AsyncForm
      url="/api/auth/signup"
      method="POST"
      className="centered-form"
      buttonText="Sign Up"
      extractFormData={prepareSignupData}
      onSuccess={moveForward}
      belowButton={<Agreement />}
    >
      <h1>Welcome to teleRooster</h1>
      <p>Engage with your audience in business messengers.</p>
      <p>
        Already have an account?{" "}
        <LinkTo
          to={
            ref ? `/auth/login?ref=${encodeURIComponent(ref)}` : "/auth/login"
          }
        >
          Log in
        </LinkTo>
      </p>
      <EmailInput />
      <PasswordInput />
    </AsyncForm>
  );
}

export default React.memo(SignupForm);

function Agreement() {
  return (
    <small>
      By signing up you are creating an account in telerooster.com and you agree
      to <LinkTo to="/terms">Terms and Conditions</LinkTo> and{" "}
      <LinkTo to="/privacy">Privacy Policy.</LinkTo>
    </small>
  );
}
