import { useAuth } from "../../contexts/auth";
import CustomButton from "../common/custom-button";
import DropdownHover from "../common/dropdown-hover";
import LinkTo from "../common/link-to";
import styles from "./auth-access.module.css";
import { panelBaseUrl } from "./../../config";

function AuthAccess() {
  const { user, isAuthenticated } = useAuth();
  if (!isAuthenticated) return <AuthButtons />;
  return <UserMenu user={user} />;
}

export default React.memo(AuthAccess);

function AuthButtons() {
  const buttonsClassName = `d-none d-lg-inline-flex ${styles.navButtons}`;
  const textClassName = `d-block d-lg-none ${styles.textLink}`;
  return (
    <>
      <LinkTo to="/auth/login" className={textClassName}>
        Log In
      </LinkTo>
      <CustomButton
        small
        link
        outline
        href="/auth/login"
        className={buttonsClassName}
      >
        Log In
      </CustomButton>
      <CustomButton small link href="/auth/signup" className={buttonsClassName}>
        Sign Up
      </CustomButton>
    </>
  );
}

function UserMenu({ user }) {
  return (
    <DropdownHover facingText={findUserName(user) || user.email}>
      <div className={styles.userMenu}>
        <UserInfo user={user} />
        <Links links={getLinks()} />
        <CustomButton block link href={panelBaseUrl}>
          Your Dashboard
        </CustomButton>
      </div>
    </DropdownHover>
  );
}

function UserInfo({ user }) {
  return (
    <div className={styles.userInfoContainer}>
      <div className={styles.boldName}>{findUserName(user) || "no name"}</div>
      <small>
        <LinkTo to={`${panelBaseUrl}/profile`}>update profile</LinkTo> {" - "}
        <LinkTo className={styles.mutedLink} to="/auth/logout">
          log out
        </LinkTo>
      </small>
    </div>
  );
}

function findUserName(user) {
  const { first_name: firstName, last_name: lastName, display_name } =
    user || {};
  if (display_name) return display_name;
  if (firstName || lastName)
    return `${firstName || ""}${lastName ? ` ${lastName}` : ""}`;
  return null;
}

function Links({ links = [] }) {
  return (
    <ul className={styles.linksContainer}>
      {links.map((l) => (
        <MenuLink key={l.id} {...l} />
      ))}
    </ul>
  );
}

function MenuLink({ title, to }) {
  return (
    <li>
      <LinkTo to={to}>{title}</LinkTo>
    </li>
  );
}

function getLinks() {
  return [
    {
      id: "menu-link-channels",
      title: "Manage channels",
      to: `${panelBaseUrl}/mychannels`,
    },
    {
      id: "menu-link-content",
      title: "Post new content",
      to: `${panelBaseUrl}/post`,
    },
    {
      id: "menu-link-pool",
      title: "create a new channel",
      to: "/pools/new",
    },
    // {
    //   id: "menu-link-account",
    //   title: "account settings",
    //   to: "/panel/account",
    // },
  ];
}
