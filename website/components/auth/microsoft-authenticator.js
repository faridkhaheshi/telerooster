import { useMicrosoftAuth } from "../../contexts/auth";
import CustomButton from "../common/custom-button";
import styles from "./microsoft-authenticator.module.css";

function MicrosoftAuthenticator() {
  const { logout } = useMicrosoftAuth();

  return (
    <div className={`${styles.container} centered-form`}>
      <h1>Logging in with Microsoft</h1>
      <div className={styles.contentContainer}>
        <CustomButton link target="_blank" href="/api/auth/microsoft">
          login
        </CustomButton>
        <CustomButton onClick={logout}>Log out</CustomButton>
      </div>
    </div>
  );
}

export default React.memo(MicrosoftAuthenticator);
