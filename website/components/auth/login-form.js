import { useRouter } from "next/router";
import { FormGroup, Label, Input } from "reactstrap";
import AsyncForm from "../common/async-form";
import LinkTo from "./../common/link-to";

function LoginForm() {
  const router = useRouter();
  const { query: { ref } = {} } = router;

  const logIn = React.useCallback(
    ({ token }) => {
      let destination = `/auth?token=${token}`;
      if (ref)
        destination = `/auth?token=${token}&ref=${encodeURIComponent(ref)}`;
      router.push(destination);
    },
    [router, ref]
  );

  const prepareSigninData = React.useCallback(
    (e) => {
      const signinInfo = getLoginInfo(e);
      if (ref) signinInfo.ref = ref;
      return signinInfo;
    },
    [ref]
  );

  return (
    <AsyncForm
      method="POST"
      url="/api/auth/login"
      className="centered-form"
      buttonText="Log In"
      extractFormData={prepareSigninData}
      onSuccess={logIn}
    >
      <h1>Log In</h1>
      <p>
        Don't have an account?{" "}
        <LinkTo
          to={
            ref ? `/auth/signup?ref=${encodeURIComponent(ref)}` : "/auth/signup"
          }
        >
          Create an account
        </LinkTo>
      </p>
      <EmailInput />
      <PasswordInput />
      <p>
        <LinkTo to="/auth/forgot">Forgot password?</LinkTo>
      </p>
    </AsyncForm>
  );
}

export default React.memo(LoginForm);

export function getLoginInfo(event) {
  return {
    email: event?.target["email"]?.value,
    password: event?.target["password"]?.value,
  };
}

export function EmailInput() {
  return (
    <FormGroup>
      <Label for="email" className="bold">
        Email:
      </Label>
      <Input
        required
        type="email"
        name="email"
        id="email-input"
        autoComplete="on"
      />
    </FormGroup>
  );
}

export function PasswordInput() {
  return (
    <FormGroup>
      <Label for="password" className="bold">
        Password:
      </Label>
      <Input
        required
        type="password"
        name="password"
        id="password-input"
        autoComplete="on"
      />
    </FormGroup>
  );
}
