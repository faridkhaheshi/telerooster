import WaitingSpinner from "../common/waiting-spinner";
import { buildRequestOptions, sendApiRequest } from "./../../adapters/api";

function MicrosoftConnector({ accessToken }) {
  const connectMicrosoftAccount = React.useCallback(async () => {
    try {
      const result = await sendApiRequest(
        "/api/auth/microsoft",
        buildRequestOptions({ method: "POST", body: { accessToken } })
      );
      console.log(result);
      // TODO: send back to where the user came from...
    } catch (error) {
      console.error(error);
    }
  }, [accessToken]);

  React.useEffect(() => {
    if (accessToken) {
      connectMicrosoftAccount();
    }
  }, [accessToken]);

  return <WaitingSpinner color="white" />;
}

export default React.memo(MicrosoftConnector);
