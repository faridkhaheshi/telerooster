import { useAuth } from '../../contexts/auth';

function LogoutHandler() {
  const { logout } = useAuth();

  React.useEffect(() => {
    logout();
  }, [logout]);

  return <p>Logging out...</p>;
}

export default React.memo(LogoutHandler);
