import { useRouter } from "next/router";
import { buildRequestOptions, sendApiRequest } from "../../adapters/api";
import WaitingSpinner from "./../common/waiting-spinner";

function PoolSubscriber({ pool, platform }) {
  const router = useRouter();
  const [subscriptionId, setSubscriptionId] = React.useState(null);
  const [errorMessage, setErrorMessge] = React.useState(null);

  const subscribeToPool = React.useCallback(async () => {
    try {
      const sub = await sendApiRequest(
        "/api/subscriptions",
        buildRequestOptions({ method: "POST", body: { poolId: pool.id } })
      );
      setSubscriptionId(sub.id);
    } catch (error) {
      setErrorMessge(error.message);
    }
  }, [pool.id]);

  React.useEffect(() => {
    subscribeToPool();
  }, []);

  React.useEffect(() => {
    if (subscriptionId) {
      router.push({
        pathname: "/channels/select",
        query: { subscriptionId, platform },
      });
    }
  }, [subscriptionId]);

  return (
    <>
      <p>
        Subscribing to "<strong>{pool.title}</strong>"
      </p>
      <WaitingSpinner color="white" />
      {errorMessage && <p className="error">{errorMessage}</p>}
    </>
  );
}

export default React.memo(PoolSubscriber);
