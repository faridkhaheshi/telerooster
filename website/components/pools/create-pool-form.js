import { useRouter } from "next/router";
import { Form, FormGroup, FormText, Label, Input, Col } from "reactstrap";
import CustomButton from "../common/custom-button";
import PriceSelector from "../common/price-selector";

import styles from "./create-pool-form.module.css";

function CreatePoolForm() {
  const router = useRouter();
  const handleSubmit = React.useCallback((e) => {
    e.preventDefault();
    const contentPoolData = extractContentPoolData(e);
    router.push({
      pathname: "/pools/create",
      query: { poolStr: JSON.stringify(contentPoolData) },
    });
  }, []);
  return (
    <Form className="centered-form" onSubmit={handleSubmit}>
      <h1>Creating a channel</h1>
      <p>
        By creating a channel you can use teleRooster to send content to your
        audience in business messengers (Slack / Microsoft Teams).
      </p>
      <NewsletterNameInput />
      <CreatorNameInput />
      <DescriptionInput />
      <PricingTypeSelection />
      <CustomButton block className="mt-3">
        Create Channel
      </CustomButton>
    </Form>
  );
}

export default React.memo(CreatePoolForm);

function NewsletterNameInput() {
  return (
    <FormGroup>
      <Label for="title" className="bold">
        Title:
      </Label>
      <Input
        required
        type="text"
        name="title"
        id="title-input"
        placeholder="specify a clear title for your channel"
        maxLength={50}
      />
      <FormText color="muted">
        Examples: "Mental health tips" or "Short marketing points for busy
        managers"
      </FormText>
    </FormGroup>
  );
}

function CreatorNameInput() {
  return (
    <FormGroup>
      <Label for="creator" className="bold">
        Creator name:
      </Label>
      <Input
        required
        type="text"
        name="creator"
        id="creator"
        placeholder="your name"
        maxLength={100}
      />
    </FormGroup>
  );
}

function DescriptionInput() {
  return (
    <FormGroup>
      <Label className="bold">Description:</Label>
      <Input
        required
        type="textarea"
        name="description"
        id="description"
        placeholder="tell your potential subscribers about the messages they will receive."
        maxLength={1500}
      />
    </FormGroup>
  );
}

function PricingTypeSelection() {
  const [pricingType, setPricingType] = React.useState(null);

  const updatePrice = React.useCallback((e) => {
    setPricingType(e.target.value);
  }, []);
  return (
    <FormGroup tag="fieldset">
      <legend>Price:</legend>
      <RadioButton
        onChange={updatePrice}
        name="pring-plan"
        value="free"
        text="Free"
        defaultChecked
      />
      <RadioButton
        onChange={updatePrice}
        name="pring-plan"
        value="paid"
        text="Paid (coming soon...)"
        disabled
      />
      <PricingPlansInput pricingType={pricingType} />
    </FormGroup>
  );
}

function RadioButton({ onChange, name, value, text, ...otherProps }) {
  return (
    <FormGroup check>
      <Label check>
        <Input
          type="radio"
          name={name}
          onChange={onChange}
          value={value}
          {...otherProps}
        />
        {text}
      </Label>
    </FormGroup>
  );
}

function PricingPlansInput({ pricingType }) {
  if (pricingType !== "paid") return null;
  const size = 4;
  const pricingRow = getPricingRows();
  return (
    <>
      {pricingRow.map((pr) => (
        <PricingRow key={pr.prefix} {...pr} size={size} />
      ))}
      <FormGroup check>
        <Label check>
          <Input type="checkbox" name="has-trial" defaultChecked /> Include a
          14-day free trial
        </Label>
      </FormGroup>
    </>
  );
}

function PricingRow({ size, label, prefix, initialValue }) {
  return (
    <FormGroup row>
      <Label className={styles.yCentered} sm={12 - size}>
        {label}
      </Label>
      <Col sm={size}>
        <PriceSelector prefix={prefix} initialValue={initialValue} />
      </Col>
    </FormGroup>
  );
}

function getPricingRows() {
  return [
    {
      label: "For small teams (under 10 users):",
      prefix: "price-small-teams",
      initialValue: 149,
    },
    {
      label: "For medium-sized teams (10 to 500 users):",
      prefix: "price-medium-teams",
      initialValue: 99,
    },
    {
      label: "For large teams (501 users and more):",
      prefix: "price-large-teams",
      initialValue: 49,
    },
  ];
}

function extractContentPoolData(event) {
  const formData = {
    title: event?.target["title"]?.value,
    creator: event?.target["creator"]?.value,
    description: event?.target["description"]?.value,
    pricingPlan: event?.target["pring-plan"]?.value,
  };
  if (event?.target["pring-plan"]?.value === "paid")
    formData.prices = {
      smallTeams:
        parseInt(event?.target["price-small-teams-dollars"]?.value) * 100 +
        parseInt(event?.target["price-small-teams-cents"]?.value),
      mediumTeams:
        parseInt(event?.target["price-medium-teams-dollars"]?.value * 100) +
        parseInt(event?.target["price-medium-teams-cents"]?.value),
      largeTeams:
        parseInt(event?.target["price-large-teams-dollars"]?.value * 100) +
        parseInt(event?.target["price-large-teams-cents"]?.value),
      hasTrial: event?.target["has-trial"]?.checked,
    };
  return formData;
}
