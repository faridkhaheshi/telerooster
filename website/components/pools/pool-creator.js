import { buildRequestOptions, sendApiRequest } from "../../adapters/api";
import WaitingSpinner from "../common/waiting-spinner";
import Router from "next/router";

function PoolCreator({ poolData }) {
  const [errorMessage, setErrorMessage] = React.useState(null);

  const createPool = React.useCallback(async () => {
    try {
      const options = buildRequestOptions({ method: "POST", body: poolData });
      const { id } = await sendApiRequest("/api/pools", options);
      Router.push(`/pools/${id}/embed`);
    } catch (error) {
      setErrorMessage(error.message || "Something went wrong");
    }
  }, [poolData]);

  React.useEffect(() => {
    createPool();
  }, []);

  if (errorMessage)
    return (
      <>
        <p>Something went wrong:</p>
        <p className="error">{errorMessage}</p>
      </>
    );

  return (
    <>
      <p>Creating your channel...</p>
      <WaitingSpinner color="white" />
    </>
  );
}

export default React.memo(PoolCreator);
