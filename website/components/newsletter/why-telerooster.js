import SmallRow from '../common/small-row';

function WhyTelerooster() {
  return (
    <section
      className='big-row align-items-center pad-x-5'
      id='why-telerooster'
    >
      <h1 className='large-heading-margin'>Why business messengers?</h1>
      <p className='bigger-p-text'>
        It is where communications occur inside organizations.
      </p>
      <SmallRow
        title='One can bring hundreds'
        description='By supporting business messengers, you are giving your current audience an easy way to bring their coworkers. TeleRooster gives you a button to facilitate this process.'
        image={{ src: '/images/audience-sm.jpg', alt: 'large audience' }}
      />
      <SmallRow
        title='Deliver your messages outside the inbox hell'
        description="Your message won't be dismissed among countless emails. It is very hard to get someone's attention when he/she is under pressure."
        linkText='Read more about email anxiety'
        link='https://www.psychologytoday.com/us/blog/in-practice/201805/3-types-email-anxiety-and-solutions'
        image={{ src: '/images/stress-sm.jpg', alt: 'stress' }}
      />
      <SmallRow
        transparent
        title='Monetize by selling to organizations'
        description='You can sell paid subscription to organizations. You just set the price. We handle the payment and charge the organization.'
        linkText='Bring your newsletter to business messengers'
        link='/pools/new'
        image={{
          src: '/images/revenue.png',
          alt: 'monetize',
        }}
      />
    </section>
  );
}

export default React.memo(WhyTelerooster);
