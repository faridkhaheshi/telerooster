import { sendApiRequest } from "../../adapters/api/send-api-request";
import { prepareContentPoolForVitrin } from "../../utils/pools";
import VitrinRow from "../common/vitrin-row";
import WaitingSpinner from "../common/waiting-spinner";

function FeaturedPoolsVitrin() {
  const [pools, setPools] = React.useState(null);

  const getFeaturedPools = React.useCallback(async () => {
    try {
      const pools = await sendApiRequest("/api/pools/featured");
      setPools(pools);
    } catch (error) {
      console.error(error);
    }
  }, []);

  React.useEffect(() => {
    getFeaturedPools();
  }, []);

  if (pools)
    return (
      <VitrinRow items={pools.map((p) => prepareContentPoolForVitrin(p))} />
    );

  return <WaitingSpinner color="var(--grass-green)" />;
}

export default React.memo(FeaturedPoolsVitrin);
