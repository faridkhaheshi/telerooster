import { Row, Col } from 'reactstrap';
import CustomButton from '../common/custom-button';
import RectImage from '../common/rect-image';

import styles from './newsletter-poster.module.css';

function NewsletterPoster() {
  return (
    <section className={styles.container}>
      <Row className={styles.centeredRow}>
        <MessageColumn />
        <ImageColumn />
      </Row>
    </section>
  );
}

export default React.memo(NewsletterPoster);

function MessageColumn() {
  return (
    <Col lg='6'>
      <h1 className='poster-heading'>
        Send your newsletters in business messengers
      </h1>
      <Steps />
      <CallToAction />
    </Col>
  );
}

function Steps() {
  return (
    <ol className={styles.stepsList}>
      <li>
        We give you <strong className={styles.boldPoint}> some buttons.</strong>
      </li>
      <li>
        You <strong className={styles.boldPoint}>add the buttons</strong> to the
        emails you send to your audience or to your website.
      </li>
      <li>
        Your audience can{' '}
        <strong className={styles.boldPoint}>easily subscribe</strong> to
        receive your content in their business messengers (Slack or Microsoft
        Teams)
      </li>
    </ol>
  );
}

function CallToAction() {
  return (
    <>
      <CustomButton link href='/pools/new'>
        Build Your Buttons
      </CustomButton>
      <p className={styles.moreInfo}>
        Want to know more?{' '}
        <a href='#why-telerooster' alt='why'>
          See benefits of using business messengers
        </a>
        .
      </p>
    </>
  );
}

function ImageColumn() {
  return (
    <Col lg='6' className={styles.imageContainer}>
      <img src='/images/buttons.png' alt='buttons' />
    </Col>
  );
}
