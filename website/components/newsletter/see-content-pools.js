import CustomButton from "../common/custom-button";
import FeaturedPoolsVitrin from "./featured-pools-vitrin";
import styles from "./see-content-pools.module.css";

function SeeContentPools() {
  return (
    <section className={styles.container}>
      <div className={styles.titleContainer}>
        <h1 className="large-heading-margin">Want to experience it first?</h1>
        <p className="bigger-p-text">
          Subscribe to a content pool to see how teleRooster works in action.
        </p>
      </div>
      <FeaturedPoolsVitrin />
    </section>
  );
}

export default React.memo(SeeContentPools);
