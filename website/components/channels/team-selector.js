import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import queryString from "querystring";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import LinkTo from "../common/link-to";
import styles from "./team-selector.module.css";

function TeamSelector({ teams = [], subscriptionId, platform }) {
  React.useEffect(() => {
    if (teams.length > 0) return;
    window.location.href = buildInstallAppUrl({
      platform,
      postInstall: {
        event: "create-channel",
        payload: { subscriptionId },
      },
    });
  }, [teams, platform]);

  if (teams.length === 0) return null;
  return (
    <div className="centered-form">
      <h1>Select Team</h1>
      <p>To which team do you want to add this new channel?</p>
      <ul className={styles.teamsContainer}>
        {teams.map((t) => (
          <TeamInfo
            key={t.id}
            {...t}
            platform={platform}
            subscriptionId={subscriptionId}
          />
        ))}
        <NewTeamBox platform={platform} />
      </ul>
    </div>
  );
}

export default React.memo(TeamSelector);

function TeamInfo({
  id,
  name,
  TeamAdmin: { profile_id: profileId },
  platform,
  subscriptionId,
}) {
  return (
    <LinkTo
      to={buildCreateUrl({
        subscriptionId,
        profileId,
        platform,
        teamId: id,
      })}
    >
      <li className={styles.teamBox}>{name}</li>
    </LinkTo>
  );
}

function NewTeamBox({ platform }) {
  return (
    <LinkTo to={buildInstallAppUrl({ platform })}>
      <li className={styles.teamBox}>
        <FontAwesomeIcon icon={faPlus} />
      </li>
    </LinkTo>
  );
}

function buildCreateUrl({ platform, subscriptionId, profileId, teamId }) {
  return `/channels/create?teamId=${teamId}&subscriptionId=${subscriptionId}&profileId=${profileId}&platform=${platform}`;
}

function buildInstallAppUrl({ platform, postInstall = {} }) {
  if (platform === "slack") return "/slack/install";
  const { event, payload } = postInstall;
  return event
    ? `/msteams/install?${queryString.encode({ event, ...payload })}`
    : `/msteams/install`;
}
