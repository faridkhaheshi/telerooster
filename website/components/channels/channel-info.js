import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import CustomButton from "../common/custom-button";
import LinkTo from "../common/link-to";
import { panelBaseUrl } from "./../../config";

import styles from "./channel-info.module.css";

function ChannelInfo({ name, webUrl }) {
  return (
    <section className={`centered-form ${styles.container}`}>
      <div className={styles.successMessage}>
        <FontAwesomeIcon icon={faCheck} size="3x" />
        <p>
          Channel <strong>"{name}"</strong> successfully added to your team.
        </p>
      </div>
      <CustomButton link block href={webUrl} target="_blank">
        Go to the Channel
      </CustomButton>
      <p className={styles.belowButton}>
        or <br />
        <LinkTo to={panelBaseUrl}>Go to your dashboard</LinkTo>
      </p>
    </section>
  );
}

export default React.memo(ChannelInfo);
