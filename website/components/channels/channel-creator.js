import { buildRequestOptions, sendApiRequest } from "../../adapters/api";
import WaitingSpinner from "../common/waiting-spinner";
import { useRouter } from "next/router";

function ChannelCreator({ teamId, subscriptionId, profileId, platform }) {
  const [errorMessage, setErrorMessage] = React.useState(null);
  const router = useRouter();

  const createChannel = React.useCallback(async () => {
    try {
      const options = buildRequestOptions({
        method: "POST",
        body: { teamId, subscriptionId, profileId, platform },
      });
      const { name, web_url } = await sendApiRequest("/api/channels", options);
      router.push({
        pathname: "/channels/success",
        query: { name, webUrl: web_url },
      });
    } catch (error) {
      setErrorMessage(error.message || "Something went wrong");
    }
  }, [teamId, subscriptionId, profileId, platform, router]);

  React.useEffect(() => {
    createChannel();
  }, []);

  return <Dialog errorMessage={errorMessage} />;
}

export default React.memo(ChannelCreator);

function Dialog({ errorMessage }) {
  if (errorMessage)
    return (
      <>
        <p>Something went wrong:</p>
        <p className="error">{errorMessage}</p>
      </>
    );

  return (
    <>
      <p>Adding the new channel to your team...</p>
      <WaitingSpinner color="white" />
    </>
  );
}
