import LinkTo from "../common/link-to";
import { FormGroup, Label, Input, Col } from "reactstrap";
import AsyncForm from "../common/async-form";

function Editor({ pools }) {
  const [successMessage, setSuccessMessage] = React.useState(null);
  const textInputRef = React.useRef();
  const timer = React.useRef();

  const handleSuccess = React.useCallback(() => {
    textInputRef.current.value = "";
    setSuccessMessage("Your message was successfully posted.");
    timer.current = setTimeout(() => {
      setSuccessMessage(null);
    }, 5000);
  }, [textInputRef]);

  React.useEffect(() => {
    return () => {
      if (timer.current) {
        clearTimeout(timer.current);
      }
    };
  }, []);

  return (
    <>
      <AsyncForm
        className="centered-form"
        method="POST"
        url="/api/messages"
        buttonText="Send"
        extractFormData={extractMessageData}
        onSuccess={handleSuccess}
        belowButton={<SuccessMessage message={successMessage} />}
      >
        <h1>Post new content</h1>
        <PoolSelectorInput pools={pools} />
        <MessageTextInput innerRef={textInputRef} />
      </AsyncForm>
      <LinkTo to="/" className="back-button">
        Back
      </LinkTo>
    </>
  );
}

export default React.memo(Editor);

function SuccessMessage({ message }) {
  if (!message) return null;
  return <strong style={{ color: "var(--grass-green)" }}>{message}</strong>;
}

function PoolSelectorInput({ pools }) {
  return (
    <FormGroup row>
      <Label for="pool-selector" sm={2}>
        Send to:
      </Label>
      <Col sm={10}>
        <Input type="select" name="pool" id="pool-selector">
          {pools.map(({ id, title }) => (
            <option key={id} value={id}>
              {title}
            </option>
          ))}
        </Input>
      </Col>
    </FormGroup>
  );
}

function MessageTextInput({ innerRef }) {
  return (
    <FormGroup>
      <Label for="message">Your new post:</Label>
      <Input
        type="textarea"
        name="message"
        id="message"
        rows={15}
        innerRef={innerRef}
      />
    </FormGroup>
  );
}

function extractMessageData(event) {
  return {
    message: event?.target["message"]?.value,
    poolId: event?.target["pool"]?.value,
  };
}
