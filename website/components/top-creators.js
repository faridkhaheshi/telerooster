import { prepareCreatorForVitrin } from '../utils/creators';
import LinkTo from './common/link-to';
import BigRow from './common/big-row';
import VitrinRow from './common/vitrin-row';

export default function TopCreators({ topCreators = [] }) {
  return (
    <BigRow color='#EFEAE6' id='top-creators'>
      <h1 className='centered-x pad-x-10'>Content creators</h1>
      <VitrinRow items={topCreators.map((p) => prepareCreatorForVitrin(p))} />
      <p className='centered-x pad-x-10 bigger-p-text'>
        Are you a content creator?{' '}
        <LinkTo to='/creators'>
          Have a look at the benefits of using business messengers.
        </LinkTo>
      </p>
    </BigRow>
  );
}
