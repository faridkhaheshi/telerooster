import { buildSubscribeLink } from "./website-embed-button";

// function EmailEmbedButton({ pool }) {
//   return (
//     <div
//       dangerouslySetInnerHTML={{
//         __html: `
//         <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
//   <tbody>
//     <tr>
//       <td align="center" valign="top" style="padding: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
//         Receive in business messengers:
//       </td>
//     </tr>
//     <tr>
//       <td align="center" valign="top" style="padding: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
//         <a href="${buildSubscribeLink({
//           poolId: pool.id,
//           platform: "slack",
//         })}" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
//           <img src="https://cdn.brandfolder.io/5H442O3W/as/pl54cs-bd9mhs-czsxst/btn-add-to-slack.svg" alt="Add to Slack" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="41" width="140" />
//         </a>
//       </td>
//     </tr>
//     <tr>
//       <td align="center" valign="top" style="padding: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
//         <a href="${buildSubscribeLink({
//           poolId: pool.id,
//           platform: "microsoft",
//         })}" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
//           <img src="/images/add-to-teams.png" alt="Add to Microsoft Teams" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="41" width="140" />
//         </a>
//       </td>
//     </tr>
//   </tbody>
// </table>
//   `,
//       }}
//     />
//   );
// }

function EmailEmbedButton({ pool }) {
  return (
    <div dangerouslySetInnerHTML={{ __html: emailEmbedButtonText(pool.id) }} />
  );
}

export default React.memo(EmailEmbedButton);

export const emailEmbedButtonText = (poolId = "{{poolId}}") =>
  `
  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
  <tbody>
    <tr>
      <td align="center" valign="top" style="padding: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
        <a href="${buildSubscribeLink({
          poolId,
          platform: "slack",
        })}" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
        <img src="https://cdn.brandfolder.io/5H442O3W/as/pl54cs-bd9mhs-3jsgg0/btn-add-to-slack_1x.png?position=180200" srcset="https://cdn.brandfolder.io/5H442O3W/as/pl54cs-bd9mhs-3jsgg0/btn-add-to-slack_1x.png?position=180200 1x, https://cdn.brandfolder.io/5H442O3W/as/pl54cs-bd9mhs-9rj1zd/btn-add-to-slack_2x.png 2x" alt="Add to Slack" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="42" width="141" />
        </a>
      </td>
    </tr>
  </tbody>
</table>
  `
    .replace(/\s+/g, " ")
    .trim();

// export const emailEmbedButtonText = (poolId = "{{poolId}}") =>
//   `
//   <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
//   <tbody>
//     <tr>
//       <td align="center" valign="top" style="padding: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
//         Receive in business messengers:
//       </td>
//     </tr>
//     <tr>
//       <td align="center" valign="top" style="padding: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
//         <a href="${buildSubscribeLink({
//           poolId,
//           platform: "slack",
//         })}" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
//           <img src="https://cdn.brandfolder.io/5H442O3W/as/pl54cs-bd9mhs-czsxst/btn-add-to-slack.svg" alt="Add to Slack" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="41" width="140" />
//         </a>
//       </td>
//     </tr>
//     <tr>
//       <td align="center" valign="top" style="padding: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
//         <a href="${buildSubscribeLink({
//           poolId,
//           platform: "microsoft",
//         })}" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
//           <img src="https://telerooster.com/images/add-to-teams.png" alt="Add to Microsoft Teams" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="41" width="140" />
//         </a>
//       </td>
//     </tr>
//   </tbody>
// </table>
//   `
//     .replace(/\s+/g, " ")
//     .trim();
