import { baseUrl } from "./../../config";

function WebsiteEmbedButton({ pool }) {
  return (
    <div>
      <a
        href={buildSubscribeLink({ poolId: pool.id, platform: "slack" })}
        target="_blank"
        style={{ display: "block", margin: 16 }}
      >
        <img
          src="https://cdn.brandfolder.io/5H442O3W/as/pl54cs-bd9mhs-czsxst/btn-add-to-slack.svg"
          alt="Add to Slack"
          width="140"
          height="41"
          style={{ display: "block", marginLeft: "auto", marginRight: "auto" }}
        />
      </a>
      {/* <a
        href={buildSubscribeLink({ poolId: pool.id, platform: "microsoft" })}
        target="_blank"
        style={{ display: "block", margin: 16 }}
      >
        <img
          src="/images/add-to-teams.png"
          alt="Add to Microsoft Teams"
          width="140"
          height="41"
          style={{ display: "block", marginLeft: "auto", marginRight: "auto" }}
        />
      </a> */}
    </div>
  );
}

export default React.memo(WebsiteEmbedButton);

export const websiteEmbedButtonText = (poolId = "{{poolId}}") =>
  `
  <div>
        <a href="${buildSubscribeLink({
          poolId,
          platform: "slack",
        })}" target="_blank" style="display: block; margin: 16px;">
          <img
            src="https://cdn.brandfolder.io/5H442O3W/as/pl54cs-bd9mhs-czsxst/btn-add-to-slack.svg"
            alt="Add to Slack"
            style="display: block; margin-left: auto; margin-right: auto; width: 140px; height: 41px;"
            width="140"
            height="41"
          />
        </a>
      </div>
  `
    .replace(/\s+/g, " ")
    .trim();

// export const websiteEmbedButtonText = (poolId = "{{poolId}}") =>
//   `
//   <div>
//         <p>Receive newsletter in business messengers:</p>
//         <a href="${buildSubscribeLink({
//           poolId,
//           platform: "slack",
//         })}" target="_blank" style="display: block; margin: 16px;">
//           <img
//             src="https://cdn.brandfolder.io/5H442O3W/as/pl54cs-bd9mhs-czsxst/btn-add-to-slack.svg"
//             alt="Add to Slack"
//             style="display: block; margin-left: auto; margin-right: auto; width: 140px; height: 41px;"
//             width="140"
//             height="41"
//           />
//         </a>
//         <a href="${buildSubscribeLink({
//           poolId,
//           platform: "microsoft",
//         })}" target="_blank" style="display: block; margin: 16px;">
//         <img
//             src="https://telerooster.com/images/add-to-teams.png"
//             alt="Add to Microsoft Teams"
//             style="display: block; margin-left: auto; margin-right: auto; width: 140px; height: 41px;"
//             width="140"
//             height="41"
//           />
//         </a>
//       </div>
//   `
//     .replace(/\s+/g, " ")
//     .trim();

export function buildSubscribeLink({ poolId = "{{poolId}}", platform }) {
  return platform
    ? `${baseUrl}/pools/${poolId}/add?platform=${platform}`
    : `${baseUrl}/pools/${poolId}`;
}
