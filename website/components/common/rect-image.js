import styles from './rect-image.module.css';

function RectImage({ src, alt }) {
  return (
    <figure className={styles.container}>
      <img src={src} alt={alt} />
    </figure>
  );
}

export default RectImage;
