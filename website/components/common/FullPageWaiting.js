import WaitingSpinner from "./waiting-spinner";
import styles from "./FullPageWaiting.module.css";

function FullPageWaiting({ show = true }) {
  if (show)
    return (
      <main className={styles.fullPageContainer}>
        <WaitingSpinner color="white" />
      </main>
    );
  return null;
}

export default React.memo(FullPageWaiting);
