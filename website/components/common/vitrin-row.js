import LinkTo from "./link-to";
import styles from "./vitrin-row.module.css";

export default function VitrinRow({ items }) {
  return (
    <div className={styles.rowContainer}>
      {items.map((item, i) => (
        <VitrinItem key={item.id} {...item} color={pickColor(i)} />
      ))}
    </div>
  );
}

function VitrinItem({
  id,
  title,
  subtitle,
  buttonText,
  linkText,
  linkAddress,
  description,
  priceTag,
  priceDetail = "some detail",
  color = "white",
  image = {
    src: "https://via.placeholder.com/300x300.png/09f/fff",
    alt: "placeholder",
  },
}) {
  return (
    <div className={styles.itemContainer} style={{ backgroundColor: color }}>
      <div className={styles.imageContainer}>
        <img {...image} />
      </div>
      <ItemInfo
        title={title}
        subtitle={subtitle}
        description={description}
        priceTag={priceTag}
        priceDetail={priceDetail}
      />
      <div className={styles.buttonContainer}>
        {buttonText && <button type="button">{buttonText}</button>}
        {linkText && (
          <LinkTo className={styles.cardButton} to={linkAddress} type="button">
            {linkText}
          </LinkTo>
        )}
      </div>
    </div>
  );
}

function ItemInfo({ title, subtitle, priceTag, description, priceDetail }) {
  return (
    <div className={styles.infoContainer}>
      <h5 className={styles.title}>{title}</h5>
      {subtitle && <p className={styles.subtitle}>{subtitle}</p>}
      {priceTag && <p className={styles.priceTag}>{priceTag}</p>}
      {priceDetail && (
        <small className={styles.priceDetail}>{priceDetail}</small>
      )}
      <p className={styles.description}>{description}</p>
    </div>
  );
}

function pickColor(index) {
  const colors = [
    "#04A777",
    "#FFBA08",
    "#3F88C5",
    "#D00000",
    "#6A7FDB",
    "#8ED081",
    "#B57F50",
    "#136F63",
    "#DA3E52",
    "#613DC1",
    "#F06543",
    "#9DC4B5",
    "#BC4749",
    "#885A89",
    "#57B8FF",
    "#F991CC",
    "#7ec850",
    "#F09D51",
  ];

  if (index < colors.length) return colors[index];
  return colors[Math.floor(Math.random() * colors.length)];
}
