import { Form } from "reactstrap";
import { sendApiRequest } from "../../adapters/api/send-api-request";
import CustomButton from "./custom-button";

function AsyncForm({
  url,
  method = "GET",
  children,
  buttonText = "Submit",
  className = "",
  extractFormData = () => ({}),
  innerRef = React.useRef(),
  onSuccess = () => ({}),
  belowButton,
  disabled = false,
  ...otherProps
}) {
  const [processing, setProcessing] = React.useState(false);
  const [errorMessage, setErrorMessage] = React.useState(null);

  const handleSubmit = React.useCallback(
    async (e) => {
      e.preventDefault();
      try {
        setErrorMessage(null);
        setProcessing(true);
        const options = {
          method,
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },
        };
        if (["POST", "PUT"].indexOf(method) > -1)
          options.body = JSON.stringify(extractFormData(e));
        const result = await sendApiRequest(url, options);
        onSuccess(result);
      } catch (error) {
        console.error(error);
        setErrorMessage(error.message || "Something went wrong");
      } finally {
        setProcessing(false);
      }
    },
    [url, method, extractFormData]
  );

  return (
    <Form
      className={className}
      onSubmit={handleSubmit}
      innerRef={innerRef}
      {...otherProps}
    >
      {children}
      <CustomButton
        block
        className={belowButton ? "mt-3 mb-3" : "mt-3"}
        disabled={disabled || processing}
      >
        {buttonText}
      </CustomButton>
      {belowButton}
      {errorMessage && <div className="error">{errorMessage}</div>}
    </Form>
  );
}

export default AsyncForm;
