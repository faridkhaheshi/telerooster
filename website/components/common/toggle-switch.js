import styles from './toggle-switch.module.css';

function ToggleSwitch({ initiallyChecked = false, onChange = () => ({}) }) {
  const [checked, setChecked] = React.useState(initiallyChecked);

  const toggle = React.useCallback((e) => {
    setChecked(e.target.checked);
    onChange(e.target.checked);
  }, []);

  return (
    <label className={styles.switch}>
      <input type='checkbox' checked={checked} onChange={toggle} />
      <span className={`${styles.slider} ${styles.round}`}></span>
    </label>
  );
}

export default React.memo(ToggleSwitch);
