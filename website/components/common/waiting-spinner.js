import { Spinner } from "reactstrap";

function WaitingSpinner({
  color = "var(--sharp-blue)",
  size = "lg",
  hide = false,
}) {
  if (hide) return null;
  return <Spinner size={size} style={{ color }} />;
}

export default React.memo(WaitingSpinner);
