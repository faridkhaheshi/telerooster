import styles from "./custom-button.module.css";
import Link from "next/link";

function CustomButton({
  className = null,
  block = false,
  round = false,
  small = false,
  outline = false,
  children,
  link = false,
  external = false,
  href,
  ...props
}) {
  const finalClassName = buildClassName({
    className,
    block,
    small,
    round,
    outline,
  });

  if (link && !external)
    return (
      <Link href={href}>
        <a role="link" tabIndex="0" className={finalClassName} {...props}>
          {children}
        </a>
      </Link>
    );
  if (link)
    return (
      <a
        href={href}
        role="link"
        tabIndex="0"
        className={finalClassName}
        {...props}
      >
        {children}
      </a>
    );
  return (
    <button className={finalClassName} {...props}>
      {children}
    </button>
  );
}

export default CustomButton;

function buildClassName({ className, block, small, round, outline }) {
  const base = styles.button;
  const fromClassName = className ? ` ${className}` : "";
  const fromBlock = block ? ` ${styles.block}` : "";
  const fromRound = round ? ` ${styles.block}` : "";
  const fromSmall = small ? ` ${styles.smallButton}` : "";
  const fromOutline = outline ? ` ${styles.outline}` : "";
  return base + fromClassName + fromBlock + fromRound + fromSmall + fromOutline;
}
