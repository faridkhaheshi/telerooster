import { FormGroup, Label, Input, Col } from "reactstrap";
import styles from "./price-selector.module.css";

function PriceSelector({
  initialValue = 149,
  dollarClassName = "",
  centClassName = "",
  prefix = "price-selector",
}) {
  return (
    <div className={styles.priceContainer}>
      <NumberInput
        initialValue={Math.floor(initialValue / 100)}
        min={0}
        max={999}
        step={1}
        className={dollarClassName}
        name={`${prefix}-dollars`}
      />
      <div>.</div>
      <NumberInput
        initialValue={initialValue % 100}
        min={0}
        max={99}
        step={10}
        className={centClassName}
        name={`${prefix}-cents`}
      />
      <div className={styles.currency}>
        USD <br /> per user <br />
        per month
      </div>
    </div>
  );
}

export default React.memo(PriceSelector);

export function NumberInput({
  onChange = () => ({}),
  min,
  max,
  step = 1,
  initialValue = 0,
  ...rest
}) {
  const [value, setValue] = React.useState(initialValue);
  const increment = React.useCallback(() => {
    setValue((old) => Math.min(max, old + step));
    onChange();
  }, []);
  const decrement = React.useCallback(() => {
    setValue((old) => Math.max(min, old - step));
    onChange();
  }, []);

  const preventDecimalPoint = React.useCallback((e) => {
    if (e.key === ".") e.preventDefault();
  }, []);

  return (
    <div className={styles.numberContainer}>
      <button onClick={increment} type="button">
        ▲
      </button>
      <input
        required
        type="number"
        pattern="[0-9]"
        {...rest}
        min={min}
        max={max}
        value={value}
        onKeyDown={preventDecimalPoint}
        onChange={(e) => setValue(Math.max(Math.min(e.target.value, max), min))}
      />
      <button onClick={decrement} type="button">
        ▼
      </button>
    </div>
  );
}
