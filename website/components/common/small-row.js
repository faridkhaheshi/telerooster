import { Row, Col } from 'reactstrap';
import LinkTo from './link-to';
import styles from './small-row.module.css';

function SmallRow({
  title,
  description,
  linkText,
  link,
  image,
  transparent = false,
}) {
  return (
    <Row className={styles.container}>
      <ImageContainer image={image} transparent={transparent} />
      <InfoContainer
        title={title}
        description={description}
        link={link}
        linkText={linkText}
      />
    </Row>
  );
}

export default React.memo(SmallRow);

function InfoContainer({ title, description, link, linkText }) {
  return (
    <Col md='6' className={styles.infoContainer}>
      <div className={styles.textContainer}>
        <h2> {title}</h2>
        <p> {description}</p>
      </div>
      {link && (
        <div className={styles.linkContainer}>
          <LinkTo to={link} target='_blank'>
            {linkText}
          </LinkTo>
        </div>
      )}
    </Col>
  );
}

function ImageContainer({ image, transparent = false }) {
  if (!image) return null;
  const { fit = 'cover' } = image || {};

  return (
    <Col md='6' className={styles.imageContainer}>
      <div
        className={
          transparent
            ? `${styles.imageCanvas} ${styles.transparentImage}`
            : styles.imageCanvas
        }
      >
        <img {...image} style={{ objectFit: fit }} />
      </div>
    </Col>
  );
}
