import styles from "./logo.module.css";

function Logo() {
  return (
    <div className={styles.container}>
      <figure className={styles.logoImage}>
        <img src="/images/rooster-2.png" alt="telerooster" />
      </figure>
      <div className={`${styles.brand} d-none d-md-block`}>
        <span className={styles.accent}>tele</span>Rooster
      </div>
    </div>
  );
}

export default Logo;
