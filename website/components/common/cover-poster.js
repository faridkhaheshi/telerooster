import Logo from './logo';

import styles from './cover-poster.module.css';

function CoverPoster({
  hideLogo = false,
  children,
  title,
  subtitle,
  color = 'var(--liver-red)',
  bottomLinkText,
  bottomLink = '#',
}) {
  return (
    <div className={styles.container}>
      <section
        className={styles.posterContainer}
        style={{ backgroundColor: color }}
      >
        <MainMessage hideLogo={hideLogo}>{children}</MainMessage>
        <CoverTitle title={title} subtitle={subtitle} />
      </section>
      <BottomLink color={color} text={bottomLinkText} href={bottomLink} />
    </div>
  );
}

export default React.memo(CoverPoster);

function MainMessage({ children, hideLogo }) {
  return (
    <div className={styles.mainMessage}>
      {!hideLogo && <Logo />}
      <div className={styles.textContainer}>{children}</div>
    </div>
  );
}

function CoverTitle({ title, subtitle }) {
  if (!title) return null;
  return (
    <div className={styles.bottomInfo}>
      <h1 className={styles.title}>{title}</h1>
      <p className={styles.subtitle}>{subtitle}</p>
    </div>
  );
}

function BottomLink({ color, text, href = '#' }) {
  if (!text) return null;
  return (
    <a className={styles.bottomLink} href={href} style={{ color }}>
      <div>{text}</div>
      <div className={styles.chevronDown} />
    </a>
  );
}
