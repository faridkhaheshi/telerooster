import AuthAccess from "../auth/auth-access";
import Logo from "./logo";
import styles from "./top-menu.module.css";

function TopMenu({ color }) {
  let headerStyle = { backgroundColor: color || undefined };
  return (
    <header className={styles.container} style={headerStyle}>
      <div className={`${styles.leftContainer} d-none d-lg-block`} />
      <Logo />
      <div className={styles.rightContainer}>
        <AuthAccess />
      </div>
    </header>
  );
}

export default TopMenu;
