import styles from './dropdown-hover.module.css';

function DropdownHover({ children, facingText = 'Menu' }) {
  return (
    <div className={styles.container}>
      <button className={`${styles.topButton} d-none d-md-block`}>
        {facingText}
      </button>
      <button className={`${styles.topButton} d-block d-md-none`}>You</button>
      <div className={styles.menu}>{children}</div>
    </div>
  );
}

export default React.memo(DropdownHover);
