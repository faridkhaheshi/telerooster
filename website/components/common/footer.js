import Link from "next/link";
import { Row, Col, Container } from "reactstrap";
import styles from "./footer.module.css";

const solutionsItems = [
  { text: "For Organizations", href: "/", key: "for-orgs" },
  { text: "For Content Creators", href: "/creators", key: "for-creators" },
];
const resourcesItems = [
  { text: "Terms and conditions", href: "/terms", key: "terms" },
  { text: "Privacy Policy", href: "/privacy", key: "privacy" },
  { text: "Blog", href: "/blog", key: "blog" },
];

export default function Footer() {
  return (
    <footer className={styles.container}>
      <Container>
        <Row className={styles.linksContainer}>
          <FooterColumn heading="Solutions" items={solutionsItems} />
          <FooterColumn heading="Resources" items={resourcesItems} />
          <FooterColumn heading="Contact">
            <ContactDetails />
          </FooterColumn>
        </Row>
      </Container>
      <div className={styles.copyRight}>
        © 2020 All Rights Reserved for Daily Say Company.
      </div>
    </footer>
  );
}

function FooterColumn({ heading, items, children }) {
  return (
    <Col md="4" className={styles.column}>
      <h5 className={styles.heading}>{heading}</h5>
      <ul>
        {items
          ? items.map(({ text, href, key }) => (
              <Link href={href} key={`footer-item-${key}`}>
                <a>
                  <li className={styles.link}>{text}</li>
                </a>
              </Link>
            ))
          : children}
      </ul>
    </Col>
  );
}

function ContactDetails() {
  return (
    <>
      <p>
        <strong>Street:</strong> 2207-140 Main St W
      </p>
      <p>
        <strong>City:</strong> Hamilton
      </p>
      <p>
        <strong>Province:</strong> Ontario
      </p>
      <p>
        <strong>Zip Code:</strong> L8P0B8
      </p>
      <p>
        <strong>Phone Number:</strong> +1 905-218-7009
      </p>
    </>
  );
}
