import Link from 'next/link';
export default function LinkTo({ children, to, ...rest }) {
  return (
    <Link href={to}>
      <a {...rest}>{children}</a>
    </Link>
  );
}
