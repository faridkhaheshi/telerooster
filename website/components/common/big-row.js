import { Row, Col, Container, Button } from 'reactstrap';
import uniqueId from 'lodash.uniqueid';
import CustomButton from './custom-button';
import RectImage from './rect-image';

import styles from './big-row.module.css';

function BigRow({
  withPadding = false,
  centered = false,
  color,
  id = uniqueId('big-row_'),
  row = false,
  children,
}) {
  const containerStyle = { backgroundColor: color || undefined };
  if (!withPadding)
    return (
      <section
        id={id}
        className={`${styles.container} ${
          row ? 'row align-items-center' : 'column'
        } ${centered ? 'align-items-center' : 'not-centered'}`}
        style={containerStyle}
      >
        {children}
      </section>
    );
  return (
    <section
      id={id}
      className={`${styles.container} ${
        row ? 'row align-items-center' : 'column'
      } ${styles.horizontalPadding}`}
      style={containerStyle}
    >
      <Row className={styles.maxWidth}>{children}</Row>
    </section>
  );
}

export default BigRow;
