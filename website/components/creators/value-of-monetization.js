import SmallRow from '../common/small-row';

function ValueOfMonetization() {
  return (
    <section
      className='big-row align-items-center pad-x-5'
      id='menetization'
      style={{ backgroundColor: '#EFEAE6' }}
    >
      <h1 className='large-heading-margin'>Your hard work will be paid</h1>
      <p className='bigger-p-text'>
        By creating professional content for organizations, you can sell them
        subscriptions.
      </p>
      <SmallRow
        title='More revenue from each conversion'
        description='By enabling organizations to subscribe to your content, every conversion will add many people to your audience. When a manager of a company finds your contents useful for her staff, you will get tens (or hundreds) of new readers.'
        image={{ src: '/images/audience-sm.jpg', alt: 'large audience' }}
      />
      <SmallRow
        title='Build long-term relationship with organizations'
        description='Each message you send to an organization makes your relationship with that organization stronger. You will be able to complement these messages with occassional seminars and talks.'
        image={{ src: '/images/seminar-sm.jpg', alt: 'relationship' }}
      />
      <SmallRow
        title='Frictionless setup for organizations'
        description='If a manager decides to use your contents, no extra step is required by each employee.'
        linkText='Subscribe to a content pool to see how easy it is'
        link='/pools'
        image={{ src: '/images/easy-sm.jpg', alt: 'easy-setup' }}
      />
    </section>
  );
}

export default React.memo(ValueOfMonetization);
