import CoverPoster from "../common/cover-poster";
import LinkTo from "../common/link-to";
import { buildSubscribeLink } from "../widgets/website-embed-button";
import { panelBaseUrl } from "./../../config";

function EmbedPoster({ pool }) {
  return (
    <CoverPoster
      hideLogo
      title={pool.title}
      bottomLinkText="Get embed buttons"
      subtitle={`Creator: ${pool.creator.display_name}`}
      bottomLink="#embed-buttons"
    >
      <InstructionMessage pool={pool} />
    </CoverPoster>
  );
}

export default React.memo(EmbedPoster);

function InstructionMessage({ pool }) {
  return (
    <>
      <p>
        <LinkTo to={buildSubscribeLink({ poolId: pool.id })}>
          Your content pool
        </LinkTo>{" "}
        is ready.
      </p>
      <p>To invite your audience, use the embed buttons below.</p>
      <p>
        To make changes, go to the{" "}
        <LinkTo to={`${panelBaseUrl}/my-channels`}>admin panel</LinkTo>.
      </p>
    </>
  );
}
