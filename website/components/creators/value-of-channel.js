import SmallRow from '../common/small-row';

function ValueOfChannel() {
  return (
    <section className='big-row align-items-center pad-x-5' id='channel-value'>
      <h1 className='large-heading-margin'>Your messages will be heard</h1>
      <p className='bigger-p-text'>
        TeleRooster delivers your messages in business messengers, e.g. Slack
        and Microsoft Teams.
      </p>
      <SmallRow
        title='Away from the inbox hell'
        description="Your message won't be dismissed among countless emails. It is very hard to get someone's attention when he/she is under pressure."
        linkText='Read more about email anxiety'
        link='https://www.psychologytoday.com/us/blog/in-practice/201805/3-types-email-anxiety-and-solutions'
        image={{ src: '/images/stress-sm.jpg', alt: 'stress' }}
      />
      <SmallRow
        title='An informal channel'
        description='Compared to emails, messages in messengers are less formal. This makes reading them less cumbersome.'
        image={{
          src: '/images/informal-channel.png',
          alt: 'informal channel',
          fit: 'contain',
        }}
      />
    </section>
  );
}

export default React.memo(ValueOfChannel);
