import { Col, Row } from 'reactstrap';
import CustomButton from '../common/custom-button';

import styles from './main-value-for-creators.module.css';

function MainValueForCreators() {
  return (
    <section className={styles.container}>
      <Row className={styles.centeredRow}>
        <InstructionColumn />
        <PictureColumn />
      </Row>
    </section>
  );
}

export default React.memo(MainValueForCreators);

function PictureColumn() {
  return (
    <Col md='6' className={styles.imageContainer}>
      <img
        className={styles.buttonPic}
        src='/images/send-to-team-button.png'
        alt='peace of mind'
      />
    </Col>
  );
}

function InstructionColumn() {
  return (
    <Col md='6'>
      <h1 className='large-heading-margin'>
        Convert individual subscribers to corporate subscribers
      </h1>

      <ol className={`${styles.stepsList}`}>
        <li>
          We give you a <span className={styles.boldPoint}>simple button.</span>
        </li>
        <li>
          You <span className={styles.boldPoint}>add the button</span> to the
          emails you send to your audience or to your website.
        </li>
        <li>
          Your audience can{' '}
          <span className={styles.boldPoint}>easily subscribe</span> to receive
          your content in their business messengers (Slack or Microsoft Teams)
        </li>
      </ol>
      <CustomButton link href='/pools/new'>
        Build Your Button
      </CustomButton>
      <p className='mt-4'>(or scroll down to learn more)</p>
    </Col>
  );
}
