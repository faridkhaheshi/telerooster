import { Row, Col } from "reactstrap";
import useCopyToClipboard from "../../hooks/use-copy-to-clipboard";
import CustomButton from "../common/custom-button";
import ToggleSwitch from "../common/toggle-switch";
import EmailEmbedButton, {
  emailEmbedButtonText,
} from "../widgets/email-embed-button";
import WebsiteEmbedButton, {
  buildSubscribeLink,
  websiteEmbedButtonText,
} from "../widgets/website-embed-button";
import styles from "./embed-buttons.module.css";

function EmbedButtons({ pool }) {
  return (
    <section className={styles.container} id="embed-buttons">
      <HeadingInfo />
      <EmbedRow
        title="Add to your website"
        embedCol={<WebsiteButtonPreviewCol pool={pool} />}
      />
      <EmbedRow
        title="Add to your emails"
        embedCol={<EmailButtonPreviewCol pool={pool} />}
      />
      <EmbedRow
        title="Simple link to use anywhere"
        embedCol={<LinkPreviewCol pool={pool} />}
      />
    </section>
  );
}

export default React.memo(EmbedButtons);

function HeadingInfo() {
  return (
    <div className={styles.headingInfo}>
      <h1 className="large-heading-margin">Embed Buttons</h1>
      <p className="bigger-p-text">
        Add the following buttons where you think is appropriate.
      </p>
      <p className="bigger-p-text">
        Need help?{" "}
        <a href="mailto:support@dailysay.net">We are here to support.</a>
      </p>
    </div>
  );
}

function EmbedRow({ title, embedCol = "embed Column" }) {
  return (
    <Row className={styles.embedRow}>
      <Col md="4" className={styles.titleContainer}>
        <h3>{title}</h3>
      </Col>
      <Col md="8" className={styles.previewCol}>
        {embedCol}
      </Col>
    </Row>
  );
}

function LinkPreviewCol({ pool }) {
  const installationLink = buildSubscribeLink({ poolId: pool.id });
  return (
    <>
      <ShowRoom light hideToggle>
        <p>
          Anyone can subscribe to your content pool by clicking on the link
          below:
        </p>
        <a href={installationLink} target="_blank">
          {installationLink}
        </a>
      </ShowRoom>
      <CodeContainer code={installationLink} />
    </>
  );
}

function EmailButtonPreviewCol({ pool }) {
  const [isLight, setIsLight] = React.useState(false);
  return (
    <>
      <ShowRoom light={isLight} handleLightChange={setIsLight}>
        <EmailEmbedButton light={isLight} pool={pool} />
      </ShowRoom>
      <CodeContainer code={emailEmbedButtonText(pool.id)} />
    </>
  );
}

function WebsiteButtonPreviewCol({ pool }) {
  const [isLight, setIsLight] = React.useState(false);

  return (
    <>
      <ShowRoom light={isLight} handleLightChange={setIsLight}>
        <WebsiteEmbedButton light={isLight} pool={pool} />
      </ShowRoom>
      <CodeContainer code={websiteEmbedButtonText(pool.id)} />
    </>
  );
}

function ShowRoom({
  children,
  hideToggle = false,
  light = false,
  handleLightChange = () => ({}),
}) {
  return (
    <div
      className={`${styles.websiteButtonContainer} ${
        light ? styles.light : styles.dark
      }`}
    >
      {!hideToggle && (
        <div className={styles.lightSwitch}>
          <ToggleSwitch onChange={handleLightChange} />
          <div className={styles.switchText}>Light</div>
        </div>
      )}
      {children}
    </div>
  );
}

function CodeContainer({ code }) {
  const { copied, copy } = useCopyToClipboard(code);
  return (
    <Row className={styles.codeBlock}>
      <Col md="10" className={styles.codeSnippet}>
        <pre>
          <code>{code}</code>
        </pre>
      </Col>
      <Col md="2" className={styles.buttonContainer}>
        <CustomButton
          block
          className={styles.copyButton}
          onClick={copy}
          disabled={copied}
        >
          {copied ? "Copied" : "Copy"}
        </CustomButton>
      </Col>
    </Row>
  );
}
