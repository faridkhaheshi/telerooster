import { Col } from "reactstrap";
import CustomButton from "./common/custom-button";
import RectImage from "./common/rect-image";
import BigRow from "./common/big-row";

function MainPoster() {
  return (
    <BigRow row withPadding>
      <Col lg="6">
        <h1 className="large-heading-margin">
          Recieve high-quality content in business messengers
        </h1>
        <p className="bigger-p-text" style={{ marginBottom: 50 }}>
          The inbox is not the best place to read high-quality content. We
          deliver those contents in business messengers so that you have fewer
          emails in your plate every day.
        </p>
        <CustomButton link href="#featured-pools">
          See Content Pools
        </CustomButton>
      </Col>
      <Col lg="6">
        <RectImage src="/images/calm.svg" alt="peace of mind" />
      </Col>
    </BigRow>
  );
}

export default React.memo(MainPoster);
