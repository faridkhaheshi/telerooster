import { prepareContentPoolForVitrin } from '../utils/pools';
import BigRow from './common/big-row';
import LinkTo from './common/link-to';
import VitrinRow from './common/vitrin-row';

export default function TopPools({ topPools = [] }) {
  return (
    <BigRow color='white' id='featured-pools'>
      <h1 className='centered-x pad-x-10'>Featured content pools</h1>
      <VitrinRow items={topPools.map((p) => prepareContentPoolForVitrin(p))} />
      <p className='centered-x pad-x-10 bigger-p-text'>
        Have an idea? <LinkTo to='/pools/new'>Let's talk.</LinkTo>
      </p>
    </BigRow>
  );
}
