import { baseUrl, slack } from "./config";

const siteName = "teleRooster";
const title = "teleRooster | High-quality content for companies";
const description =
  "We enable content creators to deliver their contents using business messengers";

export default {
  title,
  description,
  openGraph: {
    type: "website",
    title,
    description,
    images: [
      {
        url: `${baseUrl}/images/rooster-rect.jpg`,
        width: 800,
        height: 800,
        alt: "telerooster",
      },
    ],
    url: baseUrl,
    site_name: siteName,
  },
  canonical: baseUrl + "/",
  additionalMetaTags: [{ name: "slack-app-id", content: slack.appId }],
  noindex: false,
  nofollow: false,
};
