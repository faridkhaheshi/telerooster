module.exports = {
  ZIP_FILE_NAME: "telerooster_dev.zip",
  BOT_NAME: "teleRooster dev",
  BASE_URL: "https://telerooster-msteamsbot.ngrok.io",
  BOT_ID: "1b9c72cd-64dc-4e0d-9f0a-9685cf848797",
  PACKAGE_NAME: "com.telerooster.msteams.dev",
  OUTLINE_ICON: "outline-dev.png",
  COLOR_ICON: "color-dev.png",
};
