module.exports = {
  ZIP_FILE_NAME: "telerooster.zip",
  BOT_NAME: "teleRooster",
  BASE_URL: "https://msteamsbot.telerooster.com",
  BOT_ID: "2bbfea18-0c6a-4992-9051-bcd64bc97b66",
  PACKAGE_NAME: "com.telerooster.msteams",
  OUTLINE_ICON: "outline.png",
  COLOR_ICON: "color.png",
};
