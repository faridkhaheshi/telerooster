const gulp = require("gulp");
const del = require("del");
const zip = require("gulp-zip");
const replace = require("gulp-token-replace");
const devConfig = require("./config/development");
const prodConfig = require("./config/production");
const { version } = require("./package.json");

function cleanOutput() {
  console.log("deleting Microsoft Teams App build directory");
  return del(["./build/**/*"]);
}

function buildDevelopmentManifest(done) {
  const globals = devConfig;
  globals.APP_VERSION = version;

  return gulp
    .src(["manifest.json"])
    .pipe(replace({ global: globals }))
    .pipe(gulp.dest("build/development"));
}

function buildProductionManifest(done) {
  const globals = prodConfig;
  globals.APP_VERSION = version;
  return gulp
    .src(["manifest.json"])
    .pipe(replace({ global: globals }))
    .pipe(gulp.dest("build/production"));
}

const buildManifests = gulp.parallel(
  buildProductionManifest,
  buildDevelopmentManifest
);

function copyDevelopmentIcons(done) {
  const outlineIcon = `icons/${devConfig.OUTLINE_ICON}`;
  const colorIcon = `icons/${devConfig.COLOR_ICON}`;
  return gulp
    .src([outlineIcon, colorIcon])
    .pipe(gulp.dest("build/development"));
}

function copyProductionIcons(done) {
  const outlineIcon = `icons/${prodConfig.OUTLINE_ICON}`;
  const colorIcon = `icons/${prodConfig.COLOR_ICON}`;
  return gulp.src([outlineIcon, colorIcon]).pipe(gulp.dest("build/production"));
}

const copyIcons = gulp.parallel(copyDevelopmentIcons, copyProductionIcons);

const prepareMsAppResources = gulp.parallel(copyIcons, buildManifests);

function zipDevelopmentApp(done) {
  console.log("building MS Teams Development App");
  return gulp
    .src(["build/development/*"])
    .pipe(zip(devConfig.ZIP_FILE_NAME))
    .pipe(gulp.dest("build"));
}

function zipProductionApp(done) {
  console.log("building MS Teams Production App");
  return gulp
    .src(["build/production/*"])
    .pipe(zip(prodConfig.ZIP_FILE_NAME))
    .pipe(gulp.dest("build"));
}

const zipApps = gulp.parallel(zipDevelopmentApp, zipProductionApp);

const defaultTask = gulp.series(cleanOutput, prepareMsAppResources, zipApps);

module.exports.default = defaultTask;
